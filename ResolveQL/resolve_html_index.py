#!/usr/bin/env python3

############################################################################
# [Function]
# Make index HTML page.
############################################################################

########################################################
# Imports
########################################################
import argparse, os, glob, re
from datetime import datetime
import math
import argparse
from xml.dom import WrongDocumentErr
import pandas as pd
from dateutil.relativedelta import relativedelta
import glob
#

########################################################
# User-defined parameters
########################################################
local_top='/data7/ALL/plots/'
remote_top='https://xrsrv1.isas.jaxa.jp/~tsujimot/ResolveQL/'
description='https://xrsrv1.isas.jaxa.jp/confluence/pages/viewpage.action?pageId=73695251'
#
resolve_life=15 # years from 2019-01-01.

########################################################
# Functions
########################################################
#------------------------------------------------------------
def get_dt_seq(filename, src, dbdir=None):

    pattern_YMD = '[0-9]{8}'
    pattern_HMS = '[0-9]{6}'
    time_str = re.sub('[a-z\.]', '', filename).replace('__','_').replace('__','_').replace('-','_').split('_')
    time_str_YMD=[]
    time_str_HMS=[]

    for str in time_str:
        if (re.match(pattern_YMD, str)):
            time_str_YMD.append(str)
        elif (re.match(pattern_HMS, str)):
            time_str_HMS.append(str)
    
    # Day
    if (len(time_str_YMD) == 1) and (len(time_str_HMS) == 0):
        dt0 = datetime.strptime(time_str_YMD[0], '%Y%m%d') #.strftime('%Y/%m/%d')
        dt1 = None
    # Epoch
    elif (len(time_str_YMD) == 1) and (len(time_str_HMS) == 1):
        dt0 = datetime.strptime(time_str_YMD[0]+time_str_HMS[0], '%Y%m%d%H%M%S') #.strftime('%Y/%m/%d %H:%M:%S')
        dt1 = None
    # Range
    elif (len(time_str_YMD) == 2) and (len(time_str_HMS) == 2):
        # Day
        if ((time_str_YMD[0] == time_str_YMD[1]) and (time_str_HMS[0]=="000000") and (time_str_HMS[1]=="235959")):
            dt0 = datetime.strptime(time_str_YMD[0], '%Y%m%d') #.strftime('%Y/%m/%d')
            dt1 = None
        # Range
        else:
            dt0 = datetime.strptime(time_str_YMD[0]+time_str_HMS[0], '%Y%m%d%H%M%S') #.strftime('%Y/%m/%d %H:%M:%S')
            dt1 = datetime.strptime(time_str_YMD[1]+time_str_HMS[1], '%Y%m%d%H%M%S') #.strftime('%Y/%m/%d %H:%M:%S')
    else:
        dt0 = None
        dt1 = None

    if (dt1 is None):
        filestem = "{}_{}".format(src, dt0.strftime("%Y%m%d-%H%M%S"))
    else:
        filestem = "{}_{}-{}".format(src, dt0.strftime("%Y%m%d_%H%M%S"), dt1.strftime("%Y%m%d_%H%M%S"))

    if (dbdir is not None):
        infiles = glob.glob('{}/odb/odb*.pkl'.format(dbdir))
        data = None
        for infile in infiles:
            datum = pd.read_pickle(infile)
            if (data is None):
                data = datum
            else:
                data = pd.concat([data, datum])
        seqnum=data[data.index==dt0].SEQNUM.values[0]
        target=data[data.index==dt0].TARGET.values[0]
    else:
        seqnum = None
        target = None
        
    return dt0, dt1, filestem, seqnum, target

#------------------------------------------------------------
def print_htaccess(htaccess, descend=True):

    if (descend is True):
        order="Descending"
    else:
        order="Ascending"

    with open(htaccess, 'w') as f:
        print("""IndexOrderDefault {} Name
Options +Indexes +IncludesNoExec +SymLinksIfOwnerMatch
AddType text/html .html
AddHandler server-parsed .html
AddOutputFilter INCLUDES .html""".format(order), file=f)

    return 0


#------------------------------------------------------------
def print_header(topdir, outfile, dt_start, dt_end, dt_incr, range_plot, flg_bottom=False):

    # str for header
    str="""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
    <body>
"""

    # Directories above current.
    str =  str + "[UP] <a href=\"{}\" target=\"_blank\">Description</a> - <a href=\"{}\">Top</a>".format(description, remote_top)
    dirs = re.sub(local_top,'',topdir).split('/')
    n_dirs = len(dirs)
    for _i, _d in enumerate(dirs):
        if (_i < n_dirs-1):
            str = str + " - <a href=\"{}\">{}</a>".format("../"*(n_dirs-_i-1), _d)
        else:
            str = str + " - {}".format(_d)

    # Open and write
    with open(outfile, 'w') as f:
        print(str, file=f)

    # Directories below current, if exists and if not botom.
    files = os.listdir(topdir)
    dirs = [f for f in files if os.path.isdir(os.path.join(topdir, f))]

    if (len(dirs)>0) and (flg_bottom is False):    
        str = "<br>[DOWN]"
        dt = dt_start
        while (dt < dt_end):
            if (range_plot == 'l'):
                _i=dt.year
                dir="{}/{:04d}".format(topdir, _i)
                if os.path.exists(dir) and (len(os.listdir(dir))>0):
                    str = str + "-<a href=\"{:04d}/index.html\">{:04d}</a>".format(_i,_i)
                else:
                    str = str + "-{:04d}".format(_i)
            if (range_plot == 'y'):
                _i=dt.month
                dir="{}/{:02d}".format(topdir, _i)
                if os.path.exists(dir) and (len(os.listdir(dir))>0):
                    str = str + "-<a href=\"{:02d}/index.html\">{:02d}</a>".format(_i,_i)
                else:
                    str = str + "-{:02d}".format(_i)
            elif (range_plot  == 'm') or (range_plot  == 'o') or (range_plot  == 's'):
                _i=dt.day
                dir="{}/{:02d}".format(topdir, _i)
                if os.path.exists(dir) and (len(os.listdir(dir))>0):
                    str = str + "-<a href=\"{:02d}/index.html\">{:02d}</a>".format(_i,_i)
                else:
                    str = str + "-{:02d}".format(_i)
            elif (range_plot  == 'd'):
                _i=dt.hour
                dir="{}/{:02d}".format(topdir, _i)
                if os.path.exists(dir) and (len(os.listdir(dir))>0):
                    str = str + "-<a href=\"{:02d}/index.html\">{:02d}</a>".format(_i,_i)
                else:
                    str = str + "-{:02d}".format(_i)                
            dt = dt + dt_incr
    else:
        str="<br>"

    # Open and append
    with open(outfile, 'a') as f:
        print(str, file=f)

    return 0

#------------------------------------------------------------
def print_footer(outfile):

    # Open and append
    with open(outfile, 'a') as f:
        print("""
    </body>
</html>""", file=f)

    print("{} generated.".format(outfile))

    return 0

#------------------------------------------------------------
def print_body_ssi(topdir, outfile, dt_start, dt_end, ssi):
    
    file = "{}_{}-{}.html".format(ssi, dt_start.strftime("%Y%m%d_%H%M%S"), dt_end.strftime("%Y%m%d_%H%M%S"))
    filefull = topdir + '/' + file
    if os.path.exists(filefull):
        str="<!--#include file=\"{}\" -->".format(file)
        with open(outfile, 'a') as f:
            print(str, file=f)
    
    return 0

#------------------------------------------------------------
def get_batch(files, n_col):
    
    files_remainder = files
    while (len(files_remainder) > n_col):
        yield files_remainder[0:n_col]
        files_remainder = files_remainder[n_col:]
    yield files_remainder

#------------------------------------------------------------
def get_str_th(filefull, filestem, dt0, dt1, seqnum=None, target=None):

    attaches=['npz', 'csv.gz', 'xls']
    flg_attach = False

    if (dt1 is None):
        th="{}".format(dt0.strftime("%Y/%m/%d %H:%M:%S"))
    else:
        #th="{}-{}".format(dt0.strftime("%Y/%m/%d %H:%M:%S"), dt1.strftime("%Y/%m/%d %H:%M:%S"))
        th="{}-".format(dt0.strftime("%Y/%m/%d %H:%M:%S"))
    
    if (seqnum is not None) and (target is not None):
        th = th + '<br>{} ({})'.format(seqnum, target)
    
    for attach in attaches:
        if os.path.exists("{}.{}".format(filefull, attach)):
            str="<th><a href=\"./{}.{}\">{}</a></th>".format(filestem, attach, th)
            flg_attach = True
            break

    if (flg_attach is False):
        str="<th>{}</th>".format(th)
        
    return str

#------------------------------------------------------------
def get_str_td(filefull, filestem):

    if (os.path.exists("{}.html".format(filefull))) and (os.path.exists("{}.png".format(filefull))):
        str="<td><a href=\"./{}.html\" target=\"_blank\"><img src=\"./{}.png\" width=\"200\"></a></td>".format(filestem, filestem)
    elif (os.path.exists("{}.html".format(filefull))) and not (os.path.exists("{}.png".format(filefull))):
        str="<td><a href=\"./{}.html\" target=\"_blank\"><img width=\"200\"></a></td>".format(filestem)
    elif not (os.path.exists("{}.html".format(filefull))) and (os.path.exists("./{}.png".format(filefull))):
        str="<td><img src=\"./{}.png\" width=\"200\"></td>".format(filestem)
    elif not (os.path.exists("{}.html".format(filefull))) and not (os.path.exists("{}.png".format(filefull))):
        str="<td><img width=\"200\"></td>"
    
    return str

#------------------------------------------------------------
def print_body_src(topdir, src, outfile, dt_start, dt_end, dt_incr, n_col, n_tot, range_plot):

    def get_dir(range_plot, dt0):
        if (range_plot == 'l'):
            return dt0.strftime("%Y")
        elif (range_plot == 'y'):
            return dt0.strftime("%m")
        elif (range_plot == 'w'):
            return "."
        elif (range_plot == 'm'):
            return dt0.strftime("%d")
        elif (range_plot == 'd'):
            return "."
            #return dt0.strftime("%H")

    with open(outfile, 'a') as f:

        print("<table border=\"1\">", file=f)

        for i in range(math.ceil(n_tot/n_col)):
            # TH
            print("""
            <thead>
                <tr>\
""", file=f)
            #
            for j in range(n_col):
                dt0 = dt_start + dt_incr * (i*n_col+j)
                dt1 = dt0 + dt_incr
                filestem = "/{}/{}_{}-{}".format(get_dir(range_plot, dt0), src, dt0.strftime("%Y%m%d_%H%M%S"), dt1.strftime("%Y%m%d_%H%M%S"))
                filefull = topdir + filestem
                str_th =get_str_th(filefull, filestem, dt0 ,dt1)
                print(str_th, file=f)
            #
            print("""
                </tr>
            </thead>
""", file=f)
            
            # TD
            print("""
            <tbody>
                <tr>
""", file=f)
            #
            for j in range(n_col):
                dt0 = dt_start + dt_incr * (i*n_col+j)
                dt1 = dt0 + dt_incr
                filestem = "/{}/{}_{}-{}".format(get_dir(range_plot, dt0), src, dt0.strftime("%Y%m%d_%H%M%S"), dt1.strftime("%Y%m%d_%H%M%S"))
                filefull = topdir + filestem
                str_td =get_str_td(filefull, filestem)
                print(str_td, file=f)
            #
            print("""
                </tr>
            </tbody>\
""", file=f)

        print("</table>", file=f)

        if (range_plot == 'm'):
            print("<table border=\"1\">", file=f)
            # TH
            print("""
            <thead>
                <tr>\
""", file=f)
            #
            dt0 = dt_start
            while (dt0 < dt_end):
                if (dt0.weekday() !=0):
                    dt0 = dt0 + dt_incr
                else:
                    dt1 = dt0 + dt_incr*7
                    filestem = "/{}/{}_{}-{}".format(get_dir("w", dt0), src, dt0.strftime("%Y%m%d_%H%M%S"), dt1.strftime("%Y%m%d_%H%M%S"))
                    filefull = topdir + filestem
                    str_th =get_str_th(filefull, filestem, dt0 ,dt1)
                    print(str_th, file=f)
                    dt0 = dt0 + dt_incr
            print("""
                </tr>
            </thead>
""", file=f)
            dt0 = dt_start
            # TD
            print("""
            <tbody>
                <tr>
""", file=f)
            while (dt0 < dt_end):
                if (dt0.weekday() !=0):
                    dt0 = dt0 + dt_incr
                else:
                    dt1 = dt0 + dt_incr*7
                    filestem = "/{}/{}_{}-{}".format(get_dir("w", dt0), src, dt0.strftime("%Y%m%d_%H%M%S"), dt1.strftime("%Y%m%d_%H%M%S"))
                    filefull = topdir + filestem
                    str_td =get_str_td(filefull, filestem)
                    print(str_td, file=f)
                    dt0 = dt0 + dt_incr
            #
            print("""
                </tr>
            </tbody>\
""", file=f)
            print("</table>", file=f)


    return 0

#------------------------------------------------------------
def print_body_src_os(topdir, src, outfile, n_col, dbdir):
    
    with open(outfile, 'a') as f:

        if (src == 'psp_pixel'):
            files = glob.glob("{}/{}_*.xls".format(topdir, src))
        else:
            files = glob.glob("{}/{}_*.html".format(topdir, src))
        files = [os.path.split(_f)[1] for _f in files]
        files = [_f for _f in files if 'index' not in _f]
        files.sort(reverse=False)

        print("<table border=\"1\">", file=f)

        for files_batch in get_batch(files, n_col):

            # TH
            print("""
            <thead>
                <tr>\
""", file=f)
            for i in range(len(files_batch)):
                #filename = files_batch[len(files_batch)-i-1]
                filename = files_batch[i]
                dt0, dt1, filestem, seqnum, target = get_dt_seq(filename, src, dbdir)
                filefull = topdir + '/' + filestem
                str_th = get_str_th(filefull, filestem, dt0 ,dt1, seqnum, target)
                print(str_th, file=f)
            #
            print("""
                </tr>
            </thead>
""", file=f)

            # TD
            print("""
            <tbody>
                <tr>
""", file=f)
            #
            for i in range(len(files_batch)):
                #filename = files_batch[len(files_batch)-i-1]
                filename = files_batch[i]
                dt0, dt1, filestem, seqnum, target = get_dt_seq(filename, src, dbdir)
                filefull = topdir + '/' + filestem
                str_td =get_str_td(filefull, filestem)
                print(str_td, file=f)
            #
            print("""
                </tr>
            </tbody>
""", file=f)

        print("</table>", file=f)

    return 0

#------------------------------------------------------------
def main(topdir, range_plot, src, ssi, dbdir, flg_bottom):

    outfile = topdir + '/index.html'
    htaccess = topdir + '/.htaccess'

    # Get start datetime from directory.
    if (range_plot=='d'):
        day=int(topdir.split('/')[-1])
        month=int(topdir.split('/')[-2])
        year=int(topdir.split('/')[-3])
        dt_start = datetime(year, month, day, 0, 0, 0)
        dt_end = dt_start + relativedelta(days=1)
        dt_incr = relativedelta(hours=1)
        n_tot = 24
        n_col=6
    elif (range_plot=='m') or (range_plot=='w') or (range_plot=='o') or (range_plot=='s'):
        month=int(topdir.split('/')[-1])
        year=int(topdir.split('/')[-2])
        dt_start = datetime(year, month, 1, 0, 0, 0)
        dt_end = dt_start + relativedelta(months=1)
        dt_incr = relativedelta(days=1)
        n_tot=(dt_end-dt_start).days-1
        n_col=7
    elif (range_plot=='y'):
        year=int(topdir.split('/')[-1])
        dt_start = datetime(year, 1, 1, 0, 0, 0)
        dt_end = dt_start + relativedelta(years=1)
        dt_incr = relativedelta(months=1)
        n_tot=12
        n_col=6
    elif (range_plot=='l'):
        dt_start = datetime(2019, 1, 1, 0, 0, 0)
        dt_end = dt_start + relativedelta(years=resolve_life)
        dt_incr = relativedelta(years=1)
        n_tot=resolve_life
        n_col=6
    else:
        print("{} is not supported".format(range_plot))
        return 1

    # Print htaccess
    if (range_plot=='o') or (range_plot=='s'):
        print_htaccess(htaccess, descend=False)
    else:
        print_htaccess(htaccess, descend=True)
    
    # Print header, body, footer.
    if (src is None) and (ssi is None):
        print_header(topdir, outfile, dt_start, dt_end, dt_incr, range_plot, flg_bottom)
        print_footer(outfile)
    elif (src is None) and (ssi is not None):
        print_header(topdir, outfile, dt_start, dt_end, dt_incr, range_plot, flg_bottom)
        print_body_ssi(topdir, outfile, dt_start, dt_end, ssi)
        print_footer(outfile)
    elif (src is not None) and (ssi is None):
        print_header(topdir, outfile, dt_start, dt_end, dt_incr, range_plot, flg_bottom)
        if (range_plot=='d') or (range_plot=='w') or (range_plot=='m') or (range_plot=='y') or (range_plot=='l'):
            print_body_src(topdir, src, outfile, dt_start, dt_end, dt_incr, n_col, n_tot, range_plot)
        elif (range_plot=='o'):
            n_col=8
            print_body_src_os(topdir, src, outfile, n_col, dbdir=None)
        elif (range_plot=='s'):
            n_col=8
            print_body_src_os(topdir, src, outfile, n_col, dbdir=dbdir)
        print_footer(outfile)
    elif (src is not None) and (ssi is not None):
        print("--src {} -ssi {} is mutually exclusive.".format(src, ssi))

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-t', '--topdir',
        help='top dir.',
        dest='topdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-r', '--ranges',
        help='h(our), d(ay), w(eek), m(onth), y(ear), l(ifetime), s(eqnum).',
        dest='ranges_plot',
        type=str,
        default=['d'],
        nargs='+'
        )
    parser.add_argument(
        '--src',
        help='src of files to source. If none, a header page is generated.',
        dest='src',
        type=str,
        default=[None],
        nargs=1
        )
    parser.add_argument(
        '--ssi',
        help='src of files to include. If none, a header page is generated.',
        dest='ssi',
        type=str,
        default=[None],
        nargs=1
        )
    parser.add_argument(
        '-d', '--db',
        help='dbdir.',
        dest='dbdir',
        type=str,
        default=[None],
        nargs=1
        )
    parser.add_argument(
        '--bottom',
        help='Index page for the bottom-most dir.',
        dest='flg_bottom',
        action='store_true',
        default=False
        )

    args = parser.parse_args()

    # Tweak args.
    topdir = args.topdir[0]
    if (topdir is not None):
        topdir=re.sub('//+','/',topdir) # Removing dupilicated //'s
        topdir = re.sub('/?$','',topdir) # Remove trailing /'s.
    if not os.path.exists(topdir):
        print("{} does not exist.".format(topdir))
        exit(1)
    
    main(topdir, args.ranges_plot[0], args.src[0], args.ssi[0], args.dbdir[0], args.flg_bottom)