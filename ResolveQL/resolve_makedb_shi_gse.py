#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for SXS MD-3/4 GSE
############################################################################

########################################################
# Imports
########################################################
import argparse
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
from conv_shigse import chebychev, conv_sxs_lhpT, conv_sxs_lhpV, conv_sxs_lhpI, conv_flow_rate_L, conv_flow_rate_S, conv_p_HeTank1, conv_p_HeTank2, conv_p_DMS, conv_unity,\
    conv_LD1, conv_LD2, conv_LD3, conv_LD, HE_TANK4, JTS5, IVCS5, MVCS3, OVCS3, DMS3, T4KS, ORFIN, BPSIN, \
    HEX1HPIN, HEX1HPOUT, HEX2HPIN, HEX2HPOUT, HEX3HPIN, HEX3LPOUT, HEX2LPOUT, HEX1LPOUT, T1STG, T2STG, \
    PPP, PPCuF, HE_TANK1, HE_TANK2, PP1, PP2, JTS1, JTS2, JTS3, JTS4, ADRIFB, PCA_2ND, PCB_2ND, SCA_2ND, SCB_2ND
from resolve_makedb_fff import save_files_fff_range
from myutils import beginning_of_world, end_of_world

########################################################
# User-defined parameters
########################################################
# TC1 (cool-down)
sheets_cols1={
    'Data_DA100' : ['S_TIME', 'T_JT_SHLD5', 'T_IVCS1', 'T_IVCS2', 'T_IVCS3', 'T_IVCS4', 'T_IVCS5', 'T_MVCS1', 'T_MVCS2', 'T_MVCS3', 'T_OVCS1', 'T_OVCS2', 'T_OVCS3', 'T_DMS3', 'p_HeTank', 'Flow_rate_L', 'Flow_rate_S', 'LD_tmp1', 'LD_tmp2', 'p_DMS', 'LD_tmp3', 'LD_tmp4', 'LD', 'tmp'], 
    'Data_MX100' : ['S_TIME', 'T_PP_plumbing', 'T_HeTank1', 'T_HeTank2', 'T_HeTank3', 'T_PP1', 'T_PP2', 'T_JT_SHLD1', 'T_JT_SHLD2', 'T_JT_SHLD3', 'T_JT_SHLD4', 'T_PP_Cu_flange'],
    }
# TC1 (warm-up)
sheets_cols2={
    'Data_DA100' : ['S_TIME', 'Flow_rate_L', 'Flow_rate_S', 'p_DMS', 'LD'], 
    }

### Channel mapping
# SHI GSE in TC1
chanmap0 = [
    ['S_TIME', 0, 1, 0],     # 0 UT, 1 JST, 2 UT base, 3 JST base
	['JT_SHLD5', conv_unity, 1, 1],
	['IVCS1', conv_unity, 1, 0],
	['IVCS2', conv_unity, 1, 0],
	['IVCS3', conv_unity, 1, 0],
	['IVCS4', conv_unity, 1, 0],
	['IVCS5', conv_unity, 1, 1],
	['MVCS1', conv_unity, 1, 0],
	['MVCS2', conv_unity, 1, 0],
	['MVCS3', conv_unity, 1, 1],
	['OVCS1', conv_unity, 1, 0],
	['OVCS2', conv_unity, 1, 0],
	['OVCS3', conv_unity, 1, 1],
	['DMS3',  conv_unity, 1, 1],
	['p_HE_TANK2', conv_p_HeTank1, 1, 1],
	['Flow_rate_L', conv_flow_rate_L, 1, 1],
	['Flow_rate_S', conv_flow_rate_S, 1, 1],
	['LD1_scale', conv_LD1, 1, 1],
	['LD1_order', None, 1, 1],
	['p_DMS', conv_p_DMS, 1, 1],
	['LD2_scale', conv_LD2, 1, 0],
	['LD2_order', None, 1, 0],
]

# SHI GSE in TC2
chanmap1 = [
    ['Date', 0, 1, 0],     # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1, 0],
	['sec', None, 1, 0],
	['HE_TANK4', HE_TANK4, 1e2, 1],
	['JT_SHLD5', JTS5, 1e5, 1],
	['IVCS5', IVCS5, 1e2, 1],
	['MVCS3', MVCS3, 1e2, 1],
	['OVCS3', OVCS3, 1e2, 1],
	['DMS3', DMS3, 1e2, 1],
	['JT_4KSTG', T4KS, 1e5, 1],
	['ORIFI_IN', ORFIN, 1e5, 1],
	['BYPSS_IN', BPSIN, 1e5, 1],
	['HEX1HPIN', HEX1HPIN, 1e2, 1],
	['HEX1HPOUT', HEX1HPOUT, 1e2, 1],
	['HEX2HPIN', HEX2HPIN, 1e2, 1],
	['HEX2HPOUT', HEX2HPOUT, 1e2, 1],
	['HEX3HPIN', HEX3HPIN, 1e2, 1],
	['HEX3LPOUT', HEX3LPOUT, 1e2, 1],
	['HEX2LPOUT', HEX2LPOUT, 1e2, 1],			
	['HEX1LPOUT', HEX1LPOUT, 1e2, 1],
	['1STSTAGE', T1STG, 1e2, 1],
	['2NDSTAGE', T2STG, 1e2, 1],
	['na0', None, 1, 0],
	['Flow_rate_L', conv_flow_rate_L, 1, 1],
	['Flow_rate_S', conv_flow_rate_S, 1, 1],
	['LD1_scale', conv_LD1, 1, 1],
	['LD1_order', None, 1, 1],
	['p_HE_TANK2', conv_p_HeTank2, 1, 1],
	['p_DMS', conv_p_DMS, 1, 1],
	]

# SHI GSE in TC2.1
# Name, Conversion, Coeff, Use (1) or non use (0)
chanmap2 = [
	['Date', 0, 1, 0],     # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1, 0],
	['sec', None, 1, 0],
	['HE_TANK4', HE_TANK4, 1e2, 1],
	['JT_SHLD5', JTS5, 1e5, 1],
	['IVCS5', IVCS5, 1e2, 1],
	['MVCS3', MVCS3, 1e2, 1],
	['OVCS3', OVCS3, 1e2, 1],
	['DMS3', DMS3, 1e2, 1],
	['JT_4KSTG', T4KS, 1e5, 1],
	['ORIFI_IN', ORFIN, 1e5, 1],
	['BYPSS_IN', BPSIN, 1e5, 1],
	['HEX1HPIN', HEX1HPIN, 1e2, 1],
	['HEX1HPOUT', HEX1HPOUT, 1e2, 1],
	['HEX2HPIN', HEX2HPIN, 1e2, 1],
	['HEX2HPOUT', HEX2HPOUT, 1e2, 1],
	['HEX3HPIN', HEX3HPIN, 1e2, 1],
	['HEX3LPOUT', HEX3LPOUT, 1e2, 1],
	['HEX2LPOUT', HEX2LPOUT, 1e2, 1],			
	['HEX1LPOUT', HEX1LPOUT, 1e2, 1],
	['1STSTAGE', T1STG, 1e2, 1],
	['2NDSTAGE', T2STG, 1e2, 1],
	['na0', None, 1, 0],
	['Flow_rate_L', conv_flow_rate_L, 1, 1],
	['Flow_rate_S', conv_flow_rate_S, 1, 1],
	['LD1_scale', conv_LD1, 1, 1],
	['LD1_order', None, 1, 1],
	['p_HE_TANK2', conv_p_HeTank2, 1, 1],
	['p_DMS', conv_p_DMS, 1, 1],
	['p_HE_TANK1', conv_p_HeTank1, 1, 1],
	['LD2_scale', conv_LD2, 1, 1],
	['na1', None, 1, 0],
	['LD2_order', None, 1, 1],	
	['TinTent', conv_unity, 1, 1],
	]

# SHI GSE in TC1A
chanmap3 = [
	['Date', 0, 1, 0],     # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1, 0],
	['sec', None, 1, 0],
	['HE_TANK1', HE_TANK1, 1e2, 1],
	['HE_TANK2', HE_TANK2, 1e2, 1],
	['HE_TANK4', HE_TANK4, 1e2, 1],
	['JT_SHLD1', JTS1, 1e5, 1],
	['JT_SHLD2', JTS2, 1e5, 1],
	['JT_SHLD3', JTS3, 1e5, 1],
	['JT_SHLD4', JTS4, 1e5, 1],
	['JT_SHLD5', JTS5, 1e5, 1],
	['IVCS1', None, 1e2, 0],
	['IVCS2', None, 1e2, 0],
	['IVCS3', None, 1e2, 0],
	['IVCS4', None, 1e2, 0],
	['IVCS5', IVCS5, 1e2, 1],
	['MVCS1', None, 1e2, 0],
	['MVCS2', None, 1e2, 0],
	['MVCS3', MVCS3, 1e2, 1],
	['OVCS1', None, 1e2, 0],
	['OVCS2', None, 1e2, 0],
	['OVCS3', OVCS3, 1e2, 1],
	['DMS3',  DMS3, 1e2, 1],
	['SCA_2ND', PCA_2ND, 1, 1],
	['SCB_2ND', SCB_2ND, 1, 1],
	['PCA_2ND', SCA_2ND, 1, 1],
	['PCB_2ND', PCB_2ND, 1, 1],
	['SCA_1ST', None, 1, 1],
	['SCB_1ST', None, 1, 1],
	['PCA_1ST', None, 1, 1],				
	['PCB 1ST', None, 1, 1],
	['SCA CMP', None, 1, 1],
	['SCB CMP', None, 1, 1],
	['Ambient', None, 1, 1],
	['HE_TANK1p', None, 1e2, 0],
	['HE_TANK2p', None, 1e2, 0],
	['PP downstrem plumbing', None, 1, 1],
	['Heater V', None, 1, 1],
	['na0', None, 1, 0],
	['na1', None, 1, 0],
	['na2', None, 1, 0],
	['p_HE_TANK2', conv_p_HeTank2, 1, 1],
	['p_HE_TANK1', conv_p_HeTank1, 1, 1],
	['p_DMS', conv_p_DMS, 1, 1],
	['LT1_temp', None, 1, 1],
	['LD1_scale', conv_LD1, 1, 1],
	['LD1_order', None, 1, 1],
	['LD2_scale', conv_LD2, 1, 1],
	['LD2_order', None, 1, 1],
	['LD3_scale', conv_LD3, 1, 1],
	['LD3_order', None, 1, 1],
	['Flow_rate_L', conv_flow_rate_L, 1, 1],
	['Flow_rate_S', conv_flow_rate_S, 1, 1],
	['PT1', None, 1, 0],
	['PT2', None, 1, 0],
	['PT3', None, 1, 0],
	['PT4', None, 1, 0],
	['PT0', None, 1, 0],
	['DS露点', None, 1, 0],
	['OS酸素濃度', None, 1, 0],
	['VPI真空度',  None, 1, 0],
	['Shield 1', None, 1, 0],
	['Shield 2', None, 1, 0],
	['PP1', PP1, 1, 1],
	['PP2', PP2, 1, 1],
	['HEX1',None, 1, 0],
	['HEX2',None, 1, 0],
	['Liquid level 10', None, 1, 0],
	['Liquid level 20', None, 1, 0],
	['Liquid level 30', None, 1, 0],
	['Liquid level 40', None, 1, 0],
	['Liquid level mid',None, 1, 0],
	['Liquid level top',None, 1, 0],
	]

# SHI GSE in TC2A
chanmap4 = [
	['Date', 0, 1, 0],     # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1, 0],
	['sec', None, 1, 0],
	['HE_TANK1', HE_TANK1, 1e2, 1],
	['HE_TANK2', HE_TANK2, 1e2, 1],
	['HE_TANK4', HE_TANK4, 1e2, 1],
	['JT_SHLD1', JTS1, 1e5, 1],
	['JT_SHLD2', JTS2, 1e5, 1],
	['JT_SHLD3', JTS3, 1e5, 1],
	['JT_SHLD4', JTS4, 1e5, 1],
	['JT_SHLD5', JTS5, 1e5, 1],
	['IVCS1', None, 1e2, 0],
	['IVCS2', None, 1e2, 0],
	['IVCS3', None, 1e2, 0],
	['IVCS4', None, 1e2, 0],
	['IVCS5', IVCS5, 1e2, 1],
	['MVCS1', None, 1e2, 0],
	['MVCS2', None, 1e2, 0],
	['MVCS3', MVCS3, 1e2, 1],
	['OVCS1', None, 1e2, 0],
	['OVCS2', None, 1e2, 0],
	['OVCS3', OVCS3, 1e2, 1],
	['DMS3',  DMS3, 1e2, 1],
	['FAN_I', None, 1, 1],
	['FAN_V', None, 1, 1],
	['PCA_2ND', SCA_2ND, 1, 1],
	['PCB_2ND', PCB_2ND, 1, 1],
	['SCA_1ST', None, 1, 1],
	['SCB_1ST', None, 1, 1],
	['PCA_1ST', None, 1, 1],				
	['PCB 1ST', None, 1, 1],
	['SCA CMP', None, 1, 1],
	['SCB CMP', None, 1, 1],
	['Ambient', None, 1, 1],
	['HE_TANK1p', None, 1e2, 0],
	['HE_TANK2p', None, 1e2, 0],
	['PP downstrem plumbing', None, 1, 1],
	['Heater V', None, 1, 1],
	['na0', None, 1, 0],
	['na1', None, 1, 0],
	['na2', None, 1, 0],
	['p_HE_TANK2', conv_p_HeTank2, 1, 1],
	['p_HE_TANK1', conv_p_HeTank1, 1, 1],
	['p_DMS', conv_p_DMS, 1, 1],
	['LT1_temp', None, 1, 1],
	['LD1_scale', conv_LD1, 1, 1],
	['LD1_order', None, 1, 1],
	['LD2_scale', conv_LD2, 1, 1],
	['LD2_order', None, 1, 1],
	['LD3_scale', conv_LD3, 1, 1],
	['LD3_order', None, 1, 1],
	['Flow_rate_L', conv_flow_rate_L, 1, 1],
	['Flow_rate_S', conv_flow_rate_S, 1, 1],
	['PT1', None, 1, 0],
	['PT2', None, 1, 0],
	['PT3', None, 1, 0],
	['PT4', None, 1, 0],
	['PT0', None, 1, 0],
	['DS露点', None, 1, 0],
	['OS酸素濃度', None, 1, 0],
	['VPI真空度',  None, 1, 0],
	['Shield 1', None, 1, 0],
	['Shield 2', None, 1, 0],
	['PP1', PP1, 1, 1],
	['PP2', PP2, 1, 1],
	['HEX1',None, 1, 0],
	['HEX2',None, 1, 0],
	['Liquid level 10', None, 1, 0],
	['Liquid level 20', None, 1, 0],
	['Liquid level 30', None, 1, 0],
	['Liquid level 40', None, 1, 0],
	['Liquid level mid',None, 1, 0],
	['Liquid level top',None, 1, 0],
]

# Format for LHP GSE in 2020/06.
# Name, Conversion, Coeff, Use (1) or non use (0)
chanmap5 = [
	['Date', 3,    1,   0],      # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1,   0],
	['sec',  None, 1,   0],
    ['T_LHP_S_SCA_CMP_P', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCA_CMP_R', conv_sxs_lhpT, 1,   1],
    ['T_LHP_V_SCA_CMP_P', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCA_CHD_P', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCA_CHD_R', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCB_CMP_P', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCB_CMP_R', conv_sxs_lhpT, 1,   1],
    ['T_LHP_V_SCB_CMP_P', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCB_CHD_P', conv_sxs_lhpT, 1,   1],
    ['T_LHP_S_SCB_CHD_R', conv_sxs_lhpT, 1,   1],
]

# Format for LHP GSE in 2021/08.
# Name, Conversion, Coeff, Use (1) or non use (0)
chanmap6 = [
	['Date', 1,    1,   0],      # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1,   0],
	['sec',  None, 1,   0],
	['HE_TANK4', HE_TANK4, 1e2, 1],
	['JT_SHLD5', JTS5, 1e6, 1],
	['IVCS5', IVCS5, 1e2, 1],
	['MVCS3', MVCS3, 1e2, 1],
	['OVCS3', OVCS3, 1e2, 1],
	['DMS3', DMS3, 1e2, 1],
	['p_HE_TANK1', conv_p_HeTank1, 1, 1],
	['p_HE_TANK2', conv_p_HeTank2, 1, 1],
	['p_DMS', conv_p_DMS, 1, 1],
	['Flow_rate_L', conv_flow_rate_L, 1, 1],
	['Flow_rate_S', conv_flow_rate_S, 1, 1],
	['LD1_scale', conv_LD1, 1, 1],
	['LD1_order', None, 1, 1],
	['LD2_scale', conv_LD2, 1, 1],
	['LD2_order', None, 1, 1],
	['LD3_scale', conv_LD3, 1, 1],
	['LD3_order', None, 1, 1],
	['na18', None, 1, 0],
	['na19', None, 1, 0],
	['na20', None, 1, 0],
	['FAN_I', None, 1, 1],
	['FAN_V', None, 1, 1],
	['na23', None, 1, 0],
	['na24', None, 1, 0],
	['na25', None, 1, 0],
	['na26', None, 1, 0],
	['na27', None, 1, 0],
	['na28', None, 1, 0],
	['na29', None, 1, 0],
	['na30', None, 1, 0],
	['na31', None, 1, 0],
	['na32', None, 1, 0],
	['na33', None, 1, 0],
	['na34', None, 1, 0],
	['na35', None, 1, 0],
	['na36', None, 1, 0],
	['na37', None, 1, 0],
	['na38', None, 1, 0],
	['na39', None, 1, 0],
	['na40', None, 1, 0],
	['na41', None, 1, 0],
	['na42', None, 1, 0],
	['na43', None, 1, 0],
	['na44', None, 1, 0],
	['na45', None, 1, 0],
	['na46', None, 1, 0],
	['na47', None, 1, 0],
	['na48', None, 1, 0],
	['na49', None, 1, 0],
	['na50', None, 1, 0],
	['T_LHP_S_SCA_CMP_P', conv_sxs_lhpT, 1, 1],	
	['T_LHP_S_SCA_CMP_R', conv_sxs_lhpT, 1, 1],	
	['T_LHP_V_SCA_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCA_CMP_P', conv_sxs_lhpT, 1, 1],	
	['T_LHP_C_SCA_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCA_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCA_CHD_R', conv_sxs_lhpT, 1, 1],
	['T_LHP_V_SCA_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCA_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_C_SCA_CHD_P', conv_sxs_lhpT, -1, 1], # bug for sign.
	['T_LHP_S_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCB_CMP_R', conv_sxs_lhpT, 1, 1],
	['T_LHP_V_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_C_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCB_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCB-CHO_R', conv_sxs_lhpT, 1, 1],
	['T_LHP_V_SCB_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCB_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_C_SCB_CHD_P', conv_sxs_lhpT, -1, 1], # bug for sign.
	['V_LHP_S_SCA_CMP_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CMP_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCA_CMP_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CMP_R', conv_sxs_lhpI, 1, 1],
	['V_LHP_V_SCA', conv_sxs_lhpV, 1, 1],
	['I_LHP_V_SCA', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCA_CHD_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CHD_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCA_CHD_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CHD_R', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CMP_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CMP_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CMP_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CMP_R', conv_sxs_lhpI, 1, 1],
	['V_LHP_V_SCB', conv_sxs_lhpV, 1, 1],
	['I_LHP_V_SCB', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CHD_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CHD_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CHD_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CHD_R', conv_sxs_lhpI, 1, 1],
]
chanmap7 = chanmap6

# Format for LHP GSE in 2021/09.
# Name, Conversion, Coeff, Use (1) or non use (0)
chanmap8 = [
	['Date', 1,    1,   0],      # 0 UT, 1 JST, 2 UT base, 3 JST base
	['Time', None, 1,   0],
	['sec',  None, 1,   0],
	['T_LHP_S_SCA_CMP_P', conv_sxs_lhpT, 1, 1],	
	['T_LHP_S_SCA_CMP_R', conv_sxs_lhpT, 1, 1],	
	['T_LHP_V_SCA_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCA_CMP_P', conv_sxs_lhpT, 1, 1],	
	['T_LHP_C_SCA_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCA_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCA_CHD_R', conv_sxs_lhpT, 1, 1],
	['T_LHP_V_SCA_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCA_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_C_SCA_CHD_P', conv_sxs_lhpT, -1, 1], # bug for sign.
	['T_LHP_S_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCB_CMP_R', conv_sxs_lhpT, 1, 1],
	['T_LHP_V_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_C_SCB_CMP_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCB_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_S_SCB-CHO_R', conv_sxs_lhpT, 1, 1],
	['T_LHP_V_SCB_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_L_SCB_CHD_P', conv_sxs_lhpT, 1, 1],
	['T_LHP_C_SCB_CHD_P', conv_sxs_lhpT, -1, 1], # bug for sign.
	['V_LHP_S_SCA_CMP_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CMP_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCA_CMP_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CMP_R', conv_sxs_lhpI, 1, 1],
	['V_LHP_V_SCA', conv_sxs_lhpV, 1, 1],
	['I_LHP_V_SCA', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCA_CHD_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CHD_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCA_CHD_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCA_CHD_R', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CMP_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CMP_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CMP_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CMP_R', conv_sxs_lhpI, 1, 1],
	['V_LHP_V_SCB', conv_sxs_lhpV, 1, 1],
	['I_LHP_V_SCB', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CHD_P', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CHD_P', conv_sxs_lhpI, 1, 1],
	['V_LHP_S_SCB_CHD_R', conv_sxs_lhpV, 1, 1],
	['I_LHP_S_SCB_CHD_R', conv_sxs_lhpI, 1, 1],
]

# File format ([skiprows, channmap, outstem])
file_formats=[
	[0,  chanmap0, "shi_gse"], # SHI GSE in TC1
	[40, chanmap1, "shi_gse"], # SHI GSE in TC2
	[39, chanmap2, "shi_gse"], # SHI GSE in TC2.1
	[39, chanmap3, "shi_gse"], # SHI GSE in TC1A
    [38, chanmap4, "shi_gse"], # SHI GSE in TC2A
    [41, chanmap5, "lhp_gse"], # LHP GSE in 2020/06, 2021/11, 12
    [40, chanmap6, "shi_gse"], # SHI GSE in TC3 and TC4
	[39, chanmap7, "lhp_gse"], # LHP GSE in 2021/08.
	[40, chanmap8, "lhp_gse"], # LHP GSE in 2021/09.
	]


########################################################
# Functions
########################################################
#------------------------------------------------------------
def index_time(data, format, shift=0, base=None):
    'Change TIME to datetime64 and use it as DataFrame index.'

    # S_TIME
    def dt2stime(x):
        x=x.replace('/','-')
        dt = datetime.strptime(x, "%Y-%m-%d %H:%M:%S")          
        return dt

    def base2stime(val_in, base):
        t = datetime.strptime(val_in,"%H:%M:%S")
        dt = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
        #es = dt.total_seconds()
        base = datetime.strptime(base,"%Y-%m-%dT%H:%M:%S")
        return base + dt

    if (format == 1) or (format == 2) or (format == 3) or (format == 6):
        #data.dropna(inplace=True)
        data['S_TIME'] = data['Date'] + ' ' + data['Time']
        data['S_TIME'] = data['S_TIME'].apply(lambda x:dt2stime(x))
    elif (format == 4) or (format == 7)  or (format == 8):
        data.dropna(inplace=True)
        data['S_TIME'] = data[['Date','Time']].apply(lambda x: "{} {}".format(x[0].strftime("%Y-%m-%d"),x[1]), axis=1)
        data['S_TIME'] = data['S_TIME'].apply(lambda x:dt2stime(x))
    elif (format == 5):
        #data.dropna(inplace=True)
        data['S_TIME'] = data['Time'].apply(lambda x:base2stime(x, base))
    else:
        print("format not suported.")
        exit

    # Convert JST to UT
    if (shift !=0):
        def jst2utc(val_in, shift):
            if (val_in < end_of_world):
                return val_in - np.timedelta64(shift, 'h')
            else:
                return val_in
        data['S_TIME'] = data['S_TIME'].apply(lambda x:jst2utc(x, shift))

    # Index S_TIME
    data=data[data['S_TIME']>beginning_of_world].copy()    
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]

    return data

#------------------------------------------------------------
def makedb(infile, outdir, format, base):

    # Decode file format
    skiprows = file_formats[format][0]
    chans = np.array(file_formats[format][1])[:,0]
    convs = np.array(file_formats[format][1])[:,1]
    coeffs = np.array(file_formats[format][1])[:,2]
    uses = np.array(file_formats[format][1])[:,3]
    outstem = file_formats[format][2]

    # Read Excel file.
    data_all = pd.read_excel(infile, engine='openpyxl', 
                            sheet_name=0, skiprows=skiprows, usecols=range(0,len(chans)),
                            parse_dates = [0], na_values=['+OVER', '-OVER', ' ', 'INVALID']
                            )
    data_all.columns = chans

    # Convert time.
    if (convs[0] == 0): # UT
        data_all = index_time(data_all, format)
    elif (convs[0] == 1): # JST
        data_all = index_time(data_all, format, shift=9)
    elif (convs[0] == 2): # Base, UT
        data_all = index_time(data_all, format, base=base)
    elif (convs[0] == 3): # Base, JST
        data_all = index_time(data_all, format, shift=9, base=base)
    
    # Remove unused channels
    chans_new=[]
    convs_new=[]
    coeffs_new=[]
    for i in range(len(uses)):
        if (uses[i]==0):
            del data_all[chans[i]]
        else:
            chans_new.append(chans[i])
            convs_new.append(convs[i])
            coeffs_new.append(coeffs[i])

    # Convert from raw to physical values
    for i in range(len(convs_new)):
        chan = chans_new[i]
        conv = convs_new[i]
        coeff = coeffs_new[i]
        if (conv is not None):
            if (chan == 'LD1_scale'):
                data_all['LD1'] = data_all[['LD1_scale', 'LD1_order']].apply(lambda x: conv(x[0], x[1]), axis=1)
                print("%s converted." % 'LD1')
            elif (chan == 'LD2_scale'):
                data_all['LD2'] = data_all[['LD2_scale', 'LD2_order']].apply(lambda x: conv(x[0], x[1]), axis=1)
                print("%s converted." % 'LD2')
            elif (chan == 'LD3_scale'):
                data_all['LD3'] = data_all[['LD3_scale', 'LD3_order']].apply(lambda x: conv(x[0], x[1]), axis=1)
                print("%s converted." % 'LD3')
            else:
                data_all[chan] = data_all[chan].apply(lambda x:conv(x*coeff))
                print("%s converted." % chan)

    # Save files.
    outdir_new = outdir + '/' + outstem + '/' 
    save_files_fff_range(data_all, outdir_new, outstem, pkl=True, csv=False, stat=True)

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-f', '--format', 
        help='Format of the data.',
        dest='format',
        type=int,
        nargs=1
        )
    parser.add_argument(
        '-b', '--base', 
        help='Base time (e.g., "2020-03-22T15:00:00")',
        dest='base',
        default=[None],
        type=str,
        nargs=1
        )
    args = parser.parse_args()
    
    makedb(args.infile[0], args.outdir[0], args.format[0], args.base[0])
