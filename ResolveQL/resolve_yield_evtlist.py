#!/usr/bin/env python3

############################################################################
# [Function]
# Yield evtlist for plots.
############################################################################


########################################################
# Imports
########################################################
import argparse
import datetime
import pandas as pd
import glob
from resolve_makedb_fff import collate_data_fff
from resolve_utils import time_to_utc
from astropy.io import fits


########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#------------------------------------------------------------
def main(dbdir, pldir):

    # ns1k
    infiles = glob.glob('{}/ns/1k/ns1k*.npz'.format(dbdir))
    infiles.sort()
    if len(infiles) > 0:
        for infile in infiles:
            _f=infile.split('/')[-1]
            year=int(_f[5:9])
            month=int(_f[9:11])
            day=int(_f[11:13])
            hour=int(_f[14:16])
            min=int(_f[16:18])
            sec=int(_f[18:20])
            dt = datetime.datetime(year, month, day, hour, min, sec)
            print("{},  , 5, nspec".format(dt.strftime("%Y/%m/%d %H:%M:%S")))

    # odb
    infiles = glob.glob('{}/odb/odb*.pkl'.format(dbdir))
    infiles.sort()
    for infile in infiles:
        data = pd.read_pickle(infile)
        for item, row in data.iterrows():
            print("{},  , 4, {:d}".format(item.strftime("%Y/%m/%d %H:%M:%S"), row.SEQNUM))

    # adr
    infiles = glob.glob('{}/*/resolve/hk/rsladr.gti'.format(pldir))
    infiles.sort()
    hdu='GTIADRON'
    hks=['START','STOP']

    for infile in infiles:
        #print("Reading %s in %s" % (hdu, infile))
        hdulist = fits.open(infile,memmap=True)
        data = None
        for _i in hks:
            column_name=[_i]
            datum = pd.DataFrame(hdulist[hdu].data.field(_i).byteswap().newbyteorder(),columns=column_name)
            if (data is None):
                data = datum
            else:
                data = pd.concat([data,datum],axis=1) 
        
        for i,row in data.iterrows():
            start = time_to_utc(row['START'])
            stop = time_to_utc(row['STOP'])
            print("{},{},9, recycle".format(start.strftime("%Y/%m/%d %H:%M:%S"), stop.strftime("%Y/%m/%d %H:%M:%S")))

    # SAA

    # Night earth occultation

    # Day earth occultation

    # Contacts

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--db',
        help='dbdir',
        dest='dbdir',
        type=str,
        default=None,
        nargs=1
        )
    parser.add_argument(
        '-p', '--pl',
        help='pldir',
        dest='pldir',
        type=str,
        default=None,
        nargs=1
        )
    args = parser.parse_args()

    main(args.dbdir[0], args.pldir[0])