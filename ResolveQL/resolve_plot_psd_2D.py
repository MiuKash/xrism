#!/usr/bin/env python

############################################################
# [Function]
# Plot PSD vs time.
############################################################

########################################################
# Imports
########################################################
import argparse, datetime, os, re, glob
from sys import exit
from os.path import exists
import numpy as np
import pandas as pd
#
from resolve_utils import get_freqs, get_datetime_from_args, filter_by_datetime
from resolve_plot_nr_ns_2D import plotly_2D
from resolve_plot_hk import get_events, str_now
from resolve_makedb_psd import chnum_default

########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_psd(psdfiles, samplingrate, recordlength, ch):
    
    starttime_all = []
    psd2d_all = None
    freq_all = get_freqs(samplingrate, recordlength)    

    for psdfile in psdfiles:        
        
        # Get time from file name.
        filename = os.path.split(psdfile)[1].replace('.npz','')
        stime = re.sub('psd(n|b|a)_', '', filename)
        stime = datetime.datetime.strptime(stime, "%Y%m%d-%H%M%S")
        starttime_all.append(stime)
        
        # Get sampling from file name.
        psd2d = np.load(psdfile)['arr_0'][ch]
        print("Reading ch%d in %s" % (ch, psdfile))
        
        # Stack data        
        if psd2d_all is None:
            psd2d_all = psd2d            
        else:
            psd2d_all = np.vstack([psd2d_all,psd2d])

    if (psd2d_all is None):
        return None, None, None
    else:
        return starttime_all, freq_all, psd2d_all.T
    

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-r', '--sampling rate',
        help='Sampling date of the PSD data.',
        dest='samplingrate',
        type=float,        
        nargs=1
        )
    parser.add_argument(
        '-l', '--record length',
        help='Record length of the PSD data.',
        dest='recordlength',
        type=float,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-R', '--resample',
        help='Resample files.',
        dest='resample',
        type=int,
        default=1,
        nargs=1
        )
    parser.add_argument(
        '-E', '--evtlists',
        help='Event lists.',
        dest='evtlists',
        type=str,
        nargs='*'
        )
    parser.add_argument(
        '-m', '--monitor',
        help='accel, mag, or busV',
        dest='monitor',
        default="accel",
        choices=["accel", "mag", "busV", "accel.LMS"],
        type=str,
        nargs='?'
        ),
    parser.add_argument(
        '-c', '--chnum',
        help='Number of channels.',
        dest='chnum',
        default=chnum_default,
        type=int,
        nargs='?'
        )
    parser.add_argument(
        '-z', '--zrange',
        help='Range for zscale.',
        default=[None, None],
        dest='zrange',
        type=float,
        nargs=2
        )
    args = parser.parse_args()
    
    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    # outstem
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end1 = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    # title
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end2 = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')

    # Get and filter infiles by time
    infiles = glob.glob('%s/psda*.npz' % (args.indir[0]))    
    infiles = filter_by_datetime(infiles, 'epoch', datetime_start, datetime_end, 1)
    infiles = infiles[::args.resample[0]]
    if ( len(infiles) == 0 ):
        print("No file found.")
        exit(1)

    # events
    events=get_events(args.evtlists, datetime_start, datetime_end)

    # Loop over channels. 
    for ch in range(args.chnum):  

        monitor = args.monitor
        # Title     
        if (monitor == "accel"):
            title='log10 Power (mG/rtHz) accel ch%02d (%s-%s) updated at %s' % (ch, str_start2, str_end2, str_now)
        elif (monitor == "busV"):
            title='log10 Power (V/rtHz) accel ch%02d (%s-%s) updated at %s' % (ch, str_start2, str_end2, str_now)
        elif (monitor == "mag"):
            title='log10 Power (V/rtHz) accel ch%02d (%s-%s) updated at %s' % (ch, str_start2, str_end2, str_now)
        elif (monitor == "accel.LMS"):
            title='log10 Power (mG/rtHz) accel ch%02d (%s-%s) updated at %s' % (ch, str_start2, str_end2, str_now)
        
        # Output
        outdir_new = args.outdir[0].replace('{}_2D'.format(monitor),'{}_2D/c{:02d}'.format(monitor, ch))
        #outdir_new = args.outdir[0] + '/c%02d' % ch
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem = outdir_new + '/%s_c%02d_%s-%s' % (monitor, ch, str_start1, str_end1)

        # Collate, plot, save
        starttime_all, freq_all, psd2d_all = collate_psd(infiles, args.samplingrate[0], args.recordlength[0], ch)                
        if (psd2d_all is not None):
            plotly_2D(starttime_all, freq_all, np.log10(psd2d_all).astype(np.float16), outstem, title, datetime_start, datetime_end, events=events, zrange=args.zrange)