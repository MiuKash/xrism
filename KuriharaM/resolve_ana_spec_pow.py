#!/usr/bin/env python3

############################################################
# Description
############################################################
# Calculate power of the PSS in the specified freq range.

########################################################
# Imports
########################################################
import argparse
import math
import numpy as np
import pandas as pd
import os
from resolve_utils import scale_y_base

########################################################
# User-defined parameters
########################################################
kinds=['ns','gse']


########################################################
# Functions
########################################################
#-------------------------------------------------------
def get_bin(freq, record_length):

    df = 1./record_length
    bin = round(freq/df) - 1 
    return bin

#-------------------------------------------------------
def get_p(datum, f_lo, f_hi, record_length, peak=False):

    datum2 = datum * datum
    f_lo_bin = math.floor(f_lo*record_length)+1
    f_hi_bin = math.ceil(f_hi*record_length)+1
    
    assert f_lo_bin != f_hi_bin # to assert a finite bin size. Expand freq range for otherwise.

    df_bin = (f_hi_bin - f_lo_bin)
    df = df_bin / record_length
    if (peak is True):
        p2 = datum2[f_lo_bin:f_hi_bin+1].max()*df_bin # nV^2
    else:
        p2 = datum2[f_lo_bin:f_hi_bin+1].mean()*df_bin # nV^2
    
    p2_e = datum2[f_lo_bin:f_hi_bin+1].std()**2.0 * df_bin
    return p2, p2_e, df_bin

#-------------------------------------------------------
def main(infile, freq_s, freq_b, kind, sampling_rate):
        
    # Load data
    data = np.load(infile, allow_pickle=True)['arr_0']
    
    # Check length
    if (kind == 'ns'):
        len_ns = len(data)
        assert (len_ns == 4096) or (len_ns == 512)
        record_length = 1./sampling_rate * len_ns * 2.0
        data = data * scale_y_base / np.sqrt(len_ns*2/1024) * 1e9
    elif (kind == 'gse'):
        assert sampling_rate != 12.5e3
        data = data.T
        len_ns = len(data)
        record_length = 1./sampling_rate * len_ns * 2.0
    
    # Calculate per in s and b.
    result = []
    for ch in range(len(data[0])):
        freq_s_tmp = freq_s.copy() # Need copy() to pop without destructing the original.
        freq_b_tmp = freq_b.copy() # Need copy() to pop without destructing the original.
        datum = data[:,ch]
        # Power in source region (nV^2)
        p2_s = 0
        p2_e_s = 0
        df_s = 0
        while (len(freq_s_tmp) > 1):
            f_lo = freq_s_tmp.pop(0)
            f_hi = freq_s_tmp.pop(0)
            if (f_lo > f_hi):
                f_tmp = f_hi
                f_hi = f_lo
                f_lo = f_tmp
            print("Integrating {:.2f}-{:.2f} Hz for s.".format(f_lo, f_hi))
            dp2_s, dp2_e_s, ddf_s =  get_p(datum, f_lo, f_hi, record_length, peak=True)
            p2_s += dp2_s
            p2_e_s += dp2_e_s
            df_s += ddf_s
        
        # Power in background region (nV^2)
        p2_b = 0
        p2_e_b = 0
        df_b = 0
        while (len(freq_b_tmp) > 1):
            f_lo = freq_b_tmp.pop(0)
            f_hi = freq_b_tmp.pop(0)
            if (f_lo > f_hi):
                f_tmp = f_hi
                f_hi = f_lo
                f_lo = f_tmp
            print("Integrating {:.2f}-{:.2f} Hz for b.".format(f_lo, f_hi))
            dp2_b, dp2_e_b, ddf_b =  get_p(datum, f_lo, f_hi, record_length, peak=False)
            p2_b += dp2_b
            p2_e_b += dp2_e_b
            df_b += ddf_b
        result.append([ch, p2_s, p2_e_s, df_s, p2_b, p2_e_b, df_b])
        
    # Collate results
    result = pd.DataFrame(result)
    result.columns=['ch', 'p2_s (nV^2)', 'p2_e_s (nV^2)', 'df_s (Hz)', 'p2_b (nV^2)', 'p2_e_b (nV^2)', 'df_b (Hz)']
    #
    result['p2_df_s (nV^2/Hz)'] = result['p2_s (nV^2)'] / result['df_s (Hz)']
    result['p2_df_b (nV^2/Hz)'] = result['p2_b (nV^2)'] / result['df_b (Hz)']
    #
    result['p2_e_df_s (nV^2/Hz)'] = np.sqrt(result['p2_e_s (nV^2)'] / result['df_s (Hz)'])
    result['p2_e_df_b (nV^2/Hz)'] = np.sqrt(result['p2_e_b (nV^2)'] / result['df_b (Hz)'])
    #
    result['p2_df_s-b (nV^2/Hz)'] = result['p2_df_s (nV^2/Hz)'] - result['p2_df_b (nV^2/Hz)']
    result['p2_df_e_s-b (nV^2/Hz)'] = result['p2_e_df_b (nV^2/Hz)'] # src err should not be included here.
    #
    result['p_df_s-b (nV/rtHz)'] = np.sqrt(result['p2_df_s-b (nV^2/Hz)'])
    result['p_df_e_s-b (nV/rtHz)'] = np.sqrt(result['p2_df_e_s-b (nV^2/Hz)'])
    result['p_df_sig_s-b'] = result['p_df_s-b (nV/rtHz)'] / result['p_df_e_s-b (nV/rtHz)'] 
    result.set_index(['ch'],inplace=True)
    result['3sig'] = 3.0*result['p_df_e_s-b (nV/rtHz)'] 

    # Output
    infile_basename = os.path.basename(infile)
    outfile = infile_basename.replace('.npz','.csv').replace('.pkl','.csv')
    result.to_csv(outfile)
    print("{} saved.".format(outfile))
    print(result)
    return 0
    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--s',
        help='Frequency range of source (s1_lo, s1_hi, s2_lo, s2_hi, ...)',
        dest='freq_s',
        type=float,
        nargs='+'
        )
    parser.add_argument(
        '-b', '--b',
        help='Frequency range of background (blg1_lo, b1_hi, blg2_lo, b2_hi, ...)',
        dest='freq_b',
        type=float,
        nargs='+'
        )
    parser.add_argument(
        '-k', '--kind',
        help='kind of data (ns or gse)',
        dest='kind',
        default='ns',
        choices=kinds,
        type=str,
        nargs='?'
        )
    parser.add_argument(
        '-r', '--rate',
        help='Sampling rate (Hz)',
        dest='sampling_rate',
        default=12.5e3,
        type=float,
        nargs='?'
        )

    args = parser.parse_args()    
    main(args.infile[0], args.freq_s, args.freq_b, args.kind, args.sampling_rate)