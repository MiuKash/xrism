#!/usr/bin/env python3

############################################################
# [Function]
# Plot accel vs freq for a given time-domain file.
############################################################

########################################################
# Imports
########################################################
import argparse, glob, os
import numpy as np
#
from resolve_plot_ns import palette, dashes
from resolve_utils import get_freqs
from resolve_plot_hk import str_now
from resolve_makedb_psd import chnum_default, parse_accrawdata, make_psd
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import seaborn as sns
#
import threading

########################################################
# User-defined parameters
########################################################

########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_data(infile, chnum, gains):

    # Read data.
    voltages, dt, shotlength, stime = parse_accrawdata(infile, format='npz', chnum=chnum)
    samplingrate = 1./dt
            
    psd2d_a = []
    #psd2d_b = [] # Use resampled data by reduction_factor.
    #psd2d_n = [] # Use the first 1/reduction_factor data.     

    # Calc PSD for each channel
    for ch in range(chnum):
        v_ch_a = voltages[ch] * gains[ch]
        #v_ch_b = v_ch_a[0::int(reduction_factor)] 
        #v_ch_n = v_ch_a[0:int(shotlength*samplingrate/reduction_factor)] 

        psddata_a = make_psd(v_ch_a, dt, [stime, stime+shotlength])
        #psddata_b = make_psd(v_ch_b, dt*reduction_factor, [stime, stime+shotlength])
        #psddata_n = make_psd(v_ch_n, dt, [stime, stime+shotlength/reduction_factor])

        psd2d_a.append(psddata_a.flatten()[3:])
        #psd2d_b.append(psddata_b.flatten()[3:])
        #psd2d_n.append(psddata_n.flatten()[3:])
    
    # Add PSD    
    freqs = get_freqs(samplingrate, shotlength)
    
    Xts, Yts = np.linspace(0,shotlength,int(shotlength/dt)+1)[:-1], voltages
    Xfs, Yfs = freqs, psd2d_a

    return Xts, Yts, Xfs, Yfs

#-------------------------------------------------------
def make_plot_psd_each(Xts, Yts, Xfs, Yfs, title, monitor, outstem):

    if (monitor=="accel"):
        yftitle='Power (mG/rtHz)'         
        yfscale = 1e3
        yfrange=[-2, 2]      
    elif (monitor=="mag"):
        yftitle='Voltage (mV/rtHz)'         
        yfscale = 1e3
        yfrange=[-2, 2]
    elif (monitor=="busV"):
        yftitle='Power (V/rtHz)'         
        yfscale = 1
        yfrange=[-4, 0]
    
    # Define layout
    fig = make_subplots(
        rows=2, cols=1, shared_xaxes=False, vertical_spacing=0.1
    )    
    Layout = go.Layout(
        title=title,
        width=1280, 
        height=1280,
        xaxis=dict(            
            autorange = True,
            showgrid = True, 
            tickmode = 'auto'
            ),
        yaxis=dict(
            autorange = True,
            showgrid = True, 
            tickmode = 'auto',
            ),
        showlegend=True,
        legend = dict(
            x = 1.02, xanchor = 'left',
            y = 1.00, yanchor = 'auto',
            bordercolor = '#444', 
            #borderwidth = 0,
            ),
        )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    # Time-domain
    print("Processing time-domain data.")
    fig.update_xaxes(title_text='Time', type='linear', row=1, col=1)
    fig.update_yaxes(title_text='ADU', type='linear', autorange = True, row=1, col=1)
    for px in range(len(Yts)):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xts, y=Yts[px,:], 
                name='c%02d' % px, 
                legendgroup=px,
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            ),
            row=1, col=1
        )

    # Freq-domain
    print("Processing freq-domain data.")
    fig.update_xaxes(title_text='Freq (Hz)', type='linear', row=2, col=1)
    fig.update_yaxes(title_text=yftitle, type='log', autorange=False, range=yfrange, row=2, col=1)
    for px in range(len(Yfs)):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xfs, y=Yfs[px]*yfscale, 
                name='c%02d' % px, 
                legendgroup=px,
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            ),
            row=2, col=1
        )

    # Output
    def write_static():
        fig.write_image("{}.png".format(outstem), validate=False, engine='kaleido')
        print("File saved in {}.png".format(outstem))
    def write_html():
        offline.plot(fig, filename='{}.html'.format(outstem), auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in {}.html".format(outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()

    return 0  



########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-m', '--monitor',
        help='accel, mag, or busV',
        dest='monitor',
        default="accel",
        type=str,
        nargs='?'
        ),
    parser.add_argument(
        '-c', '--chnum',
        help='Number of channels.',
        dest='chnum',
        default=chnum_default,
        type=int,
        nargs='?'
        )
    parser.add_argument(
        '-g', '--gain',
        help='Gains with for the number of channels.',
        dest='gains',
        type=float,
        nargs='+'
        )
    args = parser.parse_args()

    
    # Outstem
    outstem=args.outdir[0] + '/' + os.path.split(args.infile[0])[1].replace('.npz','')
    
    # Title
    if (args.monitor == "accel"):
        title='Accel PSD (%s) updated at %s' % (os.path.split(args.infile[0])[1].replace('.npz',''), str_now)
    elif (args.monitor == "mag"):
        title='Mag EMI PSD (%s) updated at %s' % (os.path.split(args.infile[0])[1].replace('.npz',''), str_now)
    elif (args.monitor == "busV"):
        title='BusV PSD (%s) updated at %s' % (os.path.split(args.infile[0])[1].replace('.npz',''), str_now)
    
    # Collate data
    Xts, Yts, Xfs, Yfs = collate_data(args.infile[0], args.chnum, args.gains)

    # Plot
    make_plot_psd_each(Xts, Yts, Xfs, Yfs, title, args.monitor, outstem)
