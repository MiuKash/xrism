#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for
# (1) lc of grade events
# (2) bl of baseline events
# (3) el of event lost events
############################################################################


########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
from resolve_plot_hk import collate_data
from resolve_makedb_fff import hks_ac, hks_px_cl, hks_px_uf, hks_el, save_files_fff_range
from resolve_utils import get_datetime_from_args, filter_by_datetime, save_files, stime_to_datetime

########################################################
# User-defined parameters
########################################################
hks_bl = hks_px_uf

########################################################
# Functions
########################################################
#------------------------------------------------------------
def get_timebins(datetime_start, datetime_end, resample):

    times=[]
    datetime_start_roi = datetime_start
    datetime_stop_roi = datetime_start + np.timedelta64(resample, 's')
    while (datetime_stop_roi <= datetime_end):
        times.append([datetime_start_roi, datetime_stop_roi])
        datetime_start_roi += np.timedelta64(resample, 's')
        datetime_stop_roi += np.timedelta64(resample, 's')
    times=pd.DataFrame(times)
    times.columns=['START', 'STOP']
    return times

#------------------------------------------------------------
def makedb_evt_bl(indir, table, dbhks, outdir, datetime_start, datetime_end, resample):

    # Collate data.
    data = collate_data(indir, dbhks, datetime_start, datetime_end, resample=0, rates=None) 

    if (data is None or len(data)==0):
        print('No data found.')
        return None

    times = get_timebins(datetime_start, datetime_end, resample)
    
    evt_all = times
    for px in range(36):
        bl_mean=[]
        bl_std=[]
        for row in times.iterrows():
            start = row[1]['START']
            stop = row[1]['STOP']
            data_roi = data[(data.index>start) & (data.index<=stop) & (data.PIXEL==px) & (data.ITYPE==5)]
            bl_mean.append(data_roi.PHA.values.mean())
            bl_std.append(data_roi.PHA.values.std())
        evt_all['BL_MEAN%02d' % px] = bl_mean
        evt_all['BL_STD%02d' % px] = bl_std
    
    # Index S_TIME
    evt_all['TIME'] = evt_all['START']
    evt_all.set_index('TIME',inplace=True)
    evt_all.sort_index(inplace=True)
    evt_all = evt_all[~evt_all.index.duplicated(keep='last')]    

    outdir_new = outdir + '/bl/'
    save_files_fff_range(evt_all, outdir_new, 'bl', stat=True)

    return 0

#------------------------------------------------------------
def makedb_evt_ac(indir, table, dbhks, outdir, datetime_start, datetime_end, resample):

    # Collate data.
    data = collate_data(indir, dbhks, datetime_start, datetime_end, resample=0, rates=None)     
    if (data is None or len(data)==0):
        print('No data found.')
        return None
    data = data[(data.PSP_ID==1)]

    times = get_timebins(datetime_start, datetime_end, resample)
    
    evt_all = times
    ac=[]
    ac_bl=[]
    for row in times.iterrows():
        start = row[1]['START']
        stop = row[1]['STOP']
        data_roi = data[(data.index>start) & (data.index<=stop)]
        ac.append(len(data_roi[data_roi.AC_ITYPE==0]))
        ac_bl.append(len(data_roi[data_roi.AC_ITYPE==1]))
    evt_all['AC'] = ac
    evt_all['AC_BL'] = ac_bl

    # Index S_TIME
    evt_all['TIME'] = evt_all['START']
    evt_all.set_index('TIME',inplace=True)
    evt_all.sort_index(inplace=True)
    evt_all = evt_all[~evt_all.index.duplicated(keep='last')]    

    outdir_new = outdir + '/lc_%s/' % table
    save_files_fff_range(evt_all, outdir_new, 'lc_%s' % table, stat=True)

    return 0

#------------------------------------------------------------
def makedb_evt_px(indir, table, dbhks, outdir, datetime_start, datetime_end, resample):
    
    # Collate data.
    data = collate_data(indir, dbhks, datetime_start, datetime_end, resample=0, rates=None) 
    
    if (data is None or len(data)==0):
        print('No data found.')
        return None

    times = get_timebins(datetime_start, datetime_end, resample)
    
    evt_all = times
    for px in range(36):
        hp=[]
        mp=[]
        ms=[]
        lp=[]
        ls=[]
        bl=[]
        for row in times.iterrows():
            start = row[1]['START']
            stop = row[1]['STOP']
            data_roi = data[(data.index>start) & (data.index<=stop) & (data.PIXEL==px)]
            hp.append(len(data_roi[data_roi.ITYPE==0]))
            mp.append(len(data_roi[data_roi.ITYPE==1]))
            ms.append(len(data_roi[data_roi.ITYPE==2]))
            lp.append(len(data_roi[data_roi.ITYPE==3]))
            ls.append(len(data_roi[data_roi.ITYPE==4]))
            bl.append(len(data_roi[data_roi.ITYPE==5]))
        evt_all['HP%02d' % px] = hp
        evt_all['MP%02d' % px] = mp
        evt_all['MS%02d' % px] = ms
        evt_all['LP%02d' % px] = lp
        evt_all['LS%02d' % px] = ls
        evt_all['BL%02d' % px] = bl
    
    # Index S_TIME
    evt_all['TIME'] = evt_all['START']
    evt_all.set_index('TIME',inplace=True)
    evt_all.sort_index(inplace=True)
    evt_all = evt_all[~evt_all.index.duplicated(keep='last')]    

    outdir_new = outdir + '/lc_%s/' % table
    save_files_fff_range(evt_all, outdir_new, 'lc_%s' % table, stat=True)

    return 0

makedb_evt_px_uf = makedb_evt_px
makedb_evt_px_cl = makedb_evt_px

#------------------------------------------------------------
def makedb_evt_el(indir, table, dbhks, outdir, datetime_start, datetime_end, resample):

    # Collate data.
    
    data = collate_data(indir, dbhks, datetime_start, datetime_end, resample=0, rates=None)     
    times = get_timebins(datetime_start, datetime_end, resample)

    # scan events
    evt_all = times
    if (data is None or len(data)==0):
        for px in range(36):
            evt_all['LTF%02d' % px] = 1.0
    else:
        for px in range(36):
            datum = data[data.PIXEL==px]
            if (datum is None or len(datum)==0):
                evt_all['LTF%02d' % px] = 1.0
            else:
                ltfs=[]
                datetime_start_roi = datetime_start # A
                datetime_stop_roi = datetime_start + np.timedelta64(resample, 's') # B
                while (datetime_stop_roi <= datetime_end):
                    duration = 0 
                    for row in datum.iterrows():
                        datetime_start_evt = row[1]['START'] # S
                        datetime_stop_evt = row[1]['STOP']   # E                
                        # SEAB
                        if (datetime_stop_evt < datetime_start_roi):
                            duration += 0
                            #print("SEAB",duration,datetime_start_roi, datetime_stop_roi, datetime_start_evt, datetime_stop_evt )
                        # ABSE
                        elif (datetime_start_evt >= datetime_stop_roi):
                            duration += 0
                            #print("ABSE",duration,datetime_start_roi, datetime_stop_roi, datetime_start_evt, datetime_stop_evt)
                        # SABE
                        elif (datetime_start_evt < datetime_start_roi) and (datetime_stop_evt >= datetime_stop_roi):
                            duration += (datetime_stop_roi - datetime_start_roi) / np.timedelta64(1, 's')
                            #print("SABE",duration,datetime_start_roi, datetime_stop_roi, datetime_start_evt, datetime_stop_evt)
                        # ASEB
                        elif (datetime_start_evt >= datetime_start_roi) and (datetime_start_evt < datetime_stop_roi) and (datetime_stop_evt >= datetime_start_roi) and (datetime_stop_evt < datetime_stop_roi):
                            duration += (datetime_stop_evt - datetime_start_evt) / np.timedelta64(1, 's')
                            #print("ASEB",duration,datetime_start_roi, datetime_stop_roi, datetime_start_evt, datetime_stop_evt)
                        # SAEB
                        elif (datetime_start_evt < datetime_start_roi) and (datetime_stop_evt >= datetime_start_roi) and (datetime_stop_evt < datetime_stop_roi):
                            duration += (datetime_stop_evt - datetime_start_roi) / np.timedelta64(1, 's')
                            #print("SAEB",duration,datetime_start_roi, datetime_stop_roi, datetime_start_evt, datetime_stop_evt)
                        # ASBE
                        elif (datetime_start_evt >= datetime_start_roi) and (datetime_start_evt < datetime_stop_roi) and (datetime_stop_evt >= datetime_stop_roi):
                            duration += (datetime_stop_roi - datetime_start_evt) / np.timedelta64(1, 's')
                            #print("ASBE",duration,datetime_start_roi, datetime_stop_roi, datetime_start_evt, datetime_stop_evt)
                        else:
                            print("This cannot happen.")
                            breakpoint()

                    #assert duration <= resample                  
                    ltf = (resample - duration)/resample
                    ltfs.append(ltf)
                    datetime_start_roi += np.timedelta64(resample, 's')
                    datetime_stop_roi += np.timedelta64(resample, 's')
                evt_all['LTF%02d' % px] = ltfs

    # Index S_TIME
    evt_all['TIME'] = evt_all['START']
    evt_all.set_index('TIME',inplace=True)
    evt_all.sort_index(inplace=True)
    evt_all = evt_all[~evt_all.index.duplicated(keep='last')]    
    
    outdir_new = outdir + '/lc_%s/' % table
    save_files_fff_range(evt_all, outdir_new, 'lc_%s' % table, stat=True)

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir name.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-r', '--resample',
        help='Resample seconds.',
        dest='resample',
        type=int,
        default=16,
        nargs='?'
        )
    parser.add_argument(
        '-t', '--table',
        help='Table.',
        dest='table',
        type=str,
        nargs="*"
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end) + np.timedelta64(1, 's')

    makedb_evt = eval('makedb_evt_'+args.table[0])
    hks = eval('hks_'+args.table[0])
    hdu= args.table[0]
    if (hdu == 'bl'):
        hdu = 'px_uf'
    dbhks = {
        hdu : hks
    }
    makedb_evt(args.indir[0], args.table[0], dbhks, args.outdir[0], datetime_start, datetime_end, args.resample)