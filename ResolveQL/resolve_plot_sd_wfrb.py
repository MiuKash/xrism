#!/usr/bin/env python3

############################################################
# [Function]
# Plot time and freq domain data of sd and wfrb.
############################################################

########################################################
# Imports
########################################################
import argparse, datetime, os, re
from os.path import exists
import math
import pandas as pd
import numpy as np
import scipy.fftpack as sf
#
from resolve_plot_nr_ns_2D import plotly_2D
from resolve_plot_ns import make_plot_ns, palette, dashes
from resolve_plot_hk import str_now
from resolve_utils import get_freqs, sampling_rate, ac_pedestal, ac_thres, save_files
from myutils import add_line
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#
import threading

########################################################
# User-defined parameters
########################################################
zmin=-50
zmax=50


########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_time(data_px, data_ac, mode):
    
    Zs = None
    Ys=np.linspace(0,35,36)

    # get len_ns
    len_ns_all = 0
    for px in range(36):
        if (mode == 'sd') :
            datum_px = data_px[(data_px.PIXEL==px) & (data_px.NOISEREC_MODE==2)].iloc[:,3:].values.flatten()
            if (len(datum_px)>0):
                len_ns = int(np.round(len(datum_px)/1024/25,0)) * 25 * 1024 # 25*(int) samples only. 
                print("Pixel {:02d} sampled for {:2d} samples.".format(px, int(len_ns/1024)))
                if (len_ns > len_ns_all):
                    len_ns_all = len_ns                
        elif (mode == 'wfrb'):
            datum_px = data_px[(data_px.PIXEL==px)].iloc[:,1+2048:2+2048*2].values.flatten()
            if (len(datum_px)>0):
                len_ns = 2**math.floor(np.log2(len(datum_px)/12500)) * 12500 # 2^(int) s only.
                print("Pixel {:02d} sampled for a {:.1f}s.".format(px, len_ns/12500))
                if (len_ns > len_ns_all):
                    len_ns_all = len_ns
    Xs=np.linspace(0,len_ns_all-1,len_ns_all)/12.5e3

    # Get values
    for px in range(36):
        if (mode == 'sd') :
            datum_px = data_px[(data_px.PIXEL==px) & (data_px.NOISEREC_MODE==2)].iloc[:,3:].values.flatten()

        elif (mode == 'wfrb'):
            datum_px = data_px[(data_px.PIXEL==px)].iloc[:,1+2048:2+2048*2].values.flatten()
        
        # Null if short, truncate and detrend if long.
        if (len(datum_px) < len_ns_all):
            datum_px = np.zeros(len_ns_all)
            datum_px[:] = np.nan
        else:
            datum_px = datum_px[:len_ns_all]
            datum_px = datum_px - datum_px.mean()

        # Stack
        if (Zs is None):
            Zs = datum_px
        else:
            Zs = np.vstack([Zs,datum_px])
    
    # ac
    if (data_ac is None):
        datum_ac = [np.nan for i in range(len_ns_all)]
    else:
        for px in range(4):
            if (mode == 'sd') :
                datum_ac = data_ac[(data_ac.PSP_ID==px) & (data_ac.NOISEREC_MODE==2)].iloc[:,3:].values.flatten() 
            elif (mode == 'wfrb'):
                datum_ac = data_ac[(data_ac.PSP_ID==px)].iloc[:,1+2048:2+2048*2].values.flatten() 

            # Skip if zero, null if short, truncate and offset if long.
            if (len(datum_ac) == 0):
                continue
            elif (len(datum_ac) < len_ns_all):
                datum_ac = np.zeros(len_ns_all)
                datum_ac[:] = np.nan
            else:
                datum_ac = datum_ac[:len_ns_all]
                datum_ac = (datum_ac - ac_pedestal[px])/ac_thres[px]

            # Stack
            if (Zs is None):
                Zs = datum_ac
            else:
                Zs = np.vstack([Zs,datum_ac])
    
    return Xs, Ys, Zs, len_ns

#-------------------------------------------------------
def collate_freq(data_px, data_ac, mode, len_ns):
    
    Zs = None
    Xs=get_freqs(sampling_rate, len_ns/sampling_rate)

    #px
    Ys=np.linspace(0,35,36)
    for px in range(36):
        if (mode == 'sd') :
            datum_px = data_px[(data_px.PIXEL==px) & (data_px.NOISEREC_MODE==2)].iloc[:,3:].values.flatten() 
        elif (mode == 'wfrb') :
            datum_px = data_px[(data_px.PIXEL==px)].iloc[:,2:2+2048].values.flatten() 
        # Truncate
        if (len(datum_px) < len_ns):
            datum_px = np.zeros(len_ns)
        else:
            datum_px = datum_px[:len_ns]

        # Detrend
        datum_px = datum_px - datum_px.mean()
            
        # Calc PSD
        if (len(datum_px) > 0):
            filter_func=np.ones(len_ns)
            #filter_func=np.hanning(len_ns)
            fft = sf.fft(datum_px*filter_func,len_ns)[1:int(len_ns/2)+1]
        else:
            fft = np.zeros(int(len_ns/2))
        
        # PSD.
        Z = np.abs(fft)
        if (Zs is None):
            Zs = Z
        else:
            Zs = np.vstack([Zs,Z])
    #ac
    if (data_ac is None):
        Z = [np.nan for i in range(int(len_ns/2))]
    else:
        for px in range(4):
            if (mode == 'sd') :
                datum_ac = data_ac[(data_ac.PSP_ID==px) & (data_ac.NOISEREC_MODE==3)].iloc[:,3:].values.flatten() 
            elif (mode == 'wfrb') :
                datum_ac = data_ac[(data_ac.PSP_ID==px)].iloc[:,2:2+2048].values.flatten() 
            # Truncate
            if (len(datum_ac) < len_ns):
                datum_ac = np.zeros(len_ns)
            else:
                datum_ac = datum_ac[:len_ns]

            # Detrend
            datum_ac = datum_ac - datum_ac.mean()
                
            # Calc PSD
            if (len(datum_ac) > 0):
                filter_func=np.ones(len_ns)
                #filter_func=np.hanning(len_ns)
                fft = sf.fft(datum_ac*filter_func,len_ns)[1:int(len_ns/2)+1]
            else:
                fft = np.zeros(int(len_ns/2))
    
            # PSD.
            Z = np.abs(fft)
            if (Zs is None):
                Zs = Z
            else:
                Zs = np.vstack([Zs,Z])

    Ys=np.linspace(0,36,38)
    return Xs, Ys, Zs

#------------------------------------------------------------
def make_plot_td(Xs, data, title, outstem, ytitle, range=[zmin,zmax]):
    "Plot time."

    # Define layout
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title ='Time (s)',
            type = 'linear',
            autorange = False,
            range = [Xs[0], Xs[-1]],
            #tickmode = 'auto'
            ),
        yaxis=dict(
            title = ytitle,
            type = 'linear',
            autorange = False,
            range = range,
            #tickmode = 'auto'
            ),
        showlegend=True,
        legend = dict(
            x = 1.02, xanchor = 'left',
            y = 1.00, yanchor = 'auto',
            bordercolor = '#444', 
            borderwidth = 0,
            ),
        )
    fig = make_subplots(
        rows=1, cols=1,
    )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    # Plot data
    for px in np.linspace(0,(len(data[0]))-1,(len(data[0]))):
        px = int(px)
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xs, y=data[:,px], 
                name='%02d' % px, 
                #legendgroup=int(px/9),
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            )
        )

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    return data    
    

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output dir.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-m', '--mode', 
        help='mode (sd or wfrb)',
        dest='mode',
        type=str,
        nargs=1
        )
    """
    parser.add_argument(
        '-l', '--length', 
        help='nspec length',
        dest='len_ns',
        type=int,
        nargs=1
        )
"""
    args = parser.parse_args()
        
    infile_name = os.path.split(args.infile[0])[1] 
    datetime_start = infile_name.replace('.','_').split('_')[1]
    datetime_start = datetime.datetime.strptime(datetime_start, '%Y%m%d-%H%M%S')

    # Read files.
    data_px = pd.read_pickle(args.infile[0])
    if os.path.exists(args.infile[1]):
        data_ac = pd.read_pickle(args.infile[1])
    else:
        data_ac = None
    mode=args.mode[0]    

    # time
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d-%H%M%S')
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    year = pd.to_datetime(datetime_start).strftime('%Y')
    month = pd.to_datetime(datetime_start).strftime('%m')
    
    # Plot time
    Xs, Ys, Zs, len_ns = collate_time(data_px, data_ac, mode)
    # 1D
    outdir = re.sub('/$','',args.outdir[0]) + '/time/%s/%s/' % (year, month)
    outstem = outdir + '/%s_time_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='Deriv [px] or (raw-ped)/thres [ac] in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    data_td = make_plot_td(Xs, Zs.T, title, outstem, 'Signal (Deriv)')
    save_files(data_td, None, "%s.npz" % outstem)

    # 2D
    outdir = re.sub('/$','',args.outdir[0]) + '_2D/time/%s/%s/' % (year, month)
    outstem = outdir + '/%s_time_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='Deriv [px] or (raw-ped)/thres [ac] in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    plotly_2D(Xs, Ys, Zs, outstem, title, xtitle='Time (s)', ytitle='Pixel', ytype='linear', yint=True, events=None, zrange=[zmin, zmax])

    # Plot freq
    Xs, Ys, Zs = collate_freq(data_px, data_ac, mode, len_ns)
    # 1D
    outdir = re.sub('/$','',args.outdir[0]) + '/freq/%s/%s/' % (year, month)
    outstem = outdir + '/%s_freq_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='log Power (ADU/rtHz) in %s (%s) updatad at %s' % (mode, str_start2, str_now)   
    data_fd = make_plot_ns(Xs, Zs.T, title, outstem, len_ns, monitor=mode) 
    save_files(data_fd, None, "%s.npz" % outstem)

    # 2D
    outdir = re.sub('/$','',args.outdir[0]) + '_2D/freq/%s/%s/' % (year, month)
    outstem = outdir + '/%s_freq_%s' % (mode, str_start1)
    if not exists(outdir):
        os.makedirs(outdir)
    title='log Power (ADU/rtHz) in %s (%s) updatad at %s' % (mode, str_start2, str_now)
    plotly_2D(Xs, Ys, np.log10(Zs), outstem, title, xtitle='Freq (Hz)', ytitle='Pixel', xtype='log', ytype='linear', yint=True, events=None)