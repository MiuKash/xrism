#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import pickle
from scipy import interpolate
from astropy.io import fits

import seaborn as sns
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.0)
palette = sns.color_palette(n_colors=24)


########################################################
# User Setting
########################################################
# Datadir
datadir='/home/astro/Work/XARM/SXS2/20190613/dat/'
from gv_Be_cal_plot_f04 import grid_x, grid_y, grid_dx, grid_dy, size_Grid, Grid_nx, Grid_ny, radius

z_thres_low=0.2
z_thres_high=0.8

########################################################
# Functions
########################################################
#-------------------------------------------------------    
def read_data(outfile_stem):

    # Read data.
    with open('%s_trans.pkl' % outfile_stem, mode='rb') as _f:
        trans_data = pickle.load(_f)

    # Observed trans (0.0 if nan)
    z = np.array([[trans_data[(trans_data['x']>=x-grid_dx) & (trans_data['x']<=x+grid_dx) & (trans_data['y']>=y-grid_dy) & (trans_data['y']<=y+grid_dy)]['Trans'].mean() for x in grid_x] for y in grid_y])
    z[np.isnan(z)] = 0.0    
    
    # Observed data.
    grid_x_new = grid_x
    grid_y_new = grid_y - grid_y[0]
    f_interp = interpolate.interp2d(grid_x_new, grid_y_new, z, kind='linear')

    # Interpolation
    Grid_x = np.linspace(0, Grid_nx-1, Grid_nx)*size_Grid
    Grid_y = np.linspace(0, Grid_ny-1, Grid_ny)*size_Grid
    Z = f_interp(Grid_x, Grid_y)

    # DataFrame for further processing
    X, Y = np.meshgrid(Grid_x, Grid_y)
    pdX=pd.DataFrame(X.flatten(), columns=['X'])
    pdY=pd.DataFrame(Y.flatten(), columns=['Y'])
    pdZ=pd.DataFrame(Z.flatten(), columns=['Z'])
    #pdX = -pdX
    pdXYZ=pd.concat([pdX,pdY,pdZ], axis=1) 
    
    # Centering
    def centering(pdXYZ, x_thres=-100.0):
        x_shift= pdXYZ[(pdXYZ.Z>z_thres_low) & (pdXYZ.Z<z_thres_high) & (pdXYZ.X>x_thres)].X.mean()    
        y_shift= pdXYZ[(pdXYZ.Z>z_thres_low) & (pdXYZ.Z<z_thres_high) & (pdXYZ.X>x_thres)].Y.mean()
        #x=shift = int(x_shift)
        #y=shift = int(y_shift)
        pdXYZ['X'] =  (pdXYZ['X'] - x_shift) + radius
        pdXYZ['Y'] =  (pdXYZ['Y'] - y_shift) + radius
        print("Center position shifted by (%.2f, %.2f)" % (x_shift-radius, y_shift-radius))
        return pdXYZ
    
    pdXYZ = centering(pdXYZ, x_thres=8.0)    
    
    # Remove data outside of the DUT    
    pdXYZ['Z']=pdXYZ.apply(lambda x : 0.0 if ( (x['X']-radius)**2.0 + (x['Y']-radius)**2.0 > radius**2.0) else x['Z'], axis=1)
    
    # RoI (square) cut
    pdXYZ_RoI= pdXYZ[(np.abs(pdXYZ.X-radius)< radius) & (np.abs(pdXYZ.Y-radius)< radius)].dropna().copy()
    
    # Centering in RoI
    pdXYZ_RoI = centering(pdXYZ_RoI)

    return pdXYZ_RoI

#-------------------------------------------------------    
def make_plot(outfile_stem):

    pdXYZ_RoI = read_data(outfile_stem)
    X_RoI = np.unique(pdXYZ_RoI['X'].values)
    Y_RoI = np.unique(pdXYZ_RoI['Y'].values)
    
    # Reflect in X.
    Z_RoI=[]    
    for x in range(len(X_RoI)):
        for y in range(len(Y_RoI)):
            index = len(X_RoI)*x + (len(Y_RoI)-y-1) 
            Z_RoI_new = np.array(pdXYZ_RoI['Z'].values)[index]
            Z_RoI.append(Z_RoI_new)
    
    Z_RoI = np.array(Z_RoI).reshape([len(Y_RoI),len(X_RoI)])
    #Z_RoI = np.array(pdXYZ_RoI['Z'].values).reshape([len(Y_RoI),len(X_RoI)])
    trans_median = np.median(Z_RoI[Z_RoI>z_thres_low])
    print("Median of transmission is %.3f." % trans_median)
    
    # Plot
    fig = plt.figure(figsize=(8.27,8.27))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)
    plt.gca().set_aspect('equal', adjustable='box')
       
    ax1 = plt.subplot(1,1,1)
    ax1.set_xlabel('X (mm)')
    ax1.set_ylabel('Y (mm)')
    ax1.set_yscale('linear')    
    plt.pcolormesh(X_RoI, Y_RoI, Z_RoI, vmin=0.95*trans_median, vmax=1.05*trans_median)
    plt.colorbar(ax=ax1)
    
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile="%s_MAP.pdf" % (outfile_stem)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')
    
    # Save data
    outfile="%s_MAP.csv" % (outfile_stem)
    pdXYZ_RoI.reset_index().to_csv("%s" % (outfile))
    print('The data are saved in %s.' % outfile)
    
    # Save fits
    outfile="%s_MAP.fits" % (outfile_stem)
    hdu = fits.PrimaryHDU(Z_RoI)
    #hdu = set_header(hdu, 1, 1)
    hdu.writeto(outfile, clobber=True)
    print('The image is saved in %s with %.2f mm/pix.' % (outfile, size_Grid))

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--output',
        help='Output file name origin.',
        dest='outfile',
        type=str,
        default='Scan',
        nargs='?'
        )
    args = parser.parse_args()

    # Check arguments.
    outfile = args.outfile
    
    # Plot
    # dataset, datadir, output file name
    make_plot("Scan_3000")
    make_plot("Scan_4000")
    make_plot("Scan_3456")
    make_plot("Scan_2700")
   