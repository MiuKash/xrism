#!/usr/bin/env python3

############################################################################
# [Function]
# Utility functions for Resolve QL.
############################################################################

########################################################
# Imports
########################################################
import os   
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), os.environ['HOME']))

from os.path import exists
from sys import exit
import datetime
import gzip
import itertools   
#
import pandas as pd 
import numpy as np 
import math
import pickle   
from pandas.io.pytables import read_hdf 
from scipy import signal 
from astropy.time import Time
import seaborn as sns
from myutils import palette


########################################################
# User-defined parameters
########################################################
# Time
year0_default=2014

# Constants.
abs_zero=273.15
lambda_point_he4=2.1768

# Conversions
mtq_B2ratio=8.9999996306552305745721
#
scale_x=6250.0
scale_y_base=5.865e-11
scale_y_base_igor=scale_y_base*np.sqrt(8/3) # Correction for Hanning window.
#
A_nslim=23.0
B_nslim=5.0
#
sampling_rate = 12500 # Hz
noise_timeout=660
ac_pedestal=[-6616, -6616, -6615, -6615]
ac_thres=[25, 25, 25, 25]
#
lines_neutral={
    'C I Ka' : 277,
    'N I Ka' : 392.4,
    'O I Ka' : 524.9,
    'F I Ka' : 676.8,
    'Mg I Ka' : 1253.6,
    'Al I Ka1' : 1486.7,
    'Si I Ka' : 1740.0,
    'Mo I La' : 2015.7,
    'Cl I Ka' : 2622.4,
    'Pd I La1' : 2838.6,
    'Ag I La' : 2984.3,
    'K I Ka1' : 3313.8,
    'Ca I Ka1' : 3691.7,
    'Sc I Ka1' : 4090.6,
    'Ti I Ka1' : 4510.8,
    'V I Ka1' : 4952.2,
    'Cr I Ka1' : 5414.7,
    'Mn I Ka1' : 5898.8,
    'Mn I Kb1,3' : 6490.4,
    'Fe I Ka1': 6403.8,
    'Fe I Ka2' : 6390.8,
    'Fe I Kb1,3': 7058.0,
    'Co I Ka1': 6930.3,
    'Ni I Ka1' : 7478.2,
    'Cu I Ka1' : 8047.8,
    'Zn I Ka1' : 8638.9,
    'Ni I Ka1' : 7478.2,
    'Au La1' : 9713.3,
    'Au Lb1' : 11442.3,
}
#
cards=['A0', 'A1', 'B0', 'B1']
cards_alt=['A1', 'A0', 'B1', 'B0']
grades=['HP', 'MP', 'MS', 'LP', 'LS']
px_grades = grades
ac_grades = ['AC', 'BL', '', 'EP']


########################################################
# Functions
########################################################
# get freq from sampling rate and record length
def get_freqs(samplingrate, recordlength):

    f_min = 1./recordlength
    f_num = int(samplingrate/2/f_min)
    
    return np.linspace(1, f_num+1, f_num) * f_min            

#------------------------------------------------------------
def stime_to_datetime(s_time, year0=year0_default):
    'Convert S_TIME to datetime64 array.'
    
    utc0 = np.datetime64('{:4d}-01-01T00:00:00'.format(year0))
    utc1 = utc0 + np.timedelta64(int(s_time), 's') + np.timedelta64(int((s_time-int(s_time))*1e6), 'us')
    
    t0 = Time(utc0, scale='utc')
    t1 = Time(utc1, scale='utc')
    
    leap_sec0=int((t0.tai.value-t0.utc.value)/1e9)
    leap_sec1=int((t1.tai.value-t1.utc.value)/1e9)

    leap_sec=leap_sec1-leap_sec0

    utc1 = utc1 + np.timedelta64(-round(leap_sec),'s')    
    return utc1

#------------------------------------------------------------
def time_to_utc(time, year0=year0_default):
    'Get UTC from TIME corrected for the leap seconds.'

    utc0 = datetime.datetime(year0,1,1)
    utc1 = utc0 + datetime.timedelta(seconds=time)

    t0 = Time(utc0, scale='utc')
    t1 = Time(utc1, scale='utc')
    
    leap_sec0=(t0.tai.value-t0.utc.value).total_seconds()
    leap_sec1=(t1.tai.value-t1.utc.value).total_seconds()

    leap_sec=leap_sec1-leap_sec0

    utc1 = utc1 + datetime.timedelta(seconds=-round(leap_sec))

    return utc1

#------------------------------------------------------------
def utc_to_time(utc, year0=year0_default):
    'Get ahtime from TIME corrected for the leap seconds.'

    utc0 = np.datetime64('{:4d}-01-01T00:00:00'.format(year0))
    utc1 = utc

    time = (utc - utc0).total_seconds()

    t0 = Time(utc0, scale='utc')
    t1 = Time(utc1, scale='utc')
    
    leap_sec0=(t0.tai.value-t0.utc.value).total_seconds()
    leap_sec1=(t1.tai.value-t1.utc.value).total_seconds()

    leap_sec=leap_sec1-leap_sec0

    time = time + round(leap_sec)

    return time

#------------------------------------------------------------
def index_stime(data, year0=year0_default):
    'Change S_TIME or TIME to datetime64 and use it as DataFrame index.'
    
    if (len(data)>0):
        if ('S_TIME' in data.columns):
            time='S_TIME'
            data[time] = data[time].apply(stime_to_datetime, year0=year0)
        elif ('TIME' in data.columns):
            time='TIME'
            # Somehow, TIME appears twice.
            data_tmp = data.loc[:,~data.columns.duplicated()]
            data = data_tmp
            # Remove not a time
            data = data[data.TIME>0]
            # Convert to datetime
            data[time] = data[time].apply(stime_to_datetime, year0=year0)
        else:
            print("Neither S_TIME nor TIME included.")
            exit(1)
        data.set_index(time,inplace=True)
        data.sort_index(inplace=True)    
        return data
    else:
        return None

#------------------------------------------------------------
def get_datetime_from_args(args_datetime):        

    if ('/' in args_datetime[0]):
        ydm=args_datetime[0].split('/')
    elif ('-' in args_datetime[0]):
        ydm=args_datetime[0].split('-')
    hms=args_datetime[1].split(':')
    try:
        dt = np.datetime64('%s-%s-%sT%s:%s:%s' % (ydm[0], ydm[1], ydm[2], hms[0], hms[1], hms[2]))
    except:
        print("%s has a wrong format" % args_datetime)
        exit(1)

    return dt

#-------------------------------------------------------
def filter_by_datetime(infiles, kind, datetime_start, datetime_end, base=0):

    infiles_new=[]    
    
    if kind in ['range']:        
        for infile in infiles:    
            infile_name = os.path.split(infile)[1] 
            file_start = infile_name.replace('.','_').split('_')[base+1]
            file_end =   infile_name.replace('.','_').split('_')[base+2]
            file_start = datetime.datetime.strptime(file_start, '%Y%m%d-%H%M%S')
            file_end = datetime.datetime.strptime(file_end, '%Y%m%d-%H%M%S')            
            if (datetime_end < file_start) or (datetime_start > file_end):
                pass
            else:
                print("%s is in the time range of interest." % infile_name)
                infiles_new.append(infile)
    elif kind in ['epoch']:
        for infile in infiles:    
            infile_name = os.path.split(infile)[1] 
            file_start = infile_name.replace('.','_').split('_')[base]
            file_start = datetime.datetime.strptime(file_start, '%Y%m%d-%H%M%S')
            
            if (datetime_end < file_start) or (datetime_start > file_start):
                pass
            else:
                print("%s is in the time range of interest." % infile_name)
                infiles_new.append(infile)
                    
    infiles_new.sort()
    return infiles_new

#------------------------------------------------------------
def check_out_of_time(datetime_data, datetime_start, datetime_end):
    'Check a given time is before the start (-1), between the start and end (0), and after the end (+1)'

    if ( datetime_data is not None):
        if ( datetime_data < datetime_start ):
            return -1
        elif ( datetime_data > datetime_end ):
            return 1
        else:
            return 0
    else:
        return None

#------------------------------------------------------------
def save_files(data, outdir, outfile, kind='npy'):
    'Save output files.'

    # Return if no data.
    if (data is None) or (len(data)==0):
        "Data has no record. No file written."
        return 1

    # Output dir if given separately from outfile.
    if (outdir is not None):
        if not exists(outdir):
            os.makedirs(outdir)
            print("The output directory is generated at %s." % outdir)
        outfile = outdir + outfile
    
    # Save files.
    if (kind == 'npy') or (kind == 'npz'):
        try:
            np.savez_compressed(outfile, data)
            print('Data saved in %s' % outfile)
        except:
            print('Failed to save %s' % outfile)            
    elif (kind == 'df') or (kind == 'pkl'):
        try:
            data.to_pickle(outfile)
            print('Data saved in %s' % outfile)
        except:
            print('Failed to save %s' % outfile)   
    elif (kind == 'stat'):
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.width', None)
        pd.set_option('display.max_colwidth', None)
        f = open(outfile, 'w')
        if (len(data.columns) > 0):
            print(data.describe().T, file=f)
            print('Stat saved in %s' % outfile)
        f.close()
    elif (kind == 'csv'):
        outfile=outfile+'.gz'
        try:
            data.to_csv(outfile, compression='gzip')
            print('Data saved in %s' % outfile)
        except:
            print('Failed to save %s' % outfile)   
    
    return 0

#------------------------------------------------------------
def get_pHe4(temp):
    "Get vapour pressure of He4 at a given T."
    #http://nuclear.unh.edu/~ryan/Doc/He4Vapor.py

    I = 4.6202
    A = 6.399
    B = 2.541
    C = 0.00612
    D = 0.5197
    alpha = 7.00
    beta  = 14.14

    log_p = I - (A/temp)+B*math.log(temp)+(C/2.0)*math.pow(temp,2) 
    log_p = log_p - D*((alpha*beta)/(math.pow(beta,2)+1) - math.pow(temp,-1))*np.arctan(alpha*temp-beta) 
    log_p = log_p - (alpha*D)/(2*(math.pow(beta,2)+1))*math.log(math.pow(temp,2)/(1+(alpha*temp-beta)*(alpha*temp-beta)))
    return 10**log_p * 133.322

#------------------------------------------------------------
def filter_50mK_he_mode(data_all):
    "Filter 50 mK in the He mode."

    return data_all[np.abs(data_all.T_CTS_CT_A-0.049997)<0.000193]

#------------------------------------------------------------
def filter_50mK_cf_mode(data_all):
    "Filter 50 mK in the cryogen-free mode."

    return data_all[np.abs(data_all.T_CTS_CT_A-0.050576)<0.000386]    

#------------------------------------------------------------
def filter_50mK(data_all):
    "Filter 50 mK in the He or cryogen-free mode."

    return data_all[(np.abs(data_all.T_CTS_CT_A-0.049997)<0.000193) | (np.abs(data_all.T_CTS_CT_A-0.050576)<0.000386)]

#------------------------------------------------------------
def detrend_data(data_all):
    "Detrend the data."

#    data_all=data_all.dropna(how='any', axis=1)
    for _hk in data_all.columns:
        try:
            data_all[_hk]=signal.detrend(data_all[_hk])
        except:
            data_all[_hk]=data_all[_hk]

    return data_all

#------------------------------------------------------------
def deriv_data(data_all):
    "Calc time-derivative of data"

#    data_all=data_all.dropna(how='any', axis=1)
    for _hk in data_all.columns:
        try:
            data_all[_hk]=np.gradient(data_all[_hk])
        except:
            data_all[_hk]=data_all[_hk]

    return data_all

#------------------------------------------------------------
def get_psp_card(pixel):
    "Get PSP Spacecard name for a given pixel."

    if (pixel >= 0) and (pixel < 9):
        return 'A0'
    elif (pixel >= 9) and (pixel < 18):
        return 'A1'
    elif (pixel >= 18) and (pixel < 27):
        return 'B0'
    elif (pixel >= 27) and (pixel < 36):
        return 'B1'

##------------------------------------------------------------
def get_psp_card_alt(pixel):
    "Get PSP Spacecard name for a given pixel in alternative config."

    if (pixel >= 0) and (pixel < 9):
        return 'A1'
    elif (pixel >= 9) and (pixel < 18):
        return 'A0'
    elif (pixel >= 18) and (pixel < 27):
        return 'B1'
    elif (pixel >= 27) and (pixel < 36):
        return 'B0'

#------------------------------------------------------------
# Change channel number from side to full.
def ch_side2full(psp_id,ipix):

    if ( psp_id == 0):# A0
        if ( ipix < 18 ):
            return ipix
        elif ( ipix == 18): # For anti-co pixel
            return 36
        else:
            return 99
    elif ( psp_id == 1 ):# A1
        if ( ipix < 18 ):
            return ipix
        elif ( ipix == 18): # For anti-co pixel
            return 36
        else:
            return 99
    elif ( psp_id == 2 ):# B0
        if ( ipix < 18 ) :
            return ipix + 18
        elif ( ipix == 18): # For anti-co pixel
            return 37
        else:
            return 99
    elif ( psp_id == 3 ):# B1
        if ( ipix < 18 ):
            return ipix + 18
        elif ( ipix == 18): # For anti-co pixel
            return 37
    else:
        return 99

##------------------------------------------------------------
def get_mtq_metric(mtq_x,mtq_y,mtq_z, mtq_max=900.0):
    "Get MTQ noise metric."

    mtq_min = -1.0 * mtq_max

    # Normalize.
    mtq_x /= mtq_max
    mtq_y /= mtq_max
    mtq_z /= mtq_max

    # Calc metric.
    def abs_and_triangle(mtq):
        if np.abs(mtq) > 0.5:
            return 1.0-np.abs(mtq)
        else:
            return np.abs(mtq)

#    metric = abs_and_triangle(mtq_x)/2 - abs_and_triangle(mtq_y) + abs_and_triangle(mtq_z)
#    metric_min =
#    metric_max =          

    metric = np.abs(mtq_x/2 - mtq_y + mtq_z)
    metric_min = 0
    metric_max = 2.5

    # Rescale to match the max.
    metric_scaled = (mtq_max - mtq_min) / (metric_max - metric_min) * (metric - metric_min) + mtq_min

    return metric_scaled

#------------------------------------------------------------
def get_ac_cycle(date):
    "Return commercial AC power cycle."
    
    if date < datetime.datetime(2021,6,30): # Niihama
        return 60.0 
    elif date < datetime.datetime(2023,4,1): # Tsukuba
        return 50.0
    elif date < datetime.datetime(2023,7,1): # Tanegashima
        return 60.0
    else: # In space,
        return 0.0

#------------------------------------------------------------
def get_color(i):
    "Return color code generated from a given integer."
    
    """
    prim1=17
    prim2=19
    prim3=89
    
    return '#%02X%02X%02X' % (prim1*i%256,prim2*i%256,prim3*i%256)
"""
    return palette[i%9]

#------------------------------------------------------------
def get_linestyle(i):
    "Return line code generated from a given integer."

    if ( i/9%4 == 0):
        return '-'
    elif ( i/9%4 == 1):
        return '--'
    elif ( i/9%4 == 2):
        return '-.'
    elif ( i/9%4 == 3):
        return ':'

#------------------------------------------------------------
def get_marker(i):
    "Return marker code generated from a given integer."

    if ( i/9%4 == 0):
        return 'o'
    elif ( i/9%4 == 1):
        return 'D'
    elif ( i/9%4 == 2):
        return '^'
    elif ( i/9%4 == 3):
        return 'v'

#------------------------------------------------------------
def set_legend(plt,ax,ncol=9,loc=2,size=10):
    "Set legend."

    def flip(items, ncol):
        return itertools.chain(*[items[i::ncol] for i in range(ncol)])

    handles, labels = ax.get_legend_handles_labels()
    legend=ax.legend(flip(handles, ncol), flip(labels, ncol), 
                loc=loc, ncol=ncol, numpoints=1, scatterpoints=1, handlelength=2, handletextpad=1, 
                frameon=1, shadow=0, columnspacing=0, prop={'size' : size})
    legend.get_frame().set_alpha(0.1)
    legend.get_frame().set_facecolor('grey')
    legend.get_frame().set_edgecolor('white')



