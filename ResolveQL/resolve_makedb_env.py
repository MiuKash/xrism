#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for SITE env monitor.
# 0 : temperature and humidity
# 1-3 : particle counters
# 4-5 : T-vac chamber pressure and shroud temp
############################################################################

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import codecs
from datetime import datetime, timedelta
from resolve_makedb_fff import save_files_fff_range

########################################################
# User-defined parameters
########################################################
env0_columns=['Date', 'Time', 'T_Kumi1', 'H_Kumi1', 'T_Kumi2', 'H_kumi2']
#
env1_columns = ['Date', 'Time', '0.3um', '0.5um', '1.0um', '2.0um', '5.0um', '10.0um']
env2_columns = env1_columns
env3_columns = ['Timestamp', '0.5um', '1.0um', '5.0um', '10.0um', 'Sample Time', 'Sample Volume', 'CALR']
#
#時間 (JST),代表真空度,真空計1,真空計2,真空計3,真空計4,真空計5,真空計6,主胴部ｼｭﾗｳﾄﾞ 4B 中部温度,主胴部ｼｭﾗｳﾄﾞ 4C 中部温度,副胴部ｼｭﾗｳﾄﾞ 2F 中部温度,主胴部ｼｭﾗｳﾄﾞ 4D 中部温度,主胴部ｼｭﾗｳﾄﾞ 4E 中部温度,供試体ｼｭﾗｳﾄﾞ A　入口温度,供試体ｼｭﾗｳﾄﾞ B　入口温度,副胴部ｼｭﾗｳﾄﾞ 1A 入口温度,副胴部ｼｭﾗｳﾄﾞ 1B 入口温度,副胴部ｼｭﾗｳﾄﾞ 1A1B 温度調節,副胴部ｼｭﾗｳﾄﾞ 1C 入口温度,副胴部ｼｭﾗｳﾄﾞ 1D 入口温度,副胴部ｼｭﾗｳﾄﾞ 1E 入口温度,副胴部ｼｭﾗｳﾄﾞ 1F 入口温度,副胴部ｼｭﾗｳﾄﾞ 1G 入口温度,副胴部ｼｭﾗｳﾄﾞ 1H 入口温度,副胴部ｼｭﾗｳﾄﾞ 2A 入口温度,副胴部ｼｭﾗｳﾄﾞ 2B 入口温度,副胴部ｼｭﾗｳﾄﾞ 2C 入口温度,副胴部ｼｭﾗｳﾄﾞ 2D 入口温度,副胴部ｼｭﾗｳﾄﾞ 2E 中部温度,主CPｼｭﾗｳﾄﾞ A 中部温度,主CPｼｭﾗｳﾄﾞ B 中部温度
env4_columns = ['Timestamp']
env4_columns.extend(["p{:d}".format(i) for i in range(7)])
#env4_columns.extend(["T{:d}".format(i) for i in range(23)])
env5_columns = ['Date', 'Time', 'pLP', 'pBP']


########################################################
# Functions
########################################################
# S_TIME
def dt2stime(x, hour_shift=0):
    x=x.replace('/','-')
    dt = datetime.strptime(x, "%Y-%m-%d %H:%M:%S") + timedelta(hours=hour_shift)
    return dt

#------------------------------------------------------------
def index_time0(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    """
    dates=[]
    for item,row in data.iterrows():
        if (row['Date'] is not pd.NaT):
            date = row['Date']
            dates.append(date)
    
    data['Date'] = dates
"""
    data.dropna(inplace=True)
    data['S_TIME'] = data[['Date','Time']].apply(lambda x: "{} {}".format(x[0].strftime("%Y/%m/%d"),x[1]), axis=1)
    data['S_TIME'] = data['S_TIME'].apply(lambda x:dt2stime(x))
    del data['Date']
    del data['Time']

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
def index_time1(data):
    'Change TIME to datetime64 and use it as DataFrame index.'
    
    data.dropna(inplace=True)
    data['S_TIME'] = data[['Date','Time']].apply(lambda x: "{} {}".format(x[0],x[1]), axis=1)
    data['S_TIME'] = data['S_TIME'].apply(lambda x:dt2stime(x))
    del data['Date']
    del data['Time']

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
index_time2 = index_time1

#------------------------------------------------------------
def index_time3(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    data['S_TIME'] = data['Timestamp'].apply(lambda x: datetime.strptime(x, "%Y/%m/%d %H:%M:%S"))
    del data['Timestamp']

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
def index_time4(data):
    'Change TIME to datetime64 and use it as DataFrame index.'

    data.dropna(inplace=True)
    data['S_TIME'] = data['Timestamp'].apply(lambda x: "{}".format(x))
    data['S_TIME'] = data['S_TIME'].apply(lambda x:dt2stime(x, hour_shift=-9))
    del data['Timestamp']

    # Index S_TIME
    data.set_index('S_TIME',inplace=True)
    data.sort_index(inplace=True)
    data = data[~data.index.duplicated(keep='last')]    
    
    return data

#------------------------------------------------------------
index_time5 = index_time1

#------------------------------------------------------------
def makedb0(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_excel(infile, engine='openpyxl', sheet_name=0, skiprows=2, usecols=[0,1,2,3,4,5])
    data_all.columns=columns
    
    # Index time.
    data_all = index_time0(data_all)

    # Remove no data.
    data_all.replace('******',np.nan, inplace=True)
    data_all.dropna(inplace=True)

    # Save files.
    outdir_new = outdir + '/env/'
    save_files_fff_range(data_all, outdir_new, 'env', pkl=True, csv=False, stat=True)
    
    return 0

#------------------------------------------------------------
def makedb1(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_csv(infile, skiprows=7, sep='\t', usecols=[0,1,7,8,9,10,11,12])
    data_all.columns=columns

    # Convert unit volume.
    ft3_m3 = 0.028316846592 
    for i in columns[2:8]:
        data_all[i] = data_all[i] / ft3_m3

    # Index time.
    data_all = index_time1(data_all)

    # Save files.
    outdir_new = outdir + '/pc1/'
    save_files_fff_range(data_all, outdir_new, 'pc1', pkl=True, csv=False, stat=True)
    
    return 0

#------------------------------------------------------------
def makedb2(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_csv(infile, skiprows=7, sep='\t', usecols=[0,1,7,8,9,10,11,12])
    data_all.columns=columns

    # Convert unit volume.
    ft3_m3 = 0.028316846592 
    for i in columns[2:8]:
        data_all[i] = data_all[i] / ft3_m3

    # Index time.
    data_all = index_time2(data_all)

    # Save files.
    outdir_new = outdir + '/pc2/'
    save_files_fff_range(data_all, outdir_new, 'pc2', pkl=True, csv=False, stat=True)
    
    return 0

#------------------------------------------------------------
def makedb3(infile, outdir, columns):

    print("Reading %s" % infile)
    data_all = pd.read_excel(infile, sheet_name=0, skiprows=6, skipfooter=4)
    data_all.columns=columns

    # Convert unit volume.
    ft3_m3 = 0.028316846592 
    for i in columns[1:5]:
        data_all[i] = data_all[i] / ft3_m3

    # Index time.
    data_all = index_time3(data_all)

    # Save files.
    outdir_new = outdir + '/pc3/'
    save_files_fff_range(data_all, outdir_new, 'pc3', pkl=True, csv=False, stat=True)
    
    return 0

#------------------------------------------------------------
def makedb4(infile, outdir, columns):

    print("Reading %s" % infile)
    with codecs.open(infile, "r", "Shift-JIS", "ignore") as _infile:        
        data_all = pd.read_csv(_infile, skiprows=9, skipfooter=0,usecols=[0,98,99,100,101,102,103,104])
        data_all.columns=columns

    # Index time.
    data_all = index_time4(data_all)

    # Save files.
    outdir_new = outdir + '/tvac1/'
    save_files_fff_range(data_all, outdir_new, 'tvac1', pkl=True, csv=False, stat=True)
    
    return 0

#------------------------------------------------------------
def makedb5(infile, outdir, columns):

    print("Reading %s" % infile)
    with codecs.open(infile, "r", "Shift-JIS", "ignore") as _infile:
        data_all = pd.read_csv(_infile, skiprows=17, skipfooter=0,usecols=[1,2,4,5])
        data_all.columns=columns

    # Index time.
    data_all = index_time5(data_all)

    # Convert into engineering values.
    for i in data_all.columns:
        data_all[i] = 10.0**(data_all[i]-8.0)

    # Save files.
    outdir_new = outdir + '/tvac2/'
    save_files_fff_range(data_all, outdir_new, 'tvac2', pkl=True, csv=False, stat=True)
    
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-f', '--format', 
        help='Format of the data.',
        dest='format',
        type=int,
        nargs=1
        )
    args = parser.parse_args()

    makedb = eval('makedb{:d}'.format(args.format[0]))
    columns = eval('env{:d}_columns'.format(args.format[0]))
    makedb(args.infile[0], args.outdir[0], columns)

