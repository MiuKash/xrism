#!/usr/bin/env python3

############################################################################
# [Function]
# Dump noise record from FFF files.
#
# [History]
# 2015/06/19    M. Tsujimoto    Branched from make_sxs_nspec_db.py
# 2015/07/09    M. Tsujimoto    Debug.
# 2015/07/27    M. Tsujimoto    -f datefile mandatory.
# 2015/07/29    M. Tsujimoto    -t option deleted.
# 2015/07/31    M. Tsujimoto    Saved file content change.
# 2015/08/25    M. Tsujimoto    S_TIME calculation by np.array, not pd.DataFrame.a
# 2019/12/29    M. Tsujimoto    Changed for Resolve
#
############################################################################


########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
from resolve_utils import save_files
from resolve_makedb_nc import yield_nspec


########################################################
# User-defined parameters
########################################################
hdu_nr='EVENTS'
hks_nr=['S_TIME', 'PIXEL', 'NOISEREC_MODE', 'NOISEREC']
hdu_pr='EVENTS'
hks_pr=['S_TIME', 'PIXEL', 'LO_RES_PH', 'PULSEREC']


########################################################
# Functions
########################################################
#------------------------------------------------------------
def makedb(infile, outdir):
    
    nm_data_all = None
    
    for _i in yield_nspec(infile, hdu_nr, hks_nr, 1024):
        stime, data =_i[0], _i[1]
        
        # Save nrec.
        outdir_new = outdir + '/nr/'
        outfile = 'nr_' + stime.strftime("%Y%m%d-%H%M%S") + '.npz'
        save_files(data, outdir_new, outfile)
    
         
    # Save nm
    columns=['S_TIME']
    for _p in range(36):
        columns.extend(['ADUMAX%02d' % _p, 'ADUMIN%02d' % _p, 'ADUMEAN%02d' % _p,'ADUMED%02d' % _p,'ADUSTD%02d' % _p])
    nm_data_all.columns = columns
    
    # Index S_TIME
    nm_data_all.set_index('S_TIME',inplace=True)
    nm_data_all.sort_index(inplace=True)
    nm_data_all = nm_data_all[~nm_data_all.index.duplicated(keep='last')]    
    
    outdir_new = outdir + '/nm/'
    outfile = 'nm_' + stime.strftime("%Y%m%d-%H%M%S_") + stime.strftime("%Y%m%d-%H%M%S") + '.pkl'
    save_files(nm_data_all, outdir_new, outfile, kind='pkl')
                
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0])
