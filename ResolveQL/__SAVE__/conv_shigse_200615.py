#!/usr/bin/env python

# -*- coding: utf-8 -*-
__author__ = 'hoshino'

ver = '2020-06-12'

import sys
from sys import exit

#import time
import glob
import numpy as np
import pandas as pd

class Calfunc:
    def __init__(self, argvs):
        """
        :type self: object
        :param argvs:
        """
        if "-version" in argvs:
            print('%s version %s by %s' % (argvs[0], ver, __author__))
            quit()
        elif "-help" in argvs:
            print('%s version %s by %s' % (argvs[0], ver, __author__))
            print('#./conv_shigse.py [filename]')
            quit()
        else:
            #self.filelist1 = '../XRISM-20200329081932.dld.0000.xls'
            #try:            
            self.filelist1 = './%s.xlsx' %  argvs[1]
            #    #self.filelist1 = '%s.xls' %  argvs[1]                
            #except:
            #    self.filelist1 = '../%s.xlsx' %  argvs[1]
            #    #self.filelist1 = '%s.xlsx' %  argvs[1]                                
            self.filename1 = []
            self.time = []
            self.of_data = pd.DataFrame()
            self.of_data_temp = pd.DataFrame()                        
            self.data = pd.DataFrame()
    
    def read_excel(self):
        """
        :type self: object
        """
        print("START")
        files1 = glob.glob(self.filelist1)
        for self.filename1 in files1:
            print(files1)
            #skiprows = 37            
            data1 = pd.read_excel(str(self.filename1),
                            sep=',',
                            index_col = False,
                            parse_dates = [0],
                            skiprows = 35          
            )
            self.of_data = pd.concat([self.of_data, data1])                        
            print(self.filename1)
            print(self.of_data)                        
            data1 = []

    def chebychev(self, res, A):
        z = np.log10(res) 
        ZL = np.log10(A[0])
        ZU = np.log10(A[1])
        n = len(A) - 2
        X = ((z - ZL) - (ZU - z)) / (ZU - ZL)
        T = 0.0
        for i in range(0,n):
                T += A[i+2] * np.cos( i * np.arccos(X) )
        return T
    
    def conversion(self):
        #for i in range( 3, np.alen(self.)65498):
        #print(self.of_data['CH0015'] == '+OVER')                
        #self.of_data['CH0015'].replace('+OVER', 'NaN')

        for i in range( 3, np.alen(self.of_data)):
                print("%d/%d" % (i,np.alen(self.of_data)))
                #Hetank4
                try:
                    #resi_Hetank4 = np.float(self.of_data.loc[i:i, 'Tag 0001'])*100.0
                    resi_Hetank4 = np.float(self.of_data.loc[i:i, 'Tag 0001'].apply(lambda x: 100.0*x))
                except:
                    resi_Hetank4 = -9999
                #JTS5
                try:
                    #resi_JTS5 = np.float(self.of_data.loc[i:i, 'Tag 0002'])*100.0*1e3
                    resi_JTS5 = np.float(self.of_data.loc[i:i, 'Tag 0002'].apply(lambda x: 100.0*1e3*x))                    
                except:
                    resi_JTS5 = -9999
                #IVCS5
                try:
                    #resi_IVCS5 = np.float(self.of_data.loc[i:i, 'Tag 0003'])*100.0
                    resi_IVCS5 = np.float(self.of_data.loc[i:i, 'Tag 0003'].apply(lambda x: 100.0*x))
                except:
                    resi_IVCS5 = -9999
                #MVCS3
                try:
                    #resi_MVCS3 = np.float(self.of_data.loc[i:i, 'Tag 0004'])/0.01
                    resi_MVCS3 = np.float(self.of_data.loc[i:i, 'Tag 0004'].apply(lambda x: x/0.01))
                except:
                    resi_MVCS3 = -9999
                #OVCS3
                try:
                    #resi_OVCS3 = np.float(self.of_data.loc[i:i, 'Tag 0005'])/0.01
                    resi_OVCS3 = np.float(self.of_data.loc[i:i, 'Tag 0005'].apply(lambda x: x/0.01))
                except:
                    resi_OVCS3 = -9999
                #MS3
                try:
                    #resi_MS3 = np.float(self.of_data.loc[i:i, 'Tag 0006'])/0.01
                    resi_MS3 = np.float(self.of_data.loc[i:i, 'Tag 0006'].apply(lambda x: x/0.01))
                except:
                    resi_MS3 = -9999
                #4K Stage
                try:
                    #resi_4KS = np.float(self.of_data.loc[i:i, 'Tag 0007'])*100.0*1e3
                    resi_4KS = np.float(self.of_data.loc[i:i, 'Tag 0007'].apply(lambda x: 100.0*1e3*x))
                except:
                    resi_4KS = -9999
                #Orifice_In
                try:
                    #resi_ORFIN = np.float(self.of_data.loc[i:i, 'Tag 0008'])*100.0*1e3
                    resi_ORFIN = np.float(self.of_data.loc[i:i, 'Tag 0008'].apply(lambda x: 100.0*1e3*x))
                except:
                    resi_ORFIN = -9999
                #Bypass_In
                try:
                    #resi_BPSIN = np.float(self.of_data.loc[i:i, 'Tag 0009'])*100.0*1e3
                    resi_BPSIN = np.float(self.of_data.loc[i:i, 'Tag 0009'].apply(lambda x: 100.0*1e3*x)) 
                except:
                    resi_BPSIN = -9999
                #HEX1_HP_IN
                try:
                    #resi_HEX1HPIN = np.float(self.of_data.loc[i:i, 'Tag 0010'])*100.0
                    resi_HEX1HPIN = np.float(self.of_data.loc[i:i, 'Tag 0010'].apply(lambda x: 100.0*x))
                except:
                    resi_HEX1HPIN = -9999
                #HEX1_HP_OUT
                try:
                    #resi_HEX1HPOUT = np.float(self.of_data.loc[i:i, 'Tag 0011'])*100.0
                    resi_HEX1HPOUT = np.float(self.of_data.loc[i:i, 'Tag 0011'].apply(lambda x: 100.0*x))
                except:
                    resi_HEX1HPOUT = -9999
                #HEX2_HP_IN
                try:
                    #resi_HEX2HPIN = np.float(self.of_data.loc[i:i, 'Tag 0012'])*100.0
                    resi_HEX2HPIN = np.float(self.of_data.loc[i:i, 'Tag 0012'].apply(lambda x: 100.0*x))
                except:
                    resi_HEX2HPIN = -9999
                #HEX2_HP_OUT
                try:
                    #resi_HEX2HPOUT = np.float(self.of_data.loc[i:i, 'Tag 0013'])*100.0
                    resi_HEX2HPOUT = np.float(self.of_data.loc[i:i, 'Tag 0013'].apply(lambda x: 100.0*x))                    
                except:
                    resi_HEX2HPOUT = -9999
                #HEX3_HP_IN
                try:
                    #resi_HEX3HPIN = np.float(self.of_data.loc[i:i, 'Tag 0014'])*100.0
                    resi_HEX3HPIN = np.float(self.of_data.loc[i:i, 'Tag 0014'].apply(lambda x: 100.0*x))                                        
                except:
                    resi_HEX3HPIN = -9999
                #HEX3_LP_OUT
                try:
                    #resi_HEX3LPOUT = np.float(self.of_data.loc[i:i, 'Tag 0015'])*100.0
                    resi_HEX3LPOUT = np.float(self.of_data.loc[i:i, 'Tag 0015'].apply(lambda x: 100.0*x))
                except:
                    resi_HEX3LPOUT = -9999
                #Tag 16 HEX2_LP_OUT
                try:
                    #resi_HEX2LPOUT = np.float(self.of_data.loc[i:i, 'Tag 0016'])*100.0
                    resi_HEX2LPOUT = np.float(self.of_data.loc[i:i, 'Tag 0016'].apply(lambda x: 100.0*x))                    
                except:
                    resi_HEX2LPOUT = -9999
                #Tag 17 HEX1_LP_OUT
                try:
                    #resi_HEX1LPOUT = np.float(self.of_data.loc[i:i, 'Tag 0017'])*100.0
                    resi_HEX1LPOUT = np.float(self.of_data.loc[i:i, 'Tag 0017'].apply(lambda x: 100.0*x))                                        
                except:
                    resi_HEX1LPOUT = -9999
                #Tag 18 1STAGE     
                try:
                    #resi_1STG = np.float(self.of_data.loc[i:i, 'Tag 0018'])*100.0
                    resi_1STG = np.float(self.of_data.loc[i:i, 'Tag 0018'].apply(lambda x: 100.0*x))
                except:
                    resi_1STG = -9999
                #Tag 19 2STAGE
                try:
                    #resi_2STG = np.float(self.of_data.loc[i:i, 'Tag 0019'])*100.0
                    resi_2STG = np.float(self.of_data.loc[i:i, 'Tag 0019'].apply(lambda x: 100.0*x))                    
                except:
                    resi_2STG = -9999
                #Tag 20 NaN
                #Tag 21 Flow Meter L
                try:
                    #PFML = np.float(self.of_data.loc[i:i, 'Tag 0021'])*100.0
                    PFML = np.float(self.of_data.loc[i:i, 'Tag 0021'].apply(lambda x: 100.0*x))                                        
                except:
                    PFML = -9999
                #Tag 22 Flow Meter S
                try:
                    #PFMS = np.float(self.of_data.loc[i:i, 'Tag 0022'])*0.5
                    PFMS = np.float(self.of_data.loc[i:i, 'Tag 0022'].apply(lambda x: 0.5*x))
                except:
                    PFMS = -9999
                #Tag 23 LD
                try:
                    LD1 = np.float(self.of_data.loc[i:i, 'Tag 0023'])
                except: 
                    LD1 = -9999                     
                #Tag 24 LD
                try:
                    LD2 = np.float(self.of_data.loc[i:i, 'Tag 0024'])
                except: 
                    LD2 = -9999
                #Tag 28 LD
                try:
                    LD21 = np.float(self.of_data.loc[i:i, 'Tag 0028'])
                except: 
                    LD21 = -9999                     
                #Tag 30 LD
                try:
                    LD22 = np.float(self.of_data.loc[i:i, 'Tag 0030'])
                except: 
                    LD22 = -9999                    
                #Tag 25 Hetank Pressure
                try:
                    V_HetankP = np.float(self.of_data.loc[i:i, 'Tag 0025'])
                except:
                    V_HetankP = -9999
                #Tag 26 DWR Pressure 
                try:
                    V_DewarP = np.float(self.of_data.loc[i:i, 'Tag 0026'])
                except:
                    V_DewarP = -9999
                #Tag 31 temp. in tent
                try:
                    V_inTent = np.float(self.of_data.loc[i:i, 'Tag 0031'])
                except:
                    V_inTent = -9999
                #
                try:
                    #resi_T12 = np.float(self.of_data.loc[i:i, 'Tag 0046'])*30.3
                    resi_T12 = np.float(self.of_data.loc[i:i, 'Tag 0046'].apply(lambda x: 30.3*x))                    
                except:
                    resi_T12 = -9999
                #
                try:
                    #resi_PPP = np.float(self.of_data.loc[i:i, 'Tag 0045'])*(-100.0)
                    resi_PPP = np.float(self.of_data.loc[i:i, 'Tag 0045'].apply(lambda x: -100.0*x))                                        
                except:
                    resi_PPP = -9999                        
                #
                try:
                    #resi_PPCuF = np.float(self.of_data.loc[i:i, 'Tag 0050'])*100.0
                    resi_PPCuF = np.float(self.of_data.loc[i:i, 'Tag 0050'].apply(lambda x: 100.0*x))
                except:
                    resi_PPCuF = -9999
                #
                try:
                    #resi_Hetank1 = np.float(self.of_data.loc[i:i, 'Tag 0041'])*100.0
                    resi_Hetank1 = np.float(self.of_data.loc[i:i, 'Tag 0041'].apply(lambda x: 100.0*x))                    
                except:
                    resi_Hetank1 = -9999
                #
                try:
                    #resi_Hetank2 = np.float(self.of_data.loc[i:i, 'Tag 0042'])*100.0
                    resi_Hetank2 = np.float(self.of_data.loc[i:i, 'Tag 0042'].apply(lambda x: 100.0*x))                                        
                except:
                    resi_Hetank2 = -9999
                #
                try:
                    #resi_PP1 = np.float(self.of_data.loc[i:i, 'Tag 0044'])*100.0
                    resi_PP1 = np.float(self.of_data.loc[i:i, 'Tag 0044'].apply(lambda x: 100.0*x))
                except:
                    resi_PP1 = -9999
                #
                try:
                    #resi_PP2 = np.float(self.of_data.loc[i:i, 'Tag 0045'])*100.0
                    resi_PP2 = np.float(self.of_data.loc[i:i, 'Tag 0045'].apply(lambda x: 100.0*x))                    
                except:
                    resi_PP2 = -9999
                #
                try:
                    #resi_JTS1 = np.float(self.of_data.loc[i:i, 'Tag 0046'])*100.0
                    resi_JTS1 = np.float(self.of_data.loc[i:i, 'Tag 0046'].apply(lambda x: 100.0*x))                                        
                except:
                    resi_JTS1 = -9999
                #
                try:
                    #resi_JTS2 = np.float(self.of_data.loc[i:i, 'Tag 0047'])*100.0
                    resi_JTS2 = np.float(self.of_data.loc[i:i, 'Tag 0047'].apply(lambda x: 100.0*x))
                except:
                    resi_JTS2 = -9999
                #
                try:
                    #resi_JTS3 = np.float(self.of_data.loc[i:i, 'Tag 0048'])*100.0
                    resi_JTS3 = np.float(self.of_data.loc[i:i, 'Tag 0048'].apply(lambda x: 100.0*x))                    
                except:
                    resi_JTS3 = -9999
                #
                try:
                    #resi_JTS4 = np.float(self.of_data.loc[i:i, 'Tag 0049'])*100.0
                    resi_JTS4 = np.float(self.of_data.loc[i:i, 'Tag 0049'].apply(lambda x: 100.0*x))                                        
                except:
                    resi_JTS4 = -9999
                #Tag 50 ADR_IF_block
                try:
                    #resi_ADRIFB = np.float(self.of_data.loc[i:i, 'Tag 0050'])*10.0
                    resi_ADRIFB = np.float(self.of_data.loc[i:i, 'Tag 0050'].apply(lambda x: 10.0*x))                                        
                except:
                    resi_ADRIFB = -9999
                #
                try:
                    #resi_PCA2 = np.float(self.of_data.loc[i:i, 'Tag 0005'])*30.3
                    resi_PCA2 = np.float(self.of_data.loc[i:i, 'Tag 0005'].apply(lambda x: 30.3*x))                    
                except:
                    resi_PCA2 = -9999
                try:
                    #resi_PCB2 = np.float(self.of_data.loc[i:i, 'Tag 0005'])*30.3
                    resi_PCB2 = np.float(self.of_data.loc[i:i, 'Tag 0005'].apply(lambda x: 30.3*x))                                        
                except:
                    resi_PCB2 = -9999
                try:
                    #resi_SCA2 = np.float(self.of_data.loc[i:i, 'Tag 0018'])*30.3
                    resi_SCA2 = np.float(self.of_data.loc[i:i, 'Tag 0018'].apply(lambda x: 30.3*x))                    
                except:
                    resi_SCA2 = -9999
                try:
                    #resi_SCB2 = np.float(self.of_data.loc[i:i, 'Tag 0020'])*30.3
                    resi_SCB2 = np.float(self.of_data.loc[i:i, 'Tag 0020'].apply(lambda x: 30.3*x))
                except: 
                    resi_SCB2 = -9999                                                                                               
                                        
                #print resi_T4, self.T4_Hetank4(resi_T4), resi_T16, self.T16_JTS5(resi_T16), resi_T23, self.T23_IVCS5(resi_T23), resi_T7, self.T7_Tc3(resi_T7), resi_T8, self.T8_Tc4(resi_T8), resi_T9, self.T9_Tc5(resi_T9), resi_T11, self.T11_Tc7(resi_T11), resi_C12, self.C12_Tc8(resi_C12), resi_T14, self.T14_Tc10(resi_T14), resi_T15, self.T15_Tc11(resi_T15), resi_T16, self.T16_Tc12(resi_T16), resi_T16, self.T18_Tc16(resi_T18), resi_T19, self.T19_Tc17(resi_T19), resi_T12, self.T12_JTS1(resi_T12)
                #print self.LD_1(LD1, LD2), LD1, LD2
                #print self.Hetank4(resi_Hetank4)
                #print resi_JTS5, self.JTS5(resi_JTS5)
                #print resi_T4KS, self.T4KS(resi_4KS)
                #print resi_ORFIN, self.ORFIN(resi_ORFIN)

                data1 = pd.DataFrame({
                        #'00Date' :   self.of_data.loc[i:i, 'Unnamed: 0'],
                        #'00Time' :   self.of_data.loc[i:i, 'Unnamed: 1'],
                        '00DateTime' : (self.of_data.loc[i:i, 'Unnamed: 0'] + str(' ') + self.of_data.loc[i:i, 'Unnamed: 1']),
                        #'R_Hetank4' : resi_Hetank4,                                
                        'T_Hetank4_X123394' : self.Hetank4(resi_Hetank4),  
                        #'R_JTS5' : resi_JTS5,
                        'T_JTS5_X129986' : self.JTS5(resi_JTS5),
                        #'R_IVCS5' : resi_IVCS5,                                
                        'T_IVCS5_X130034' : self.IVCS5(resi_IVCS5),
                        #'R_MVCS3' : resi_MVCS3,
                        'T_MVCS3_Pt' : self.MVCS3(resi_MVCS3),
                        #'R_OVCS3' : resi_OVCS3,
                        'T_OVCS3_Pt' : self.OVCS3(resi_OVCS3),
                        #'R_MS3' : resi_MS3,
                        'T_MS3_Pt' : self.MS3(resi_MS3),
                        #'R_4KStage' : resi_4KS,
                        'T_4KStage_X129982' : self.T4KS(resi_4KS),                                
                        #'R_Orificein' : resi_ORFIN,
                        'T_Orificein_X129940' : self.ORFIN(resi_ORFIN),                    
                        #'R_Bypassin' : resi_BPSIN,
                        'T_Bypassin_X129939' : self.BPSIN(resi_BPSIN),
                        #'R_HEX1HPIN' : resi_HEX1HPIN,
                        'T_HEX1HPIN' : self.HEX1HPIN(resi_HEX1HPIN),
                        #'R_HEX1HPOUT' : resi_HEX1HPOUT,
                        'T_HEX1HPOUT_X129944' : self.HEX1HPOUT(resi_HEX1HPOUT),
                        #'R_HEX2HPIN' : resi_HEX2HPIN,
                        'T_HEX2HPIN' : self.HEX2HPIN(resi_HEX2HPIN),
                        #'R_HEX1HPOUT' : resi_HEX1HPOUT,
                        'T_HEX2HPOUT_X129946' : self.HEX2HPOUT(resi_HEX2HPOUT),
                        #'R_HEX3HPIN' : resi_HEX3HPIN,
                        'T_HEX3HPIN_X129947' : self.HEX3HPIN(resi_HEX3HPIN),                        
                        #'R_HEX3LPOUT' : resi_HEX3LPOUT,
                        'T_HEX3LPOUT_X129968' : self.HEX3LPOUT(resi_HEX3LPOUT),                        
                        #'R_HEX2LPOUT' : resi_HEX2LPOUT,
                        'T_HEX2LPOUT_X129969' : self.HEX2LPOUT(resi_HEX2LPOUT),                        
                        #'R_HEX1LPOUT' : resi_HEX2LPOUT,
                        'T_HEX1LPOUT_Pt' : self.HEX1LPOUT(resi_HEX1LPOUT),                        
                        #'R_1STSTAGE' : resi_1STG,
                        'T_1STSTAGE_X129983' : self.T1STG(resi_1STG),                        
                        #'R_2NDSTAGE' : resi_2STG,
                        'T_2NDSTAGE_X129988' : self.T2STG(resi_2STG),
                        #'T_inTent' : T_inTent,
                        'T_inTent' : self.TinTent(V_inTent),
                        'FlowMeter L L/min' : PFML,
                        'FlowMeter S L/min' : PFMS,
                        'LD#1 Pam3/s' : self.LD_1(LD1, LD2),
                        'LD#2 Pam3/s' : self.LD_2(LD21, LD22),                    
                        'Hetank_P Pa' : self.HetankP(V_HetankP),
                        'Dewar_P Pa' : self.DewarP(V_DewarP),                                
                        #'R_JTS1' : resi_T12,
                        #'T_JTS1_X129985' : self.T12_JTS1(resi_T12),                      
                        #'R_PPP' : resi_PPP,
                        #'T_PPP_X123395' : self.PPP(resi_PPP),                    
                        #'R_PPCuF' : resi_PPCuF,
                        #'T_PPCuF_X123396' : self.PPCuF(resi_PPCuF),                     
                        #'R_Hetank1' : resi_Hetank1,
                        #'T_Hetank1_X123387' : self.Hetank1(resi_Hetank1),                        
                        #'R_Hetank2' : resi_Hetank2,
                        #'T_Hetank2_X126026' : self.Hetank2(resi_Hetank2),                      
                        #'R_PP1' : resi_PP1,
                        #'T_PP1_X126088' : self.PP1(resi_PP1),                      
                        #'R_PP2' : resi_PP2,
                        #'T_PP2_X123347' : self.PP2(resi_PP2),                       
                        #'R_JTS2' : resi_JTS2,
                        #'T_JTS2_X129941' : self.JTS2(resi_JTS2),                      
                        #'R_JTS3' : resi_JTS3,
                        #'T_JTS3_X129943' : self.JTS3(resi_JTS3), 
                        #'R_JTS4' : resi_JTS4,
                        #'T_JTS4_X129942' : self.JTS4(resi_JTS4),
                        #'R_ADRIFB' : resi_ADRIFB,
                        #'T_ADRIFB_X129990' : self.ADRIFB(resi_ADRIFB),
                        #'R_PCA2' : resi_PCA2,
                        #'T_PCA2_X130032' : self.PCA2(resi_PCA2),                       
                        #'R_PCB2' : resi_PCB2,
                        #'T_PCB2_X130030' : self.PCB2(resi_PCB2),
                        #'R_SCA2' : resi_SCA2,
                        #'T_SCA2_X130031' : self.SCA2(resi_SCA2),
                        #'R_SCB2' : resi_SCB2,
                        #'T_SCB2_X130027' : self.SCB2(resi_SCB2)
                })
                self.of_data_temp = pd.concat([self.of_data_temp, data1])
                data1 = []                        
        print(self.of_data_temp)                        
        self.of_data_temp.to_csv( '%s.csv' % argvs[1])
        #print self.of_data_temp['LD Pam3/s']
        print("finish conversion")        

    #Tag 0001: Hetank4
    def Hetank4(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  399.8174 and res < 4831.8469:
                    c = [
                            399.8174,
                            4831.8469,
                            1.15E+00,
                            -1.31E+00,
                            5.89E-01,
                            -2.30E-01,
                            8.14E-02,
                            -2.63E-02,
                            7.94E-03,
                            -2.03E-03,
                            3.79E-04
                    ]
            elif res > 156.5758 and res < 399.8174:
                    c = [
                            156.5758,
                            399.8174,
                            9.69E+00,
                            -9.47E+00,
                            2.91E+00,
                            -5.99E-01,
                            6.13E-02,
                            9.63E-03,
                            -5.48E-03
                    ]
            elif res > 70.2022 and res < 156.5758:
                    c = [
                            70.2022,
                            156.5758,
                            5.60E+01,
                            -4.52E+01,
                            8.02E+00,
                            -7.81E-01,
                            4.76E-02,
                            1.48E-02,
                            -4.79E-04
                    ]
            elif res > 32.0781 and res < 70.2022:
                    c = [
                            32.0781,
                            70.2022,
                            1.89E+02,
                            -1.20E+02,
                            1.85E+01,
                            -2.66E+00,
                            5.37E-01,
                            -9.84E-02,
                            1.68E-02,
                            -4.95E-03
                    ]        
            else:
                    c = [
                            0.00,
                            32.0781,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)                        

    #Tag 0002: JTS5
    def JTS5(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2514.4953 and res < 38833.4885:
                    c = [
                            2514.4953,
                            38833.4885,
                            1.18E+01,
                            -1.11E+01,
                            3.60E+00,
                            -8.39E-01,
                            1.23E-01,
                            -3.82E-03,
                            -2.01E-03,
                            -3.30E-04                               
                    ]
            elif res > 390.5907 and res < 2514.4953:
                    c = [
                            390.5907,
                            2514.4953,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.60E+00,
                            1.56E-01,
                            -6.48E-03,
                            5.35E-04,
                            -1.00E-03                                
                    ]
            elif res > 101.366 and res < 390.5907:
                    c = [
                            101.366,
                            390.5907,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.48E+00,
                            3.93E-01,
                            -6.30E-02,
                            9.33E-03,
                            -2.26E-03
                    ]        
            else:
                    c = [
                            0.00,
                            101.366,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def IVCS5(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  1095.1636 and res < 8163.62:
                    c = [
                            1095.1636,
                            8163.62,
                            1.19E+01,
                            -1.11E+01,
                            3.56E+00,
                            -8.01E-01,
                            1.09E-01,
                            -5.58E-05,
                            -3.20E-03                                
                    ]
            elif res > 263.6981 and res < 1095.1636:
                    c = [
                            263.6981,
                            1095.1636,
                            6.44E+01,
                            -5.30E+01,
                            1.11E+01,
                            -1.44E+00,
                            1.10E-01,
                            3.21E-03
                    ]
            elif res > 88.8629 and res < 263.6981:
                    c = [
                            88.8629,
                            263.6981,
                            1.95E+02,
                            -1.15E+02,
                            1.75E+01,
                            -2.22E+00,
                            3.53E-01,
                            -5.55E-02,
                            8.95E-03
                    ]
            else:
                    c = [
                            0.00,
                            88.8629,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #MVCS3
    def MVCS3(self, res):
            T = ((-3.948954+(15.59424+0.00364937*(1000.344-res))**0.5)/(-0.001824685))+273.15
            return T
    
    #OVCS3
    def OVCS3(self, res):
            T =  ((-3.948954+(15.59424+0.00364937*(1000.344-res))**0.5)/(-0.001824685))+273.15
            return T
    
    #MS3
    def MS3(self, res):
            T =  ((-3.948954+(15.59424+0.00364937*(1000.344-res))**0.5)/(-0.001824685))+273.15
            return T
            
    #4KStage
    def T4KS(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2765.4184 and res < 45373.6738:
                    c = [
                            2765.4184,
                            45373.6738,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.45E-01,
                            1.26E-01,
                            -4.20E-03,
                            -1.86E-03,
                            -4.20E-04
                    ]
            elif res > 415.9485 and res < 2765.4184:
                    c = [
                            415.9485,
                            2765.4184,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.61E+00,
                            1.57E-01,
                            -6.46E-03,
                            5.78E-04,
                            -1.71E-03                                
                    ]
            elif res > 105.5874 and res < 415.9485:
                    c = [
                            105.5874,
                            415.9485,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.48E+00,
                            3.95E-01,
                            -6.50E-02,
                            1.00E-02,
                            -1.41E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            105.5874,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Orifice_IN
    def ORFIN(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2725.3215 and res < 45031.1242:
                    c = [
                            2725.3215,
                            45031.1242,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.46E-01,
                            1.26E-01,
                            -4.49E-03,
                            -1.85E-03,
                            -3.97E-04                                
                    ]
            elif res > 409.1071 and res < 2725.3215:
                    c = [
                            409.1071,
                            2725.3215,
                            6.39E+01,
                            -5.28E+01,
                            1.15E+01,
                            -1.61E+00,
                            1.59E-01,
                            -7.21E-03,
                            1.11E-03,
                            -1.67E-03                                
                    ]
            elif res > 103.9408 and res < 409.1071:
                    c = [
                            103.9408,
                            409.1071,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.48E+00,
                            3.92E-01,
                            -6.23E-02,
                            9.40E-03,
                            -1.55E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            103.9408,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Bypass_In
    def BPSIN(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2734.4497 and res < 44611.2759:
                    c = [
                            2734.4497,
                            44611.2759,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.44E-01,
                            1.25E-01,
                            -4.27E-03,
                            -1.90E-03,
                            -2.24E-04                                
                    ]
            elif res > 412.5929 and res < 2734.4497:
                    c = [
                            412.5929,
                            2734.4497,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.61E+00,
                            1.57E-01,
                            -7.05E-03                                
                    ]
            elif res > 104.9357 and res < 412.5929:
                    c = [
                            104.9353,
                            412.5929,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.48E+00,
                            3.93E-01,
                            -6.32E-02,
                            9.24E-03,
                            -1.05E-03,
                            1.54E-03
                    ]
            else:
                    c = [
                            0.00,
                            104.9357,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #HEX1_HP_IN        
    def HEX1HPIN(self, res):
            return ((-3.948954+(15.59424+0.00364937*(1000.344-res))**0.5)/(-0.001824685))+273.15
    
    #HEX1_HP_OUT
    def HEX1HPOUT(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2617.3592 and res < 41716.296:
                    c = [
                            2617.3592,
                            41716.296,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.41E-01,
                            1.24E-01,
                            -4.03E-03,
                            -1.93E-03,
                            -3.83E-04                                
                    ]
            elif res > 400.0039 and res < 2617.3592:
                    c = [
                            400.0039,
                            2617.3592,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.61E+00,
                            1.57E-01,
                            -6.93E-03,
                            1.31E-03,
                            -1.65E-03                                
                    ]
            elif res > 102.7071 and res < 400.0039:
                    c = [
                            102.7071,
                            400.0039,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.91E-01,
                            -6.16E-02,
                            9.07E-03,
                            -2.41E-03,
                            1.37E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            104.9357,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 0012 HEX2_HP_IN
    def HEX2HPIN(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2506.1922 and res < 38564.482:
                    c = [
                            2506.1922,
                            38564.482,
                            11.81237074,
                            -11.09830567,
                            3.604864482,
                            -0.839392156,
                            0.123426468,
                            -0.003603504,
                            -0.002091391,
                            -0.000582707                                
                    ]
            elif res > 390.1777 and res < 2506.1922:
                    c = [
                            390.1777,
                            2506.1922,
                            63.97626261,
                            -52.84491316,
                            11.42472585,
                            -1.59840371,
                            0.155300565,
                            -0.006266604                                
                    ]
            elif res > 101.1777 and res < 390.1777:
                    c = [
                            101.1777,
                            390.1777,
                            193.858271,
                            -114.970474,
                            18.25763617,
                            -2.468994325,
                            0.390431482,
                            -0.061743033,
                            0.007574706                                
                    ]
            else:
                    c = [
                            0.00,
                            104.9357,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    #Tag 0013 HEX2_HP_OUT
    def HEX2HPOUT(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2418.8295 and res < 35863.6964:
                    c = [
                            2418.8295,
                            35863.6964,
                            1.18E+01,
                            -1.11E+01,
                            3.60E+00,
                            -8.33E-01,
                            1.21E-01,
                            -3.09E-03,
                            -2.00E-03,
                            -4.37E-04                                
                    ]
            elif res >  382.9073 and res < 2418.8295:
                    c = [
                            382.9073,
                            2418.8295,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.59E+00,
                            1.54E-01,
                            -6.43E-03,
                            1.31E-03,
                            -1.28E-03                                
                    ]
            elif res > 100.4219 and res < 382.9073:
                    c = [
                            100.4219,
                            382.9073,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.49E+00,
                            4.00E-01,
                            -6.30E-02,
                            7.59E-03,
                            -1.00E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            100.4219,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 14 HEX3_HP_IN
    def HEX3HPIN(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2622.6972 and res < 41131.5888:
                    c = [
                            2622.6972,
                            41131.5888,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.40E-01,
                            1.24E-01,
                            -3.86E-03,
                            -1.83E-03,
                            -4.33E-04
                    ]
            elif res >  403.1731 and res < 2622.6972:
                    c = [
                            403.1731,
                            2622.6972,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.60E+00,
                            1.55E-01,
                            -6.84E-03,
                            1.38E-03,
                            -1.07E-03                                
                    ]
            elif res > 103.5072 and res < 403.1731:
                    c = [
                            103.5072,
                            403.1731,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.91E-01,
                            -6.26E-02,
                            9.64E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            103.5072,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 15 HEX3_LP_OUT
    def HEX3LPOUT(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2447.0301 and res < 37018.536:
                    c = [
                            2447.0301,
                            37018.536,
                            1.18E+01,
                            -1.11E+01,
                            3.60E+00,
                            -8.37E-01,
                            1.23E-01,
                            -3.64E-03,
                            -1.80E-03,
                            -5.26E-04                                
                    ]
            elif res > 384.6279 and res < 2447.0301:
                    c = [
                            384.6279,
                            2447.0301,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.60E+00,
                            1.54E-01,
                            -5.52E-03,
                            7.23E-04,
                            -2.01E-03                                
                    ]
            elif res > 100.4085 and res < 384.6279:
                    c = [
                            100.4085,
                            384.6279,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.95E-01,
                            -6.16E-02,
                            7.69E-03,
                            -2.66E-03,
                            1.61E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            100.4085,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 16 HEX2_LP_OUT
    def HEX2LPOUT(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2598.9169 and res < 41009.7708:
                    c = [
                            2598.9169,
                            41009.7708,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.41E-01,
                            1.24E-01,
                            -3.75E-03,
                            -2.01E-03,
                            -4.96E-04                                
                    ]
            elif res > 399.2912 and res < 2598.9169:
                    c = [
                            399.2912,
                            2598.9169,
                            63.95705818,
                            -52.8343598,
                            11.44075566,
                            -1.606489696,
                            0.156872079,
                            -0.006335257,
                            0.001040973,
                            -0.001858593                                
                    ]
            elif res > 102.8822 and res < 399.2912:
                    c = [
                            102.8822,
                            399.2912,
                            193.8257453,
                            -114.9612061,
                            18.29040438,
                            -2.4759698,
                            0.392735199,
                            -0.062339043,
                            0.007551541                                
                    ]
            else:
                    c = [
                            0.00,
                            102.8822,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 17 HEX1_LP_OUT
    def HEX1LPOUT(self, res):
            return ((-3.948954+(15.59424+0.00364937*(1000.344-res))**0.5)/(-0.001824685))+273.15
    
    #Tag 18 1STG
    def T1STG(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2421.8621 and res < 36373.9795:
                    c = [
                            2421.8621,
                            36373.9795,
                            11.81800285,
                            -11.10134121,
                            3.601477605,
                            -0.836863809,
                            0.122536702,
                            -0.003472477,
                            -0.002176183,
                            -0.000100812                                
                    ]
            elif res > 381.998 and res < 2421.8621:
                    c = [
                            381.998,
                            2421.8621,
                            63.98938268,
                            -52.8502504,
                            11.41359104,
                            -1.592380735,
                            0.15312315,
                            -0.005216621,
                            0.000492539,
                            -0.001437238,
                            3.46E-04                                
                    ]
            elif res > 99.8277 and res < 381.998:
                    c = [
                            99.8277,
                            381.998,
                            193.8734399,
                            -114.9766013,
                            18.24170587,
                            -2.461689083,
                            0.390845493,
                            -0.061333901,
                            0.008199452,
                            -0.002329973                                
                    ]
            else:
                    c = [
                            0.00,
                            99.8277,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 19 2nd Stage
    def T2STG(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2559.1143 and res < 39123.8851:
                    c = [
                            2559.1143,
                            39123.8851,
                            11.81311489,
                            -11.09984623,
                            3.605282307,
                            -0.838187232,
                            0.122583149,
                            -0.003384351,
                            -0.002035478,
                            -0.000382554,
                            -1.85E-03                                
                    ]
            elif res > 398.2557 and res < 2559.1143:
                    c = [
                            398.2557,
                            2559.1143,
                            64.00127279,
                            -52.85368125,
                            11.40163742,
                            -1.589343546,
                            0.153273699,
                            -0.005052623                                
                    ]
            elif res > 103.1605 and res < 398.2557:
                    c = [
                            103.1605,
                            398.2557,
                            193.8167147,
                            -114.9418375,
                            18.29350069,
                            -2.491314059,
                            0.397852233,
                            -0.064879612,
                            0.010300169,
                            -0.004224907                                
                    ]
            else:
                    c = [
                            0.00,
                            103.1605,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    #Tag 23, Tag 24 LD
    def LD(self, LD1, LD2):
            if isinstance(LD1,float) == False:
                    return "nan"
            if isinstance(LD2,float) == False:
                    return "nan"

            scale = np.float(LD1)


            #pressure_flow_rate!D19
            #D19IF(LD1<1, 0.000000000001,IF(LD1<1.5,0.00000000001,IF(LD1<2,0.0000000001,IF(LD1<2.5,0.000000001,IF(@Data!Oi10<3,0.00000001,IF(LD1<3.5,0.0000001,IF(LD1<4,0.000001)))))))
            #D20=IF(LD1<4.5, 0.00001,IF(LD1<5,0.0001,IF(LD1<5.5,0.001,IF(LD1<6,0.01))))                
            if LD2 < 1.0:
                    order = 0.000000000001
            elif LD2 < 1.5:
                    order = 0.00000000001
            elif LD2 < 2.0:
                    order = 0.0000000001
            elif LD2 < 2.5:
                    order = 0.000000001
            elif LD2 < 3.0:
                    order = 0.00000001
            elif LD2 < 3.5:
                    order = 0.0000001
            elif LD2 < 4.0:
                    order = 0.000001
            elif LD2 < 4.5:
                    order =  0.00001
            elif LD2 < 5.0:
                    order = 0.0001
            elif LD2 < 5.5:
                    order = 0.001
            elif LD2 < 6.0:
                    order = 0.01
            else:
                    order = 0.0
                    
            #order = =IF(Data!S10<4,pressure_flow_rate!D19,pressure_flow_rate!D20)                
            #if LD1 < 4.0:
            #        #pressure_flow_rate!D19
            #        order = 
            #else:
            #        #pressure_flow_rate!D20
            #        order = 
            LD = scale*np.float(order)
            return LD

    def LD_1(self, LD1, LD2):
            if isinstance(LD1,float) == False:
                    return "nan"
            if isinstance(LD2,float) == False:
                    return "nan"
            scale = np.float(LD1)
            order = np.float(LD2)
            value =scale*10.0**(-round(order,0)-13.0)
            return value
                                
    def LD_2(self, LD1, LD2):
            if isinstance(LD1,float) == False:
                    return "nan"
            if isinstance(LD2,float) == False:
                    return "nan"
            scale = np.float(LD1)
            order = np.float(LD2)
            value =scale*10.0**(-round(order,1)*2.0-14.0)
            return value

    def HetankP(self, v):

            P = np.float(v)*100.0
            
            return P

    def DewarP(self, v):

            P = 133*10**(1.6667*np.float(v)-11.46)
            return P

    def TinTent(self, v):
         # To be fixed
           value = v
           return value
       
    def T12_JTS1(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2847.9436 and res < 48057.1808:
                    c = [
                            2847.9436,
                            48057.1808,
                            1.18E+01,
                            -1.11E+01,
                            3.62E+00,
                            -8.48E-01,
                            1.27E-01,
                            -4.70E-03,
                            -1.83E-03,
                            -3.17E-04                                
                    ]
            elif res > 422.6424 and res < 2847.9436:
                    c = [
                            422.6424,
                            2847.9436,
                            6.39E+01,
                            -5.28E+01,
                            1.15E+01,
                            -1.61E+00,
                            1.59E-01,
                            -7.09E-03,
                            1.13E-03,
                            -1.62E-03                                
                    ]
            elif res > 106.477 and res < 422.6424:
                    c = [
                            106.477,
                            422.6424,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.92E-01,
                            -6.30E-02,
                            7.34E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            106.477,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def PPP(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  525.3 and res < 1.79e4:
                    c = [
                            525.3,
                            1.79E+04,
                            5.109142,
                            -5.856231,
                            2.400654,
                            -0.761887,
                            0.18534,
                            -0.032541,
                            0.003772,
                            -0.000253,
                            0.000007                                
                    ]
            elif res > 167.5 and res < 525.3:
                    c = [
                            167.5,
                            525.3,
                            41.104217,
                            -36.516877,
                            6.866708,
                            -0.236423,
                            -0.096847,
                            0.008572,
                            0.000297                                
                    ]
            elif res > 37.0 and res < 167.5:
                    c = [
                            37.0,
                            167.5,
                            177.755534,
                            -126.691212,
                            21.512299,
                            -2.985688,
                            0.574229,
                            -0.091602,
                            0.004532,
                            0.000349                                
                    ]
            elif res > 30.9339 and res < 71.7953:
                    c = [
                            30.9339,
                            71.7953,
                            1.88E+02,
                            -1.20E+02,
                            1.89E+01,
                            -2.73E+00,
                            5.35E-01,
                            -9.84E-02,
                            1.76E-02,
                            -3.24E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            30.9339,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    def PPCuF(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  536.3229 and res < 15455.254:
                    c = [
                            536.3229,
                            15455.254,
                            1.12E+00,
                            -1.28E+00,
                            6.04E-01,
                            -2.52E-01,
                            9.62E-02,
                            -3.41E-02,
                            1.15E-02,
                            -3.56E-03,
                            7.82E-04                                
                    ]
            elif res > 176.1341 and res < 536.3229:
                    c = [
                            176.1341,
                            536.3229,
                            9.60E+00,
                            -9.41E+00,
                            2.98E+00,
                            -6.53E-01,
                            7.94E-02,
                            6.62E-03,
                            -5.84E-03                                
                    ]
            elif res > 71.7953 and res < 176.1341:
                    c = [
                            71.7953,
                            176.1341,
                            5.57E+01,
                            -4.51E+01,
                            8.34E+00,
                            -8.85E-01,
                            6.09E-02,
                            1.21E-02                                
                    ]
            elif res > 30.9339 and res < 71.7953:
                    c = [
                            30.9339,
                            71.7953,
                            1.88E+02,
                            -1.20E+02,
                            1.89E+01,
                            -2.73E+00,
                            5.35E-01,
                            -9.84E-02,
                            1.76E-02,
                            -3.24E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            30.9339,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def Hetank1(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  723.717 and res < 28453.8201:
                    c = [
                            723.717,
                            28453.8201,
                            1.10E+00,
                            -1.26E+00,
                            6.13E-01,
                            -2.64E-01,
                            1.05E-01,
                            -3.89E-02,
                            1.38E-02,
                            -4.62E-03,
                            9.52E-04,
                            -2.81E-04                                
                    ]
            elif res > 227.8295 and res < 723.717:
                    c = [
                            227.8295,
                            723.717,
                            9.59E+00,
                            -9.41E+00,
                            2.99E+00,
                            -6.56E-01,
                            7.89E-02,
                            7.13E-03,
                            -5.88E-03                                
                    ]
            elif res > 89.219 and res < 227.8295:
                    c = [
                            89.219,
                            227.8295,
                            5.58E+01,
                            -4.52E+01,
                            8.22E+00,
                            -8.26E-01,
                            5.09E-02,
                            1.26E-02,
                            -1.63E-03                                
                    ]
            elif res > 36.183 and res < 89.219:
                    c = [
                            36.183,
                            89.219,
                            1.89E+02,
                            -1.20E+02,
                            1.84E+01,
                            -2.52E+00,
                            4.73E-01,
                            -7.87E-02,
                            1.43E-02,
                            -6.53E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            36.183,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def Hetank2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  643.436 and res < 19727.5027:
                    c = [
                            643.436,
                            19727.5027,
                            1.11E+00,
                            -1.27E+00,
                            6.11E-01,
                            -2.60E-01,
                            1.02E-01,
                            -3.77E-02,
                            1.31E-02,
                            -4.18E-03,
                            3.97E-04,
                            -7.38E-04                                
                    ]
            elif res > 214.8786 and res < 643.436:
                    c = [
                            214.8786,
                            643.436,
                            9.60E+00,
                            -9.41E+00,
                            2.99E+00,
                            -6.54E-01,
                            7.82E-02,
                            7.03E-03,
                            -5.26E-03                                
                    ]
            elif res > 88.2433 and res < 214.8786:
                    c = [
                            88.2433,
                            214.8786,
                            5.58E+01,
                            -4.52E+01,
                            8.23E+00,
                            -8.32E-01,
                            5.09E-02,
                            1.23E-02                                
                    ]
            elif res > 37.4001 and res < 88.2433:
                    c = [
                            37.4001,
                            88.2433,
                            1.89E+02,
                            -1.20E+02,
                            1.83E+01,
                            -2.47E+00,
                            4.62E-01,
                            -7.80E-02,
                            1.17E-02,
                            -3.86E-03,
                            2.46E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            37.4001,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)
    
    def PP1(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  682.3113 and res < 24045.8183:
                    c = [
                            682.3113,
                            24045.8183,
                            1.10E+00,
                            -1.26E+00,
                            6.13E-01,
                            -2.64E-01,
                            1.05E-01,
                            -3.88E-02,
                            1.39E-02,
                            -4.55E-03,
                            9.04E-04,
                            -6.12E-04                                
                    ]
            elif res > 222.948 and res < 682.3113:
                    c = [
                            222.948,
                            682.3113,
                            9.58E+00,
                            -9.40E+00,
                            3.00E+00,
                            -6.61E-01,
                            8.06E-02,
                            6.95E-03,
                            -5.59E-03                                
                    ]
            elif res > 90.5926 and res < 222.948:
                    c = [
                            90.5926,
                            222.948,
                            5.58E+01,
                            -4.51E+01,
                            8.26E+00,
                            -8.42E-01,
                            5.15E-02,
                            1.34E-02,
                            -1.21E-03                                
                    ]
            elif res > 38.1142 and res < 90.5926:
                    c = [
                            38.1142,
                            90.5926,
                            1.89E+02,
                            -1.20E+02,
                            1.83E+01,
                            -2.46E+00,
                            4.53E-01,
                            -7.24E-02,
                            1.21E-02,
                            -3.73E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            38.1142,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def PP2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  525.3 and res < 1.79e4:
                    c = [
                            525.3,
                            1.79E+04,
                            5.109142,
                            -5.856231,
                            2.400654,
                            -0.761887,
                            0.18534,
                            -0.032541,
                            0.003772,
                            -0.000253,
                            0.000007                                
                    ]
            elif res > 167.5 and res < 525.3:
                    c = [
                            167.5,
                            525.3,
                            41.104217,
                            -36.516877,
                            6.866708,
                            -0.236423,
                            -0.096847,
                            0.008572,
                            0.000297                                
                    ]
            elif res > 37.0 and res < 167.5:
                    c = [
                            37,
                            167.5,
                            177.755534,
                            -126.691212,
                            21.512299,
                            -2.985688,
                            0.574229,
                            -0.091602,
                            0.004532,
                            0.000349                                
                    ]
            else:
                    c = [
                            0.00,
                            38.1142,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)
    
    def JTS2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2706.5631 and res < 44651.9275:
                    c = [
                            2706.5631,
                            44651.9275,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.46E-01,
                            1.26E-01,
                            -4.41E-03,
                            -1.85E-03,
                            -4.10E-04                                
                    ]
            elif res > 406.9901 and res < 2706.5631:
                    c = [
                            406.9901,
                            2706.5631,
                            6.39E+01,
                            -5.28E+01,
                            1.15E+01,
                            -1.62E+00,
                            1.60E-01,
                            -7.61E-03,
                            1.53E-03,
                            -2.18E-03                                
                    ]
            elif res > 103.6678 and res < 406.9901:
                    c = [
                            103.6678,
                            406.9901,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.49E+00,
                            3.97E-01,
                            -6.35E-02,
                            9.43E-03,
                            -2.34E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            103.6678,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def JTS3(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2654.2556 and res < 42001.7647:
                    c = [
                            2654.2556,
                            42001.7647,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.42E-01,
                            1.24E-01,
                            -3.80E-03,
                            -2.00E-03,
                            -4.20E-04                                
                    ]
            elif res > 406.0249 and res < 2654.2556:
                    c = [
                            406.0249,
                            2654.2556,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.60E+00,
                            1.55E-01,
                            -6.82E-03,
                            1.30E-03,
                            -1.96E-03                                
                    ]
            elif res > 103.9241 and res < 406.0249:
                    c = [
                            103.9241,
                            406.0249,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.93E-01,
                            -6.37E-02,
                            1.13E-02,
                            -3.25E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            103.9241,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    
    def JTS4(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2723.2143 and res < 44097.169:
                    c = [
                            2723.2143,
                            44097.169,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.43E-01,
                            1.25E-01,
                            -4.13E-03,
                            -1.81E-03,
                            -4.28E-04                                
                    ]
            elif res > 412.1041 and res < 2723.2143:
                    c = [
                            412.1041,
                            2723.2143,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.60E+00,
                            1.56E-01,
                            -5.96E-03,
                            9.04E-04                                
                    ]
            elif res > 104.843 and res < 412.1041:
                    c = [
                            104.843,
                            412.1041,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.91E-01,
                            -6.15E-02,
                            6.98E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            104.843,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


    def ADRIFB(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  2723.2143 and res < 44097.169:
                    c = [
                            2723.2143,
                            44097.169,
                            1.18E+01,
                            -1.11E+01,
                            3.61E+00,
                            -8.43E-01,
                            1.25E-01,
                            -4.13E-03,
                            -1.81E-03,
                            -4.28E-04                                
                    ]
            elif res > 412.1041 and res < 2723.2143:
                    c = [
                            412.1041,
                            2723.2143,
                            6.40E+01,
                            -5.28E+01,
                            1.14E+01,
                            -1.60E+00,
                            1.56E-01,
                            -5.96E-03,
                            9.04E-04                                
                    ]
            elif res > 104.843 and res < 412.1041:
                    c = [
                            104.843,
                            412.1041,
                            1.94E+02,
                            -1.15E+02,
                            1.83E+01,
                            -2.47E+00,
                            3.91E-01,
                            -6.15E-02,
                            6.98E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            104.843,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    
    def PCA2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  1159.5805 and res < 9040.3312:
                    c = [
                            1159.5805,
                            9040.3312,
                            11.86561886,
                            -11.13190599,
                            3.56649612,
                            -0.808838344,
                            0.111665144,
                            -0.000458656,
                            -0.00342413                                
                    ]
            elif res > 272.8716 and res < 1159.5805:
                    c = [
                            272.8716,
                            1159.5805,
                            64.33821307,
                            -53.00322262,
                            11.11558364,
                            -1.440908295,
                            0.111055141,
                            0.004009397                                
                    ]
            elif res > 90.388 and res < 272.8716:
                    c = [
                            90.388,
                            272.8716,
                            194.7740451,
                            -115.287087,
                            17.41457729,
                            -2.196947088,
                            0.345199773,
                            -0.053941209,
                            0.006865541                                
                    ]
            else:
                    c = [
                            0.00,
                            90.388,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)
    
    def PCB2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  1133.6462 and res < 8732.4928:
                    c = [
                            1133.6462,
                            8732.4928,
                            11.86831873,
                            -11.13309562,
                            3.564015313,
                            -0.807181689,
                            0.11114493,
                            -0.000395345,
                            -0.003308203                                
                    ]
            elif res > 268.5733 and res < 1133.6462:
                    c = [
                            268.5733,
                            1133.6462,
                            64.3354091,
                            -52.9978034,
                            11.11669691,
                            -1.444883445,
                            0.113041918,
                            0.002372643,
                            0.000990893,
                            -0.001009968                                
                    ]
            elif res > 89.7001 and res < 268.5733:
                    c = [
                            89.7001,
                            268.5733,
                            194.6727109,
                            -115.2367213,
                            17.5014595,
                            -2.240523865,
                            0.356914113,
                            -0.056317731,
                            0.009856165,
                            -0.001254897                                
                    ]
            else:
                    c = [
                            0.00,
                            89.7001,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)

    def SCA2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  1168.2692 and res < 9091.8879:
                    c = [
                            1168.2692,
                            9091.8879,
                            1.19E+01,
                            -1.11E+01,
                            3.57E+00,
                            -8.09E-01,
                            1.12E-01,
                            -7.79E-04,
                            -2.93E-03                                
                    ]
            elif res > 275.3646 and res < 1168.2692:
                    c = [
                            275.3646,
                            1168.2692,
                            6.43E+01,
                            -5.30E+01,
                            1.11E+01,
                            -1.45E+00,
                            1.12E-01,
                            3.07E-03,
                            1.04E-03,
                            -1.25E-03                                
                    ]
            elif res > 91.5748 and res < 275.3646:
                    c = [
                            91.5748,
                            275.3646,
                            1.95E+02,
                            -1.15E+02,
                            1.75E+01,
                            -2.23E+00,
                            3.53E-01,
                            -5.50E-02,
                            8.21E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            91.5748,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)
    
    def SCB2(self, res):
            if isinstance(res,float) == False:
                    return "nan"
            elif res >  1142.9882 and res < 8776.8401:
                    c = [
                            1142.9882,
                            8776.8401,
                            1.19E+01,
                            -1.11E+01,
                            3.56E+00,
                            -8.06E-01,
                            1.11E-01,
                            -4.07E-04,
                            -3.13E-03                                
                    ]
            elif res > 271.0652 and res < 1142.9882:
                    c = [
                            271.0652,
                            1142.9882,
                            6.43E+01,
                            -5.30E+01,
                            1.11E+01,
                            -1.44E+00,
                            1.11E-01,
                            2.98E-03,
                            1.48E-03,
                            -1.61E-03                                
                    ]
            elif res > 90.4237 and res < 271.0652:
                    c = [
                            90.4237,
                            271.0652,
                            1.95E+02,
                            -1.15E+02,
                            1.75E+01,
                            -2.22E+00,
                            3.51E-01,
                            -5.21E-02,
                            6.91E-03,
                            -1.18E-03                                
                    ]
            else:
                    c = [
                            0.00,
                            90.4237,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0
                    ]
            return self.chebychev(res,c)


if __name__ == '__main__':
    argvs = sys.argv  #
    argc = len(argvs)  #
    func = Calfunc(argvs)
    func.read_excel()
    func.conversion()
    exit()
