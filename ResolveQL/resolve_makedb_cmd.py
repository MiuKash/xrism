#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for commmand logs.
############################################################################


########################################################
# Imports
########################################################
import argparse
import re
import datetime
import codecs
import pandas as pd
from myutils import beginning_of_world, end_of_world
from resolve_utils import save_files

########################################################
# User-defined parameters
########################################################
pattern1 = '^E'
pattern2 = '^C/ SELECT'
pattern3 = '^S'
pattern4 = '.* CALL .*'
hk_cmd=['S_TIME','COMP','CMD','PAR']

########################################################
# Functions
########################################################
def main(infiles, outdir):

    for infile in infiles:

        # Open file.
        print("Openning {}.".format(infile))
        
        #with open(infile, encoding='EUC-JP') as f:
        with open(infile) as f:
            lines = f.readlines()

            data = []
            
            for line in lines:
                
                line = line.rstrip('\n') # Remove return
                line = line.split('#')[0] # Remove all after the first #.
                
                if ( re.match(pattern1, line)) :
                    
                    # Date & Time
                    date = line[2:10]
                    time = line[11:21]
                    dt = datetime.datetime.strptime('20'+date+' '+time, '%Y-%m-%d %H:%M:%S.%f')
                    
                    # Component, command, parameters
                    comp_cmd_par = line[38:]
                    comp_cmd = comp_cmd_par.split(' ')[0]
                    par = ' '.join(comp_cmd_par.split(' ')[1:])
                    comp = comp_cmd.split('.')[0]
                    cmd = '.'.join(comp_cmd.split('.')[1:])

                    data.append([dt, comp, cmd, par])
                    #print(dt, comp, cmd, par)
                
                elif ( re.match(pattern2, line)) :
                    date = line.split(' ')[3]
                    time = line.split(' ')[4]
                    file = line.split(' ')[2]
                    dt = datetime.datetime.strptime(date+' '+time, '%Y-%m-%d %H:%M:%S')
                    data.append([dt, 'FILE', file, ''])
                    
                elif ( re.match(pattern3, line) and re.match(pattern4, line)) :

                    # Date & Time
                    date = line[2:10]
                    time = line[11:21]
                    dt = datetime.datetime.strptime('20'+date+' '+time, '%Y-%m-%d %H:%M:%S.%f')

                    # File name
                    comp_cmd_par = line[38:]                    
                    file=comp_cmd_par.split(' ')[1]
                    data.append([dt, 'FILE', file, ''])
            
            # Save data.
            if (len(data)>0):
                data=pd.DataFrame(data, columns=hk_cmd)
                
                # Index S_TIME
                data=data[data['S_TIME']>beginning_of_world].copy()    
                data.set_index('S_TIME',inplace=True)
                data.sort_index(inplace=True)
                data = data[~data.index.duplicated(keep='last')]
                
                # Save data
                outfile='cmd_{}_{}.pkl'.format(data.index[0].strftime('%Y%m%d-%H%M%S'), data.index[-1].strftime('%Y%m%d-%H%M%S'))
                save_files(data, outdir, outfile, kind='pkl')

    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infiles',
        help='Input files.',
        dest='infiles',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir.',
        dest='outdir',
        type=str,
        default='./',
        nargs='?'
        )
    args = parser.parse_args()
    
    main(args.infiles, args.outdir)