#!/bin/bash

############################################################
# Description
############################################################
# Make db and plots for Resolve QL/DL.


############################################################
# Command-line options
############################################################
# The input sources are one of the following, which are distinguished by -C option.
# (1) Resolve-PPL/PL for all ground test campaigns.
# (2) QL-FFF for in-orbit operations.
# (3) Trend archive for in-orbit operation.
#
# -o : reprocess all files.
# -l : reprocess last few days.
# -q : quick mode (ignore files older than ${ctime}).
# -d : debug mode (selected processes for debugging only).
# -t : enable transfer
# -t : enable data transfer
# -c : enable processing of commands
# -h : enable processing of HK data
# -n : enable processing of noise data
# -e : enable processing of evt data
# -g : enable processing of other GSE data
# -m : enable processing of monitor GSE data
# -b : enable processing makedb
# -p : enable processing plots

# Mode default setting
flg_overwrite=0 # False (0) or True (1)
flg_loop=0 # False (0) or True (1)
flg_quick=0 # False (0) or True (1)
flg_debug=0 # False (0) or True (1)
flg_trans=0 # False (0) or True (1)
flg_com=0 # False (0) or True (1)
flg_hk=0 # False (0) or True (1)
flg_noise=0 # False (0) or True (1)
flg_evt=0 # False (0) or True (1)
flg_gse=0 # False (0) or True (1)
flg_mon=0 # False (0) or True (1)
flg_db=0 # False (0) or True (1)
flg_plot=0 # False (0) or True (1)
flg_ground=1 # False (0) or True (1)

suffix="xa[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]rsl"

############################################################
# Setting
############################################################
TC_default=TC5

# today
today=`date '+%Y/%m/%d' -d '-9 hours'`
todate=`date '+%d' -d '-9 hours'`
today8=`date '+%Y%m%d' -d '-9 hours'`	
# years, months, days and hours
years="seq 2019 2029"
months="seq 1 12"
days=""
hours=`seq 0 23 | awk '{printf("%02d ",$1)}'`

# Scripts from raw to collated data.
makedb_shi_gse="${HOME}/bin/resolve_makedb_shi_gse.py"
makedb_lhp_gse=${makedb_shi_gse}
makedb_adrg="${HOME}/bin/resolve_makedb_adrg.py"
makedb_power_gse="${HOME}/bin/resolve_makedb_power_gse.py"
makedb_power_gse2="${HOME}/bin/resolve_makedb_power_gse2.py"
makedb_dmsfbv_gse="${HOME}/bin/resolve_makedb_dmsfbv_gse.py"
makedb_pc="${HOME}/bin/resolve_makedb_pc.py"
makedb_env="${HOME}/bin/resolve_makedb_env.py"
makedb_mon_gse="${HOME}/bin/resolve_makedb_psd.py"
makedb_fff="${HOME}/bin/resolve_makedb_fff.py"
makedb_lc="${HOME}/bin/resolve_makedb_lc.py"
makedb_nm="${HOME}/bin/resolve_makedb_nm.py -t nm"
makedb_bl="${HOME}/bin/resolve_makedb_lc.py"
makedb_nr_ns="${HOME}/bin/resolve_makedb_nr_ns.py"	
makedb_cmd="${HOME}/bin/resolve_makedb_cmd.py" 
makedb_seq="${HOME}/bin/resolve_makedb_seq.py" 

# Scripts from collated data to plots.
plot_hk="${HOME}/bin/resolve_plot_hk_old.py"
plot_ns="${HOME}/bin/resolve_plot_ns.py"
plot_nr_ns="${HOME}/bin/resolve_plot_nr_ns.py"
plot_nr_ns_2D="${HOME}/bin/resolve_plot_nr_ns_2D.py"
plot_psd_each="${HOME}/bin/resolve_plot_psd_each.py"
plot_psd="${HOME}/bin/resolve_plot_psd.py"
plot_psd_2D="${HOME}/bin/resolve_plot_psd_2D.py"
plot_sd="${HOME}/bin/resolve_plot_sd.py"
plot_wfrb="${HOME}/bin/resolve_plot_sd.py"
plot_map="${HOME}/bin/resolve_plot_map.py"
plot_spec="${HOME}/bin/resolve_plot_spec.py"
plot_evt="${HOME}/bin/resolve_plot_evt.py"
plot_evt_2D="${HOME}/bin/resolve_plot_evt_2D.py"
plot_avgptmpl="${HOME}/bin/resolve_plot_avgptmpl.py"

# Scripts for conversion
conv_shi_gse="python ${HOME}/bin/conv_shigse.py"	

# Scripts from collated data to HTML.
make_index="${HOME}/bin/resolve_html_index_old.py"
make_html_cmd="${HOME}/bin/resolve_html_cmd.py"

# Dir common to all TCs
gitdir="/data6/git/SIB2_DATA/"
power_gse_files_src=${gitdir}/forGPIB_inWindows7_atSHI/log/
power_gse2_files_src=${gitdir}/nec_gse/Resolve_Power_Container/CSV/


############################################################
# Functions (Common)
############################################################
# TC setting
function setting(){
	
	TC=$1

	shi_gse_files_src=""
	adrg_files_pattern=""
	shi_gse_files_pattern=""
	lhp_gse_files_pattern=""
	dmsfbv_gse_files_pattern=""
	power_gse_files_pattern=""
	env_gse_files_pattern=""
	#
	tables=""
	plots=""

	# Ground tests
	if [ "${TC}x" = "PostVibx" ] || [ "${TC}x" = "1909_CSI_TESTx" ]; then
		TC="CSI_TEST"
		topdir="/data6/1909_CSI_TEST/"
		#
		days="${days} `seq 1 30 | awk '{printf("201909%02d ",$1)}'`"
		days="${days} `seq 1 ${todate} | awk '{printf("201910%02d ",$1)}'`"
		TC_start="2019/09/01 00:00:00"
		TC_end="2019/10/18 23:59:59"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="xbox rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		# SHI GSE
		#shi_gse_files_pattern="Dewar_log_*_TC1_*.xlsx"
		shi_gse_files_ver=0
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
	elif [ "${TC}x" = "TC1x" ] || [ "${TC}x" = "1911_ResolveIT_TC1x" ]; then
		TC="TC1" 
		topdir="/data6/1911_ResolveIT_TC1"
		#
		days="${days} `seq 7 18 | awk '{printf("201912%02d ",$1)}'`"
		TC_start="2019/12/07 00:00:00"
		TC_end="2019/12/18 23:59:59"		
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"	
		plots="plumbing adrg cde_pow temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		adrg_files_pattern="TC1*.tsv"
		adrg_time0="2019-12-07T00:00:00"
		# SHI GSE
		shi_gse_files_pattern="Dewar_log_*_TC1_*.xlsx"
		shi_gse_files_ver=1
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
		power_gse_files_pattern="all_2019-10*.txt all_2019-11*.txt"	
	elif [ "${TC}x" = "TC2x" ] || [ "${TC}x" = "2003_ResolveIT_TC2x" ] ; then
		TC="TC2"
		topdir="/data6/2003_ResolveIT_TC2/"
		#
		days="${days} `seq 16 31 | awk '{printf("202003%02d ",$1)}'`"
		days="${days} `seq 1 30 | awk '{printf("202004%02d ",$1)}'`"
		days="${days} `seq 1 31 | awk '{printf("202005%02d ",$1)}'`"		
		TC_start="2020/03/26 00:00:00"
		TC_end="2020/04/28 23:59:59"		
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="plumbing adrg cde_pow temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		adrg_files_pattern="TC2*.tsv"
		adrg_time0="2020-03-22T15:00:00"
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2003_SHI_TC2/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=1
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20200218/
		dmsfbv_gse_files_pattern="202003* 202004* 202005*"
		# Power GSE
		power_gse_files_pattern="all_2020-02-*.txt all_2020-03-*.txt"		
	elif [ "${TC}x" = "TC2.1x" ] || [ "${TC}x" = "2006_ResolveIT_TC2.1x" ] ; then
		TC="TC2.1"
		topdir="/data6/2006_ResolveIT_TC2.1/"
		#
		days="${days} `seq 1 30 | awk '{printf("202006%02d ",$1)}'`"		
		TC_start="2020/06/01 00:00:00"
		TC_end="2020/06/30 23:59:59"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="plumbing cde_pow lhp temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff xbox adrc rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		adrg_files_pattern="TC2.1*.tsv"
		adrg_time0="2020-06-08T15:00:00"
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2006_SHI_TC2.1/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=2
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20200218/
		dmsfbv_gse_files_pattern="202006*"
		# LHP GSE
		lhp_gse_files_pattern="XRISM-202006*xlsx"
		lhp_gse_files_ver=5
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE
		power_gse_files_pattern="all_2020-06-*.txt"	
	elif [ "${TC}x" = "DWR_reassemblyx" ] || [ "${TC}x" = "2007_ResolveIT_DWR_reassemblyx" ] ; then 
		TC="2007_ResolveIT_DWR_reassembly"
		topdir="/data6/2007_ResolveIT_DWR_reassembly/"
		#
		days="${days} `seq 18 31 | awk '{printf("202007%02d ",$1)}'`"		
		TC_start="2020/07/18 00:00:00"
		TC_end="${today} 23:59:59"
		#
		tables=""
		plots="plumbing env"
		# ADRG
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2007_ISAS/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=3
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20200218/
		dmsfbv_gse_files_pattern="202007*"
		# LHP GSE
		# Power GSE
		# Particle counter GSE
		pc_files_src=${gitdir}/HHPC_Particle_Counter/2007_DWR_deInteg/
		pc_files_pattern1="CR100-HPC-DATA-20????_????_UT.TSV"
		pc_files_pattern2="Small-CR-HPC-DATA-20????_????_UT.TSV"	
	elif [ "${TC}x" = "SHI_Leak_Searchx" ] || [ "${TC}x" = "2008_SHI_Leak_Searchx" ] ; then 
		TC="2008_SHI_Leak_Search"
		topdir="/data6/2008_SHI_Leak_Search/"
		#
		days="${days} `seq 18 31 | awk '{printf("202007%02d ",$1)}'`"		
		TC_start="2020/08/01 00:00:00"
		TC_end="${today} 23:59:59"
		#
		tables=""
		plots="plumbing"
		# ADRG
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2008_SHI_Leak_Search/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=3
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
	elif [ "${TC}x" = "DIST_RPL_MON_TESTx" ] || [ "${TC}x" = "2102_DIST_RPL_MON_TESTx" ] ; then 
		TC="2102_DIST_RPL_MON_TEST"
		topdir="/data6/2102_DIST_RPL_MON_TEST/"
		#
		days="20210203 20210204"
		#
	elif [ "${TC}x" = "TC1Ax" ] || [ "${TC}x" = "2105_ResolveIT_TC1Ax" ] ; then
		TC="TC1A"
		topdir="/data6/2105_ResolveIT_TC1A/"
		#
		days="${days} `seq 05 25 | awk '{printf("202105%02d ",$1)}'`"
		days="${days} 20210702 20210703 20210704"	
		TC_start="2021/05/05 00:00:00"
		TC_end="2021/05/25 23:59:59"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="plumbing cde_pow temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc nm rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2105_SHI_TC1A/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=3
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
		power_gse_files_pattern="all_2021-05-*"	
	elif [ "${TC}x" = "TC2Ax" ] || [ "${TC}x" = "2107_ResolveIT_TC2Ax" ] ; then
		TC="TC2A"
		topdir="/data6/2107_ResolveIT_TC2A/"
		#
		days="${days} `seq 14 31 | awk '{printf("202107%02d ",$1)}'`"
		days="${days} `seq 1 31 | awk '{printf("202108%02d ",$1)}'`"
		TC_start="2021/07/18 00:00:00"
		TC_end="2021/08/24 23:59:59"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="adrg plumbing cde_pow temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc nm rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		adrg_files_pattern="TC-2A*.tsv"
		adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2107_TKSC_TC2A
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=4
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="202107* 202108*"
		# LHP GSE
		# Power GSE
		power_gse_files_pattern="all_2021-07-* all_2021-08-*"
	elif [ "${TC}x" = "TC3x" ] || [ "${TC}x" = "2108_ResolveIT_TC3x" ] ; then
		TC="TC3"
		topdir="/data6/2108_ResolveIT_TC3/"
		#
		days="${days} `seq 27 31 | awk '{printf("202108%02d ",$1)}'`"
		days="${days} `seq 1 30 | awk '{printf("202109%02d ",$1)}'`"
		TC_start="2021/08/25 00:00:00"
		TC_end="2021/09/30 23:59:59"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="plumbing cde_pow temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc nm rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		#adrg_files_pattern="TC-3*.tsv"
		#adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2107_TKSC_TC2A
		shi_gse_files_pattern="XRISM-20210827*.xlsx XRISM-20210828*.xlsx XRISM-20210829*.xlsx XRISM-2021083*.xlsx XRISM-202109*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="2021082* 202109*"
		# LHP GSE
		lhp_gse_files_pattern="XRISM-20210*xlsx"
		lhp_gse_files_ver=8 # 7
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE
		power_gse_files_pattern="all_2021-08-* all_2021-09-*"
		# Env monitor
		env_files_pattern="*.xlsx"
	elif [ "${TC}x" = "TC4x" ] || [ "${TC}x" = "2110_ResolveIT_TC4x" ] ; then
		TC="TC4"
		topdir="/data6/2110_ResolveIT_TC4/"
		#
		days="${days} `seq 1 31 | awk '{printf("202110%02d ",$1)}'`"
		days="${days} `seq 1 30 | awk '{printf("202111%02d ",$1)}'`"
		days="${days} `seq 1 31 | awk '{printf("202112%02d ",$1)}'`"
		TC_start="2021/09/26 00:00:00"
		TC_end="2021/12/31 23:59:59"
		#
		plot_hk="${plot_hk} --with-busgse"
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 fwe dist_pcu"
		plots="plumbing temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc nm fwe dist_pcu rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		#adrg_files_pattern="TC-3*.tsv"
		#adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2110_TKSC_TC4
		shi_gse_files_pattern="XRISM-2021*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="202110* 202111* 202112* 202201*"
		# LHP GSE
		lhp_gse_files_pattern="Dewar_log_2111*"
		lhp_gse_files_ver=8 # 7
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE (CDE)
		#power_gse_files_pattern=""
		# Power GSE 2 (SXS-FAN, FWE)
		power_gse2_files_pattern="fanfwe_2021*.csv"
		# Env monitor
		env_files_pattern="*.xlsx"
	elif [ "${TC}x" = "TC5x" ] || [ "${TC}x" = "2201_ResolveIT_TC5x" ] ; then
		TC="TC5"
		topdir="/data6/2201_ResolveIT_TC5/"
		#
		#days="${days} `seq 24 31 | awk '{printf("202201%02d ",$1)}'`"
		#days="${days} `seq 1 28 | awk '{printf("202202%02d ",$1)}'`"
		#days="${days} `seq 1 31 | awk '{printf("202203%02d ",$1)}'`"
		days="${days} `seq 1 31 | awk '{printf("202203%02d ",$1)}'`"
		TC_start="2022/01/24 00:00:00"
		TC_end="2022/03/24 00:00:00"
		#TC_end="${today} 23:59:59"
		#
		plot_hk="${plot_hk} --with-busgse"
		tables="temp coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 fwe dist_pcu"
		plots="temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc dist_pcu" #adrg plumbing nm fwe rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		# ADRG
		#adrg_files_pattern="TC-3*.tsv"
		#adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2110_TKSC_TC4
		shi_gse_files_pattern="XRISM-2022*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="202201* 202202* 202203*"
		# LHP GSE
		lhp_gse_files_pattern="Dewar_log_2111*"
		lhp_gse_files_ver=8 # 7
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE (CDE)
		#power_gse_files_pattern=""
		# Power GSE 2 (SXS-FAN, FWE)
		power_gse2_files_pattern="fanfwe_2022*.csv"
		# Env monitor
		env_files_pattern="*.xlsx"
	elif [ "${TC}x" = "TC6x" ] || [ "${TC}x" = "2203_ResolveIT_TC6x" ] ; then
		TC="TC6"
		topdir="/data6/2203_ResolveIT_TC6/"
	elif [ "${TC}x" = "TC7x" ] || [ "${TC}x" = "2203_ResolveIT_TC7x" ] ; then
		TC="TC7"
		topdir="/data6/2203_ResolveIT_TC7/"
	elif [ "${TC}x" = "TC8x" ] || [ "${TC}x" = "2203_ResolveIT_TC8x" ] ; then
		TC="TC8"
		topdir="/data6/2203_ResolveIT_TC8/"
	elif [ "${TC}x" = "TC9x" ] || [ "${TC}x" = "2203_ResolveIT_TC9x" ] ; then
		TC="TC9"
		topdir="/data6/2203_ResolveIT_TC9/"
	elif [ "${TC}x" = "TC10x" ] || [ "${TC}x" = "2203_ResolveIT_TC10x" ] ; then
		TC="TC10"
		topdir="/data6/2203_ResolveIT_TC10/"
	elif [ "${TC}x" = "TC11x" ] || [ "${TC}x" = "2203_ResolveIT_TC11x" ] ; then
		TC="TC11"
		topdir="/data6/2203_ResolveIT_TC11/"
	# All TCs on the ground.
	elif [ "${TC}x" = "ALLx" ] ; then
		TC="ALL"
		topdir="/data6/ALL/"
		plots="plumbing cde_pow temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc nm rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a
1 lc_b0 lc_b1"
		#
		tables="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 fwe dist_pcu"
		plots="plumbing temp_cc temp_csi temp_dwr cc jtc sc_eff pc_eff ccfreq xbox adrc nm fwe dist_pcu rates_hp rates_mp rates_ms rates_lp rates_ls lc_a0 lc_a1 lc_b0 lc_b1"
		#
		years=`seq 2019 2029`
		months=`seq 1 12 | awk '{printf("%02d\n",$1)}'`
		days=`seq 1 31 | awk '{printf("%02d\n",$1)}'`
	# QLFFF, in orbit.
	elif [ "${TC}x" = "QLx" ] ; then
		TC="QL"
		flg_ground=0
		#
		TC_start="2023/07/01 00:00:00"
		#TC_end="2029/12/31 23:59:59"
		TC_end="${today} 23:59:59"
	# Trend archive, in orbit
	elif [ "${TC}x" = "TAx" ] ; then
		TC="TA"
		flg_ground=0
		#
		TC_start="2023/07/01 00:00:00"
		#TC_end="2029/12/31 23:59:59"
		TC_end="${today} 23:59:59"
	else
		cat <<EOF
echo "Test campaign ${TC} is not supported."
exit -1
EOF
	fi

	# Remove days after today
	today=`date '+%Y%m%d' -d '-9 hours'`
	new_days=""
	for day in ${days} ; do
		if [ ${day} -le ${today} ]; then
			new_days="${new_days} ${day}"
		fi
	done
	days=${new_days}
	
	# Input path setting
	fffdir="${topdir}/fff/" # fff -> TA or QL-FFF
	sxsfffdir="${topdir}/sxsfff/"
	pldir="${topdir}/PL/" # PL -> TA or QL-FFF
	shigsedir="${topdir}/shi_gse/"	
	lhpgsedir="${topdir}/lhp_gse/"
	adrgdir="${topdir}/adrg/"
	powergsedir="${topdir}/power_gse/"
	powergse2dir="${topdir}/power_gse2/"
	dmsfbvgsedir="${topdir}/dmsfbv/"
	pcgsedir="${topdir}/pc/"
	envdir="${topdir}/env/"
	acceldir="${topdir}/accel/"
	busVdir="${topdir}/busV/"

	# Output path setting
	dbdir="${topdir}/db/"
	plotdir="${topdir}/plots/"
	[ -d ${dbdir} ] || mkdir -p ${dbdir}
	[ -d ${plotdir} ] || mkdir -p ${plotdir}
	tempdir="${plotdir}/.template/"

	# event list file
	evtlist="${plotdir}/evtlist.csv"
	obsids="${topdir}/fff/obsids.dat"
	odbinfo="${fffdir}/*/odb/*.dat"
}

# Update proclist
function update_proclist(){

	proclist=$1

	# mkdir & touch if not exist.
	[ -d ${proclist%/*} ] || mkdir -p ${proclist%/*}
	touch ${proclist}

	# uniq
	cat <<EOF
cat ${proclist} | sort -k 2 | uniq > ${proclist}_tmp
/bin/mv ${proclist}_tmp ${proclist}
EOF

	# Remove recent days if loop
	past_days=""
	# reprocess the last few days if loop
	if [ ${flg_loop} -eq 1 ] ; then		
		_past_day=`date '+%Y%m%d' -d '-9 hours'`
		past_days="${past_days} ${_past_day}"		
		#_past_day=`date '+%Y%m%d' -d '-33 hours'`
		#past_days="${past_days} ${_past_day}"		
		#_past_day=`date '+%Y%m%d' -d '-57 hours'`
		#past_days="${past_days} ${_past_day}"
		#_past_day=`date '+%Y%m%d' -d '-81 hours'`
		#past_days="${past_days} ${_past_day}"
		#_past_day=`date '+%Y%m%d' -d '-129 hours'`
		#past_days="${past_days} ${_past_day}"
		#_past_day=`date '+%Y%m%d' -d '-153 hours'`
		#past_days="${past_days} ${_past_day}"
		#_past_day=`date '+%Y%m%d' -d '-177 hours'`
		#past_days="${past_days} ${_past_day}"
		for past_day in ${past_days} ; do			
			cat <<EOF
egrep -v '^${past_day}$' ${proclist} > ${proclist}_tmp
/bin/mv ${proclist}_tmp ${proclist}
EOF
		done	
	fi
}

# Checksum
function checksum(){
	
	file="$1"
	proclist="$2"

	md5sum_new=`md5sum "${file}"`
	md5sum_old=`egrep "${file}$" "${proclist}"` 	

	if [ "${md5sum_new}x" = "${md5sum_old}x" ] ; then
		echo "1"
	else
		cat ${proclist} | egrep -v "^${file}" | sort | uniq > ${proclist}_tmp
		/bin/mv ${proclist}_tmp ${proclist}
		echo "0"
	fi
}

# Update obsid list
function update_obsid(){
	touch ${obsids}
	(cd ${topdir}/fff/
		for i in ????????? ; do
			OBSID=`echo ${i} | sed 's+/++'`
			OBSDATE=`cat ${OBSID}/odb/${OBSID}.dat | grep "/ManeuverStartTime" | awk -F"=" '{print $NF}'`
			OBSEND=`cat ${OBSID}/odb/${OBSID}.dat | grep "/NextManeuverStartTime" | awk -F"=" '{print $NF}'`
			echo ${OBSID} ${OBSDATE} ${OBSEND} >> ${obsids}
		done
	)
	cat ${obsids} | sort | uniq | awk '(NF==3){print}' > ${obsids}_tmp
	/bin/mv ${obsids}_tmp ${obsids}
}

# Update event list
function update_evtlist(){

	# nspec
	for i in `/bin/ls -1 ${dbdir}/ns/1k/ns1k*.npz` ; do
		dt=`echo ${i##*/} | sed 's/ns1k_//;s/\.npz//;s/-//'`
		y=`echo ${dt} | cut -c 1-4`
		m=`echo ${dt} | cut -c 5-6`
		d=`echo ${dt} | cut -c 7-8`
		H=`echo ${dt} | cut -c 9-10`
		M=`echo ${dt} | cut -c 11-12`
		S=`echo ${dt} | cut -c 13-14`
		cat <<EOF >> ${evtlist}
${y}/${m}/${d} ${H}:${M}:${S},  , 5, nspec
EOF
	done 

	# obsid
	cat ${obsids} | while read line ; do
		OBSID=`echo ${line} | cut -c 1-9`
		y=`echo ${line} | cut -c 11-14`
		m=`echo ${line} | cut -c 15-16`
		d=`echo ${line} | cut -c 17-18`
		H=`echo ${line} | cut -c 19-20`
		M=`echo ${line} | cut -c 21-22`
		S=`echo ${line} | cut -c 23-24`
		cat <<EOF >> ${evtlist}
${y}/${m}/${d} ${H}:${M}:${S},  , 4, ${OBSID}
EOF
	done

	cat ${evtlist} | sort | uniq > ${evtlist}_tmp
	/bin/cp ${evtlist}_tmp ${evtlist}
}

# get_last
function get_latest(){
	latest=${dbdir}/latest.txt
	/bin/cp /dev/null ${latest}
	echo "[QL] Plots updated at https://xrsrv1.isas.jaxa.jp/~tsujimot/ResolveQL/ using" >> ${latest}
	echo "last proc (UT)    db file" >> ${latest}
	echo "-----------------------------" >> ${latest}

	dirs='cmd coolers nr ns/8k nr_ns/p00 sd/px sd/ac shi_gse dmsfbv_gse ac pr accel/psd_32.0s_10.0kHz busV/psd_32.0s_25.0kHz'
	for dir in ${dirs} ; do
		last=`/bin/ls ${dbdir}/${dir}/ | egrep -v '(proclist|.txt)' | sort -r | head -1 | sed 's/\..*$//' | awk -F"_" '{print $NF}'`
		echo "${last##*/}, ${dir}" >> ${latest}
	done
	echo "-----------------------------" >> ${latest}
}

# Get data
# The following data are copied manually: ADRG, LHP GSE, Paricle counter 
function get_data(){

	# PPL FFF
	if [ -f ${topdir}/fff/.fetch.sh ] ; then
		cat <<EOF
echo "==> Getting new data for PPL FFF from rfxarm..."
sh ${topdir}/fff/.fetch.sh get
EOF
	fi

	update_obsid
	
	# SXS FFF
#	cat <<EOF
#echo "==> Getting new data (SXS FFF) from rfxarm..."
#sh ${fffdir}/.fetch.sh get
#EOF

	# Accel
	if [ -f ${topdir}/accel/.fetch.sh ] ; then
		cat <<EOF
echo "==> Getting new data for accel from rfxarm..."
sh ${topdir}/accel/.fetch.sh get
EOF
	fi

	# Bus V ripple
	cat <<EOF
echo "==> Getting new data for busV from rfxarm..."
sh ${topdir}/busV/.fetch.sh get
EOF

	# git
	cat <<EOF	
echo "==> Getting new data (SHI, DMSfbv, Power GSE) from git..."
git --git-dir=${gitdir}/.git --work-tree=${gitdir} pull
EOF
	cat <<EOF	
echo "==> Getting new data (eclispe) from git..."
git --git-dir=${gitdir}/../eclipse/.git --work-tree=${gitdir}/../eclipse/ pull
EOF

	# SHI GSE
	if [ "${shi_gse_files_pattern}x" != "x" ] && [ "${shi_gse_files_src}x" != "x" ] ; then
		_src=${shi_gse_files_src}
		_dest=${shigsedir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${shi_gse_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" | sort` ; do
			cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi	    

    # DMSfbv GSE
	if [ "${dmsfbv_gse_files_pattern}x" != "x" ]  && [ "${dmsfbv_gse_files_src}x" != "x" ] ; then
		_src=${dmsfbv_gse_files_src}
		_dest=${dmsfbvgsedir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${dmsfbv_gse_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" | sort` ; do
				cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi	    

	# Power GSE
	if [ "${power_gse_files_pattern}x" != "x" ]  ; then
		_src=${power_gse_files_src}
		_dest=${powergsedir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${power_gse_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" | sort` ; do
				cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi  

	# Power GSE2
	if [ "${power_gse2_files_pattern}x" != "x" ]  ; then
		_src=${power_gse2_files_src}
		_dest=${powergse2dir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${power_gse2_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" | sort` ; do
				cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi  

}

# Put data
function put_data(){

	# rsync to xrsrv1.
	cat <<EOF
echo "==> Syncing toppy to xrsrv1...."
sh ${topdir}/plots/.fetch.sh put
EOF
}

############################################################
# Functions (makedb)
############################################################
# command
function makedb_cmd(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}
	proclist=${dbdir}/fff/proclist_cmd.txt
	update_proclist ${proclist}

	cmdlogdir=${gitdir}/cmdlog
	for _f in `find ${cmdlogdir}/ -ctime ${ctime} -name "*cml*" | sort`; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF      
${makedb_cmd} -i ${_f} -o ${dbdir}/cmd/
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# hk
function makedb_hk(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}
	proclist=${dbdir}/fff/proclist_hk.txt
	update_proclist ${proclist}

	tables=$*
	for table in ${tables} ; do
		#for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0.hk1.gz" | sort` ; do
		for _f in `find /data6/2108_ResolveIT_TC3/fff/ -ctime ${ctime} -name "${suffix}_a0.hk1.gz" | grep 09709 | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}
function makedb_hk_pl(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}
	proclist=${dbdir}/fff/proclist_hk_pl.txt
	update_proclist ${proclist}

	fffdir=${pldir} # HK data is taken from PL products.	
	tables=$*
	for table in ${tables} ; do
		for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0.hk1.gz" | egrep '(old|obsolete|rawdata)' | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

function makedb_hk_sxsfff(){
	
	proclist=${dbdir}/fff/proclist_hk.txt
	update_proclist ${proclist}
	
	for _f in `find ${sxsfffdir}/ -ctime ${ctime} -name "ah[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_sxs.hk.gz"` ; do		
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir}
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# ns1k, ns8k
function makedb_ns(){
	
	proclist=${dbdir}/fff/proclist_ns.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0.hk2.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ns1k
${makedb_fff} -i ${_f} -o ${dbdir} -t ns8k
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# avgp, templ
function makedb_avgptmpl(){
	
	proclist=${dbdir}/fff/proclist_avgptmpl.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0.hk2.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t avgp
${makedb_fff} -i ${_f} -o ${dbdir} -t tmpl
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# pixHK
function makedb_pixhk(){
	proclist=${dbdir}/fff/proclist_pixhk.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0.hk1.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t psp_pixel
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# wfrb (px)
function makedb_wfrbpx(){
	
	proclist=${dbdir}/fff/proclist_wfrbpx.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0pxwf_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t wfrbpx
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# wfrb (ac)
function makedb_wfrbac(){

	proclist=${dbdir}/fff/proclist_wfrbac.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0acwf_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t wfrbac
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# nr
function makedb_nr(){
	
	proclist=${dbdir}/fff/proclist_nr.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0pxnr_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t nr
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# pr
function makedb_pr(){
	
	proclist=${dbdir}/fff/proclist_pr.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0pxpr_uf.evt.gz"` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t pr
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# sd
function makedb_sdpx(){
	
	proclist=${dbdir}/fff/proclist_sdpx.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0pxnr_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir}/ -t sdpx
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

function makedb_sdac(){
	
	proclist=${dbdir}/fff/proclist_sdac.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0acnr_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir}/ -t sdac
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# evt
function makedb_evt(){

	evts=$*
	for evt in ${evts} ; do
		
		proclist=${dbdir}/fff/proclist_${evt}.txt
		update_proclist ${proclist}

		if [ "${evt}x" = "px_calx" ] ; then
			name="${suffix}_a0pxcal*_uf.evt.gz"
		elif [ "${evt}x" = "px_clx" ] ; then
			name="${suffix}_p0px*_cl.evt.gz"
		elif [ "${evt}x" = "px_ufx" ] ; then
			name="${suffix}_a0px_uf.evt.gz"
		elif [ "${evt}x" = "elx" ] ; then
			name="${suffix}_el.gti.gz"
		elif [ "${evt}x" = "acx" ] ; then
			name="${suffix}_a0ac_uf.evt.gz"
		fi
		for _f in `find ${pldir}/ -ctime ${ctime} -name "${name}" | egrep -v '(old|obsolete|rawdata)' | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${evt}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done	
	done
}

# lc
function makedb_lc(){

	local start end

	[ -d ${dbdir}/lc/ ] || mkdir -p ${dbdir}/lc/
	
	evts=$*
	for evt in ${evts} ; do

		proclist=${dbdir}/fff/proclist_${evt}.txt
		update_proclist ${proclist}

		for day in ${days} ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then		
				start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
				end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
				cat <<EOF
${makedb_lc} -t ${evt} -i ${dbdir} -o ${dbdir}/ -s ${start} -e ${end} -r 16
echo "${day}" >> ${proclist}
EOF
			fi
		done
	done
}

# bl
function makedb_bl(){

	local start end

	[ -d ${dbdir}/bl/ ] || mkdir -p ${dbdir}/bl/
	
	evts="bl"
	for evt in ${evts} ; do

		proclist=${dbdir}/fff/proclist_${evt}.txt
		update_proclist ${proclist}

		for day in ${days} ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then		
				start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
				end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
				cat <<EOF
${makedb_bl} -t ${evt} -i ${dbdir} -o ${dbdir}/ -s ${start} -e ${end} -r 900
echo "${day}" >> ${proclist}
EOF
			fi
		done
	done
}

# gain histroy file
function makedb_ghf(){
	
	proclist=${dbdir}/fff/proclist_ghf.txt
	update_proclist ${proclist}
	
	for _f in `find ${pldir}/ -ctime ${ctime} -name "${suffix}_010_pxcal.ghf.gz" | egrep '(old|obsolete|rawdata)' | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ghf
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# nr_ns
function makedb_nr_ns(){

	proclist=${dbdir}/fff/proclist.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0pxnr_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_nr_ns} -i ${_f} -o ${dbdir} -s 100
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# nm
function makedb_nm(){

	proclist=${dbdir}/fff/proclist_nm.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${suffix}_a0pxnr_uf.evt.gz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_nm} -i ${_f} -o ${dbdir} -s 600
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# monitor psd
function makedb_mon_gse(){

	chnum=$1
	monitor=$2
	proclist=${dbdir}/${monitor}/proclist.txt
	update_proclist ${proclist}

	if [ "${monitor}x" == "accelx" ] ; then
		for _f in `find ${topdir}/${monitor}/ -ctime ${ctime} -name "raw[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].npz" | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_mon_gse} -i ${_f} -o ${dbdir}/${monitor}/ -f 'npz' -c ${chnum} -g 1 1 1 1 1 1 1 1
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	elif [ "${monitor}x" == "busVx" ] ; then
		for _f in `find ${topdir}/${monitor}/ -ctime ${ctime} -name "raw[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].npz" | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_mon_gse} -i ${_f} -o ${dbdir}/${monitor}/ -f 'npz' -c ${chnum} -g 12 6
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# ADRG
function makedb_adrg(){
	
	proclist=${dbdir}/adrg/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${adrgdir} ] ; then
		for _f in `find ${adrgdir}/ -ctime ${ctime} -name "${adrg_files_pattern}" | sort` ; do	
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_adrg} -i "${_f}" -o ${dbdir}/ -b ${adrg_time0}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# shi_gse
function makedb_shi_gse(){

	proclist=${dbdir}/shi_gse/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${shigsedir} ] ; then
		for _f in `find ${shigsedir} -ctime ${ctime} -name "*.xlsx" | sort` ; do		
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				if [ "${shi_gse_time0}x" != "x" ] ; then
					opt="-b ${shi_gse_time0}"
				else
					opt=""
				fi
				cat <<EOF
${makedb_shi_gse} -i "${_f}" -o ${dbdir}/ --format ${shi_gse_files_ver} ${opt}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi		
}

# dmsfbv_gse
function makedb_dmsfbv_gse(){
	
	proclist=${dbdir}/dmsfbv_gse/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${dmsfbvgsedir} ] ; then
		for _f in `find ${dmsfbvgsedir}/ -ctime ${ctime} -name "20*" | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_dmsfbv_gse} -i ${_f} -o ${dbdir}/
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# lhp_gse
function makedb_lhp_gse(){

	proclist=${dbdir}/lhp_gse/proclist.txt
	update_proclist ${proclist}
	echo ${lhpgsedir} 
	if [ -d ${lhpgsedir} ] ; then
	
		for _f in `find ${lhpgsedir}/ -ctime ${ctime} -name "*.xlsx" | sort` ; do		
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_lhp_gse} -i "${_f}" -o ${dbdir}/ --format ${lhp_gse_files_ver} -b ${lhp_gse_time0}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi		
}

# power_gse
function makedb_power_gse(){	

	proclist=${dbdir}/power_gse/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${powergsedir} ] ; then
		for _f in `find ${powergsedir}/ -ctime ${ctime} -name "*.txt" | sort` ; do				
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_power_gse} -i ${_f} -o ${dbdir}/
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# power_gse2
function makedb_power_gse2(){	

	proclist=${dbdir}/power_gse2/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${powergse2dir} ] ; then
		for _f in `find ${powergse2dir}/ -ctime ${ctime} -name "*.csv" | sort` ; do				
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_power_gse2} -i ${_f} -o ${dbdir}/
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# particle counter
function makedb_pc(){
	
	proclist=${dbdir}/pc1/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${pcgsedir} ] ; then
		for _f in `find ${pcgsedir}/ -ctime ${ctime} -name "*" | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_pc} -i ${_f} -o ${dbdir}/ -n 1
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi

	proclist=${dbdir}/pc2/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${pcgsedir} ] ; then
		for _f in `find ${pcgsedir}/ -ctime ${ctime} -name "${pc_files_pattern2}" | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_pc} -i ${_f} -o ${dbdir}/ -n 2
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# env
function makedb_env(){
	
	proclist=${dbdir}/env/proclist.txt
	update_proclist ${proclist}

	if [ -d ${envdir} ] ; then
		for _f in `find ${envdir}/ -ctime ${ctime} -name "${env_files_pattern}" | sort` ; do	
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_env} -i "${_f}" -o ${dbdir}/
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}


############################################################
# Functions (plot, longer than a day)
############################################################
# all TC
function plot_hk_all(){

	plots=$*
	plotdir_new="${plotdir}/ALL/${TC}/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	
	for plot in ${plots} ; do
		cat <<EOF		
${plot_hk} -p ${plot} -i ${dbdir} -o ${plotdir_new}/${plot}_${TC} -s ${TC_start} -e ${TC_end} -R 60 -E ${evtlist}
EOF
	done
}

############################################################
# Functions (plot, daily or one shot)
############################################################
# hk
function plot_hk(){

    local start end

	plots=$*
		
	for plot in ${plots} ; do
		
		if [ "rates" = `echo ${plot} | awk -F"_" '{print $1}'` ] ; then
			rates=`echo ${plot} | awk -F"_" '{print $2}'`
			proclist=${plotdir}/rates/${rates}/proclist.txt
			plotdir_new="${plotdir}/rates/${rates}/"
		elif [ "lc" = `echo ${plot} | awk -F"_" '{print $1}'` ] ; then
			lc=`echo ${plot} | awk -F"_" '{print $2}'`
			proclist=${plotdir}/lc/${lc}/proclist.txt
			plotdir_new="${plotdir}/lc/${lc}/"
		else
			proclist=${plotdir}/${plot}/proclist.txt
			plotdir_new="${plotdir}/${plot}/"
		fi

		update_proclist ${proclist}
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		
		for day in ${days} ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then		
				start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
				end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
				cat <<EOF
${plot_hk} -p ${plot} -i ${dbdir} -o ${plotdir_new}/${plot}_${day} -s ${start} -e ${end} -E ${evtlist}
echo "${day}" >> ${proclist}
EOF
			fi
		done
	done
}

# px, ac, cal (spec)
function plot_spec(){

	local start end

	plots="$*"
	for plot in ${plots} ; do

		plotdir_new="${plotdir}/spec/"
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

		proclist=${plotdir_new}/proclist_${plot}.txt
		update_proclist ${proclist}

		for day in ${days} ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then				
				start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
				end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
				cat <<EOF
${plot_spec} -p ${plot} -i ${dbdir} -o ${plotdir_new}/ -s ${start} -e ${end}
echo "${day}" >> ${proclist}
EOF
			fi
		done
	done
}

# px, ac, cal (evt)
function plot_evt(){

	local start end plot
	resample=100
	plots="$*"
	for plot in ${plots} ; do

		plotdir_new="${plotdir}/evt/"
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

		proclist=${plotdir_new}/proclist_${plot}.txt
		update_proclist ${proclist}

		for day in ${days} ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then				
				start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
				end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
				cat <<EOF
${plot_evt} -p ${plot} -i ${dbdir} -o ${plotdir_new}/ -s ${start} -e ${end} -r 100
echo "${day}" >> ${proclist}
EOF
			fi
		done
	done
}

# px, ac, cal (2D)
function plot_evt_2D(){

	local start end plot
	resample=600
	plots=$*		
	for plot in ${plots} ; do

		plotdir_new="${plotdir}/evt_2D/"
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

		proclist=${plotdir_new}/proclist_${plot}.txt
		update_proclist ${proclist}

		for day in ${days} ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then				
				start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
				end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
				cat <<EOF
${plot_evt_2D} -p ${plot} -i ${dbdir} -o ${plotdir_new}/ -s ${start} -e ${end} -R ${resample} -E ${evtlist}
echo "${day}" >> ${proclist}
EOF
			fi
		done
	done
}

# map
function plot_map(){

	local start end

	plotdir_new="${plotdir}/map/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}
			
	for day in ${days} ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ]  ; then
			start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
			end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
			cat <<EOF
${plot_map} -i ${dbdir} -o ${plotdir_new} -s ${start} -e ${end} -r 600 -g Hp Mp Ms Lp Ls
echo "${day}" >> ${proclist}
EOF
		fi
	done
}		

# average pulse & templates
function plot_avgptmpl_each(){
	
	types=$*
	for type in ${types} ; do

		plotdir_new=${plotdir}/${type}/
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

		proclist=${plotdir_new}/proclist.txt
		update_proclist ${proclist}

		for _f in `find ${dbdir}/${type}/ -name "${type}_*-*.pkl" | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${plot_avgptmpl} -i ${_f} -o ${plotdir_new} -t ${type}
md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

# pixhk
function copy_pixhk_each(){

	plotdir_new="${plotdir}/pixhk/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	for _f in `find ${dbdir}/psp_pixel/ -name "psp_pixel_*-*.xls" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ] ; then			
			cat <<EOF
/bin/cp ${_f} ${plotdir_new}/
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# ns
function plot_ns_each(){
	
	len=$1
	
	plotdir_new=${plotdir}/ns_${len}k/
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	for _f in `find ${dbdir}/ns/${len}k/ -name "ns${len}k_*-*.npz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${plot_ns} -i ${_f} -o ${plotdir_new} -l ${len}
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}		

# sd
function plot_sd_each(){
	
	len=$1
	
	plotdir_new=${plotdir}/sd
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	for subdir in time freq ; do
		[ -d ${plotdir_new}/${subdir} ] || mkdir -p ${plotdir_new}/${subdir}				
		[ -d ${plotdir_new}_2D/${subdir} ] || mkdir -p ${plotdir_new}_2D/${subdir}				
	done

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	tmp_px=`mktemp /tmp/${USER}/XXXXXX_px.lst`
	tmp_ac=`mktemp /tmp/${USER}/XXXXXX_ac.lst`
	find ${dbdir}/sd/px/ -name "sdpx_*-*.pkl" | sort > ${tmp_px}
	# find ${dbdir}/sd/ac/ -name "sdac_*-*.pkl" | sort > ${tmp_ac}
	sed 's+/sd/px/sdpx+/sd/ac/sdac+' ${tmp_px} > ${tmp_ac}
	
	paste ${tmp_px} ${tmp_ac} | while read _line ; do
		_f_px=`echo ${_line} | awk '{print $1}'`
		_f_ac=`echo ${_line} | awk '{print $2}'`
	#for _f_px in `cat ${tmp_px}` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f_px} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${plot_sd} -i ${_f_px} ${_f_ac} -m "sd" -o ${plotdir_new}
md5sum "${_f_px}" >> ${proclist}
EOF
		fi
	done
	/bin/rm ${tmp_ac} ${tmp_px}
}		

# wfrb
function plot_wfrb_each(){
	
	len=$1
	
	plotdir_new=${plotdir}/wfrb
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	for subdir in time freq ; do
		[ -d ${plotdir_new}/${subdir} ] || mkdir -p ${plotdir_new}/${subdir}				
		[ -d ${plotdir_new}_2D/${subdir} ] || mkdir -p ${plotdir_new}_2D/${subdir}				
	done

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	#tmp_ac=`mktemp /tmp/${USER}/XXXXXX_ac.lst`
	tmp_px=`mktemp /tmp/${USER}/XXXXXX_px.lst`
	#find ${dbdir}/wfrb/ac/ -name "wfrbac_*-*.pkl" > ${tmp_ac}
	find ${dbdir}/wfrb/px/ -name "wfrbpx_*-*.pkl" > ${tmp_px}
	#paste ${tmp_px} ${tmp_ac} | while read _line ; do
	#	_f_px=`echo ${_line} | awk '{print $1}'`
	#	_f_ac=`echo ${_line} | awk '{print $2}'`
	for _f_px in `cat ${tmp_px}` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f_px} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${plot_wfrb} -i ${_f_px} -m "wfrb" -l 400 -o ${plotdir_new}
md5sum "${_f_px}" >> ${proclist}
EOF
		fi
	done
	/bin/rm ${tmp_ac} ${tmp_px}
}

# nr_ns
function plot_nr_ns(){

	local start end

	plotdir_new="${plotdir}/nr_ns/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}
			
	for day in ${days} ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then
			for hour in ${hours} ; do
				start=`date -d ${day} "+%Y/%m/%d" | awk -v H=${hour} '{printf("%s %s:00:00",$1,H)}'`
				end=`date -d ${day} "+%Y/%m/%d" | awk -v H=${hour} '{printf("%s %s:59:59",$1,H)}'`
				cat <<EOF
${plot_nr_ns} -i ${dbdir}/nr_ns/ -o ${plotdir_new} -s ${start} -e ${end}
EOF
			done
			cat <<EOF
echo "${day}" >> ${proclist}
EOF
		fi
	done
}		

# nr_ns_2D
function plot_nr_ns_2D(){

	local start end
	resample=1
			
	proclist=${plotdir}/nr_ns_2D/proclist.txt
	update_proclist ${proclist}	
	for day in ${days} ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then
			start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
			end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
			plotdir_new=${plotdir}/nr_ns_2D/
			[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
			cat <<EOF
${plot_nr_ns_2D} -i ${dbdir}/nr_ns/ -o ${plotdir_new} -s ${start} -e ${end} -R ${resample} -E ${evtlist} -z 0.5 3.0
echo "${day}" >> ${proclist}
EOF
		fi
	done
}		

# psd each
function plot_psd_each(){

	shotlength=`echo $1 | awk '{printf("%.1f",$1)}'`
	samplingrate=`echo $2 | awk '{printf("%.1f",$1)}'`
	samplingrate_kHz=`echo $2 | awk '{printf("%.1f",$1/1e3)}'`
	chnum=$3
	monitor=$4
	nba="psda"
	
	plotdir_new="${plotdir}/${monitor}/${nba}_${shotlength}s_${samplingrate_kHz}kHz_each/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	for _f in `find ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz/ -name "${nba}_*-*.npz" | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ] ; then			
			cat <<EOF
${plot_psd_each} -l ${shotlength} -r ${samplingrate} -i ${_f} -o ${plotdir_new} -c ${chnum} -m ${monitor}
md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# psd
function plot_psd(){

	local start end

	shotlength=`echo $1 | awk '{printf("%.1f",$1)}'`
	samplingrate=`echo $2 | awk '{printf("%.1f",$1)}'`
	samplingrate_kHz=`echo $2 | awk '{printf("%.1f",$1/1e3)}'`	
	chnum=$3
	monitor=$4
	nba="psda"

	if ! [ -d ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz ] ; then
		return 0
	fi
	
	plotdir_new="${plotdir}/${monitor}/${nba}_${shotlength}s_${samplingrate_kHz}kHz/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}
	
	for day in ${days} ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ]  ; then
			for hour in ${hours} ; do
				start=`date -d ${day} "+%Y/%m/%d" | awk -v H=${hour} '{printf("%s %s:00:00",$1,H)}'`
				end=`date -d ${day} "+%Y/%m/%d" | awk -v H=${hour} '{printf("%s %s:59:59",$1,H)}'`
				cat <<EOF
echo "Processing ${monitor} 1D ${shotlength}s ${samplingrate_kHz}kHz ${start} ${end}..."
${plot_psd} -l ${shotlength} -r ${samplingrate} -i ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz/ -o ${plotdir_new} -s ${start} -e ${end} -c ${chnum} -m ${monitor}
EOF
			done
			cat <<EOF
echo "${day}" >> ${proclist}
EOF
		fi
	done
}

# psd_2D
function plot_psd_2D(){
	
	local start end

	shotlength=`echo $1 | awk '{printf("%.1f",$1)}'`
	samplingrate=`echo $2 | awk '{printf("%.1f",$1)}'`
	samplingrate_kHz=`echo $2 | awk '{printf("%.1f",$1/1e3)}'`	
	chnum=$3
	resample=$4
	monitor=$5
	nba="psda"

	if ! [ -d ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz ] ; then
		return 0
	fi
		
	plotdir_new="${plotdir}/${monitor}_2D/${nba}_${shotlength}s_${samplingrate_kHz}kHz/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}	

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}
			
	for day in ${days} ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then			
			start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
			end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
			cat <<EOF
echo "Processing ${monitor} 2D ${shotlength}s ${samplingrate_kHz}kHz ${start} ${end}..."
${plot_psd_2D} -l ${shotlength} -r ${samplingrate} -i ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz/ -o ${plotdir_new} -s ${start} -e ${end} -R ${resample} -E ${evtlist} -c ${chnum} -m ${monitor}
EOF
			cat <<EOF
echo "${day}" >> ${proclist}
EOF
		fi
	done
}		


############################################################
# Functions (html)
############################################################
# Command table
function make_html_cmd(){

	local start end

	plot="CMD"
	plotdir_new="${plotdir}/${plot}/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	for day in ${days} ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${day} ${proclist}` -eq 0 ] ; then				
			start=`date -d ${day} "+%Y/%m/%d 00:00:00"`
			end=`date -d ${day} "+%Y/%m/%d 23:59:59"`
			cat <<EOF
${make_html_cmd} -i ${dbdir}/cmd/ -o ${plotdir_new}/ -s ${start} -e ${end}
echo "${day}" >> ${proclist}
EOF
		fi
	done
}

# update index_${TC}.html
function update_index(){

	TC=$1

	# No ASCII data uploaded. Plots made with others.
	dirs1="" # In-orbit
	dirs1="" # Spacecraft
	dirs1="${dirs1} nr_ns nr_ns_2D/p* ns_1k ns_8k sd_2D/* wfrb/* wfrb_2D/* avgp tmpl" # Resolve
	dirs1="${dirs1} evt_2D/ac evt_2D/p* map evt/ac evt/p* spec/ac spec/px spec/cal" # Resolve
	dirs1="${dirs1} accel/psda* accel_2D/psda*/ch* mag/psda* mag_2D/psda*/ch* busV/psda* busV_2D/psda*/ch*" # GSE
	# npz uploaded. Only for plots requested.
	dirs2="sd/*"
	# csv.gz uploaded. Plots made with plot_hk.
	dirs3="" # In-orbit
	dirs3="" # Spacecraft
	dirs3="${dirs3} dist_pcu adrc xbox cc ccfreq jtc pc_eff sc_eff temp_csi temp_cc temp_dwr fwe rates/* nm lc/* gain bl" # Resolve
	dirs3="${dirs3} plumbing adrg cde_pow lhp env" # GSE

	for dir in ${dirs1} ; do
		#if [ -d "${plotdir}/${dir}" ] ; then
			for _d in `find ${plotdir}/${dir}/ -type d -print | sort` ; do
				if [ -d ${_d} ] ; then
					cat <<EOF
${make_index} -n 7 -i ${_d} > ${_d}/index_${TC}.html
/bin/cp ${tempdir}/.htaccess ${_d}/
/bin/cp ${tempdir}/index.html ${_d}/
EOF
				fi
			done
		#fi
	done

	for dir in ${dirs2} ; do
		#if [ -d "${plotdir}/${dir}" ] ; then
			for _d in `find ${plotdir}/${dir}/ -type d -print | sort` ; do
				if [ -d ${_d} ] ; then
					cat <<EOF
${make_index} -n 7 -i ${_d} -d npz > ${_d}/index_${TC}.html
/bin/cp ${tempdir}/.htaccess ${_d}/
/bin/cp ${tempdir}/index.html ${_d}/
EOF
				fi
			done
		#fi
	done

	for dir in ${dirs3} ; do
		#if [ -d "${plotdir}/${dir}" ] ; then
			for _d in `find ${plotdir}/${dir}/ -type d -print | sort` ; do
				if [ -d ${_d} ] ; then
					cat <<EOF
${make_index} -n 7 -i ${_d} -d csv.gz > ${_d}/index_${TC}.html
/bin/cp ${tempdir}/.htaccess ${_d}/
/bin/cp ${tempdir}/index.html ${_d}/
EOF
				fi
			done
		#fi
	done
}

############################################################
# Main routine
############################################################
OPTIND=1
while getopts "olqdtchnegmbpC:" OPT ; do
    case ${OPT} in
	o) flg_overwrite=1 ;;
	l) flg_loop=1 ;;
	q) flg_quick=1 ;;
	d) flg_debug=1 ;;
	t) flg_trans=1 ;;
	c) flg_com=1 ;;
	h) flg_hk=1 ;;
	n) flg_noise=1 ;;
	e) flg_evt=1 ;;
	g) flg_gse=1 ;;
	m) flg_mon=1 ;;
	b) flg_db=1 ;;
	p) flg_plot=1 ;;
	C) TC=${OPTARG} ;;
    esac
done

# -c option (or lack thereof)
if  [ "${TC}x" = "x" ] ; then
	TCs=${TC_default}
else
	shift `expr ${OPTIND} - 2`
	TCs="$*"
fi 

# -p and -l are mutually exclusive.
if [ ${flg_overwrite} -eq 1 ] && [ ${flg_loop} -eq 1 ] ; then
	echo "This is not what you intend; overwriting files in an infinate loop."
	exit -1
fi

# -q mode
if [ ${flg_quick} -eq 1 ] ; then
	ctime=-3
else
	ctime=-10000
fi

for TC in ${TCs} ; do
	# Time stamp
	date=`export LC_ALL=C ; date`
	cat <<EOF
	echo "=========================================================================="
	echo "==> Process starting at ${date} for ${TC}."
	echo "==>   Mode     : [${flg_debug}] Debug. [${flg_overwrite}] Overwrite [${flg_loop}] Loop over recent days"
	echo "==>   Retrieval: [${flg_trans}] Transfer from rfxarm2 [${flg_quick}] Process only recent files"
	echo "==>   Data     : [${flg_com}] Cmd [${flg_hk}] HK [${flg_evt}] Evt [${flg_noise}] Noise [${flg_gse}] GSE [${flg_mon}] Monitor"
	echo "==>   Products : [${flg_db}] database [${flg_plot}] plot"
	echo "=========================================================================="
EOF

	setting ${TC}
	
	if [ ${flg_debug} -eq 1 ]  ; then
		: # Need something. Do not delete.
		####################
		# debugging
		####################
		#get_data
		#plot_hk adrg
		#plot_sd_each
		#makedb_ns
		plot_sd_each
		#plot_ns_each 1
		#plot_ns_each 8
		#plot_hk_all ${plots}
		#
		#get_latest
		#update_index ${TC}
		#put_data
	else
		####################
		# transfer
		####################
		if [ ${flg_trans} -eq 1 ]  ; then
			get_data
		fi

		####################
		# commands
		####################
		if [ ${flg_com} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				makedb_cmd
				
				# Update
				get_latest
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				make_html_cmd
			fi			
		fi

		####################
		# gse
		####################
		if [ ${flg_gse} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				# GSE
				#makedb_adrg
				makedb_shi_gse
				makedb_power_gse
				makedb_power_gse2
				makedb_dmsfbv_gse
				#makedb_lhp_gse
				#makedb_pc
				makedb_env
				
				# Update
				update_evtlist
				get_latest
			fi

			####################
			# plots
			####################
		fi

		####################
		# hk
		####################
		if [ ${flg_hk} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				# HK
				makedb_hk ${tables}
				makedb_hk_pl temp
				
				# Update
				update_evtlist
				get_latest
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then

				# plot (hk)
				plot_hk ${plots}

				# plot (hk all)
				# plot_hk_all ${plots}
				
				# plot (rates & map)
				# plot_hk rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb		
				# plot_map

				# Update
				update_index ${TC}
			fi			
		fi

		####################
		# noise
		####################
		if [ ${flg_noise} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				
				# Noise (PPL)
				makedb_ns
				makedb_avgptmpl
				makedb_pixhk
				makedb_sdpx
				makedb_sdac
				makedb_wfrbpx
				makedb_wfrbac	
				
				# Noise (db)
				makedb_nr_ns	
				makedb_nm

				# Update
				update_evtlist
				get_latest
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then				
				
				# plot (PSP par)
				plot_avgptmpl_each avgp tmpl
				copy_pixhk_each
				# plot (noise)
				plot_ns_each 1
				plot_ns_each 8
				plot_sd_each
				plot_wfrb_each
				plot_nr_ns
				plot_nr_ns_2D
				plot_hk nm

				# Update
				update_index ${TC}
			fi			
		fi

		####################
		# evt
		####################
		if [ ${flg_evt} -eq 1 ]  ; then
			
			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				# Records (pr, nr)
				makedb_pr
				makedb_nr
				# Events (px, el, cal, ac from PL)
				makedb_evt px_cal px_cl px_uf el ac
				# Light curves (px_cl, ,px_uf, ac, el from db)
				makedb_lc px_cl px_uf el ac
				# Gain history file (PL)
				makedb_ghf
				# Baseline
				makedb_bl

				# Updates
				get_latest
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				# plot (events, ghf)
				plot_hk bl gain
				plot_evt px ac cal
				plot_evt_2D px ac cal
				plot_spec ac cal px

				# Updates
				update_index ${TC}
			fi
		fi

		####################
		# mon
		####################
		if [ ${flg_mon} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				
				makedb_mon_gse 8 "accel"
				#makedb_mon_gse 8 "mag"
				#makedb_mon_gse 2 "busV"
				
				# Updates
				get_latest
			fi

			####################
			# Plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				# plot (accel)
				plot_psd 32.0 10e3 8 "accel"
				plot_psd_2D 32.0 10e3 8 12 "accel"

				# plot (mag)
				#plot_psd 32.0 10e3 8 "mag"
				#plot_psd_2D 32.0 10e3 8 1 "mag"

				# plot (busV)
				plot_psd 32.0 25e3 2 "busV"
				plot_psd_2D 32.0 25e3 2 24 "busV"

				# Updates
				update_index ${TC}
			fi
		fi

		####################
		# transfer
		####################
		if [ ${flg_trans} -eq 1 ]  ; then
			update_index ${TC}
			put_data
		fi
	fi
done

exit
