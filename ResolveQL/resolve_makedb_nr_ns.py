#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK database for noise spec from noise records.
############################################################################


########################################################
# Imports
########################################################
import argparse
import numpy as np
import scipy.fftpack as sf
from resolve_utils import save_files
from resolve_makedb_fff import collate_data_fff, hdu_nr, hks_nr


########################################################
# User-defined parameters
########################################################
len_nspec=512


########################################################
# Functions
########################################################
def makedb(infile, outdir, sample):
    
    data = collate_data_fff(infile, hdu_nr, hks_nr, 0)
    if (data is None) or (len(data)==0):
        return 1
    data = data[data.NOISEREC_MODE==0]
    
    for px in range(36):        
        data_RoI = data[data.PIXEL==px]
        num_rec = int(len(data[data.PIXEL==px])/sample)
        print("%d sets of %s nrec samples for px%d." % (num_rec, sample, px))

        for i in range(num_rec):
            data_RoI_sample = data_RoI[sample*i:sample*(i+1)]
            stime = data_RoI_sample.index[0]
            
            # nr_ns
            psd_sum=np.zeros(len_nspec)  # Summed noise record.
            for index, row in data_RoI_sample.iterrows():                
                datum = row[2:].values
                # Detrend
                datum = datum - datum.mean()
                # Calc PSD
                filter_func=np.ones(len_nspec*2)
                fft = sf.fft(datum*filter_func,len_nspec*2)[1:len_nspec+1]
                # Add PSD.
                psd_sum += np.abs(fft)*np.abs(fft)
            
            asd_mean = np.sqrt(psd_sum/sample)
                        
            outdir_new = outdir + '/nr_ns/p%02d/' % px
            outfile = 'nr_ns_p%02d_' % px + stime.strftime("%Y%m%d-%H%M%S") + '.npz'
            save_files(asd_mean, outdir_new, outfile)
            
    return 0
    

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--sample',
        help='Number of noise rec samples to make noise spec.',
        dest='sample',
        type=int,
        nargs='+',
        default=[100]
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0], args.sample[0])