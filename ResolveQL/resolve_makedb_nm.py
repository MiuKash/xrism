#!/usr/bin/env python3

############################################################################
# [Function]
# Make SXS HK databse for noise metric from noise records.
############################################################################


########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
from resolve_makedb_fff import collate_data_fff, save_files_fff_range, hdu_nr, hks_nr


########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#------------------------------------------------------------
def makedb_nm(infile, outdir, sample):
    
    data = collate_data_fff(infile, hdu_nr, hks_nr, 0)
    if (data is None) or (len(data)==0):
        return 1

    data = data[data.NOISEREC_MODE==0]
    if (data is None) or (len(data)==0):
        return 1

    num_rec = int(len(data)/sample)
    print("%d sets of nrec samples for all pixels." % num_rec)
    
    nm_data_all = None
    
    # For each chunk        
    for i in range(num_rec):
        data_RoI = data[sample*i:sample*(i+1)]
        stime = data_RoI.index[0]
                
        nm_data = [stime]
        for px in range(36):
            data_RoI_px = data_RoI[data_RoI.PIXEL==px].iloc[:,2:].values.flatten()            
            if (len(data_RoI_px)>0):
                max = np.max(data_RoI_px)
                min= np.min(data_RoI_px)
                mean = np.mean(data_RoI_px)
                med = np.median(data_RoI_px)
                std = np.std(data_RoI_px)
            else:
                max, min, mean, med, std = np.nan, np.nan, np.nan, np.nan, np.nan
            nm_data.extend([max, min, mean, med, std])        
        nm_data_df = pd.DataFrame(nm_data).T
        if (nm_data_all is None):
            nm_data_all = nm_data_df
        else:
            nm_data_all = pd.concat([nm_data_all, nm_data_df])

    # Save nm
    columns=['S_TIME']
    for _p in range(36):
        columns.extend(['ADUMAX%02d' % _p, 'ADUMIN%02d' % _p, 'ADUMEAN%02d' % _p,'ADUMED%02d' % _p,'ADUSTD%02d' % _p])
    nm_data_all.columns = columns
    
    # Index S_TIME
    nm_data_all.set_index('S_TIME',inplace=True)
    nm_data_all.sort_index(inplace=True)
    nm_data_all = nm_data_all[~nm_data_all.index.duplicated(keep='last')]    
    
    outdir_new = outdir + '/nm/'
    save_files_fff_range(nm_data_all, outdir_new, 'nm')

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--sample',
        help='Number of noise rec samples to make noise spec.',         
        dest='sample',
        type=int,
        nargs='+',
        default=[360]
        )
    parser.add_argument(
        '-t', '--tables',
        help='Tables',
        dest='tables',
        type=str,
        nargs="*",
        )
    args = parser.parse_args()
    
    for table in args.tables:
        if (table == 'nm'):
            makedb_nm(args.infile[0], args.outdir[0], args.sample[0])
