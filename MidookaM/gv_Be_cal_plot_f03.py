#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
from anaconda_navigator.widgets.dialogs.tests import data
from astropy.units import decahour
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.0)


########################################################
# User Setting
########################################################
# Datadir
datadir='/home/astro/Work/XARM/SXS2/20190613/figures/'

# Data files
dataset_kekpf_center=
"1800_2100_5eV_10s_InSbC.pkl",
"2050_2400_5eV_10s_InSbC.pkl",
"2060_2200_2eV_4s_C.pkl",
"2060_2400_2eV_2s_B.pkl",
"2060_2400_2eV_2s_T.pkl",
"2150_2400_2eV_2s_C.pkl",
"2350_2600_2eV_2s_C.pkl",
"2350_3000_2eV_2s_T.pkl",
"2350_3050_5eV_10s_InSbC.pkl",
"2550_3000_2eV_2s_C.pkl",
"2950_3500_2eV_2s_C.pkl",
"2950_4500_2eV_2s_T.pkl",
"3000_3400_5eV_3400_3500_2eV_2s_InSbC.pkl",
"3450_4000_2eV_2s_C.pkl",
"3950_5000_2eV_2s_C.pkl",
"4000_5000_2eV_1s_B.pkl",
"4000_5000_2eV_1s_C.pkl",
"4000_5000_2eV_1s_T.pkl",
"4000_6000_2eV_2s_C.pkl",
"4900_6000_2eV_1s_B.pkl",
"4900_6000_2eV_1s_T.pkl",
"4900_8000_2eV_1s_C.pkl",
"5900_8000_2eV_1s_B.pkl",
"5900_8000_2eV_1s_T.pkl",
"5900_8000_2eV_2s_C.pkl",
"7900_10000_2eV_2s_C.pkl",
"7900_12000_2eV_1s_B.pkl",
"7900_12000_2eV_1s_T.pkl",
"9900_12000_2eV_2s_C.pkl",




########################################################
# Functions
########################################################
#-------------------------------------------------------
def make_plot(outfile):

    # Plot setup.
    plt.figure(figsize=(8.27,11.69))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)

    # I
    ax1 = plt.subplot(3,1,3)
    ax1.set_xlabel('Energy (eV)')
    ax1.set_ylabel('$I$')
    ax1.set_yscale('log')
    
    # |F|
    ax2 = plt.subplot(3,1,2, sharex=ax1)
    plt.setp(ax2.get_xticklabels(), visible=False)
    ax2.set_xlabel('')
    ax2.set_ylabel('$|F|$')
    ax2.set_yscale('log')
    #ax2.set_ylim(0.6,1)
    
    # 2theta
    ax3 = plt.subplot(3,1,1, sharex=ax1)
    plt.setp(ax3.get_xticklabels(), visible=False)
    ax3.set_xlabel('')
    ax3.set_ylabel('2$\\theta$')
    ax3.set_yscale('linear')
                        
    # Plot setting
    for ax in [ax1, ax2, ax3]:
        ax.set_xlim(3.0, 12.5)
        ax.grid(which='both')
        ax.set_xscale('linear')
    ax1.legend(fontsize=12, ncol=3)

    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    outfile="%s.pdf" % (outfile)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)
    plt.close('all')
       
    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--output',
        help='Output file name origin.',
        dest='outfile',
        type=str,
        default='GV_comp',
        nargs='?'
        )
    args = parser.parse_args()

    # Check arguments.
    outfile = args.outfile
    
    # Plot
    make_plot(outfile)
