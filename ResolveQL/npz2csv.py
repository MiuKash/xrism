#!/usr/bin/env python3

########################################################
# Imports
########################################################
import argparse
import pandas as pd
import numpy as np
from resolve_utils import get_freqs

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    samplingrate = 10000
    recordlength = 32
    freqs = get_freqs(samplingrate, recordlength)

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infiles',
        help='Input files.',
        dest='infiles',
        type=str,
        nargs='*'
        )
    args = parser.parse_args()

    for infile in args.infiles:
        data = np.load(infile)['arr_0']
        data_freq = np.vstack([freqs, data.T])
        outfile=infile.replace('npz','csv.gz')
        np.savetxt(outfile, data_freq.T, delimiter=',', fmt='%.5f')
        print("{} converted to {}".format(infile, outfile))