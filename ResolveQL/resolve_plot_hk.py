#!/usr/bin/env python3

###########################################################
# [Function]
# Generate an Resolve HK plot.
############################################################

########################################################
# Imports
########################################################
import argparse, glob, collections, datetime, time, os
from os.path import exists
import numpy as np
import pandas as pd
from scipy import signal
#
from resolve_utils import get_datetime_from_args, filter_by_datetime, abs_zero, save_files, get_pHe4
from resolve_makedb_env import env0_columns, env1_columns, env2_columns, env3_columns, env4_columns, env5_columns
from myutils import yield_color, add_line, add_range
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
#
import threading


########################################################
# User-defined parameters
########################################################
now_utc = datetime.datetime.utcfromtimestamp(time.time())
str_now  = now_utc.strftime('%Y/%m/%d %H:%M:%S')

# Number of panels in each plot.
n_panels={
    'plumbing': 11,
    'cde_pow' :7,
    'lhp_gse' : 12,
    'adrg': 7,
    'env' : 4,
    'temp_dwr': 8,
    'temp_cc': 8,
    'temp_csi': 9,
    'dist_pcu': 7,
    'fwe' :15,
    'cc' : 10,
    'ccfreq' : 8,
    'jtc' : 10,
    'sc_eff': 9,
    'pc_eff': 9,
    'adrc': 8,
    'xbox' : 8,
    'rates': 6,
    'nm' : 8,
    'bl' : 8,
    'gain': 8,
    'lc' : 8,
    'pcu_pow' : 10,
    'eps' : 9,
    'aocs': 8,
    'hce' : 6,
    'lhp' : 11,
    'vis' : 9,
    'dr' : 8,
    'rf' : 9,
    'xtend' : 7, 
    }

# dbfile, HK name
ABs=['A', 'B']
HLs=['H', 'L']
CDs=['CDA', 'CDB', 'JTD']
STs=['SC', 'PC']
VIPs=['V', 'I', 'P']
Units=['CMP', 'DSP', 'BLN']
Quads=['A0', 'A1', 'B0', 'B1']
adrc_cards = ['CAMC', 'WAMC', 'HAMC']

temp_adrg=[
    "T: m02 [X128905] JT strap I/F",
    "T: m03 [X128881] IVCS bracket",
    "T: m10 [X128907] S3 magnet T",
    "T: m11 [X128318] He tank strap",
    "T: m12 [X128239] HS4 cold end",
    "T: m13 [X128908] HS4 warm end",
    "T: m14 [AHFMRO] HS3 getter 1",
    "T: m16 [AHFMRO] HS4 getter 1",
    "T: m18 [X128878] HTS3 WE",
    "T: m19 [X128906] HTS3 CE",
    "T: m30 [X123394] He tank 4",
    "T: Keithley 21 [X128313] S3 salt pill",
    "T: Lakeshore372 1 [X128295] PP3",
    "T: Lakeshore372 2 [L31380] S1 Salt Pill 1",
    "T: Lakeshore372 3 [X128181] He tank 3",
    "T: Lakeshore372 4 [X128302] S2 CTRL (S2 Salt A)",
]
#        ["T: m11 [X128318] He tank strap", "T: m30 [X128181] He tank 3", "T: m31 [X128294] PP3", "T: m02 [X128905] JT strap I/F",
#        "Pressure: m23 [SHI_P001] P1", "Pressure: m24 [SHI_P301] P301", "Leak rate: m22 LD1 leak rate", "Leak rate: m27 LD2 leak rate"

temp_cda = [
    "HE_TANK1", 
    "PP1", 
    "JT_SHLD1",
    "JT_SHLD3",
    "IVCS1",
    "IVCS3",
    "SCA_2ND",
    "PCA_2ND",
    "MVCS1",
    "OVCS1",
    "SCA_1ST",
    "PCA_1ST",
    "DMS1",
    "SCA_CMP",
    "PCA_CMP",
    "CDA"
]
temp_cdb= [
    "HE_TANK2", 
    "PP2", 
    "JT_SHLD2", 
    "JT_SHLD4", 
    "IVCS2", 
    "IVCS4", 
    "SCB_2ND", 
    "PCB_2ND", 
    "MVCS2", 
    "OVCS2", 
    "SCB_1ST", 
    "PCB_1ST", 
    "GV1", 
    "SCB_CMP", 
    "PCB_CMP", 
    "CDB"
]
temp_jtd = [
    "JT_4KSTG",
    "ORIFI_IN",
    "BYPSS_IN",
    "DMS2",
    "GV2",
    "SCA_CHD",
    "SCB_CHD",
    "PCA_CHD",
    "PCB_CHD",
    "SCA_CPY",
    "SCB_CPY",
    "PCA_CPY",
    "PCB_CPY",
    "JTL_CMP",
    "JTH_CMP",
    "JTD",
]
temp_adrc=[
    "HTS12CLD",
    "HTS3_CLD",
    "HTS12HOT",
    "HTS3_HOT",
    "HS3_CLDE",
    "IVCS_FLT",
    "OVCS_FLT",
    "DMS_FLT",
    "SITH_TEMP0",
    "SITH_TEMP1",
    "SITH_PS_TEMP0",
    "SITH_PS_TEMP1",
    "ST1_PIL1",
    "ST1_STRP",
    "ST1_MAG",
    "HS1_CLDE",
    "HS1_GET1",
    "HS1_GET2",
    "ST1_STRP",
    "DET_ASSY",
    "ST1_CTL1",
    "ST1_CTL2",
    "ST1_CTL3",
    "ST1_CTL4",
    "CAMC_TEMP0",
    "CAMC_TEMP1",
    "CAMC_TEMP2",
    "CAMC_TEMP3",
    "CAMC_TEMP4",
    "MAG_SHLD",
    "ST2_MAG",
    "HS2_CLDE",
    "HS2_GET1",
    "HS2_GET2",
    "HS2_WRME",
    "JT_STRP",
    "HTS12BRH",
    "ST2_CTL1",
    "ST2_CTL2",
    "ST2_CTL3",
    "ST2_CTL4",
    "WAMC_TEMP0",
    "WAMC_TEMP1",
    "WAMC_TEMP2",
    "WAMC_TEMP3",
    "WAMC_TEMP4",
    "ST3_MAG",
    "HS3_GET1",
    "HS3_GET2",
    "HS3_WRME",
    "HS4_CLDE",
    "HS4_GET1",
    "HS4_GET2",
    "HS4_WRME",
    "ST3_CTL1",
    "HE_TANK3",
    "ST3_CTL3",
    "PP3",
    "HAMC_TEMP0",
    "HAMC_TEMP1",
    "HAMC_TEMP2",
    "HAMC_TEMP3",
    "HAMC_TEMP4",
    "CAMC_CT0",
    "CAMC_CT1",
    "ST1_CTL",
    "WAMC_CT0",
    "WAMC_CT1",
    "ST2_CTL",
    "HAMC_CT0",
    "HAMC_CT1",
    "ST3_CTL",
    "ADRC_CT_CTL_FLUC",
    "ADRC_CT_MON_FLUC"
]

r_cda=['CDA_TEMP{:02d}_CAL'.format(i+1) for i in range(16)]
r_cdb=['CDB_TEMP{:02d}_CAL'.format(i+1) for i in range(16)]
r_jtd=['JTD_TEMP{:02d}_CAL'.format(i+1) for i in range(16)]

# Note that CAMC_CT0, CAMC_CT1, and ST1_CTL are defined in unit of mK, not K.
dbhks_cc={
    'temp' :
        ["CDA", "CDB", "JTD"],
    'coolers' : 
        [
        [[[['CD%s_%s%s_%s_%s_CAL' % (i, j, i, k, l) for i in ABs] for j in STs] for k in Units] for l in VIPs],
        [['JTD_DRV%s_CMP_%s_CAL' % (i, j) for i in HLs] for j in VIPs],        
        ['JTD_P%s_CAL' % i for i in ['H', 'M', 'L', 'J']],
        ]
    }
dbhks_jtc={
    'coolers' : 
        [
        [['JTD_DRV%s_CMP_%s_CAL' % (i, j) for i in HLs] for j in VIPs], 
        ['JTD_P%s_CAL' % i for i in ['H', 'M', 'L', 'J']],
        ['JTD_VALVE%d_OPN_CLS' % i for i in range(1,4)] 
        ],
    'temp' :
        [temp_jtd],
    'shi_gse':
        ['HE_TANK4', 'JT_SHLD5', 'IVCS5', 'MVCS3', 'OVCS3', 'DMS3', 
                    'JT_4KSTG', 'ORIFI_IN', 'BYPSS_IN',
                    'HEX1HPIN', 'HEX1HPOUT', 'HEX2HPIN', 'HEX2HPOUT', 
                    'HEX3HPIN', 'HEX3LPOUT', 'HEX2LPOUT', 'HEX1LPOUT', 
                    '1STSTAGE', '2NDSTAGE'],
    'smu_a_hce_a' : [
        "HCE_A_STS_SXS_JT_PR_CMP_L1_TEMP_CAL", "HCE_A_STS_SXS_JT_BU_CMP_H1_TEMP_CAL", "HCE_A_STS_SXS_JT_PR_CMP_L1_ON_OFF", "HCE_A_STS_SXS_JT_BU_CMP_H1_ON_OFF"
    ],
    'smu_a_hce_b' : [
        "HCE_B_STS_SXS_JT_BU_CMP_L1_TEMP_CAL", "HCE_B_STS_SXS_JT_PR_CMP_H1_TEMP_CAL", "HCE_B_STS_SXS_JT_BU_CMP_L1_ON_OFF", "HCE_B_STS_SXS_JT_PR_CMP_H1_ON_OFF"
    ],
    'smu_a_mhce' : [
        "M_HCE_STS_SXS_JT_PR_CMP_L2_TEMP_CAL", "M_HCE_STS_SXS_JT_PR_CMP_H2_TEMP_CAL", "M_HCE_STS_SXS_JT_PR_CMP_H2_ON_OFF", "M_HCE_STS_SXS_JT_PR_CMP_L2_ON_OFF"
    ],
    'smu_b_hce_a' : [
        "HCE_A_STS_SXS_JT_PR_CMP_L1_TEMP_CAL", "HCE_A_STS_SXS_JT_BU_CMP_H1_TEMP_CAL", "HCE_A_STS_SXS_JT_PR_CMP_L1_ON_OFF", "HCE_A_STS_SXS_JT_BU_CMP_H1_ON_OFF"
    ],
    'smu_b_hce_b' : [
        "HCE_B_STS_SXS_JT_BU_CMP_L1_TEMP_CAL", "HCE_B_STS_SXS_JT_PR_CMP_H1_TEMP_CAL", "HCE_B_STS_SXS_JT_BU_CMP_L1_ON_OFF", "HCE_B_STS_SXS_JT_PR_CMP_H1_ON_OFF"
    ],
    'smu_b_mhce' : [
        "M_HCE_STS_SXS_JT_PR_CMP_L2_TEMP_CAL", "M_HCE_STS_SXS_JT_PR_CMP_H2_TEMP_CAL", "M_HCE_STS_SXS_JT_PR_CMP_H2_ON_OFF", "M_HCE_STS_SXS_JT_PR_CMP_L2_ON_OFF"
    ],

    }
dbhks_ccfreq={
    'coolers' : 
        [
            ["%s_CMP_FREQ_SET_CAL" % (i) for i in CDs],
            [[["CD%s_%s%s_%s_PHASE_OFFSET_CAL" % (i, j, i, k) for i in ABs] for j in STs] for k in Units[1:]],
            ["CDA_PCA_CMP_PHASE_OFFSET_CAL"],
            ["CDB_SCB_CMP_PHASE_OFFSET_CAL"],            
            ["CDB_PCB_CMP_PHASE_OFFSET_CAL"],            
            ['JTD_DRVH_CMP_PHASE_OFFSET_CAL']
        ]
    }
dbhks_adrc={
    'temp' :
        [temp_adrc],
    'coolers' : 
        [
        ['ADRC_SITH_HTR%d_I_CAL' % i for i in range(3)],
        ['ADRC_SITH_FLT_HTR_PS_V_CAL'],
        [['ADRC_%s_MAG_%s_I_CAL' % (c, hl) for c in adrc_cards] for hl in ['LO', 'HI']],
        [['ADRC_%s_MAG_%s_V_CAL' % (c, sd) for c in adrc_cards] for sd in ['SEN', 'DRV']],
        ['ADRC_SEQ%d_EXEC_LINE' % i for i in range(3)], 
        ]
    }
dbhks_xbox={
    'xboxa' :
        [
            ["XBOXA_TEMP%d_CAL" % i for i in range(0,5)],
            ["XBOXA_JFET_VSS_CAL", "XBOXA_JFET_VDD1_CAL", "XBOXA_JFET_VDD2_CAL"],
            ["XBOXA_CALO_BIAS1_CAL", "XBOXA_CALO_BIAS2_CAL"],
            ["XBOXA_AC_BIAS_VIN_CAL", "XBOXA_AC_BIAS_VOUT_CAL"],
            ["XBOXA_JFET_TEMP_CAL", "XBOXA_JFET_HTR_VIN_CAL", "XBOXA_JFET_HTR_VOUT_CAL"]
        ],
    'xboxb' :
        [
            ["XBOXB_TEMP%d_CAL" % i for i in range(0,5)],
            ["XBOXB_JFET_VSS_CAL", "XBOXB_JFET_VDD1_CAL", "XBOXB_JFET_VDD2_CAL"],
            ["XBOXB_CALO_BIAS1_CAL", "XBOXB_CALO_BIAS2_CAL"],
            ["XBOXB_AC_BIAS_VIN_CAL", "XBOXB_AC_BIAS_VOUT_CAL"],
            ["XBOXB_JFET_TEMP_CAL", "XBOXB_JFET_HTR_VIN_CAL", "XBOXB_JFET_HTR_VOUT_CAL"]
        ]
    }
dbhks_dist_pcu={
    'dist_pcu':
        [
            'PCU_BUS_V_CAL',
            'PCU_SXS_FWE_I_CAL',
            'SXS_DIST_BUS_V_CAL',
            'SXS_DIST_DIST_CNTL_I_CAL',
            'SXS_DIST_SXS_SWRA_I_CAL',
            'SXS_DIST_SXS_SWRB_I_CAL',
            'SXS_DIST_PSPA_XBOXA_I_CAL',
            'SXS_DIST_PSPB_XBOXB_I_CAL',
            'SXS_DIST_CDA_CNTL_I_CAL',
            'SXS_DIST_CDA_DRV1_I_CAL',
            'SXS_DIST_CDA_DRV2_I_CAL',
            'SXS_DIST_CDB_CNTL_I_CAL',
            'SXS_DIST_CDB_DRV1_I_CAL',
            'SXS_DIST_CDB_DRV2_I_CAL',
            'SXS_DIST_JTD_I_CAL',
            'SXS_DIST_ADRC_PSYNC_I_CAL',
        ],
    }
dbhks_fwe={
    'power_gse2':
        ["{}_FWE".format(i) for i in ['V', 'I', 'P']],
    'dist_pcu':
        [
            'PCU_BUS_V_CAL',
            'PCU_SXS_FWE_I_CAL',
        ],
    'fwe' :
        [
            [['FWE_DCDC{}_{}_V_CAL'.format(i+1,j) for i in range(2)] for j in ['P05', 'P15', 'N15']],
            ['FWE_V_MOT{}_CAL'.format(i+1) for i in range(2)],
            [['FWE_I_MOT{}{}_CAL'.format(i+1,j) for i in range(2)] for j in ABs],
            ['FWE_T_MOT_SUPPLY{}_CAL'.format(i+1) for i in range(2)],
            ['FWE_T_MOT{}_CAL'.format(i+1) for i in range(2)],
            ['FWE_FW_POSITION{}_CAL'.format(i+1) for i in range(2)],
            [['FWE_{}_IN_HV{}_CAL'.format(j, i+1) for i in range(2)] for j in ['V', 'I']],
            [['FWE_{}_LED{}_CAL'.format(j, i+1) for i in range(4)] for j in ['V', 'I', 'T']],
            [['FWE_LED{}_PLS_{}_CAL'.format(i+1,j) for i in range(4)] for j in ['SPC', 'LEN']],
            ['FWE_HV{}_ON_OFF'.format(i+1) for i in range(2)],
            ['FWE_HV{}_LEVEL'.format(i+1) for i in range(2)],
            ['FWE_LED{}_ON_OFF'.format(i+1) for i in range(4)],
            ['FWE_TI_DIST_GEN_CNT', 'FWE_TIMECODE_GEN_CNT', 'FWE_SPW_RECONNECT_CNT','FWE_RMAP_REJ_CNT']
        ],
    }

dbhks_plumbing={
    'power_gse2':
        ["{}_SXS_FAN".format(i) for i in ['V', 'I', 'P']],
#    'temp' :
#        [temp_cda, temp_cdb, temp_jtd, temp_adrc],
    'shi_gse':
        ['p_HE_TANK1', 'p_HE_TANK2', 'p_DMS', 'Flow_rate_L', 'Flow_rate_S', 'LD1', 'LD2', 'LD3', 'FAN_V', 'FAN_I', 'HE_TANK4',  'JT_SHLD5', 'IVCS5', 'MVCS3', 'OVCS3', 'DMS3'],
    'dmsfbv_gse':
        ['dP1', 'dP2'],
    }
dbhks_lhp_gse={
    'lhp_gse':
    [
        [[["T_LHP_{}_SC{}_{}_P".format(_i, _j, _k) for _i in ['S', 'V', 'L', 'C']] for _j in ABs] for _k in ['CMP', 'CHD']],
        [[["T_LHP_{}_SC{}_{}_R".format(_i, _j, _k) for _i in ['S']] for _j in ABs] for _k in ['CMP', 'CHD']],
        [[["V_LHP_S_SC{}_{}_{}".format(_i, _j, _k) for _i in ABs] for _j in ['CMP', 'CHD']] for _k in ['P', 'R']],
        [[["I_LHP_S_SC{}_{}_{}".format(_i, _j, _k) for _i in ABs] for _j in ['CMP', 'CHD']] for _k in ['P', 'R']],
        ['V_LHP_V_SC{}'.format(_i) for _i in ABs],
        ['I_LHP_V_SC{}'.format(_i) for _i in ABs],
    ]
    }
dbhks_temp_dwr={
    'coolers' :
        [r_cda, r_cdb, r_jtd],
    'temp' :
        [temp_cda, temp_cdb, temp_jtd, temp_adrc],
    'shi_gse':
        ['HE_TANK4', 'JT_SHLD5', 'IVCS5', 'MVCS3', 'OVCS3', 'DMS3', 'JT_4KSTG', 'ORIFI_IN', 'BYPSS_IN',
        'HEX1HPOUT', 'HEX2HPOUT', 'HEX3HPIN', 'HEX3LPOUT', 'HEX2LPOUT', 'HEX1LPOUT', '1STSTAGE', '2NDSTAGE'],
    }    
dbhks_temp_cc={
    'temp' :
        [temp_cda, temp_cdb, temp_jtd],
    }
dbhks_temp_csi={
    'temp' :
        [temp_adrc],
    }
dbhks_adrg={
    'adrg' :
        [temp_adrg],
    'coolers' :
        [r_cda, r_cdb, r_jtd],
    }   
dbhks_env={
    'env' :
        env0_columns[2:6],
    'pc1' :
        env1_columns[2:],
    'pc2' :
        env2_columns[2:],
    'pc3' :
        env3_columns[1:],
    'tvac1' :
        env4_columns[1:],
    'tvac2' :
        env5_columns[2:],
    }
dbhks_cde_pow={
    'power_gse' :
        [
            "V_CDA_CTRL","I_CDA_CTRL","P_CDA_CTRL",
            "V_CDA_DRV1","I_CDA_DRV1","P_CDA_DRV1",
            "V_CDA_DRV2","I_CDA_DRV2","P_CDA_DRV2",    
            "V_CDB_CTRL","I_CDB_CTRL","P_CDB_CTRL",
            "V_CDB_DRV1","I_CDB_DRV1","P_CDB_DRV1",
            "V_CDB_DRV2","I_CDB_DRV2","P_CDB_DRV2",
            "V_JTD","I_JTD","P_JTD"
        ],
    'dist_pcu':
        [
            'SXS_DIST_BUS_V_CAL',
            'SXS_DIST_CDA_CNTL_I_CAL',
            'SXS_DIST_CDA_DRV1_I_CAL',
            'SXS_DIST_CDA_DRV2_I_CAL',
            'SXS_DIST_CDB_CNTL_I_CAL',
            'SXS_DIST_CDB_DRV1_I_CAL',
            'SXS_DIST_CDB_DRV2_I_CAL',
        ],
    }
dbhks_sc_eff={
    'dist_pcu':
        [
            'SXS_DIST_BUS_V_CAL',
            'SXS_DIST_CDA_CNTL_I_CAL',
            'SXS_DIST_CDA_DRV1_I_CAL',
            'SXS_DIST_CDA_DRV2_I_CAL',
            'SXS_DIST_CDB_CNTL_I_CAL',
            'SXS_DIST_CDB_DRV1_I_CAL',
            'SXS_DIST_CDB_DRV2_I_CAL',
        ],
    'temp' :
        [["CD%s" % i for i in ABs], [[["%s%s_1ST" % (i,j) for i in STs] for j in ABs]], [[["%s%s_2ND" % (i,j) for i in STs] for j in ABs]]],
    'coolers' : 
        [
        [[[['CD%s_%s%s_%s_%s_CAL' % (i, j, i, k, l) for i in ABs] for j in STs] for k in Units] for l in VIPs],
        ],
    'power_gse' :
        [
            "V_CDA_CTRL","I_CDA_CTRL","P_CDA_CTRL",
            "V_CDA_DRV1","I_CDA_DRV1","P_CDA_DRV1",
            "V_CDA_DRV2","I_CDA_DRV2","P_CDA_DRV2",    
            "V_CDB_CTRL","I_CDB_CTRL","P_CDB_CTRL",
            "V_CDB_DRV1","I_CDB_DRV1","P_CDB_DRV1",
            "V_CDB_DRV2","I_CDB_DRV2","P_CDB_DRV2",
            "V_JTD","I_JTD","P_JTD"
        ],
    }
dbhks_pc_eff=dbhks_sc_eff
dbhks_rates={
    'temp': 
        ['ST1_CTL'],
    'psp_stat3' :
        [
            ["%s_CPUTIME_US_IDLE" % i for i in Quads],
        ],
    'psp_stat1' :
        [
            ["A%1d_P%02d_HP_CNT" % (i/9,i) for i in range(0,18)],
            ["B%1d_P%02d_HP_CNT" % (i/9,i+18) for i in range(0,18)],
            ["%s_ACP_EDB_CNT" % i for i in Quads],
            ["%s_ACP_LOST_CNT" % i for i in Quads]
        ]
    }

dbhks_lc={
    'temp': 
        ['ST1_CTL'],
    'psp_stat3' :
        [
            ["%s_CPUTIME_US_IDLE" % i for i in Quads],
        ],
    'psp_stat1' :
        [
            ["A%1d_P%02d_PEDB_CNT" % (i/9,i) for i in range(0,18)],
            ["B%1d_P%02d_PEDB_CNT" % (i/9,i+18) for i in range(0,18)],
            ["A%1d_P%02d_REJ_CNT" % (i/9,i) for i in range(0,18)],
            ["B%1d_P%02d_REJ_CNT" % (i/9,i+18) for i in range(0,18)],
        ],
    'lc_el' :
        [
            ["LTF{:02d}".format(i) for i in range(0,36)],
        ],
    'lc_ac' :
        [
            ["AC", "AC_BL"],
        ],
    'lc_px_uf' :
        [
            [["{}{:02d}".format(g,p) for g in ['HP', 'MP', 'MS', 'LP', 'LS'] for p in range(0,36)]],
        ]

    }

dbhks_nm={
    'nm' : [['ADUMAX%02d' % _p, 'ADUSTD%02d' % _p] for _p in range(36)]
}

dbhks_bl={
    'bl' : [['BL_MEAN%02d' % _p, 'BL_STD%02d' % _p] for _p in range(36)]
}

dbhks_gain={
    'ghf' : ['PIXEL', 'COR_FIT', 'COR_AVE', 'CHISQ', 'AVGUNBIN', 'AVGBIN', 'AVGFIT', 'SHIFT', 'BGRND', 'SLOPE', 'WIDTH', 'TELAPSE', 'EXPOSURE', 'NEVENT']
}

dbhks_xtend={
    'pcu' : [
        'PCU_SXI_CD_I_CAL', 'PCU_SXI_DE_I_CAL', 'PCU_SXI_PE_I_CAL', 'PCU_BUS_V_CAL',
    ],
    'sxi_cd' : [
        'SXI_CD_CMP_FREQ_SET_CAL', 'SXI_CD_DRVA_BLN_PHASE_OFFSET_CAL', 'SXI_CD_DRVB_BLN_PHASE_OFFSET_CAL',
        [[['SXI_CD_DRV{}_{}_{}_CAL'.format(i,j,k) for i in ABs] for j in ['CMP', 'BLN']] for k in ['V', 'I', 'P']]
    ],
}

dbhks_pcu_pow={
    'pcu' : [
        'PCU_SHNT1_I_CAL', 'PCU_SHNT2_I_CAL', 'PCU_SHNT_CNT_V_CAL', 'PCU_LOAD_I_CAL', 'PCU_PCU_I_CAL', 'PCU_BUS_V_CAL',
        [['PCU_{}_{}_I_CAL'.format(C,AB) for AB in ABs] for C in ['SMU', 'AOCP', 'DR', 'SWR1', 'SWR2', 'SWR3', 'HCE', 'ACIM_MTQ', 'ACIM_RW_IRU_SG', 'ACIM_RCS']],
        'PCU_M_HCE_I_CAL', 'PCU_MSE_I_CAL',  'PCU_TCIM_STRP_I_CAL', 'PCU_TCIM_XMOD_XPA_I_CAL', 'PCU_GPSP_I_CAL',
        ['PCU_ACIM_RCS_{}DRV_I_CAL'.format(AB) for AB in ABs],
        ['PCU_ACIM_STT_STT{:d}_I_CAL'.format(N+1) for N in range(3)],
        ['PCU_RW{:d}_I_CAL'.format(N+1) for N in range(4)],
        ['PCU_IRU_{}_I_CAL'.format(C) for C in ['A2', 'A3', 'B1', 'B2']]
    ],
}

dbhks_eps={
    'pcu' : [
        'PCU_SHNT1_I_CAL', 'PCU_SHNT2_I_CAL', 'PCU_SHNT_CNT_V_CAL', 'PCU_LOAD_I_CAL', 'PCU_PCU_I_CAL', 'PCU_BUS_V_CAL',
    ],
    'bccu1':[
        ['BCCU1_BAT1_CEL_{:d}V_CAL'.format(i+1) for i in range(12)],
        'BCCU1_BAT1_BAT_V_CAL', 'BCCU1_BAT1_BAT_I1_CHG_CAL', 'BCCU1_BAT1_BAT_I2_CHG_CAL', 'BCCU1_BAT1_BAT_I1_DIS_CAL', 'BCCU1_BAT1_BAT_I2_DIS_CAL'
    ],
    'bccu2' :[
        ['BCCU2_BAT2_CEL_{:d}V_CAL'.format(i+1) for i in range(12)],
        'BCCU2_BAT2_BAT_V_CAL', 'BCCU2_BAT2_BAT_I1_CHG_CAL', 'BCCU2_BAT2_BAT_I2_CHG_CAL', 'BCCU2_BAT2_BAT_I1_DIS_CAL', 'BCCU2_BAT2_BAT_I2_DIS_CAL'
    ],
}

dbhks_aocs={
    'pcu' : [
            ['PCU_RW{:d}_I_CAL'.format(i+1) for i in range(4)],
            ['PCU_ACIM_MTQ_{}_I_CAL'.format(s) for s in ABs],
            ['PCU_IRU_B2_I_CAL', 'PCU_IRU_B1_I_CAL', 'PCU_IRU_A3_I_CAL', 'PCU_IRU_A2_I_CAL'],
            ],
    'acpa_nom' : [
                ['ACPA_RW{:d}_RATE_CAL'.format(i+1) for i in range(4)],
                #['ACPA_IRU_A_TDG2_ON_OFF_ST', 'ACPA_IRU_A_TDG3_ON_OFF_ST', 'ACPA_IRU_B_TDG1_ON_OFF_ST', 'ACPA_IRU_B_TDG2_ON_OFF_ST'],
                ],
    'acpa_mtq_a' : [
                ['ACPA_ACIM_MTQ_A_MTQ_{}_MAGMOM_CAL'.format(i) for i in ['X', 'Y', 'Z']],
                ['ACPA_ACIM_MTQ_A_MTQ_{}_SIGN'.format(i) for i in ['X', 'Y', 'Z']],
    ],
    'acpa_mtq_b' : [
                ['ACPA_ACIM_MTQ_B_MTQ_{}_MAGMOM_CAL'.format(i) for i in ['X', 'Y', 'Z']],
                ['ACPA_ACIM_MTQ_B_MTQ_{}_SIGN'.format(i) for i in ['X', 'Y', 'Z']],
    ],
    #'acpa_nm_1' : [
    #            ['ACPA_MTQ_{}_MAG_MOM'.format(s) for s in ['X', 'Y','Z']],
    #            ],
    #'acpa_nm_2' : [
    #            ['ACPA_RW{:d}_RATE_COR_CAL'.format(i+1) for i in range(4)],
    #            ['ACPA_RW{:d}_TC_CAL'.format(i+1) for i in range(4)],
    #            ],
    "acpa_sg" : [
            ["ACPA_GAS_GEOMAG_BDY_{}_CAL".format(i) for i in ['X', 'Y', 'Z']],
            ["ACPA_ENV_GEOMAG_{}_CAL".format(i) for i in ['X', 'Y', 'Z']],
            ["ACPA_IRU_A_TDG{}_RATE_CAL".format(i) for i in ['2_B', '2_C', '3_C', '3_A']],
            ["ACPA_IRU_B_TDG{}_RATE_CAL".format(i) for i in ['1_A', '1_B', '2_B', '2_C']],
            ["ACPA_DSS_ANG{:d}_CAL".format(i+1) for i in range(2)],
            ],
    'acpb_nom' : [
                ['ACPB_RW{:d}_RATE_CAL'.format(i+1) for i in range(4)],
                ['ACPB_IRU_A_TDG2_ON_OFF_ST', 'ACPB_IRU_A_TDG3_ON_OFF_ST', 'ACPB_IRU_B_TDG1_ON_OFF_ST', 'ACPb_IRU_B_TDG2_ON_OFF_ST'],
                ],
    'acpb_mtq_a' : [
                ['ACPB_ACIM_MTQ_A_MTQ_{}_MAGMOM_CAL'.format(i) for i in ['X', 'Y', 'Z']],
                ['ACPB_ACIM_MTQ_A_MTQ_{}_SIGN'.format(i) for i in ['X', 'Y', 'Z']],
    ],
    'acpb_mtq_b' : [
                ['ACPB_ACIM_MTQ_B_MTQ_{}_MAGMOM_CAL'.format(i) for i in ['X', 'Y', 'Z']],
                ['ACPB_ACIM_MTQ_B_MTQ_{}_SIGN'.format(i) for i in ['X', 'Y', 'Z']],
    ],
    #'acpb_nm_1' : [
    #            ['ACPB_MTQ_{}_MAG_MOM'.format(s) for s in ['X', 'Y','Z']],
    #            ],
    #'acpb_nm_2' : [
    #            ['ACPB_RW{:d}_RATE_COR_CAL'.format(i+1) for i in range(4)],
    #            ['ACPB_RW{:d}_TC_CAL'.format(i+1) for i in range(4)],
    #            ],
    "acpb_sg" : [
            ["ACPB_GAS_GEOMAG_BDY_{}_CAL".format(i) for i in ['X', 'Y', 'Z']],
            ["ACPB_ENV_GEOMAG_{}_CAL".format(i) for i in ['X', 'Y', 'Z']],
            ["ACPB_IRU_A_TDG{}_RATE_CAL".format(i) for i in ['2_B', '2_C', '3_C', '3_A']],
            ["ACPB_IRU_B_TDG{}_RATE_CAL".format(i) for i in ['1_A', '1_B', '2_B', '2_C']],
            ["ACPB_DSS_ANG{:d}_CAL".format(i+1) for i in range(2)],
    ]
}

dbhks_hce={
    'smu_a_hce_a' : [
        "HCE_A_STS_SP1U_TEMP_CAL", "HCE_A_STS_SP2U_TEMP_CAL", "HCE_A_STS_SP2M_TEMP_CAL", "HCE_A_STS_SP3U_TEMP_CAL", "HCE_A_STS_SP3L_TEMP_CAL", "HCE_A_STS_SP4U_TEMP_CAL", "HCE_A_STS_SP4L_TEMP_CAL", "HCE_A_STS_SP5U_TEMP_CAL", "HCE_A_STS_SP5L_TEMP_CAL", "HCE_A_STS_SP6U_TEMP_CAL", "HCE_A_STS_SP6L_TEMP_CAL", "HCE_A_STS_SP8U_TEMP_CAL", "HCE_A_STS_SP8L_TEMP_CAL", "HCE_A_STS_SXS_RAD6_1_BU_TEMP_CAL", "HCE_A_STS_SXS_RAD6_2_PR_TEMP_CAL", "HCE_A_STS_SXS_RAD8_1_BU_TEMP_CAL", "HCE_A_STS_SXS_RAD8_2_PR_TEMP_CAL", "HCE_A_STS_SXS_BRK6_PR_TEMP_CAL", "HCE_A_STS_SXS_BRK8_BU_TEMP_CAL", "HCE_A_STS_SXS_CP6_PR_TEMP_CAL", "HCE_A_STS_SXS_CP8_BU_TEMP_CAL", "HCE_A_STS_SXS_PSP_PR_TEMP_CAL", "HCE_A_STS_SXS_PSU_BU_TEMP_CAL", "HCE_A_STS_SXS_CDE_BU_TEMP_CAL", "HCE_A_STS_XBOX_PR_TEMP_CAL", "HCE_A_STS_SXS_ADRC_TEMP_CAL", "HCE_A_STS_SXT_S_PR4_TEMP_CAL", "HCE_A_STS_SXT_S_BU1_TEMP_CAL", "HCE_A_STS_SXT_S_BU2_TEMP_CAL", "HCE_A_STS_SXT_S_BU3_TEMP_CAL", "HCE_A_STS_SXS_JT_PR_CMP_L1_TEMP_CAL", "HCE_A_STS_SXS_JT_BU_CMP_H1_TEMP_CAL"
    ],
    'smu_a_hce_b' : [
        "HCE_B_STS_SXS_RAD6_1_PR_TEMP_CAL", "HCE_B_STS_SXS_RAD6_2_BU_TEMP_CAL", "HCE_B_STS_SXS_RAD8_1_PR_TEMP_CAL", "HCE_B_STS_SXS_RAD8_2_BU_TEMP_CAL", "HCE_B_STS_SXS_BRK6_BU_TEMP_CAL", "HCE_B_STS_SXS_BRK8_PR_TEMP_CAL", "HCE_B_STS_SXS_CP6_BU_TEMP_CAL", "HCE_B_STS_SXS_CP8_PR_TEMP_CAL", "HCE_B_STS_SXS_PSP_BU_TEMP_CAL", "HCE_B_STS_SXS_PSU_PR_TEMP_CAL", "HCE_B_STS_SXS_CDE_PR_TEMP_CAL", "HCE_B_STS_XBOX_BU_TEMP_CAL", "HCE_B_STS_SXT_S_PR1_TEMP_CAL", "HCE_B_STS_SXT_S_PR2_TEMP_CAL", "HCE_B_STS_SXT_S_PR3_TEMP_CAL", "HCE_B_STS_SXT_S_BU4_TEMP_CAL", "HCE_B_STS_SXS_JT_BU_CMP_L1_TEMP_CAL", "HCE_B_STS_SXS_JT_PR_CMP_H1_TEMP_CAL"
    ],
    'smu_a_mhce' : [
        "M_HCE_STS_SXS_JT_PR_CMP_L2_TEMP_CAL", "M_HCE_STS_SXS_JT_PR_CMP_H2_TEMP_CAL"
    ],
    'smu_b_hce_a' : [
        "HCE_A_STS_SP1U_TEMP_CAL", "HCE_A_STS_SP2U_TEMP_CAL", "HCE_A_STS_SP2M_TEMP_CAL", "HCE_A_STS_SP3U_TEMP_CAL", "HCE_A_STS_SP3L_TEMP_CAL", "HCE_A_STS_SP4U_TEMP_CAL", "HCE_A_STS_SP4L_TEMP_CAL", "HCE_A_STS_SP5U_TEMP_CAL", "HCE_A_STS_SP5L_TEMP_CAL", "HCE_A_STS_SP6U_TEMP_CAL", "HCE_A_STS_SP6L_TEMP_CAL", "HCE_A_STS_SP8U_TEMP_CAL", "HCE_A_STS_SP8L_TEMP_CAL", "HCE_A_STS_SXS_RAD6_1_BU_TEMP_CAL", "HCE_A_STS_SXS_RAD6_2_PR_TEMP_CAL", "HCE_A_STS_SXS_RAD8_1_BU_TEMP_CAL", "HCE_A_STS_SXS_RAD8_2_PR_TEMP_CAL", "HCE_A_STS_SXS_BRK6_PR_TEMP_CAL", "HCE_A_STS_SXS_BRK8_BU_TEMP_CAL", "HCE_A_STS_SXS_CP6_PR_TEMP_CAL", "HCE_A_STS_SXS_CP8_BU_TEMP_CAL", "HCE_A_STS_SXS_PSP_PR_TEMP_CAL", "HCE_A_STS_SXS_PSU_BU_TEMP_CAL", "HCE_A_STS_SXS_CDE_BU_TEMP_CAL", "HCE_A_STS_XBOX_PR_TEMP_CAL", "HCE_A_STS_SXS_ADRC_TEMP_CAL", "HCE_A_STS_SXT_S_PR4_TEMP_CAL", "HCE_A_STS_SXT_S_BU1_TEMP_CAL", "HCE_A_STS_SXT_S_BU2_TEMP_CAL", "HCE_A_STS_SXT_S_BU3_TEMP_CAL", "HCE_A_STS_SXS_JT_PR_CMP_L1_TEMP_CAL", "HCE_A_STS_SXS_JT_BU_CMP_H1_TEMP_CAL"
    ],
    'smu_b_hce_b' : [
        "HCE_B_STS_SXS_RAD6_1_PR_TEMP_CAL", "HCE_B_STS_SXS_RAD6_2_BU_TEMP_CAL", "HCE_B_STS_SXS_RAD8_1_PR_TEMP_CAL", "HCE_B_STS_SXS_RAD8_2_BU_TEMP_CAL", "HCE_B_STS_SXS_BRK6_BU_TEMP_CAL", "HCE_B_STS_SXS_BRK8_PR_TEMP_CAL", "HCE_B_STS_SXS_CP6_BU_TEMP_CAL", "HCE_B_STS_SXS_CP8_PR_TEMP_CAL", "HCE_B_STS_SXS_PSP_BU_TEMP_CAL", "HCE_B_STS_SXS_PSU_PR_TEMP_CAL", "HCE_B_STS_SXS_CDE_PR_TEMP_CAL", "HCE_B_STS_XBOX_BU_TEMP_CAL", "HCE_B_STS_SXT_S_PR1_TEMP_CAL", "HCE_B_STS_SXT_S_PR2_TEMP_CAL", "HCE_B_STS_SXT_S_PR3_TEMP_CAL", "HCE_B_STS_SXT_S_BU4_TEMP_CAL", "HCE_B_STS_SXS_JT_BU_CMP_L1_TEMP_CAL", "HCE_B_STS_SXS_JT_PR_CMP_H1_TEMP_CAL"
    ],
    'smu_b_mhce' : [
        "M_HCE_STS_SXS_JT_PR_CMP_L2_TEMP_CAL", "M_HCE_STS_SXS_JT_PR_CMP_H2_TEMP_CAL"
    ],
}

dbhks_lhp_a={
    'temp' :
        [temp_cda, temp_cdb, temp_jtd],
    'smu_a_hce_a' : [
        "HCE_A_STS_SXS_LHP_L_SCA_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCA_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCA_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCA_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCB_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCB_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCB_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCB_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHPS_SCB_CMP_BU_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCA_CMP_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCA_CMP_ON_OFF", "HCE_A_STS_SXS_LHP_L_SCA_CHD_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCA_CHD_ON_OFF", "HCE_A_STS_SXS_LHP_L_SCB_CMP_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCB_CMP_ON_OFF", "HCE_A_STS_SXS_LHPS_SCB_CMP_BU_ON_OFF", "HCE_A_STS_SXS_LHP_L_SCB_CHD_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCB_CHD_ON_OFF"
    ],
    'smu_a_hce_b' : [
        "HCE_B_STS_SXS_LHP_V_SCA_CHD_TEMP_CAL", "HCE_B_STS_SXS_LHPS_SCA_CMP_BU_TEMP_CAL", "HCE_B_STS_SXS_LHP_V_SCB_CHD_TEMP_CAL", "HCE_B_STS_SXS_LHPS_SCA_CMP_BU_ON_OFF", "HCE_B_STS_SXS_LHP_V_SCA_CHD_ON_OFF", "HCE_B_STS_SXS_LHP_V_SCB_CHD_ON_OFF"
    ],
    'smu_a_mhce' : [
        "M_HCE_STS_SXS_LHP_V_SCA_CMP_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCA_CMP_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCA_CHD_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCA_CHD_BU_TEMP_CAL", "M_HCE_STS_SXS_LHP_V_SCB_CMP_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCB_CMP_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCB_CHD_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCB_CHD_BU_TEMP_CAL", "M_HCE_STS_SXS_LHP_V_SCA_CMP_ON_OFF", "M_HCE_STS_SXS_LHPS_SCA_CMP_PR_ON_OFF", "M_HCE_STS_SXS_LHPS_SCA_CHD_PR_ON_OFF", "M_HCE_STS_SXS_LHPS_SCA_CHD_BU_ON_OFF", "M_HCE_STS_SXS_LHP_V_SCB_CMP_ON_OFF", "M_HCE_STS_SXS_LHPS_SCB_CMP_PR_ON_OFF", "M_HCE_STS_SXS_LHPS_SCB_CHD_BU_ON_OFF"
    ],
}
dbhks_lhp_b={
    'temp' :
        [temp_cda, temp_cdb, temp_jtd],
    'smu_b_hce_a' : [
        "HCE_A_STS_SXS_LHP_L_SCA_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCA_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCA_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCA_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCB_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCB_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCB_CMP_TEMP_CAL", "HCE_A_STS_SXS_LHP_C_SCB_CHD_TEMP_CAL", "HCE_A_STS_SXS_LHPS_SCB_CMP_BU_TEMP_CAL", "HCE_A_STS_SXS_LHP_L_SCA_CMP_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCA_CMP_ON_OFF", "HCE_A_STS_SXS_LHP_L_SCA_CHD_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCA_CHD_ON_OFF", "HCE_A_STS_SXS_LHP_L_SCB_CMP_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCB_CMP_ON_OFF", "HCE_A_STS_SXS_LHPS_SCB_CMP_BU_ON_OFF", "HCE_A_STS_SXS_LHP_L_SCB_CHD_ON_OFF", "HCE_A_STS_SXS_LHP_C_SCB_CHD_ON_OFF"
    ],
    'smu_b_hce_b' : [
        "HCE_B_STS_SXS_LHP_V_SCA_CHD_TEMP_CAL", "HCE_B_STS_SXS_LHPS_SCA_CMP_BU_TEMP_CAL", "HCE_B_STS_SXS_LHP_V_SCB_CHD_TEMP_CAL", "HCE_B_STS_SXS_LHPS_SCA_CMP_BU_ON_OFF", "HCE_B_STS_SXS_LHP_V_SCA_CHD_ON_OFF", "HCE_B_STS_SXS_LHP_V_SCB_CHD_ON_OFF"
    ],
    'smu_b_mhce' : [
        "M_HCE_STS_SXS_LHP_V_SCA_CMP_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCA_CMP_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCA_CHD_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCA_CHD_BU_TEMP_CAL", "M_HCE_STS_SXS_LHP_V_SCB_CMP_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCB_CMP_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCB_CHD_PR_TEMP_CAL", "M_HCE_STS_SXS_LHPS_SCB_CHD_BU_TEMP_CAL", "M_HCE_STS_SXS_LHP_V_SCA_CMP_ON_OFF", "M_HCE_STS_SXS_LHPS_SCA_CMP_PR_ON_OFF", "M_HCE_STS_SXS_LHPS_SCA_CHD_PR_ON_OFF", "M_HCE_STS_SXS_LHPS_SCA_CHD_BU_ON_OFF", "M_HCE_STS_SXS_LHP_V_SCB_CMP_ON_OFF", "M_HCE_STS_SXS_LHPS_SCB_CMP_PR_ON_OFF", "M_HCE_STS_SXS_LHPS_SCB_CHD_BU_ON_OFF"
    ],
}
dbhks_lhp = dbhks_lhp_a

dbhks_vis_a={
    'smu_a_hce_a' : [
        "HCE_A_STS_SXS_VIS_SCA_BU_TEMP_CAL", "HCE_A_STS_SXS_VIS_PCB_BU_TEMP_CAL", "HCE_A_STS_SXS_VIS_SCA_BU_ON_OFF", "HCE_A_STS_SXS_VIS_PCB_BU_ON_OFF"
    ],
    'smu_a_hce_b' : [
        "HCE_B_STS_SXS_VIS_SCB_BU_TEMP_CAL", "HCE_B_STS_SXS_VIS_PCA_BU_TEMP_CAL", "HCE_B_STS_SXS_VIS_SCB_BU_ON_OFF", "HCE_B_STS_SXS_VIS_PCA_BU_ON_OFF"
    ],
    'smu_a_mhce' : [
        "M_HCE_STS_SXS_VIS_SCA_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_PCA_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_SCB_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_PCB_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_SCA_PR_ON_OFF", "M_HCE_STS_SXS_VIS_SCB_PR_ON_OFF", "M_HCE_STS_SXS_VIS_PCA_PR_ON_OFF", "M_HCE_STS_SXS_VIS_PCB_PR_ON_OFF"
    ],
    'smu_a_mse' : [
        'MSE_SXS_VIS1_SC_A_ST',  'MSE_SXS_VIS2_SC_B_ST', 'MSE_SXS_VIS3_PC_A_ST', 'MSE_SXS_VIS4_PC_B_ST'
    ],
    'smu_b_mse' : [
        'MSE_SXS_VIS1_SC_A_ST',  'MSE_SXS_VIS2_SC_B_ST', 'MSE_SXS_VIS3_PC_A_ST', 'MSE_SXS_VIS4_PC_B_ST'
    ],
}
dbhks_vis_b={
    'smu_b_hce_a' : [
        "HCE_A_STS_SXS_VIS_SCA_BU_TEMP_CAL", "HCE_A_STS_SXS_VIS_PCB_BU_TEMP_CAL", "HCE_A_STS_SXS_VIS_SCA_BU_ON_OFF", "HCE_A_STS_SXS_VIS_PCB_BU_ON_OFF"
    ],
    'smu_b_hce_b' : [
        "HCE_B_STS_SXS_VIS_SCB_BU_TEMP_CAL", "HCE_B_STS_SXS_VIS_PCA_BU_TEMP_CAL", "HCE_B_STS_SXS_VIS_SCB_BU_ON_OFF", "HCE_B_STS_SXS_VIS_PCA_BU_ON_OFF"
    ],
    'smu_b_mhce' : [
        "M_HCE_STS_SXS_VIS_SCA_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_PCA_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_SCB_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_PCB_PR_TEMP_CAL", "M_HCE_STS_SXS_VIS_SCA_PR_ON_OFF", "M_HCE_STS_SXS_VIS_SCB_PR_ON_OFF", "M_HCE_STS_SXS_VIS_PCA_PR_ON_OFF", "M_HCE_STS_SXS_VIS_PCB_PR_ON_OFF"
    ],
}
dbhks_vis = dbhks_vis_a

dbhks_dr_a={
    'dr_a' : [
        "DR_A_UNUSED_PE_NUM", "DR_A_REP_QUE_CNT"
    ],
    'dr_a_lv1' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*0) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*0,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv2' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*1) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*1,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv3' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*2) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*2,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv4' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*3) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*3,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv5' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*4) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*4,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv6' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*5) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*5,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv7' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*6) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*6,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_a_lv8' : [
        ["DR_A_LV_{:02X}_CURR_PE".format(i+16*7) for i in range(16)],
        [["DR_A_LV_{:02X}_USED_PE{:d}".format(i+16*7,j+1) for i in range(16)] for j in range(31)],
    ],   
}
dbhks_dr_b={
    'dr_b' : [
        "DR_B_UNUSED_PE_NUM", "DR_B_REP_QUE_CNT"
    ],
    'dr_b_lv1' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*0) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*0,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv2' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*1) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*1,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv3' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*2) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*2,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv4' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*3) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*3,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv5' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*4) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*4,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv6' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*5) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*5,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv7' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*6) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*6,j+1) for i in range(16)] for j in range(31)],
    ],
    'dr_b_lv8' : [
        ["DR_B_LV_{:02X}_CURR_PE".format(i+16*7) for i in range(16)],
        [["DR_B_LV_{:02X}_USED_PE{:d}".format(i+16*7,j+1) for i in range(16)] for j in range(31)],
    ], 
}
dbhks_dr=dbhks_dr_a

dbhks_rf_a={
    'pcu' : [
        'PCU_TCIM_STRP_I_CAL', 'PCU_TCIM_XMOD_XPA_I_CAL','PCU_GPSP_I_CAL'
    ],
    'smu_a_mse' : [
        'MSE_XMOD_PWR_ST', 'MSE_XMOD_MODULATION_ST','MSE_XPA_PWR_MONI', 'MSE_XPA_XPA_ONOFF','MSE_XPA_CURRENT_MONI',
        'MSE_GPSR_ORBIT_MODE', 'MSE_GPSR_ACQ_MODE', 'MSE_GPSR_OPR_MODE'
    ],
    'smu_a_tcim_s_a' : [
        'TCIM_S_A_TCIM_ID_1', 'TCIM_S_A_TCIM_ID_2', 'TCIM_S_A_STRP_SEL_ST', 'TCIM_S_A_TCIM_MODE', 'TCIM_S_A_TLM_RATE',
        ['TCIM_S_A_STRP_{}_USB_RNG_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_USB_MODE_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_QPSK_MODE_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_TX_ON_OFF'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_USB_TX_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_USB_TX_PWR_H_L'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_INP_LVL_CAL'.format(c) for c in ABs],
        ['TCIM_S_A_STRP_{}_AGC_VOLT_CAL'.format(c) for c in ABs],        
    ],
    'smu_a_tcim_s_b,' : [
        'TCIM_S_B_TCIM_ID_1', 'TCIM_S_B_TCIM_ID_2', 'TCIM_S_B_STRP_SEL_ST', 'TCIM_S_B_TCIM_MODE', 'TCIM_S_B_TLM_RATE',
        ['TCIM_S_B_STRP_{}_USB_RNG_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_USB_MODE_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_QPSK_MODE_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_TX_ON_OFF'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_USB_TX_ENA_DIS'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_USB_TX_PWR_H_L'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_INP_LVL_CAL'.format(c) for c in ABs],
        ['TCIM_S_B_STRP_{}_AGC_VOLT_CAL'.format(c) for c in ABs],       
    ],
    'smu_a_tcim_x' : [
        'TCIM_X_TCIM_ID_1', 'TCIM_X_TCIM_ID_2', 'TCIM_X_TRP_SEL_ST', 'TCIM_X_TCIM_MODE', 'TCIM_X_TLM_RATE',
    ],
}
dbhks_rf = dbhks_rf_a

# Palette
palette=('grey','red','blue','green','purple','orange', 'magenta','cyan','yellow','brown','violet','turquoise')


########################################################
# Functions
########################################################            
#-------------------------------------------------------
def flatten(l):
    "Flatten a list for dbhks."
    
    for el in l:
        if isinstance(el, collections.abc.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

#-------------------------------------------------------
def get_events(evtlists, datetime_start, datetime_end):
    "Get events from event list"

    def strpfile(x):
        try:
            y=datetime.datetime.strptime(x, '%Y/%m/%d %H:%M:%S')
        except:
            y=pd.NaT
        return y

    events_all=None
    for evtlist in evtlists:
        if (os.path.isfile(evtlist) and os.stat(evtlist).st_size > 0):
            events=pd.read_csv(evtlist, header=None, sep=' *, *')
            if (events_all is None):
                events_all = events
            else:
                events_all = pd.concat([events_all, events])                         
    if (events_all is not None):
        events_all.columns=['Event_start', 'Event_stop', 'Color', 'Event']
        events_all['Event_start'] = events_all['Event_start'].apply(strpfile)        
        events_all['Event_stop'] = events_all['Event_stop'].apply(strpfile)
    
    return events_all    

#-------------------------------------------------------
def annotate_events(events, fig, datetime_start, datetime_end):
    
    if (events is not None):
    
        if ( (datetime_end-datetime_start)/np.timedelta64(1, 's') > 24*3600):
            flg_simple=True
        else:
            flg_simple=False

        for index, row in events.iterrows():
            event_start, event_stop, i_color, label=row[0:4]       

            # Skip obvious labeling for clarity.
            if (label == 'nspec'):
                label = ''
            
            # Skip nspec and obsid for plots longer than a day. 
            if (flg_simple is True) and ( (i_color == 4) or (i_color == 5) ):
                continue

            # Line
            if (event_stop is pd.NaT):
                if (event_start >= datetime_start) and (event_start <= datetime_end):
                    add_line(fig, event_start, label, vert=True, color=palette[i_color], width=1, dash='dot')
            # Range
            else:
                if ((event_start >= datetime_start) and (event_start <= datetime_end)) or ((event_stop >= datetime_start) and (event_stop <= datetime_end)):
                    add_range(fig, event_start, event_stop, label, vert=True, color=palette[i_color])
    return fig

#-------------------------------------------------------
def collate_data_db(hks, infiles, datetime_start, datetime_end):
    "Collate data of multiple HKs from multile infiles in a sinlge db ."
    
    data_all = None    
    for infile in infiles:        
        
        # Open the infile.
        try:
            data = pd.read_pickle(infile)    
            print("Loaded %s." % infile)
        except:
            print("Cannot load %s." % infile)
            continue

        # Extract data in the infile                        
        try:
            data = data[hks].copy()
        except:
            hks_new=[]
            for hk in hks:
                if (hk in data.columns):
                    hks_new.append(hk)
                else:
                    print("%s was not found in %s." % (hk, infile))                                    
            data = data[hks_new].copy()
        
        # Concat data
        if data_all is None:
            data_all = data
        else:
            data_all = pd.concat([data_all, data])
        
    # Sort and uniq, filter by time.
    if ((data_all is None) or len(data_all)==0):        
        return None

    data_all.sort_index(inplace=True)
    data_all = data_all[datetime_start:datetime_end]    
    data_all = data_all[~data_all.index.duplicated(keep='last')]
    
    return data_all

#-------------------------------------------------------
def collate_data(indir, dbhks, datetime_start, datetime_end, resample=0, rates=None, quads=None):
    "Collate data of multile HKs from multile infiles in all dbs."

    data_all = None
    for db in dbhks.keys():
        
        # All infiles.
        infiles = glob.glob('%s/%s/*.pkl' % (indir, db))

        # Filter infiles by time        
        infiles = filter_by_datetime(infiles, 'range', datetime_start, datetime_end, base=db.count("_"))
        # Continue if no data.        
        if (len(infiles)==0):
            continue
        
        # HKs
        hks=list(flatten(dbhks[db]))
        if (rates is not None):
            hks_rates=[]
            for _r in rates:                
                hks_rate=[hk.replace("HP",_r.upper()) for hk in hks]
                hks_rates.extend(hks_rate)
            hks = hks_rates
        data = collate_data_db(hks, infiles, datetime_start, datetime_end)        

        # Concat data        
        if data_all is None:
            data_all = data
        else:
            data_all = pd.concat([data_all, data], axis=1)

    # resample if needed.
    if ( resample != 0) and ( data_all is not None):
        data_all=data_all.resample(str(resample) + 'S').median()

    return data_all

#-------------------------------------------------------
def plotly_hk(indir, dbhks, n_panels, plot_title, update_fig, outstem, datetime_start, datetime_end, resample, events, rates=None, quads=None, flg_adrc=True, flg_cde=True, flg_dist=True, flg_bus=False):
    "Plot HK data."

    # Collate data.
    data = collate_data(indir, dbhks, datetime_start, datetime_end, resample=resample, rates=rates, quads=quads)     
    
    if (data is None or len(data)==0):
        'No data found.'
        return None
    
    # Define fig.
    fig = make_subplots(
        rows=n_panels, cols=1, shared_xaxes=True, vertical_spacing=0.02
    )    
    fig = update_fig(fig, data, rates=rates, quads=quads, flg_adrc=flg_adrc, flg_cde=flg_cde, flg_dist=flg_dist, flg_bus=flg_bus)

    # Plot title
    str_start = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')
    if (resample == 0):
        _title='%s (%s - %s) updated at %s' % (plot_title, str_start, str_end, str_now)
    else:
        _title='%s (%s - %s; %d s resampled) updated at %s' % (plot_title, str_start, str_end, resample, str_now)
    
    # Define layout    
    Layout = go.Layout(
        title=_title,
        showlegend=True,
        width=1280, 
        height=200*n_panels,
        xaxis=dict(                      
            automargin = False,
            autorange = False, 
            range = [datetime_start, datetime_end],            
            ),
        yaxis=dict(
            exponentformat = 'e',
            )
        )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    # Annotation (common)
    fig = annotate_events(events, fig, datetime_start, datetime_end)
    
    # Output (multi-thread)    
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % outstem)
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % outstem)
    def write_file(): 
        save_files(data, None, outstem+'.csv', kind='csv')
        #print("File saved in %s.csv" % outstem)
    
    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    t3 = threading.Thread(target=write_file)
    #
    t1.start()
    t2.start()
    t3.start()
    #
    t1.join()
    t2.join()
    t3.join()

    return 0
    
#-------------------------------------------------------
def plot_data(fig, i_panel, datum, name, i_color=-1, ms=3, enum=None):

    _color=yield_color(i_color)    

    # Dummuy data if none.
    if (datum is None):
        Xs=[datetime_start,datetime_end]
        Ys=[np.nan, np.nan]
    else:
        # Replace enum if any.
        if (enum is not None):
            def replace_enum(datum, enum):
                for i, enum in enumerate(enum):
                    datum.replace(i, enum, inplace=True)
                return datum
            datum = replace_enum(datum, enum)
        # All
        Xs = datum.index
        if (type(datum) is pd.core.frame.DataFrame):
            Ys = datum.iloc[:,1].values
        else:
            Ys = datum.values

    fig.add_trace(go.Scattergl(x=Xs, y=Ys, 
                    mode='lines+markers', marker_size=ms, 
                    #legendgroup=i_panel, 
                    name='{:d}.{}'.format(i_panel,name), 
                    marker=dict(color=_color, opacity=0.8)
                    ), 
                row=i_panel, col=1
                )                   
    return 0

#-------------------------------------------------------
def get_datum(data, hk, lo=None, hi=None, detrend=False, stdev=False):

    if (hk in data.columns):
        datum = data[hk]
        if (lo is not None):
            datum[datum<=lo]=np.nan
        if (hi is not None):
            datum[datum>=hi]=np.nan
        if (detrend is True):
            datum = signal.detrend(datum.values)
        if (stdev is True):
            pass
    else:
        datum = None
    return datum

#-------------------------------------------------------
def update_fig_cde_pow(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    cdes=['CDA', 'CDB']
    powers=['CTRL', 'DRV1', 'DRV2']
    
    # Panel 1
    i_panel += 1
    i_color = 0
    fig.update_yaxes(title_text='{}.Bus V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for cde in cdes:
        for power in powers:
            datum=get_datum(data, 'V_%s_%s' % (cde,power))    
            plot_data(fig, i_panel, datum, '%s_%s' % (cde,power), i_color)
            i_color+=1
    datum=get_datum(data, 'V_JTD')
    plot_data(fig, i_panel, datum, 'JTD', i_color)

    # Panel 2
    i_panel += 1
    i_color = 0
    fig.update_yaxes(title_text='{}.P_CTRL (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for cde in cdes:
        datum=get_datum(data, 'P_%s_CTRL' % (cde))    
        plot_data(fig, i_panel, datum, '%s_CTRL' % (cde), i_color)
        i_color+=3
    
    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.P_2ST (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'P_CDA_DRV1')
    plot_data(fig, i_panel, datum, 'P_CDA_DRV1', 1)
    datum=get_datum(data, 'P_CDB_DRV1')    
    plot_data(fig, i_panel, datum, 'P_CDB_DRV1', 4)
    datum=get_datum(data, 'P_CDA_DRV2')
    plot_data(fig, i_panel, datum, 'P_CDA_DRV2', 2)
    datum=get_datum(data, 'P_CDB_DRV2')    
    plot_data(fig, i_panel, datum, 'P_CDB_DRV2', 5)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.P_JTD (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'P_JTD')
    plot_data(fig, i_panel, datum, 'P_JTD', 6)

    # Panel 5
    i_panel += 1
    i_color = 0
    fig.update_yaxes(title_text='{}.I_CTRL (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for cde in cdes:
        datum=get_datum(data, 'I_%s_CTRL' % (cde))    
        plot_data(fig, i_panel, datum, '%s_CTRL' % (cde), i_color)
        i_color+=3
    
    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.I_2ST (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'I_CDA_DRV1')
    plot_data(fig, i_panel, datum, 'I_CDA_DRV1', 1)
    datum=get_datum(data, 'I_CDB_DRV1')    
    plot_data(fig, i_panel, datum, 'I_CDB_DRV1', 4)
    datum=get_datum(data, 'I_CDA_DRV2')    
    plot_data(fig, i_panel, datum, 'I_CDA_DRV2', 2)
    datum=get_datum(data, 'I_CDB_DRV2')    
    plot_data(fig, i_panel, datum, 'I_CDB_DRV2', 5)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.I_JTD (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'I_JTD')
    plot_data(fig, i_panel, datum, 'I_JTD', 6)

    return fig

#-------------------------------------------------------
def update_fig_lhp_gse(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0

    # Panel 1-4
    for i, c in enumerate(['A_CMP', 'A_CHD', 'B_CMP', 'B_CHD']):
        i_panel += 1
        fig.update_yaxes(title_text='{}.T {} (degC)'.format(i_panel,c), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'T_LHP_S_SC{}_P'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'S_SC{}_P'.format(c), 0) 
        datum=get_datum(data, 'T_LHP_S_SC{}_R'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'S_SC{}_R'.format(c), 1) 
        datum=get_datum(data, 'T_LHP_V_SC{}_P'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'V_SC{}_P'.format(c), 2) 
        datum=get_datum(data, 'T_LHP_L_SC{}_P'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'L_SC{}_P'.format(c), 3) 
        datum=get_datum(data, 'T_LHP_C_SC{}_P'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'C_SC{}_P'.format(c), 4) 

    # Panel 5-8
    for i, c in enumerate(['A_CMP', 'A_CHD', 'B_CMP', 'B_CHD']):
        i_panel += 1
        fig.update_yaxes(title_text='{}.I {} (A)'.format(i_panel,c), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'I_LHP_S_SC{}_P'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'S_SC{}_P'.format(c), 0)
        datum=get_datum(data, 'I_LHP_S_SC{}_R'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'S_SC{}_R'.format(c), 2)
    
    # Panel 9-12
    for i, c in enumerate(['A_CMP', 'A_CHD', 'B_CMP', 'B_CHD']):
        i_panel += 1
        fig.update_yaxes(title_text='{}.V {} (V)'.format(i_panel,c), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'V_LHP_S_SC{}_P'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'S_SC{}_P'.format(c), 0)
        datum=get_datum(data, 'V_LHP_S_SC{}_R'.format(c), hi=50, lo=0)
        plot_data(fig, i_panel, datum, 'S_SC{}_R'.format(c), 2)
        if (c == 'A_CMP'):
            datum=get_datum(data, 'V_LHP_V_SCA', hi=50, lo=0)
            plot_data(fig, i_panel, datum, 'V_SCA', 4)
        elif (c == 'B_CMP'):
            datum=get_datum(data, 'V_LHP_V_SCB', hi=50, lo=0)
            plot_data(fig, i_panel, datum, 'V_SCB', 4)
    
    return fig

#-------------------------------------------------------
def update_fig_plumbing(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.DWR temp (1)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HE_TANK4', hi=250, lo=0)
    plot_data(fig, i_panel, datum, 'HE_TANK4', 0)
    datum=get_datum(data, 'JT_SHLD5', hi=250, lo=0)
    plot_data(fig, i_panel, datum, 'JT_SHLD5', 4)    
    add_line(fig, 2.1768, "lambda", vert=False, row=i_panel, col=1, color='grey', width=2, dash='dot')
    add_line(fig, 4.222, "norm boil", vert=False, row=i_panel, col=1, color="grey", width=1, dash='dot')
    
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.DWR temp (2)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'IVCS5', hi=350, lo=0)
    plot_data(fig, i_panel, datum, 'IVCS5', 0) 
    datum=get_datum(data, 'MVCS3', hi=350, lo=0)
    plot_data(fig, i_panel, datum, 'MVCS3', 2) 
    datum=get_datum(data, 'OVCS3', hi=350, lo=0)
    plot_data(fig, i_panel, datum, 'OVCS3', 4)
    datum=get_datum(data, 'DMS3', hi=350, lo=200)
    plot_data(fig, i_panel, datum, 'DMS3', 6) 
    
    # Panel 3
    i_panel += 1    
    fig.update_yaxes(title_text='{}.He tank p (Pa)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')
    datum=get_datum(data, 'p_HE_TANK1', lo=100, hi=1e6)
    plot_data(fig, i_panel, datum, 'p HE_TANK coarse', 0)    
    datum=get_datum(data, 'p_HE_TANK2', lo=0.1, hi=2*133.322)
    plot_data(fig, i_panel, datum, 'p HE_TANK fine', 2)
    datum=get_datum(data, 'HE_TANK1', hi=250, lo=0)
    if (datum is not None):
        p_from_T=datum.apply(lambda x:get_pHe4(x))
        plot_data(fig, i_panel, p_from_T, 'Vapour p', 4)
    add_line(fig, 101325, "air", vert=False, row=i_panel, col=1, color='grey', width=1, dash='dot')

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.DMS pressure (Pa)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')
    datum=get_datum(data, 'p_DMS', lo=1e-6)
    plot_data(fig, i_panel, datum, 'p DMS', 0)
    
    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.Flow rate (l/min)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')
    datum=get_datum(data, 'Flow_rate_L')    
    plot_data(fig, i_panel, datum, 'Flow rate (L)', 0)        
    datum=get_datum(data, 'Flow_rate_S')
    plot_data(fig, i_panel, datum, 'Flow rate (S)', 2)        

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.LD1 (Pa m^3/s)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')    
    datum=get_datum(data, 'LD1', lo=1e-15)    
    plot_data(fig, i_panel, datum, 'LD1', 0)        

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.LD2, 3 (Pa m^3/s)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')    
    datum=get_datum(data, 'LD2', lo=1e-9)
    plot_data(fig, i_panel, datum, 'LD2', 2) 
    datum=get_datum(data, 'LD3')
    plot_data(fig, i_panel, datum, 'LD3', 4)     

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.DMSfbv dP (torr)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum1=get_datum(data, 'dP1')
    datum2=get_datum(data, 'dP2')  
    plot_data(fig, i_panel, datum1, 'DMS flt bypass dP', 0)
    plot_data(fig, i_panel, datum2, 'DMS flt bypass dP_ref', 2)      

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.DMSfbv dP - dP_ref (torr)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (datum1 is not None) and (datum2 is not None):
        plot_data(fig, i_panel, datum1-datum2, 'DMS flt bypass dP-dP_ref', 2)      
        #add_range(fig, -0.3, 0.3, "", vert=False, row=i_panel, col=1, color='red', width=1, dash='dot')

    # Panel 10 (TC4)
    i_panel += 1
    fig.update_yaxes(title_text='{}.FAN (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'V_SXS_FAN') # TC4
    # datum=get_datum(data, 'FAN_V') # TC3
    plot_data(fig, i_panel, datum, 'SXS-FAN V', 0)        
    
    # Panel 11
    i_panel += 1
    fig.update_yaxes(title_text='{}.FAN I (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'I_SXS_FAN') # TC4
    # datum=get_datum(data, 'FAN_I') # TC3
    plot_data(fig, i_panel, datum, 'SXS-FAN I', 0)   
    
    return fig

#-------------------------------------------------------
def update_fig_adrg(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0
        
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.He tank'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T: Lakeshore372 3 [X128181] He tank 3", hi=100)
    plot_data(fig, i_panel, datum, 'HE_TANK3', 0) 
    datum=get_datum(data, "T: m30 [X123394] He tank 4", hi=100)
    plot_data(fig, i_panel, datum, 'HE_TANK4', 1)   
    datum=get_datum(data, "T: Lakeshore372 1 [X128295] PP3", hi=100)
    plot_data(fig, i_panel, datum, 'PP3', 2)    
    datum=get_datum(data, "T: m11 [X128318] He tank strap", hi=100)
    plot_data(fig, i_panel, datum, 'HE_TANK strap', 3)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.S12 Ctrl'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T: Lakeshore372 2 [L31380] S1 Salt Pill 1", hi=100)
    plot_data(fig, i_panel, datum, 'S1 Salt A', 0)    
    datum=get_datum(data, "T: Lakeshore372 4 [X128302] S2 CTRL (S2 Salt A)", hi=100)
    plot_data(fig, i_panel, datum, 'S2 Salt A', 2)        
    datum=get_datum(data, "T: Lakeshore372 2 [L31380] S1 CTRL (HS1 CE)", hi=100)
    plot_data(fig, i_panel, datum, 'HS1 CE', 4)    
    datum=get_datum(data, "T: m04 [X128320] HS2 cold end", hi=100)
    plot_data(fig, i_panel, datum, 'HS2 CE', 6)    
    datum=get_datum(data, "T: Lakeshore372 3 [X128319] S2 CTRL (CF mode)", hi=100)
    plot_data(fig, i_panel, datum, 'S2 Ctrl', 8)    

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.S12 mag'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, "T: m00 [X128309] S1 Magnet T", hi=100)
    plot_data(fig, i_panel, datum, 'S1 mag', 0)    
    datum=get_datum(data, "T: m01 [X128238] S2 Magnet T", hi=100)
    plot_data(fig, i_panel, datum, 'S2 mag', 1)    
    
    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.HS12 getter'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T: m06 [AHFMRO] HS1 getter 1", hi=100)
    plot_data(fig, i_panel, datum, 'HS1 getter1 ', 0)    
    datum=get_datum(data, "T: m07 [AHFMRO] HS1 getter 2", hi=100)
    plot_data(fig, i_panel, datum, 'HS1 getter2', 2)    
    datum=get_datum(data, "T: m08 [AHFMRO] HS2 getter 1", hi=100)
    plot_data(fig, i_panel, datum, 'HS2 getter1', 4)    
    datum=get_datum(data, "T: m09 [AHFMRO] HS2 getter 2", hi=100)
    plot_data(fig, i_panel, datum, 'HS2 getter2', 6)    

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.S3'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T: Stage 3 [X128313]", hi=100)
    plot_data(fig, i_panel, datum, 'S3', 0)    
    datum=get_datum(data, "T: Keithley 21 [X128313] S3 salt pill", hi=100)
    plot_data(fig, i_panel, datum, 'S3 Salt', 2)    
    datum=get_datum(data, "T: m10 [X128907] S3 magnet T", hi=100)
    plot_data(fig, i_panel, datum, 'S3 mag', 4)    
    datum=get_datum(data, "T: m12 [X128239] HS4 cold end", hi=100)
    plot_data(fig, i_panel, datum, 'HS4 CE', 6)    
    datum=get_datum(data, "T: m13 [X128908] HS4 warm end", hi=100)
    plot_data(fig, i_panel, datum, 'HS4 WE', 8)    
    datum=get_datum(data, "T: m02 [X128905] JT strap I/F", hi=100)
    plot_data(fig, i_panel, datum, 'JT strap I/F', 10)    

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.HS34 getter'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T: m14 [AHFMRO] HS3 getter 1", hi=100)
    plot_data(fig, i_panel, datum, 'HS3 getter1 ', 0)        
    datum=get_datum(data, "T: m15 [AHFMRO] HS3 getter 2", hi=100)
    plot_data(fig, i_panel, datum, 'HS3 getter2', 2)    
    datum=get_datum(data, "T: m16 [AHFMRO] HS4 getter 1", hi=100)
    plot_data(fig, i_panel, datum, 'HS4 getter1', 4)    
    datum=get_datum(data, "T: m17 [AHFMRO] HS4 getter 2", hi=100)
    plot_data(fig, i_panel, datum, 'HS4 getter2', 6)    

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.IVCS'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T: m03 [X128881] IVCS bracket", hi=100)
    plot_data(fig, i_panel, datum, 'IVCS bracket', 0)    
    datum=get_datum(data, "T: m18 [X128878] HTS3 WE", hi=100)
    plot_data(fig, i_panel, datum, 'HTS3 WE', 2)    

    return fig

#-------------------------------------------------------
def update_fig_env(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Temp (degC)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "T_Kumi1", lo=0, hi=100)
    plot_data(fig, i_panel, datum, 'Temp@Kumi1', 0) 
    datum=get_datum(data, "T_Kumi2", lo=0, hi=100)
    plot_data(fig, i_panel, datum, 'Temp@Kumi2', 1) 

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.Humidity (%)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, "H_Kumi1", lo=0, hi=100)
    plot_data(fig, i_panel, datum, 'Humidity@Kumi1', 0) 
    datum=get_datum(data, "H_Kumi2", lo=0, hi=100)
    plot_data(fig, i_panel, datum, 'Humidity@Kumi2', 1) 

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.PC (/m^3)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')
    sizes=['0.3um', '0.5um', '1.0um', '2.0um', '5.0um', '10.0um']
    for i, size in enumerate(sizes):
        datum=get_datum(data, size, lo=0)
        plot_data(fig, i_panel, datum, "{}".format(size), i) 

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.Pressure (Pa)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='log')
    for i in range(7):
        datum=get_datum(data, "p{:d}".format(i), lo=0)
        plot_data(fig, i_panel, datum, "Chamber p{:d}".format(i), i*2) 
    datum=get_datum(data, "pLP", lo=0)
    plot_data(fig, i_panel, datum, "pLP", 1) 
    datum=get_datum(data, "pBP", lo=0)
    plot_data(fig, i_panel, datum, "pBP", 3) 
    add_line(fig, 1.33e-3, "", vert=False, row=i_panel, col=1, color='red', width=2, dash='dot')

    """
    # Panel 5-8
    for i0 in [0, 6, 12, 18]:
        i_panel += 1
        fig.update_yaxes(title_text='{}.T (deg)'.format(i_panel), showgrid=True, row=i_panel, col=1, type='linear')
        for i in range(6):
            datum=get_datum(data, "T{:d}".format(i+i0))
            plot_data(fig, i_panel, datum, "Chamber T{:d}".format(i+i0), i*2) 
"""

    return fig


#-------------------------------------------------------
def update_fig_temp_dwr(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.He tank'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        HeTank1=get_datum(data, 'HE_TANK1', hi=250)
        plot_data(fig, i_panel, HeTank1, 'HeTank1 (bottom)', 0)        
        HeTank2=get_datum(data, 'HE_TANK2', hi=250)
        plot_data(fig, i_panel, HeTank2, 'HeTank2 (middle)', 1)    
        PP1=get_datum(data, 'PP1', hi=250)
        plot_data(fig, i_panel, PP1, 'PP1', 3)    
        PP2=get_datum(data, 'PP2', hi=250)
        plot_data(fig, i_panel, PP2, 'PP2', 4)        
    # MD4 GSE
    else:
        datum=get_datum(data, 'HE_TANK4', hi=250)
        plot_data(fig, i_panel, datum, 'HeTank4', 6)    
    # ADRC    
    if flg_adrc is True:
        HeTank3=get_datum(data, 'HE_TANK3', hi=250)
        plot_data(fig, i_panel, HeTank3, 'HeTank3 (top)', 2)
        PP3=get_datum(data, 'PP3', hi=250)
        plot_data(fig, i_panel, PP3, 'PP3', 5)
        datum=get_datum(data, 'DET_ASSY', hi=250)
        plot_data(fig, i_panel, datum, 'DA', 6)
        datum=get_datum(data, 'ST1_STRP', hi=250)
        plot_data(fig, i_panel, datum, 'St1 strap', 6)
    add_line(fig, 2.1768, "lambda", vert=False, row=i_panel, col=1, color='grey', width=2, dash='dot')
    add_line(fig, 4.222, "norm boil", vert=False, row=i_panel, col=1, color="grey", width=1, dash='dot')

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.dT'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (HeTank1 is not None) and (HeTank1 is not None):
        mb=HeTank2-HeTank1
        plot_data(fig, i_panel, mb, 'HeTank mid-bottom', 0)
    if (HeTank3 is not None) and (HeTank1 is not None):
        tb=HeTank3-HeTank1
        plot_data(fig, i_panel, tb, 'HeTank top-bottom', 2)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.4K stage'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        datum=get_datum(data, 'JT_4KSTG', hi=250)            
        plot_data(fig, i_panel, datum, 'JT 4Kstg', 1)
        datum=get_datum(data, 'ORIFI_IN', hi=250)            
        plot_data(fig, i_panel, datum, 'JT Orifice', 3)
        #datum=get_datum(data, 'BYPSS_IN', hi=250)            
        #plot_data(fig, i_panel, datum, 'BYPSS_IN', 5)
    # MD4 GSE
    else:            
        datum=get_datum(data, 'JT_4KSTG', hi=250)
        plot_data(fig, i_panel, datum, 'JT 4Kstg', 1)
        datum=get_datum(data, 'ORIFI_IN', hi=250)
        plot_data(fig, i_panel, datum, 'Orifice', 3)
        #datum=get_datum(data, 'BYPSS_IN', hi=250)            
        #plot_data(fig, i_panel, datum, 'BYPSS_IN', 5)
    if flg_adrc is True:
        datum=get_datum(data, 'JT_STRP', hi=250)
        plot_data(fig, i_panel, datum, 'JT strap', 2)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.JT shield'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        for _i in range(1,5):       
            datum=get_datum(data, 'JT_SHLD%d' % _i, hi=250)            
            plot_data(fig, i_panel, datum, 'JT_SHLD%d' % _i, 2*_i)
    # MD4 GSE
    else:
        for _i in range(5,6):        
            datum=get_datum(data, 'JT_SHLD%d' % _i, hi=250)
            plot_data(fig, i_panel, datum, 'JT_SHLD%d' % _i, 2*_i)
    # ADRC
    if flg_adrc is True:
        pass
    # ADRG
    else:  
        pass
    
    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.IVCS'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        for _i in range(1,5):
            datum=get_datum(data, 'IVCS%d' % _i, hi=250, lo=10)            
            plot_data(fig, i_panel, datum, 'IVCS%d' % _i, 2*_i)
    # MD4 GSE
    else:
        for _i in range(5,6):        
            datum=get_datum(data, 'IVCS%d' % _i, hi=250, lo=10)
            plot_data(fig, i_panel, datum, 'IVCS%d' % _i, 2*_i)
    # ADRG
    if flg_adrc is True:
        datum=get_datum(data, 'IVCS_FLT', hi=250, lo=10)
        plot_data(fig, i_panel, datum, 'IVCS filter', 0)
    
    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.MVCS'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        for _i in range(1,3):        
            datum=get_datum(data, 'MVCS%d' % _i, hi=250, lo=10)
            plot_data(fig, i_panel, datum, 'MVCS%d' % _i, 2*_i)
    # MD GSE4
    else:
        for _i in range(3,4):        
            datum=get_datum(data, 'MVCS%d' % _i, hi=250, lo=10)        
            plot_data(fig, i_panel, datum, 'MVCS%d' % _i, 2*_i)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.OVCS'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        for _i in range(1,3):        
            datum=get_datum(data, 'OVCS%d' % _i, hi=250, lo=10)
            plot_data(fig, i_panel, datum, 'OVCS%d' % _i, 2*_i)
    # MD GSE4
    else:   
        for _i in range(3,4):                
            datum=get_datum(data, 'OVCS%d' % _i, hi=250, lo=10)            
            plot_data(fig, i_panel, datum, 'OVCS%d' % _i, 2*_i)
    # ADRG
    if flg_adrc is True:
        datum=get_datum(data, 'OVCS_FLT', hi=250, lo=10)
        plot_data(fig, i_panel, datum, 'OVCS filter', 0)
    
    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.DMS'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        for _i in range(1,3):        
            datum=get_datum(data, 'DMS%d' % _i, hi=400, lo=200)
            plot_data(fig, i_panel, datum, 'DMS%d' % _i, 2*_i-2)
        for _i in range(1,3):    
            datum=get_datum(data, 'GV%d' % _i, hi=400, lo=10)            
            plot_data(fig, i_panel, datum, 'GV%d' % _i, 2*_i+2)
    # MD GSE4
    else:
        for _i in range(3,4):        
            datum=get_datum(data, 'DMS%d' % _i, hi=400, lo=200)
            plot_data(fig, i_panel, datum, 'DMS%d' % _i, 2*_i+4)
    # ADRG
    if flg_adrc is True:
        datum=get_datum(data, 'DMS_FLT', hi=400, lo=200)
        plot_data(fig, i_panel, datum, 'DMS filter', 0)

    return fig


#-------------------------------------------------------
def update_fig_temp_cc(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0

    def temp_cc_sub_sc(elem):
        datum=get_datum(data, 'SCA_%s' % elem, lo=10, hi=350)
        plot_data(fig, i_panel, datum, 'SCA_%s' % elem, 0)    
        datum=get_datum(data, 'SCB_%s' % elem, lo=10, hi=350)
        plot_data(fig, i_panel, datum, 'SCB_%s' % elem, 2)    
    def temp_cc_sub_pc(elem):
        datum=get_datum(data, 'PCA_%s' % elem, lo=10, hi=350) 
        plot_data(fig, i_panel, datum, 'PCA_%s' % elem, 4)    
        datum=get_datum(data, 'PCB_%s' % elem, lo=10, hi=350)
        plot_data(fig, i_panel, datum, 'PCB_%s' % elem, 6)
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.SC 1st'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('1ST')
    
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.SC 2nd'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('2ND')

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.PC 1st'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_pc('1ST')
    
    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.PC 2nd'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_pc('2ND')

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST CPY'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('CPY')
    temp_cc_sub_pc('CPY')

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST CHD'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('CHD')
    temp_cc_sub_pc('CHD')

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST CMP'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('CMP')
    temp_cc_sub_pc('CMP')

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.CDE'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA', lo=100, hi=350)
    plot_data(fig, i_panel, datum, 'CDA', 1)
    datum=get_datum(data, 'CDB', lo=100, hi=350)
    plot_data(fig, i_panel, datum, 'CDB', 3)
    datum=get_datum(data, 'JTD', lo=100, hi=350)
    plot_data(fig, i_panel, datum, 'JTD', 5)

    return fig

#-------------------------------------------------------
def update_fig_temp_csi(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.S1 T (mK)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST1_CTL', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'Ctrl', 0)
    datum=get_datum(data, 'CAMC_CT0', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'CAMC CT0', 1)        
    datum=get_datum(data, 'CAMC_CT1', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'CAMC CT1', 2)        

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.S1 Trms (mK)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ADRC_CT_CTL_FLUC', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'Ctl', 0)
    datum=get_datum(data, 'ADRC_CT_MON_FLUC', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'Mon', 1)     

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.S2 T (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST2_CTL', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'Ctrl', 0)
    datum=get_datum(data, 'WAMC_CT0', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'WAMC CT0', 1)        
    datum=get_datum(data, 'WAMC_CT1', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'WAMC CT1', 2)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.S3 T (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST3_CTL', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'Ctrl', 0)
    datum=get_datum(data, 'HAMC_CT0', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HAMC CT0', 1)        
    datum=get_datum(data, 'HAMC_CT1', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HAMC CT1', 2)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.SPills, mag (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST1_PIL1', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S1 SPill1', 0)        
    datum=get_datum(data, 'ST1_PIL2', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S1 SPill2', 1)
    datum=get_datum(data, 'ST1_MAG', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S1 mag', 2)        
    datum=get_datum(data, 'ST2_MAG', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S2 mag', 3)        
    datum=get_datum(data, 'ST3_MAG', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S3 mag', 4)
    datum=get_datum(data, 'MAG_SHLD', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'Mag shield', 5)        

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.HS (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'HS1_CLDE', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS1 cold', 0)        
    datum=get_datum(data, 'HS1_WRME', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS1 warm', 1)        
    datum=get_datum(data, 'HS2_CLDE', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS2 cold', 2)        
    datum=get_datum(data, 'HS2_WRME', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS2 warm', 3)        
    datum=get_datum(data, 'HS3_CLDE', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS3 cold', 4)        
    datum=get_datum(data, 'HS3_WRME', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS3 warm', 5)        
    datum=get_datum(data, 'HS4_CLDE', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS4 cold', 6)        
    datum=get_datum(data, 'HS4_WRME', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HS4 warm', 7)        

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.Getters (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    i_color=0
    for i in range(4):
        for j in range(2):
            datum=get_datum(data, 'HS%d_GET%d' % (i,j), hi=100, lo=0)
            plot_data(fig, i_panel, datum, 'HS%d_GET%d' % (i, j), i_color)        
            i_color+=1

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTS (K)'.format(i_panel), showgrid=True, row=i_panel, col=1) 
    datum=get_datum(data, 'HTS12CLD', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HTS12 cold', 0)        
    datum=get_datum(data, 'HTS12HOT', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HTS12 hot', 1)        
    datum=get_datum(data, 'HTS12BRH', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HTS12 bracket', 2)        
    datum=get_datum(data, 'HTS3_CLD', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HTS3 cold', 3)        
    datum=get_datum(data, 'HTS3_HOT', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'HTS3 hot', 4)

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.DA & straps (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)     
    datum=get_datum(data, 'DET_ASSY', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'DA', 0)      
    datum=get_datum(data, 'ST1_STRP', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'St1 strap', 1)        
    datum=get_datum(data, 'JT_STRP', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'JT strap', 2)

    return fig

#-------------------------------------------------------
def update_fig_adrc(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
            
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Exec line'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(3):
        datum=get_datum(data, 'ADRC_SEQ%d_EXEC_LINE' % i)
        plot_data(fig, i_panel, datum, 'seq%d' % i, i*2)
        
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.Mag Vdrv (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for c in adrc_cards:    
        datum=get_datum(data, 'ADRC_%s_MAG_DRV_V_CAL' % c)
        plot_data(fig, i_panel, datum, '%s' % c, adrc_cards.index(c)*2)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.Mag Vsen (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for c in adrc_cards:    
        datum=get_datum(data, 'ADRC_%s_MAG_SEN_V_CAL' % c)
        plot_data(fig, i_panel, datum, '%s' % c, adrc_cards.index(c)*2)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.Mag Ihi (mA)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for c in adrc_cards:    
        datum=get_datum(data, 'ADRC_%s_MAG_HI_I_CAL' % c)
        plot_data(fig, i_panel, datum, '%s' % c, adrc_cards.index(c)*2)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.Mag Ilo (mA)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for c in adrc_cards:    
        datum=get_datum(data, 'ADRC_%s_MAG_LO_I_CAL' % c)
        plot_data(fig, i_panel, datum, '%s' % c, adrc_cards.index(c)*2)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.Htr V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'ADRC_SITH_FLT_HTR_PS_V_CAL')
    plot_data(fig, i_panel, datum, 'HTR V', 6)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.Htr I (mA)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ADRC_SITH_HTR0_I_CAL')
    plot_data(fig, i_panel, datum, 'IVCS', 0)
    datum=get_datum(data, 'ADRC_SITH_HTR1_I_CAL')
    plot_data(fig, i_panel, datum, 'OVCS', 2)
    datum=get_datum(data, 'ADRC_SITH_HTR2_I_CAL')
    plot_data(fig, i_panel, datum, 'DMS', 4)
        
    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.Temp (degC)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color = -1
    for i in range(2):
        i_color += 1
        datum=get_datum(data, 'SITH_TEMP%d' % i, lo=-50, hi=100)    
        plot_data(fig, i_panel, datum, 'SITH_T%d' % i, i_color)
    for i in range(2):
        i_color += 1
        datum=get_datum(data, 'SITH_PS_TEMP%d_R' % i, lo=-50, hi=100)    
        plot_data(fig, i_panel, datum, 'PS%d' % i, i_color)
    for c in adrc_cards:
        for i in range(5):
            i_color += 1
            datum=get_datum(data, '%s_TEMP%d' % (c,i), lo=-50, hi=100)    
            plot_data(fig, i_panel, datum, '%s%d' % (c,i), i_color)

    return fig

#-------------------------------------------------------
def update_fig_cc(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0

    # Panel 1-6
    for ST in STs:    
        for VIP in VIPs:
            i_panel += 1
            fig.update_yaxes(title_text='{}.{} {}'.format(i_panel, ST, VIP), showgrid=True, row=i_panel, col=1)
            datum=get_datum(data, 'CDA_%sA_CMP_%s_CAL' % (ST, VIP))
            plot_data(fig, i_panel, datum, 'A CMP', 1)    
            datum=get_datum(data, 'CDA_%sA_DSP_%s_CAL' % (ST, VIP))
            plot_data(fig, i_panel, datum*10.0, 'A DSPx10', 3)    
            #datum=get_datum(data, 'CDA_%sA_BLN_%s_CAL' % (ST, VIP))
            #plot_data(fig, i_panel, datum*10.0, 'A BLNx10', 3)    
            datum=get_datum(data, 'CDB_%sB_CMP_%s_CAL' % (ST, VIP))
            plot_data(fig, i_panel, datum, 'B CMP', 5)    
            datum=get_datum(data, 'CDB_%sB_DSP_%s_CAL' % (ST, VIP))
            plot_data(fig, i_panel, datum*10.0, 'B DSPx10', 7)    
            #datum=get_datum(data, 'CDB_%sB_BLN_%s_CAL' % (ST, VIP))
            #plot_data(fig, i_panel, datum*10.0, 'B BLNx10', 6)    
    
    # Panel 7-9
    for VIP in VIPs:
        i_panel += 1
        fig.update_yaxes(title_text='{}.JTC {}'.format(i_panel, VIP), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'JTD_DRVL_CMP_%s_CAL' % (VIP))
        plot_data(fig, i_panel, datum, 'L CMP', 1)    
        datum=get_datum(data, 'JTD_DRVH_CMP_%s_CAL' % (VIP))
        plot_data(fig, i_panel, datum, 'H CMP', 3)            

    # Panel 10
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC p (MPa)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color = 0
    for HMLJ in ['H', 'M', 'L', 'J']:
        datum=get_datum(data, 'JTD_P%s_CAL' % HMLJ)
        plot_data(fig, i_panel, datum, 'p%s' % HMLJ, i_color*2)        
        i_color+=1

    return fig

#-------------------------------------------------------
def update_fig_jtc(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
        
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'JTD_DRVH_CMP_V_CAL')
    plot_data(fig, i_panel, datum, 'CMP_H', 1)    
    datum=get_datum(data, 'JTD_DRVL_CMP_V_CAL')
    plot_data(fig, i_panel, datum, 'CMP_L', 2)    
    
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC I (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'JTD_DRVH_CMP_I_CAL')
    plot_data(fig, i_panel, datum, 'CMP_H', 1)        
    datum=get_datum(data, 'JTD_DRVL_CMP_I_CAL')
    plot_data(fig, i_panel, datum, 'CMP_L', 2)        
    
    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'JTD_DRVH_CMP_P_CAL')
    plot_data(fig, i_panel, datum, 'CMP_H', 1)        
    datum=get_datum(data, 'JTD_DRVL_CMP_P_CAL')
    plot_data(fig, i_panel, datum, 'CMP_H', 2)        

    # Panel 4
    i_panel += 1    
    def change_literal(datum):
        datum=datum.dropna()
        datum[datum>1.0]=np.nan
        datum=datum.dropna()
        
        datum[(datum>=0.0) & (datum<0.5)]=0.0
        datum[(datum>0.5) & (datum<=1.0)]=1.0
        
        datum[datum==0.0]='CLS'
        datum[datum==1.0]='OPN'
        
        return datum
    
    fig.update_yaxes(title_text='{}.Valves'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(1,4):
        datum=get_datum(data, 'JTD_VALVE%d_OPN_CLS' % i)
        datum=change_literal(datum)
        plot_data(fig, i_panel, datum, 'JTC V%d' % i, i)        
    
    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC p (MPa)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color = 5
    for HMLJ in ['H', 'M', 'L', 'J']:
        datum=get_datum(data, 'JTD_P%s_CAL' % HMLJ)
        plot_data(fig, i_panel, datum, 'p%s' % HMLJ, i_color)        
        i_color+=1

    # Panel 6
    data['pH-pM'] = data['JTD_PH_CAL']-data['JTD_PM_CAL']
    data['pM-pL'] = data['JTD_PM_CAL']-data['JTD_PL_CAL']
    
    i_panel += 1
    fig.update_yaxes(title_text=r'JTC dp (MPa)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'pH-pM')
    plot_data(fig, i_panel, datum, 'pH-pM', 3)        
    datum=get_datum(data, 'pM-pL')
    plot_data(fig, i_panel, datum, 'pM-pL', 4)           

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.4K stage (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    # CDE
    if flg_cde is True:
        datum=get_datum(data, 'JT_4KSTG', hi=250)            
        plot_data(fig, i_panel, datum, 'JT_4KSTG', 1)
        datum=get_datum(data, 'ORIFI_IN', hi=250)            
        plot_data(fig, i_panel, datum, 'ORIFI_IN', 3)
        #datum=get_datum(data, 'BYPSS_IN', hi=250)            
        #plot_data(fig, i_panel, datum, 'BYPSS_IN', 5)
    # MD4 GSE
    else:            
        datum=get_datum(data, 'JT_4KSTG', hi=250)
        plot_data(fig, i_panel, datum, 'JT_4KSTG', 1)
        datum=get_datum(data, 'ORIFI_IN', hi=250)
        plot_data(fig, i_panel, datum, 'ORIFI_IN', 3)
        #datum=get_datum(data, 'BYPSS_IN', hi=250)            
        #plot_data(fig, i_panel, datum, 'BYPSS_IN', 5)

    if flg_bus is True:
        # Panel 8
        i_panel += 1
        fig.update_yaxes(title_text='{}.JT heaters'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')    
        datum=get_datum(data, 'HCE_A_STS_SXS_JT_PR_CMP_L1_ON_OFF')
        plot_data(fig, i_panel, datum, 'HCE-A CMP-L Pr', 1, enum=['off', 'on']) 
        datum=get_datum(data, 'HCE_B_STS_SXS_JT_BU_CMP_L1_ON_OFF')
        plot_data(fig, i_panel, datum, 'HCE-B CMP-L Bu', 3, enum=['off', 'on'])    
        datum=get_datum(data, 'M_HCE_STS_SXS_JT_PR_CMP_L2_ON_OFF')
        plot_data(fig, i_panel, datum, 'M-HCE CMP-L Pr', 5, enum=['off', 'on'])
        #
        datum=get_datum(data, 'HCE_B_STS_SXS_JT_PR_CMP_H1_ON_OFF')
        plot_data(fig, i_panel, datum, 'HCE-B CMP-H Pr', 2, enum=['off', 'on'])    
        datum=get_datum(data, 'HCE_A_STS_SXS_JT_BU_CMP_H1_ON_OFF')
        plot_data(fig, i_panel, datum, 'HCE-A CMP-H Bu', 4, enum=['off', 'on'])
        datum=get_datum(data, 'M_HCE_STS_SXS_JT_PR_CMP_H2_ON_OFF')
        plot_data(fig, i_panel, datum, 'M-HCE CMP-H Pr', 6, enum=['off', 'on'])    

        # Panel 9
        i_panel += 1
        fig.update_yaxes(title_text='{}.JT CMP-L T (deg)'.format(i_panel), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'HCE_A_STS_SXS_JT_PR_CMP_L1_TEMP_CAL')
        plot_data(fig, i_panel, datum, 'HCE-A Pr', 1)    
        datum=get_datum(data, 'HCE_B_STS_SXS_JT_BU_CMP_L1_TEMP_CAL')
        plot_data(fig, i_panel, datum, 'HCE-B Bu', 3)    
        datum=get_datum(data, 'M_HCE_STS_SXS_JT_PR_CMP_L2_TEMP_CAL')
        plot_data(fig, i_panel, datum, 'M-HCE Pr', 5)    

        
        # Panel 10
        i_panel += 1
        fig.update_yaxes(title_text='{}.JT CMP-H T (deg)'.format(i_panel), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'HCE_B_STS_SXS_JT_PR_CMP_H1_TEMP_CAL')
        plot_data(fig, i_panel, datum, 'HCE-B Pr', 1)    
        datum=get_datum(data, 'HCE_A_STS_SXS_JT_BU_CMP_H1_TEMP_CAL')
        plot_data(fig, i_panel, datum, 'HCE-A Bu', 3)    
        datum=get_datum(data, 'M_HCE_STS_SXS_JT_PR_CMP_H2_TEMP_CAL')
        plot_data(fig, i_panel, datum, 'M-HCE Pr', 5)    
    else:
        # Panel 8
        i_panel += 1
        fig.update_yaxes(title_text='{}.HEX3 & PC 2ND (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
        datum=get_datum(data, 'HEX3LPOUT')
        plot_data(fig, i_panel, datum, 'HEX3 LP OUT', 2)    
        datum=get_datum(data, 'HEX3HPIN')
        plot_data(fig, i_panel, datum, 'HEX3 HP IN', 11)    
        datum=get_datum(data, '2NDSTAGE')
        plot_data(fig, i_panel, datum, 'PC 2nd stage', 10)    
        datum=get_datum(data, 'HEX2HPOUT')
        plot_data(fig, i_panel, datum, 'HEX2 HP OUT', 9)    

        # Panel 9
        i_panel += 1
        fig.update_yaxes(title_text='{}.HEX2 & PC 1ST (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'HEX2LPOUT')
        plot_data(fig, i_panel, datum, 'HEX2 LP OUT', 3)    
        datum=get_datum(data, 'HEX2HPIN')
        plot_data(fig, i_panel, datum, 'HEX2 HP IN', 8)    
        datum=get_datum(data, '1STSTAGE')
        plot_data(fig, i_panel, datum, 'PC 1st stage', 7)    
        datum=get_datum(data, 'HEX1HPOUT')
        plot_data(fig, i_panel, datum, 'HEX1 HP OUT', 6)    
        
        # Panel 10
        i_panel += 1
        fig.update_yaxes(title_text='{}.HEX1 (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
        datum=get_datum(data, 'HEX1LPOUT')
        plot_data(fig, i_panel, datum, 'HEX1 LP OUT', 4)    
        datum=get_datum(data, 'HEX1HPIN')
        plot_data(fig, i_panel, datum, 'HEX1 HP IN', 5)    

    return fig

#-------------------------------------------------------
def update_fig_sc_eff(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bus V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (flg_dist is True):
        busV=get_datum(data, 'SXS_DIST_BUS_V_CAL')
        plot_data(fig, i_panel, busV, 'Bus V', 1)    
    else:
        datum=get_datum(data, 'V_CDA_DRV1')
        plot_data(fig, i_panel, datum, 'CDA_DRV1', 1)    
        datum=get_datum(data, 'V_CDB_DRV1')
        plot_data(fig, i_panel, datum, 'CDB_DRV1', 7)    

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.CMP P1 (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (flg_dist is True):
        datum=get_datum(data, 'SXS_DIST_CDA_DRV1_I_CAL')
        plot_data(fig, i_panel, datum*busV, 'CDA_DRV1', 1)    
        datum=get_datum(data, 'SXS_DIST_CDB_DRV1_I_CAL')
        plot_data(fig, i_panel, datum*busV, 'CDB_DRV1', 7)    
    else:
        datum=get_datum(data, 'P_CDA_DRV1')
        plot_data(fig, i_panel, datum, 'CDA_DRV1', 1)    
        datum=get_datum(data, 'P_CDB_DRV1')
        plot_data(fig, i_panel, datum, 'CDB_DRV1', 7)    

    # Panel 3
    for i in ABs:        
        data['CD%s_SC%s_ALL_P_CAL' % (i,i)] = data['CD%s_SC%s_CMP_P_CAL' % (i,i)] + data['CD%s_SC%s_DSP_P_CAL' % (i,i)] + data['CD%s_SC%s_BLN_P_CAL' % (i,i)]
        data['CD%s_PC%s_ALL_P_CAL' % (i,i)] = data['CD%s_PC%s_CMP_P_CAL' % (i,i)] + data['CD%s_PC%s_DSP_P_CAL' % (i,i)] + data['CD%s_PC%s_BLN_P_CAL' % (i,i)]

    i_panel += 1
    fig.update_yaxes(title_text='{}.P2 (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_SCA_ALL_P_CAL')
    plot_data(fig, i_panel, datum, 'CDA_SCA_ALL', 1)    
    datum=get_datum(data, 'CDB_SCB_ALL_P_CAL')
    plot_data(fig, i_panel, datum, 'CDB_SCA_ALL', 7)    
    
    # Panel 4
    if (flg_dist is True):
        data['CDA_SCA_CMP_dP12'] = data['SXS_DIST_CDA_DRV1_I_CAL']*busV - data['CDA_SCA_CMP_P_CAL']
        data['CDA_SCB_CMP_dP12'] = data['SXS_DIST_CDB_DRV1_I_CAL']*busV - data['CDB_SCB_CMP_P_CAL']
    else:
        data['CDA_SCA_CMP_dP12'] = data['P_CDA_DRV1'] - data['CDA_SCA_CMP_P_CAL']
        data['CDA_SCB_CMP_dP12'] = data['P_CDB_DRV1'] - data['CDB_SCB_CMP_P_CAL']

    i_panel += 1
    fig.update_yaxes(title_text='{}.P1-P2 (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_SCA_CMP_dP12')
    plot_data(fig, i_panel, datum, 'CDA_SCA', 1)    
    datum=get_datum(data, 'CDA_SCB_CMP_dP12')
    plot_data(fig, i_panel, datum, 'CDB_SCB', 7)    

    # Panel 5
    if (flg_dist is True):
        data['CDA_SCA_CMP_P1P2'] = data['CDA_SCA_CMP_P_CAL']/data['SXS_DIST_CDA_DRV1_I_CAL']/busV
        data['CDA_SCB_CMP_P1P2'] = data['CDB_SCB_CMP_P_CAL']/data['SXS_DIST_CDB_DRV1_I_CAL']/busV
    else:   
        data['CDA_SCA_CMP_P1P2'] = data['CDA_SCA_CMP_P_CAL']/data['P_CDA_DRV1']
        data['CDA_SCB_CMP_P1P2'] = data['CDB_SCB_CMP_P_CAL']/data['P_CDB_DRV1']

    i_panel += 1
    fig.update_yaxes(title_text='{}.P2/P1'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_SCA_CMP_P1P2')
    plot_data(fig, i_panel, datum, 'CDA_SCA', 1)    
    datum=get_datum(data, 'CDA_SCB_CMP_P1P2')
    plot_data(fig, i_panel, datum, 'CDB_SCB', 7)    

    # Panel 6
    for i in ABs:
        for j in ['CMP', 'DSP', 'BLN']:
            data['CD{}_SC{}_{}_PF'.format(i,i,j)] = data['CD{}_SC{}_{}_P_CAL'.format(i,i,j)] / data['CD{}_SC{}_{}_V_CAL'.format(i,i,j)] / data['CD{}_SC{}_{}_I_CAL'.format(i,i,j)]
    i_panel += 1
    fig.update_yaxes(title_text='{}.PF'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_SCA_CMP_PF')
    plot_data(fig, i_panel, datum, 'CDA_SCA_CMP_PF', 1)    
    datum=get_datum(data, 'CDA_SCA_DSP_PF')
    plot_data(fig, i_panel, datum, 'CDA_SCA_DSP_PF', 2)    
    datum=get_datum(data, 'CDB_SCB_CMP_PF')
    plot_data(fig, i_panel, datum, 'CDB_SCB_CMP_PF', 7)    
    datum=get_datum(data, 'CDB_SCB_DSP_PF')
    plot_data(fig, i_panel, datum, 'CDB_SCB_DSP_PF', 8)    

    # Panel 7   
    i_panel += 1
    fig.update_yaxes(title_text='{}.CDE temp'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA', lo=250)
    plot_data(fig, i_panel, datum, 'CDA', 1)    
    datum=get_datum(data, 'CDB', lo=250)
    plot_data(fig, i_panel, datum, 'CDB', 7)    

    # Panel 8   
    i_panel += 1
    fig.update_yaxes(title_text='{}.2nd stage (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SCA_2ND', lo=10, hi=250)
    plot_data(fig, i_panel, datum, 'SCA_2ND', 1)    
    datum=get_datum(data, 'SCB_2ND', lo=10, hi=250)
    plot_data(fig, i_panel, datum, 'SCB_2ND', 7)    
    
    # Panel 9   
    i_panel += 1
    fig.update_yaxes(title_text='{}.1st stage (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SCA_1ST', hi=250, lo=10)
    plot_data(fig, i_panel, datum, 'SCA_1ST', 1)    
    datum=get_datum(data, 'SCB_1ST', hi=250, lo=10)
    plot_data(fig, i_panel, datum, 'SCB_1ST', 7)    
    
    return fig

#-------------------------------------------------------
def update_fig_pc_eff(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bus V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (flg_dist is True):
        busV=get_datum(data, 'SXS_DIST_BUS_V_CAL')
        plot_data(fig, i_panel, busV, 'Bus V', 1)    
    else:
        datum=get_datum(data, 'V_CDA_DRV2')
        plot_data(fig, i_panel, datum, 'CDA_DRV2', 1)    
        datum=get_datum(data, 'V_CDB_DRV2')
        plot_data(fig, i_panel, datum, 'CDB_DRV2', 7)    

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.CMP P1 (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (flg_dist is True):
        datum=get_datum(data, 'SXS_DIST_CDA_DRV2_I_CAL')
        plot_data(fig, i_panel, datum*busV, 'CDA_DRV2', 1)    
        datum=get_datum(data, 'SXS_DIST_CDB_DRV2_I_CAL')
        plot_data(fig, i_panel, datum*busV, 'CDB_DRV2', 7)    
    else:
        datum=get_datum(data, 'P_CDA_DRV2')
        plot_data(fig, i_panel, datum, 'CDA_DRV2', 1)    
        datum=get_datum(data, 'P_CDB_DRV2')
        plot_data(fig, i_panel, datum, 'CDB_DRV2', 7)    

    # Panel 3
    for i in ABs:        
        data['CD%s_PC%s_ALL_P_CAL' % (i,i)] = data['CD%s_PC%s_CMP_P_CAL' % (i,i)] + data['CD%s_PC%s_DSP_P_CAL' % (i,i)] + data['CD%s_PC%s_BLN_P_CAL' % (i,i)]
        data['CD%s_PC%s_ALL_P_CAL' % (i,i)] = data['CD%s_PC%s_CMP_P_CAL' % (i,i)] + data['CD%s_PC%s_DSP_P_CAL' % (i,i)] + data['CD%s_PC%s_BLN_P_CAL' % (i,i)]

    i_panel += 1
    fig.update_yaxes(title_text='{}.P2 (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_PCA_ALL_P_CAL')
    plot_data(fig, i_panel, datum, 'CDA_PCA_ALL', 1)    
    datum=get_datum(data, 'CDB_PCB_ALL_P_CAL')
    plot_data(fig, i_panel, datum, 'CDB_PCA_ALL', 7)    
    
    # Panel 4
    if (flg_dist is True):
        data['CDA_PCA_CMP_dP12'] = data['SXS_DIST_CDA_DRV2_I_CAL']*busV - data['CDA_PCA_CMP_P_CAL']
        data['CDA_PCB_CMP_dP12'] = data['SXS_DIST_CDB_DRV2_I_CAL']*busV - data['CDB_PCB_CMP_P_CAL']
    else:
        data['CDA_PCA_CMP_dP12'] = data['P_CDA_DRV2'] - data['CDA_PCA_CMP_P_CAL']
        data['CDA_PCB_CMP_dP12'] = data['P_CDB_DRV2'] - data['CDB_PCB_CMP_P_CAL']

    i_panel += 1
    fig.update_yaxes(title_text='{}.P1-P2 (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_PCA_CMP_dP12')
    plot_data(fig, i_panel, datum, 'CDA_PCA', 1)    
    datum=get_datum(data, 'CDA_PCB_CMP_dP12')
    plot_data(fig, i_panel, datum, 'CDB_PCB', 7)    

    # Panel 5
    if (flg_dist is True):
        data['CDA_PCA_CMP_P1P2'] = data['CDA_PCA_CMP_P_CAL']/data['SXS_DIST_CDA_DRV2_I_CAL']/busV
        data['CDA_PCB_CMP_P1P2'] = data['CDB_PCB_CMP_P_CAL']/data['SXS_DIST_CDB_DRV2_I_CAL']/busV
    else:   
        data['CDA_PCA_CMP_P1P2'] = data['CDA_PCA_CMP_P_CAL']/data['P_CDA_DRV2']
        data['CDA_PCB_CMP_P1P2'] = data['CDB_PCB_CMP_P_CAL']/data['P_CDB_DRV2']

    i_panel += 1
    fig.update_yaxes(title_text='{}.P2/P1'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_PCA_CMP_P1P2')
    plot_data(fig, i_panel, datum, 'CDA_PCA', 1)    
    datum=get_datum(data, 'CDA_PCB_CMP_P1P2')
    plot_data(fig, i_panel, datum, 'CDB_PCB', 7)    

    # Panel 6
    for i in ABs:
        for j in ['CMP', 'DSP', 'BLN']:
            data['CD{}_PC{}_{}_PF'.format(i,i,j)] = data['CD{}_PC{}_{}_P_CAL'.format(i,i,j)] / data['CD{}_PC{}_{}_V_CAL'.format(i,i,j)] / data['CD{}_PC{}_{}_I_CAL'.format(i,i,j)]
    i_panel += 1
    fig.update_yaxes(title_text='{}.PF'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_PCA_CMP_PF')
    plot_data(fig, i_panel, datum, 'CDA_PCA_CMP_PF', 1)    
    datum=get_datum(data, 'CDA_PCA_DSP_PF')
    plot_data(fig, i_panel, datum, 'CDA_PCA_DSP_PF', 2)    
    datum=get_datum(data, 'CDB_PCB_CMP_PF')
    plot_data(fig, i_panel, datum, 'CDB_PCB_CMP_PF', 7)    
    datum=get_datum(data, 'CDB_PCB_DSP_PF')
    plot_data(fig, i_panel, datum, 'CDB_PCB_DSP_PF', 8)    

    # Panel 7   
    i_panel += 1
    fig.update_yaxes(title_text='{}.CDE temp'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA', lo=250)
    plot_data(fig, i_panel, datum, 'CDA', 1)    
    datum=get_datum(data, 'CDB', lo=250)
    plot_data(fig, i_panel, datum, 'CDB', 7)    

    # Panel 8   
    i_panel += 1
    fig.update_yaxes(title_text='{}.2nd stage (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCA_2ND', lo=10, hi=250)
    plot_data(fig, i_panel, datum, 'PCA_2ND', 1)    
    datum=get_datum(data, 'PCB_2ND', lo=10, hi=250)
    plot_data(fig, i_panel, datum, 'PCB_2ND', 7)    
    
    # Panel 9   
    i_panel += 1
    fig.update_yaxes(title_text='{}.1st stage (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCA_1ST', hi=250, lo=10)
    plot_data(fig, i_panel, datum, 'PCA_1ST', 1)    
    datum=get_datum(data, 'PCB_1ST', hi=250, lo=10)
    plot_data(fig, i_panel, datum, 'PCB_1ST', 7)    
    
    return fig

#-------------------------------------------------------
def update_fig_ccfreq(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST freq'.format(i_panel), showgrid=True, row=i_panel, col=1)
    #fig.update_yaxes(title_text=f'SC/PC<br> &phi; Hz'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_CMP_FREQ_SET_CAL')
    plot_data(fig, i_panel, datum, 'SC/PC-A', 1)  
    datum=get_datum(data, 'CDB_CMP_FREQ_SET_CAL')
    plot_data(fig, i_panel, datum, 'SC/PC-B', 2)    
    
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC freq'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'JTD_CMP_FREQ_SET_CAL')
    plot_data(fig, i_panel, datum, 'JTC', 3)        

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.Beat freq'.format(i_panel), showgrid=True, row=i_panel, col=1)
    data['Beat1'] = data['JTD_CMP_FREQ_SET_CAL']*7-data['CDB_CMP_FREQ_SET_CAL']*24
    data['Beat1']=data['Beat1'].apply(np.abs)
    datum=get_datum(data, 'Beat1')
    plot_data(fig, i_panel, datum, 'JTCx7-STx24', 3)        
    
    data['Beat2'] = data['JTD_CMP_FREQ_SET_CAL']*4-data['CDB_CMP_FREQ_SET_CAL']*14
    data['Beat2']=data['Beat2'].apply(np.abs)
    datum=get_datum(data, 'Beat2')
    plot_data(fig, i_panel, datum, 'JTCx4-STx14', 6)        
    
    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.SC-A phase'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_SCA_DSP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 5)        
    datum=get_datum(data, 'CDA_SCA_BLN_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'BLN', 6)        

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.SC-B phase'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDB_SCB_CMP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 4)        
    datum=get_datum(data, 'CDB_SCB_DSP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 6)        
    datum=get_datum(data, 'CDB_SCB_BLN_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'BLN', 8)        

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.PC-A phase'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDA_PCA_CMP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 4)        
    datum=get_datum(data, 'CDA_PCA_DSP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 6)        
    datum=get_datum(data, 'CDA_PCA_BLN_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'BLN', 8)        

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.PC-B phase'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CDB_PCB_CMP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 4)        
    datum=get_datum(data, 'CDB_PCB_DSP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'DSP', 6)        
    datum=get_datum(data, 'CDB_PCB_BLN_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'BLN', 8)        

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.JTC phase'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'JTD_DRVH_CMP_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'CMP_H', 3)        

    return fig

#-------------------------------------------------------
def update_fig_xbox(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Calo V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'XBOXA_CALO_BIAS1_CAL')
    plot_data(fig, i_panel, datum, 'A0', 0)    
    datum=get_datum(data, 'XBOXA_CALO_BIAS2_CAL')
    plot_data(fig, i_panel, datum, 'A1', 1)    
    datum=get_datum(data, 'XBOXB_CALO_BIAS1_CAL')
    plot_data(fig, i_panel, datum, 'B0', 2)    
    datum=get_datum(data, 'XBOXB_CALO_BIAS2_CAL')
    plot_data(fig, i_panel, datum, 'B1', 3)    
    
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.AC V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'XBOXA_AC_BIAS_VIN_CAL')
    plot_data(fig, i_panel, datum, 'A_IN', 4)        
    datum=get_datum(data, 'XBOXA_AC_BIAS_VOUT_CAL')
    plot_data(fig, i_panel, datum, 'A_OUT', 5)        
    datum=get_datum(data, 'XBOXB_AC_BIAS_VIN_CAL')
    plot_data(fig, i_panel, datum, 'B_IN', 6)        
    datum=get_datum(data, 'XBOXB_AC_BIAS_VOUT_CAL')
    plot_data(fig, i_panel, datum, 'B_OUT', 7)        
    
    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.JFET V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'XBOXA_JFET_VDD1_CAL')
    plot_data(fig, i_panel, datum, 'A0', 1)        
    datum=get_datum(data, 'XBOXA_JFET_VDD2_CAL')
    plot_data(fig, i_panel, datum, 'A1', 2)        
    datum=get_datum(data, 'XBOXB_JFET_VDD1_CAL')
    plot_data(fig, i_panel, datum, 'B0', 3)        
    datum=get_datum(data, 'XBOXB_JFET_VDD2_CAL')
    plot_data(fig, i_panel, datum, 'B1', 4)        

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.JFET VSS (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'XBOXA_JFET_VSS_CAL')
    plot_data(fig, i_panel, datum, 'A', 1)        
    datum=get_datum(data, 'XBOXB_JFET_VSS_CAL')
    plot_data(fig, i_panel, datum, 'B', 3)           

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.JFET T (K)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'XBOXA_JFET_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'A', 1)        
    datum=get_datum(data, 'XBOXB_JFET_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'B', 3)           

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.JFET HTR (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'XBOXA_JFET_HTR_VIN_CAL')
    plot_data(fig, i_panel, datum, 'A_IN', 4)        
    datum=get_datum(data, 'XBOXA_JFET_HTR_VOUT_CAL')
    plot_data(fig, i_panel, datum, 'A_OUT', 5)        
    datum=get_datum(data, 'XBOXB_JFET_HTR_VIN_CAL')
    plot_data(fig, i_panel, datum, 'B_IN', 6)        
    datum=get_datum(data, 'XBOXB_JFET_HTR_VOUT_CAL')
    plot_data(fig, i_panel, datum, 'B_OUT', 7)        

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.A temp (degC)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(0,5):
        datum=get_datum(data, 'XBOXA_TEMP%d_CAL' % i) 
        plot_data(fig, i_panel, datum - abs_zero, 'A%d' % i, i)        
    
    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.B temp (degC)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(0,5):
        datum=get_datum(data, 'XBOXB_TEMP%d_CAL' % i)
        plot_data(fig, i_panel, datum - abs_zero, 'B%d' % i, i)        

    return fig

#-------------------------------------------------------
def update_fig_rates(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.S1 Ctrl (mK)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST1_CTL', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S1 Ctrl', 0)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.CPU load'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for q in Quads:
        datum=1.0-data['%s_CPUTIME_US_IDLE' % q]/1e6
        plot_data(fig, i_panel, datum, q, Quads.index(q))
    
    # Panel 3-6
    # Only one rate to plot.
    _r = rates[0]
    base_rate = 0.01
    
    q_base=0
    for q in Quads:
        i_panel += 1
        fig.update_yaxes(title_text='{}.r (1/s)+{:.1e}'.format(i_panel, base_rate), type="log", showgrid=True, row=i_panel, col=1)
        for i in range(0,9):            
            datum=get_datum(data, '%s_P%02d_HP_CNT'.replace('HP',_r.upper()) % (q, i+q_base))            
            plot_data(fig, i_panel, datum+base_rate, '%02d' % (i+q_base), i)    
        if (_r == "PEDB"):
            datum=get_datum(data, '%s_ACP_EDB_CNT' % q)
            plot_data(fig, i_panel, datum+base_rate, 'AC', 9)    
        elif (_r == "PEDB"):
            datum=get_datum(data, '%s_ACP_LOST_CNT' % q)
            plot_data(fig, i_panel, datum+base_rate, 'AC', 9)    
        q_base+=9
    
    return fig

#-------------------------------------------------------
def update_fig_dist_pcu(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    busV1=get_datum(data, 'SXS_DIST_BUS_V_CAL')
    plot_data(fig, i_panel, busV1, 'DIST', 0)
    if (flg_bus is True):
        busV2=get_datum(data, 'PCU_BUS_V_CAL')
        plot_data(fig, i_panel, busV2, 'PCU', 2)
        if (busV2 is not None):
            plot_data(fig, i_panel, (busV2-busV1)*10, 'PCU-DIST x10', 4)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXS_DIST_DIST_CNTL_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'DIST', 1)    
    datum=get_datum(data, 'SXS_DIST_SXS_SWRA_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'SXS-SWR-A', 3)
    datum=get_datum(data, 'SXS_DIST_SXS_SWRB_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'SXS-SWR-B', 5)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXS_DIST_PSPA_XBOXA_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'PSP/XBOX-A', 1)
    datum=get_datum(data, 'SXS_DIST_PSPB_XBOXB_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'PSP/XBOX-B', 3)
    datum=get_datum(data, 'PCU_SXS_FWE_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'FWE', 5)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXS_DIST_CDA_CNTL_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'CDA CNTL', 1)
    datum=get_datum(data, 'SXS_DIST_CDB_CNTL_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'CDB CTRL', 7)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXS_DIST_CDA_DRV1_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'CDA DRV1/SC-A', 1)
    datum=get_datum(data, 'SXS_DIST_CDB_DRV1_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'CDB DRV1/SC-B', 7)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXS_DIST_CDA_DRV2_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'CDA DRV1/SC-A', 1)
    datum=get_datum(data, 'SXS_DIST_CDB_DRV2_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'CDB DRV2/SC-B', 7)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.P (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXS_DIST_JTD_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'JTD', 6)
    datum=get_datum(data, 'SXS_DIST_ADRC_PSYNC_I_CAL')
    plot_data(fig, i_panel, datum * busV1, 'ADRC/PSYNC', 7)

    return fig

#-------------------------------------------------------
def update_fig_fwe(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):

    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bus I'.format(i_panel), showgrid=True, row=i_panel, col=1)
    if (flg_bus is True):
        datum=get_datum(data, 'PCU_SXS_FWE_I_CAL')
        plot_data(fig, i_panel, datum, 'I_FWE (A)', 0)   
    else:
        datum=get_datum(data, 'I_FWE')
        plot_data(fig, i_panel, datum, 'I_FWE (A)', 0)    
        datum=get_datum(data, 'V_FWE')
        plot_data(fig, i_panel, datum, 'V_FWE (V)', 2)    
        datum=get_datum(data, 'P_FWE')
        plot_data(fig, i_panel, datum, 'P_FWE (W)', 4)    

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.DC/DC V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_DCDC1_P05_V_CAL', lo=-20, hi=20)
    plot_data(fig, i_panel, datum, '+5 V (nom)', 0)
    datum=get_datum(data, 'FWE_DCDC2_P05_V_CAL', lo=-20, hi=20)
    plot_data(fig, i_panel, datum, '+5 V (red)', 1)    
    datum=get_datum(data, 'FWE_DCDC1_P15_V_CAL', lo=-20, hi=20)
    plot_data(fig, i_panel, datum, '+15 V (nom)', 2)
    datum=get_datum(data, 'FWE_DCDC2_P15_V_CAL', lo=-20, hi=20)
    plot_data(fig, i_panel, datum, '+15 V (red)', 3)    
    datum=get_datum(data, 'FWE_DCDC1_N15_V_CAL', lo=-20, hi=20)
    plot_data(fig, i_panel, datum, '-15 V (nom)', 4)    
    datum=get_datum(data, 'FWE_DCDC2_N15_V_CAL', lo=-20, hi=20)
    plot_data(fig, i_panel, datum, '-15 V (red)', 5)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.MOT V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_V_MOT1_CAL')
    plot_data(fig, i_panel, datum, 'MOT V Nom', 0)    
    datum=get_datum(data, 'FWE_V_MOT2_CAL')
    plot_data(fig, i_panel, datum, 'MOT V Red', 1)  

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.MOT I (mA)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_I_MOT1A_CAL', lo=0, hi=300)
    plot_data(fig, i_panel, datum, 'MOT I Nom-A', 0)    
    datum=get_datum(data, 'FWE_I_MOT1B_CAL', lo=0, hi=300)
    plot_data(fig, i_panel, datum, 'MOT I Nom-B', 2)
    datum=get_datum(data, 'FWE_I_MOT2A_CAL', lo=0, hi=300)
    plot_data(fig, i_panel, datum, 'MOT I Red-A', 1)    
    datum=get_datum(data, 'FWE_I_MOT2B_CAL', lo=0, hi=300)
    plot_data(fig, i_panel, datum, 'MOT I Red-B', 3)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.MOT T (degC)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_T_MOT_SUPPLY1_CAL', lo=-20, hi=80)
    plot_data(fig, i_panel, datum, 'MOT T sup Nom', 0)    
    datum=get_datum(data, 'FWE_T_MOT_SUPPLY2_CAL', lo=-20, hi=80)
    plot_data(fig, i_panel, datum, 'MOT T sup Red', 1)   
    datum=get_datum(data, 'FWE_T_MOT1_CAL', lo=-20, hi=80)
    plot_data(fig, i_panel, datum, 'MOT T Nom', 2)    
    datum=get_datum(data, 'FWE_T_MOT2_CAL', lo=-20, hi=80)
    plot_data(fig, i_panel, datum, 'MOT T Red', 3)            

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.FW (deg)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_FW_POSITION1_CAL')
    plot_data(fig, i_panel, datum, 'FW Nom', 0)    
    datum=get_datum(data, 'FWE_FW_POSITION2_CAL')
    plot_data(fig, i_panel, datum, 'FW Red', 1)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.HV V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_V_IN_HV1_CAL')
    plot_data(fig, i_panel, datum, 'V HV Nom', 0)    
    datum=get_datum(data, 'FWE_V_IN_HV2_CAL')
    plot_data(fig, i_panel, datum, 'V HV Red', 1)

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.HV I (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'FWE_I_IN_HV1_CAL',lo=0, hi=200)
    plot_data(fig, i_panel, datum, 'I HV Nom', 0)    
    datum=get_datum(data, 'FWE_I_IN_HV2_CAL',lo=0, hi=200)
    plot_data(fig, i_panel, datum, 'I HV Red', 1)

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.HV, LED ON/OFF'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    for i in range(4):
        datum=get_datum(data, 'FWE_LED{}_ON_OFF'.format(i+1), hi=2)
        plot_data(fig, i_panel, datum, 'LED{}_ON/OFF'.format(i+1), i*2, enum=['off', 'on'])
    for i in range(2):
        datum=get_datum(data, 'FWE_HV{}_LEVEL'.format(i+1), hi=5)
        plot_data(fig, i_panel, datum, 'FWE_HV{}_LEVEL'.format(i+1), i*2+8, enum=['off', 'on'])    

    # Panel 10
    i_panel += 1
    fig.update_yaxes(title_text='{}.LED V (V)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum=get_datum(data, 'FWE_V_LED{}_CAL'.format(i+1), lo=0, hi=20)
        plot_data(fig, i_panel, datum, 'V LED{}'.format(i+1), i*2)    

    # Panel 11
    i_panel += 1
    fig.update_yaxes(title_text='{}.LED I (mA)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum=get_datum(data, 'FWE_I_LED{}_CAL'.format(i+1), lo=0, hi=50)
        plot_data(fig, i_panel, datum, 'I LED{}'.format(i+1), i*2)    
    
    # Panel 12
    i_panel += 1
    fig.update_yaxes(title_text='{}.LED (degC)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum=get_datum(data, 'FWE_T_LED{}_CAL'.format(i+1), lo=-20, hi=70)
        plot_data(fig, i_panel, datum, 'T LED{}'.format(i+1), i*2)    
    
    # Panel 13
    i_panel += 1
    fig.update_yaxes(title_text='{}.Pls spc (ms)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum_spc=get_datum(data, 'FWE_LED{}_PLS_SPC_CAL'.format(i+1), lo=0, hi=256)
        plot_data(fig, i_panel, datum_spc, 'LED{}_SPC (ms)'.format(i+1), i*2)

    # Panel 14
    i_panel += 1
    fig.update_yaxes(title_text='{}.Pls len (ms)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum_len=get_datum(data, 'FWE_LED{}_PLS_LEN_CAL'.format(i+1), lo=0, hi=256)
        plot_data(fig, i_panel, datum_len, 'LED{}_LEN (ms)'.format(i+1), i*2) 

    # Panel 15
    i_panel += 1
    fig.update_yaxes(title_text='{}.FWE time cnts'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum_len=get_datum(data, 'FWE_TI_DIST_GEN_CNT')
    plot_data(fig, i_panel, datum_len, 'FWE_TI_DIST_GEN_CNT', 0) 
    datum_len=get_datum(data, 'FWE_TIMECODE_GEN_CNT')
    plot_data(fig, i_panel, datum_len, 'FWE_TIMECODE_GEN_CNT', 2) 
    datum_len=get_datum(data, 'FWE_SPW_RECONNECT_CNT')
    plot_data(fig, i_panel, datum_len, 'FWE_SPW_RECONNECT_CNT', 4) 
    datum_len=get_datum(data, 'FWE_RMAP_REJ_CNT')
    plot_data(fig, i_panel, datum_len, 'FWE_RMAP_REJ_CNT', 6) 
    return fig

#-------------------------------------------------------
def update_fig_rates(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.S1 Ctrl (mK)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST1_CTL', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S1 Ctrl', 0)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.CPU load'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for q in Quads:
        datum=1.0-data['%s_CPUTIME_US_IDLE' % q]/1e6
        plot_data(fig, i_panel, datum, q, Quads.index(q))
    
    # Panel 3-6
    # Only one rate to plot.
    _r = rates[0]
    base_rate = 0.01
    
    q_base=0
    for q in Quads:
        i_panel += 1
        fig.update_yaxes(title_text='{}.r (1/s)+{:.1e}'.format(i_panel, base_rate), type="log", showgrid=True, row=i_panel, col=1)
        for i in range(0,9):            
            datum=get_datum(data, '%s_P%02d_HP_CNT'.replace('HP',_r.upper()) % (q, i+q_base))            
            plot_data(fig, i_panel, datum+base_rate, '%02d' % (i+q_base), i)    
        if (_r == "PEDB"):
            datum=get_datum(data, '%s_ACP_EDB_CNT' % q)
            plot_data(fig, i_panel, datum+base_rate, 'AC', 9)    
        elif (_r == "PEDB"):
            datum=get_datum(data, '%s_ACP_LOST_CNT' % q)
            plot_data(fig, i_panel, datum+base_rate, 'AC', 9)    
        q_base+=9
    
    return fig

#-------------------------------------------------------
def update_fig_lc(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    quad = quads[0]
    if  ( quad == 'a0' ) :
        base = 0
    elif  ( quad == 'a1' ) :
        base = 9
    if  ( quad == 'b0' ) :
        base = 18
    elif  ( quad == 'b1' ) :
        base = 27
    
    duration = 16.0
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.S1 Ctrl (mK)'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum=get_datum(data, 'ST1_CTL', hi=100, lo=0)
    plot_data(fig, i_panel, datum, 'S1 Ctrl', 0)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.FPGA (1/s)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for _px in range(0,9):
        px=_px + base
        datum0=get_datum(data, '%s_P%02d_PEDB_CNT' % (quad.upper(), _px))
        datum1=get_datum(data, '%s_P%02d_REJ_CNT' % (quad.upper(), _px))
        if (datum0 is not None) and (datum1 is not None):
            plot_data(fig, i_panel, (datum0-datum1)/duration, 'PEDB-REJ (px{:02d})'.format(px), px)   

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.uf H (/s)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for _px in range(0,9):
        px=_px + base
        datum_Hp=get_datum(data, 'HP%02d' % px)
        if (datum_Hp is not None):
            plot_data(fig, i_panel, datum_Hp/duration, 'H (px{:02d})'.format(px), px)   

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.uf M (/s)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for _px in range(0,9):
        px=_px + base
        datum_Mp=get_datum(data, 'MP%02d' % px, hi=2047)
        datum_Ms=get_datum(data, 'MS%02d' % px, hi=2047)
        if (datum_Mp is not None) and (datum_Ms is not None):
            plot_data(fig, i_panel, (datum_Mp+datum_Ms)/duration, 'M (px{:02d})'.format(px), px)   

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.uf L (/s)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for _px in range(0,9):
        px=_px + base
        datum_Lp=get_datum(data, 'LP%02d' % px)
        datum_Ls=get_datum(data, 'LS%02d' % px)
        if (datum_Lp is not None) and (datum_Ls is not None):
            plot_data(fig, i_panel, datum_Lp+datum_Ls, 'L (px{:02d})'.format(px), px)   

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.AC'.format(i_panel), showgrid=True, row=i_panel, col=1)    
    datum_AC=get_datum(data, 'AC')
    if (datum_AC is not None):
        plot_data(fig, i_panel, datum_AC, 'AC', 0)
    datum_AC_BL=get_datum(data, 'AC_BL')
    if (datum_AC_BL is not None):
        plot_data(fig, i_panel, datum_AC_BL, 'AC_BL', 1)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.Live time frac'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for _px in range(0,9):
        px=_px + base
        datum=get_datum(data, 'LTF%02d' % px)
        plot_data(fig, i_panel, datum, 'LTF (px{:02d})'.format(px), px)   

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.CPU load'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=1.0-data['%s_CPUTIME_US_IDLE' % quad.upper()]/1e6
    plot_data(fig, i_panel, datum, 'CPU rate', 0)

    def get_cpu_est(card, r_h, r_m, r_l, r_ac, r_fpga):
        'Return estimated CPU load.'

        pars=[
            0.01336677, # H
            0.01468106, # M
            0.00016663, # L
            0.00,       # AC
            0.00045255, # FPGA
            0.0659873,  # BL
        ]
        if (card == 'A0' or card == 'B0'):
            cpu_est = pars[0]*r_h + pars[1]*r_m + pars[2]*r_l + pars[4] * r_fpga + pars[5]
        elif (card == 'A1' or card == 'B1'):
            cpu_est = pars[0]*r_h + pars[1]*r_m + pars[2]*r_l + pars[3] * r_ac + pars[4] * r_fpga + pars[5]

        # Clip at 100%.
        if (cpu_est > 1.0):
            cpu_est = 1.0
            
        return cpu_est

    grades=['HP', 'MP', 'MS', 'LP', 'LS']
    for grade in grades :
        data[grade] = data[[
            '%s%02d' % (grade, base+0),\
            '%s%02d' % (grade, base+1),\
            '%s%02d' % (grade, base+2),\
            '%s%02d' % (grade, base+3),\
            '%s%02d' % (grade, base+4),\
            '%s%02d' % (grade, base+5),\
            '%s%02d' % (grade, base+6),\
            '%s%02d' % (grade, base+7),\
            '%s%02d' % (grade, base+8)]].sum(axis=1)
    grades_ext = grades
    grades_ext.append('AC')        
    #grades_ext.append('AC')
    #datum0=get_datum(data, '%s_P%02d_PEDB_CNT' % (quad.upper(), _px))
    #datum1=get_datum(data, '%s_P%02d_REJ_CNT' % (quad.upper(), _px))
    #datum=data[grades_ext].apply(lambda x : get_cpu_est(quad.upper(), x[0], x[1]+x[2], x[3]+x[4], x[5], x[6]), axis=1)
    #plot_data(fig, i_panel, datum, 'CPU rate est', 1)

    return fig

#-------------------------------------------------------
def update_fig_nm(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    cards = ['A0', 'A1', 'B0', 'B1']    
    
    # Panel 1-4
    for _c in range(len(cards)):        
        i_panel += 1
        fig.update_yaxes(title_text='{}.{} ADUSTD'.format(i_panel, cards[_c]), showgrid=True, row=i_panel, col=1)
        for _p in range(0+9*_c,9+9*_c):
            datum=get_datum(data, 'ADUSTD%02d' % _p)
            plot_data(fig, i_panel, datum, 'p%02d' % _p, _p%9)    

    # Panel 5-8
    for _c in range(len(cards)):        
        i_panel += 1
        fig.update_yaxes(title_text='{}.{} ADUMAX'.format(i_panel, cards[_c]), showgrid=True, row=i_panel, col=1)
        for _p in range(0+9*_c,9+9*_c):
            datum=get_datum(data, 'ADUMAX%02d' % _p)
            plot_data(fig, i_panel, datum, 'p%02d' % _p, _p%9)    

    return fig

#-------------------------------------------------------
def update_fig_bl(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    cards = ['A0', 'A1', 'B0', 'B1']
    # p1 of 50 mK gain curve for TC4.
    gains = [
    0.28182, 0.31013, 0.30365, 0.30124, 0.30363, 0.29100,
    0.30189, 0.31029, 0.30144, 0.30259, 0.30088, 0.28976,
    0.28701, 0.29218, 0.26977, 0.29352, 0.34558, 0.29548,
    0.28664, 0.28196, 0.28366, 0.29045, 0.28698, 0.26369,
    0.28782, 0.30054, 0.28833, 0.29031, 0.28839, 0.27471,
    0.26462, 0.29120, 0.29195, 0.29430, 0.29628, 0.28673,
    ]
    sigma2fwhm=2*np.sqrt(2*np.log(2))

    # Panel 1-4
    for _c in range(len(cards)):        
        i_panel += 1
        fig.update_yaxes(title_text='{}.{} FWHM (eV)'.format(i_panel, cards[_c]), showgrid=True, row=i_panel, col=1)
        for _p in range(0+9*_c,9+9*_c):
            datum=get_datum(data, 'BL_STD%02d' % _p)
            plot_data(fig, i_panel, datum*gains[_p]*sigma2fwhm, 'p%02d_STD' % _p, _p%9)    

    # Panel 5-8
    for _c in range(len(cards)):        
        i_panel += 1
        fig.update_yaxes(title_text='{}.{} MEAN (eV)'.format(i_panel, cards[_c]), showgrid=True, row=i_panel, col=1)
        for _p in range(0+9*_c,9+9*_c):
            datum=get_datum(data, 'BL_MEAN%02d' % _p)
            plot_data(fig, i_panel, datum*gains[_p]*sigma2fwhm, 'p%02d_MEAN' % _p, _p%9)    

    return fig

#-------------------------------------------------------
def update_fig_gain(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Correction'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'COR_FIT')
    plot_data(fig, i_panel, datum, 'fit', 0)    
    datum=get_datum(data, 'COR_AVG')
    plot_data(fig, i_panel, datum, 'average', 1)    
    
    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.chi^2'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'CHISQ')
    plot_data(fig, i_panel, datum, 'A_IN', 0)
        
    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.Average'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'AVGUNBIN')
    plot_data(fig, i_panel, datum, 'Unbin', 0)        
    datum=get_datum(data, 'AVGBIN')
    plot_data(fig, i_panel, datum, 'bin', 1)        
    datum=get_datum(data, 'AVGFIT')
    plot_data(fig, i_panel, datum, 'fit', 2)        

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.Shift'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SHIFT')
    plot_data(fig, i_panel, datum, 'Shift', 0)        

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.Scale'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SCALE')
    plot_data(fig, i_panel, datum, 'Scale', 0)        

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.Background'.format(i_panel), showgrid=True, type="log", row=i_panel, col=1)
    datum=get_datum(data, 'BGRND')
    plot_data(fig, i_panel, datum, 'Bkg', 0)        

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.Slope'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SLOPE')
    plot_data(fig, i_panel, datum, 'Slope', 0)       

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.Width'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'WIDTH')
    plot_data(fig, i_panel, datum, 'Width', 0)   
    
    return fig

#-------------------------------------------------------
def update_fig_xtend(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):

    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.I_SXI (A)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_SXI_CD_I_CAL')
    plot_data(fig, i_panel, datum, 'I_CD', 0)
    datum=get_datum(data, 'PCU_SXI_DE_I_CAL')
    plot_data(fig, i_panel, datum, 'I_DE', 3)
    datum=get_datum(data, 'PCU_SXI_PE_I_CAL')
    plot_data(fig, i_panel, datum, 'I_PE', 6)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.P_SXI (W)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    bus_v=get_datum(data, 'PCU_BUS_V_CAL')
    datum=get_datum(data, 'PCU_SXI_CD_I_CAL')
    plot_data(fig, i_panel, datum*bus_v, 'P_CD', 0)
    datum=get_datum(data, 'PCU_SXI_DE_I_CAL')
    plot_data(fig, i_panel, datum*bus_v, 'P_DE', 3)
    datum=get_datum(data, 'PCU_SXI_PE_I_CAL')
    plot_data(fig, i_panel, datum*bus_v, 'P_PE', 6)

    # Panel 3-5
    i_panel = 2
    for VIP in VIPs:
        i_panel += 1
        i_color = 0
        fig.update_yaxes(title_text='{}.{}_SXI_CD'.format(i_panel, VIP), showgrid=True, row=i_panel, col=1)
        for AB in ['A']: #ABs:
            datum=get_datum(data, 'SXI_CD_DRV%s_CMP_%s_CAL' % (AB, VIP))
            if ((datum is not None) and (len(data)>0)):
                plot_data(fig, i_panel, datum, '%s_CMP_%s' % (VIP, AB), i_color*4)
            datum=get_datum(data, 'SXI_CD_DRV%s_BLN_%s_CAL' % (AB, VIP))
            if ((datum is not None) and (len(data)>0)):
                plot_data(fig, i_panel, datum*10.0, '%s_BLN_%sx10' % (VIP, AB), i_color*4+2)    
            i_color += 1

    # Panel 6
    i_panel += 1 
    fig.update_yaxes(title_text='{}.f_SXI_CD (Hz)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXI_CD_CMP_FREQ_SET_CAL')
    plot_data(fig, i_panel, datum, 'f_SXI_CD', 0)        

    # Panel 7
    i_panel += 1 
    fig.update_yaxes(title_text='{}.phi_SXI_CD (deg)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'SXI_CD_DRVA_BLN_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'phi_SXI_CD_A', 1)        
    datum=get_datum(data, 'SXI_CD_DRVB_BLN_PHASE_OFFSET_CAL')
    plot_data(fig, i_panel, datum, 'phi_SXI_CD_B', 3)    
    
    return fig

#-------------------------------------------------------
def update_fig_pcu_pow(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bus V'.format(i_panel), showgrid=True, row=i_panel, col=1)
    busV=get_datum(data, 'PCU_BUS_V_CAL')
    plot_data(fig, i_panel, busV, 'Bus V', 0)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.Load I'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_PCU_I_CAL')
    plot_data(fig, i_panel, datum, 'PCU I', 0)
    datum=get_datum(data, 'PCU_LOAD_I_CAL')
    plot_data(fig, i_panel, datum, 'Load I', 1)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.SMU/AOCP/DR'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for C in ['SMU', 'AOCP', 'DR']:
        for AB in ABs:
            datum=get_datum(data, 'PCU_{}_{}_I_CAL'.format(C,AB))
            plot_data(fig, i_panel, datum*busV, 'P_{}_{}'.format(C,AB), i_color)
            i_color += 2

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.SWR'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for C in ['SWR1', 'SWR2', 'SWR3']:
        for AB in ABs:
            datum=get_datum(data, 'PCU_{}_{}_I_CAL'.format(C,AB))
            plot_data(fig, i_panel, datum*busV, 'P_{}_{}'.format(C,AB), i_color)
            i_color += 2

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.HCE/MSE'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for C in ['HCE']:
        for AB in ABs:
            datum=get_datum(data, 'PCU_{}_{}_I_CAL'.format(C,AB))
            plot_data(fig, i_panel, datum*busV, 'P_{}_{}'.format(C,AB), i_color)
            i_color += 2
    datum=get_datum(data, 'PCU_M_HCE_I_CAL')
    plot_data(fig, i_panel, datum*busV, 'P_MHCE', i_color)
    i_color += 2
    datum=get_datum(data, 'PCU_MSE_I_CAL')
    plot_data(fig, i_panel, datum*busV, 'P_MSE', i_color)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.RF'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    datum=get_datum(data, 'PCU_TCIM_STRP_I_CAL')
    plot_data(fig, i_panel, datum*busV, 'P_STRP', i_color)
    i_color += 2
    datum=get_datum(data, 'PCU_TCIM_XMOD_XPA_I_CAL')
    plot_data(fig, i_panel, datum*busV, 'P_XMOD_XPA', i_color)
    i_color += 2
    datum=get_datum(data, 'PCU_GPSP_I_CAL')
    plot_data(fig, i_panel, datum*busV, 'P_GPSP', i_color)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.ACIM1'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for C in ['ACIM_MTQ', 'ACIM_RW_IRU_SG', 'ACIM_RCS']:
        for AB in ABs:
            datum=get_datum(data, 'PCU_{}_{}_I_CAL'.format(C,AB))
            plot_data(fig, i_panel, datum*busV, 'P_{}_{}'.format(C,AB), i_color)
            i_color += 2

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.ACIM1'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for C in ['ACIM_RCS']:
        for AB in ABs:
            datum=get_datum(data, 'PCU_{}_{}DRV_I_CAL'.format(C,AB))
            plot_data(fig, i_panel, datum*busV, 'P_{}_DRV_{}'.format(C,AB), i_color)
            i_color += 2
    for N in range(3) : 
        datum=get_datum(data, 'PCU_ACIM_STT_STT{:d}_I_CAL'.format(N+1))
        plot_data(fig, i_panel, datum*busV, 'P_STT{:d}'.format(N+1), i_color)
        i_color += 2

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.RW'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for N in range(4) : 
        datum=get_datum(data, 'PCU_RW{:d}_I_CAL'.format(N+1))
        plot_data(fig, i_panel, datum*busV, 'P_RW{:d}'.format(N+1), i_color)
        i_color += 2

    # Panel 10
    i_panel += 1
    fig.update_yaxes(title_text='{}.IRU'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for C in ['A2', 'A3', 'B1', 'B2'] : 
        datum=get_datum(data, 'PCU_IRU_{}_I_CAL'.format(C))
        plot_data(fig, i_panel, datum*busV, 'P_IRU_{}'.format(C), i_color)
        i_color += 2

    return fig

#-------------------------------------------------------
def update_fig_eps(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bus V'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_BUS_V_CAL')
    plot_data(fig, i_panel, datum, 'Bus V', 0)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.Load I'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_PCU_I_CAL')
    plot_data(fig, i_panel, datum, 'PCU I', 0)
    datum=get_datum(data, 'PCU_LOAD_I_CAL')
    plot_data(fig, i_panel, datum, 'Load I', 1)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.Shunt V'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_SHNT_CNT_V_CAL')
    plot_data(fig, i_panel, datum, 'Shunt V', 0)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.Shunt I'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(2):
        datum=get_datum(data, 'PCU_SHNT{:d}_I_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'Shunt I{:d}'.format(i+1), 2*i+1)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bat V'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(2):
        datum=get_datum(data, 'BCCU{:d}_BAT{:d}_BAT_V_CAL'.format(i+1, i+1))
        plot_data(fig, i_panel, datum, 'BAT{:d}'.format(i+1), 2*i+1)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bat I1'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(2):
        datum=get_datum(data, 'BCCU1_BAT1_BAT_I{:d}_CHG_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'BAT CHG I{:d}'.format(i+1), 2*i)
        datum=get_datum(data, 'BCCU1_BAT1_BAT_I{:d}_DIS_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'BAT DIS I{:d}'.format(i+1), 2*i+1)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.Bat I2'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(2):
        datum=get_datum(data, 'BCCU2_BAT2_BAT_I{:d}_CHG_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'BAT CHG I{:d}'.format(i+1), 2*i)
        datum=get_datum(data, 'BCCU2_BAT2_BAT_I{:d}_DIS_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'BAT DIS I{:d}'.format(i+1), 2*i+1)

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.SAP cell V'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(12):
        datum=get_datum(data, 'BCCU1_BAT1_CEL_{:d}V_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'Cell 1-{:02d}'.format(i+1), i)

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.SAP cell V'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(12):
        datum=get_datum(data, 'BCCU2_BAT2_CEL_{:d}V_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'Cell 2-{:02d}'.format(i+1), i)

    return fig

#-------------------------------------------------------
def update_fig_aocs(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.I_RW'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum=get_datum(data, 'PCU_RW{:d}_I_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'I_RW{:d}'.format(i+1), i*2)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.RW rpm'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum=get_datum(data, 'ACPA_RW{:d}_RATE_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'A:RW{:d} rpm'.format(i+1), 2*i)
    for i in range(4):
        datum=get_datum(data, 'ACPB_RW{:d}_RATE_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'B:RW{:d} rpm'.format(i+1), 2*i+1)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.RW Totque (Nm)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i in range(4):
        datum=get_datum(data, 'ACPA_RW{:d}_TC_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'A:RW{:d} torque'.format(i+1), 2*i)
    for i in range(4):
        datum=get_datum(data, 'ACPB_RW{:d}_TC_CAL'.format(i+1))
        plot_data(fig, i_panel, datum, 'B:RW{:d} torque'.format(i+1), 2*i+1)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.I_MTQ'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum1=get_datum(data, 'PCU_ACIM_MTQ_A_I_CAL')
    datum2=get_datum(data, 'PCU_ACIM_RCS_ADRV_I_CAL')
    datum3=get_datum(data, 'PCU_ACIM_RCS_A_I_CAL')
    datum4=get_datum(data, 'PCU_ACIM_RW_IRU_SG_A_I_CAL')
    plot_data(fig, i_panel, datum1, 'I_ACIM_A', 0)

    datum1=get_datum(data, 'PCU_ACIM_MTQ_B_I_CAL')
    datum2=get_datum(data, 'PCU_ACIM_RCS_BDRV_I_CAL')
    datum3=get_datum(data, 'PCU_ACIM_RCS_B_I_CAL')
    datum4=get_datum(data, 'PCU_ACIM_RW_IRU_SG_B_I_CAL')
    plot_data(fig, i_panel, datum1, 'I_ACIM_B', 4)
    
    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.MTQ mag mon'.format(i_panel), showgrid=True, row=i_panel, col=1)
    for i, c in enumerate(['X', 'Y', 'Z']) : 
        datum1=get_datum(data, 'ACPA_ACIM_MTQ_A_MTQ_{}_MAGMOM_CAL'.format(c))
        datum2=get_datum(data, 'ACPA_ACIM_MTQ_A_MTQ_{}_SIGN'.format(c))
        if ( len(datum1) > 0 ) and  ( len(datum2) > 0 ):
            datum3 = datum1 * (2*datum2-1)
            plot_data(fig, i_panel, datum3, 'A:m_{}'.format(c), i*2)
        datum1=get_datum(data, 'ACPB_ACIM_MTQ_B_MTQ_{}_MAGMOM_CAL'.format(c))
        datum2=get_datum(data, 'ACPB_ACIM_MTQ_B_MTQ_{}_SIGN'.format(c))
        if ( len(datum1) > 0 ) and  ( len(datum2) > 0 ):
            datum3 = datum1 * (2*datum2-1)
            plot_data(fig, i_panel, datum3, 'B:m_{}'.format(c), i*2+1)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.Geomag (nT)'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for s in ['X', 'Y', 'Z']:
        datum=get_datum(data, 'ACPA_GAS_GEOMAG_BDY_{}_CAL'.format(s))
        plot_data(fig, i_panel, datum, 'A:GAS_{}'.format(s), 2*i_color)
        i_color +=1
    #i_color=0
    #for s in ['X', 'Y', 'Z']:
    #    datum=get_datum(data, 'ACPA_ENV_GEOMAG_{}_CAL'.format(s))
    #    plot_data(fig, i_panel, datum, 'A:ENV_{}'.format(s), 2*i_color+1)
    #    i_color +=1 
    i_color=0
    for s in ['X', 'Y', 'Z']:
        datum=get_datum(data, 'ACPB_GAS_GEOMAG_BDY_{}_CAL'.format(s))
        plot_data(fig, i_panel, datum, 'B:GAS_{}'.format(s), 2*i_color)
        i_color +=1
    #i_color=0
    #for s in ['X', 'Y', 'Z']:
    #    datum=get_datum(data, 'ACPB_ENV_GEOMAG_{}_CAL'.format(s))
    #    plot_data(fig, i_panel, datum, 'B:ENV_{}'.format(s), 2*i_color+1)
    #    i_color +=1 

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.I_IRU'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_IRU_A2_I_CAL')
    plot_data(fig, i_panel, datum, 'IRU_A2', 0)
    datum=get_datum(data, 'PCU_IRU_A3_I_CAL')
    plot_data(fig, i_panel, datum, 'IRU_A3', 2)
    datum=get_datum(data, 'PCU_IRU_B1_I_CAL')
    plot_data(fig, i_panel, datum, 'IRU_B1', 4)
    datum=get_datum(data, 'PCU_IRU_B2_I_CAL')
    plot_data(fig, i_panel, datum, 'IRU_B2', 6)

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.Rate_IRU'.format(i_panel), showgrid=True, row=i_panel, col=1)
    i_color=0
    for i in ['2_B', '2_C', '3_C', '3_A']:
        datum=get_datum(data, 'ACPA_IRU_A_TDG{}_RATE_CAL'.format(i))
        plot_data(fig, i_panel, datum, 'Rate_TDG_{}'.format(i), i_color)
        i_color+=2
    for i in ['1_A', '1_B', '2_B', '2_C']:
        datum=get_datum(data, 'ACPA_IRU_B_TDG{}_RATE_CAL'.format(i))
        plot_data(fig, i_panel, datum, 'Rate_TDG_{}'.format(i), i_color+1)
        i_color+=2  
    for i in ['2_B', '2_C', '3_C', '3_A']:
        datum=get_datum(data, 'ACPB_IRU_A_TDG{}_RATE_CAL'.format(i))
        plot_data(fig, i_panel, datum, 'Rate_TDG_{}'.format(i), i_color)
        i_color+=2
    for i in ['1_A', '1_B', '2_B', '2_C']:
        datum=get_datum(data, 'ACPB_IRU_B_TDG{}_RATE_CAL'.format(i))
        plot_data(fig, i_panel, datum, 'Rate_TDG_{}'.format(i), i_color+1)
        i_color+=2  

    # Panel 9 (STT, DSS)

    return fig

#-------------------------------------------------------
def update_fig_lhp(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_SCA_CMP'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCA_CMP_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_L_SCA_CMP', 0, enum=['off', 'on'])
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCA_CMP_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_C_SCA_CMP', 2, enum=['off', 'on'])
    datum=get_datum(data, 'M_HCE_STS_SXS_LHP_V_SCA_CMP_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_V_SCA_CMP', 4, enum=['off', 'on'])
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCA_CMP_PR_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_S_SCA_CMP_Pr', 1, enum=['off', 'on'])
    datum=get_datum(data, 'HCE_B_STS_SXS_LHPS_SCA_CMP_BU_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_S_SCA_CMP_Bu', 3, enum=['off', 'on'])

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SCA_CMP'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCA_CMP_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_L_SCA_CMP', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCA_CMP_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_C_SCA_CMP', 2)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHP_V_SCA_CMP_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_V_SCA_CMP', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCA_CMP_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCA_CMP_Pr', 1)
    datum=get_datum(data, 'HCE_B_STS_SXS_LHPS_SCA_CMP_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCA_CMP_Bu', 3)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_SCA_CHD'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCA_CHD_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_L_SCA_CHD', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCA_CHD_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_C_SCA_CHD', 2)
    datum=get_datum(data, 'HCE_B_STS_SXS_LHP_V_SCA_CHD_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_V_SCA_CHD', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCA_CHD_PR_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_S_SCA_CHD_Pr', 1)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCA_CHD_BU_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_S_SCA_CHD_Bu', 3)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SCA_CHD'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCA_CHD_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_L_SCA_CHD', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCA_CHD_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_C_SCA_CHD', 2)
    datum=get_datum(data, 'HCE_B_STS_SXS_LHP_V_SCA_CHD_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_V_SCA_CHD', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCA_CHD_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCA_CHD_Pr', 1)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCA_CHD_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCA_CHD_Bu', 3)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_SCB_CMP'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCB_CMP_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_L_SCB_CMP', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCB_CMP_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_C_SCB_CMP', 2)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHP_V_SCB_CMP_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_V_SCB_CMP', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCB_CMP_PR_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_S_SCB_CMP_Pr', 1)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHPS_SCB_CMP_BU_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_S_SCB_CMP_Bu', 3)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SCB_CMP'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCB_CMP_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_L_SCB_CMP', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCB_CMP_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_C_SCB_CMP', 2)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHP_V_SCB_CMP_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_V_SCB_CMP', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCB_CMP_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCB_CMP_Pr', 1)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHPS_SCB_CMP_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCB_CMP_Bu', 3)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_SCB_CHD'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCB_CHD_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_L_SCB_CHD', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCB_CHD_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_C_SCB_CHD', 2)
    datum=get_datum(data, 'HCE_B_STS_SXS_LHP_V_SCB_CHD_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_V_SCB_CHD', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCB_CHD_BU_ON_OFF', enum=['off', 'on'])
    plot_data(fig, i_panel, datum, 'HTR_S_SCB_CHD_Pr', 1)
    
    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SCB_CHD'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_L_SCB_CHD_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_L_SCB_CHD', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_LHP_C_SCB_CHD_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_C_SCB_CHD', 2)
    datum=get_datum(data, 'HCE_B_STS_SXS_LHP_V_SCB_CHD_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_V_SCB_CHD', 4)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCB_CHD_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCB_CHD_Pr', 1)
    datum=get_datum(data, 'M_HCE_STS_SXS_LHPS_SCB_CHD_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_S_SCB_CHD_Bu', 3)

    def temp_cc_sub_sc(elem):
        datum=get_datum(data, 'SCA_%s' % elem, lo=10, hi=350)
        plot_data(fig, i_panel, datum, 'SCA_%s' % elem, 0)    
        datum=get_datum(data, 'SCB_%s' % elem, lo=10, hi=350)
        plot_data(fig, i_panel, datum, 'SCB_%s' % elem, 2)    
    def temp_cc_sub_pc(elem):
        datum=get_datum(data, 'PCA_%s' % elem, lo=10, hi=350) 
        plot_data(fig, i_panel, datum, 'PCA_%s' % elem, 4)    
        datum=get_datum(data, 'PCB_%s' % elem, lo=10, hi=350)
        plot_data(fig, i_panel, datum, 'PCB_%s' % elem, 6)

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST CPY'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('CPY')
    temp_cc_sub_pc('CPY')

    # Panel 10
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST CHD'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('CHD')
    temp_cc_sub_pc('CHD')

    # Panel 11
    i_panel += 1
    fig.update_yaxes(title_text='{}.2ST CMP'.format(i_panel), showgrid=True, row=i_panel, col=1)
    temp_cc_sub_sc('CMP')
    temp_cc_sub_pc('CMP')
    return fig

#-------------------------------------------------------
def update_fig_hce(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    
    # Somehow, TIME appears twice.
    data_tmp = data.loc[:,~data.columns.duplicated()]
    data = data_tmp
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SP2-4'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SP2U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP2U', 0)
    datum=get_datum(data, 'HCE_A_STS_SP2M_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP2M', 1)
    datum=get_datum(data, 'HCE_A_STS_SP3U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP3U', 2)
    datum=get_datum(data, 'HCE_A_STS_SP3L_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP3L', 3)
    datum=get_datum(data, 'HCE_A_STS_SP4U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP4U', 4)
    datum=get_datum(data, 'HCE_A_STS_SP4L_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP4L', 5)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SP5-1'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SP5U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP5U', 0)
    datum=get_datum(data, 'HCE_A_STS_SP5L_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP5L', 1)
    datum=get_datum(data, 'HCE_A_STS_SP6U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP6U', 2)
    datum=get_datum(data, 'HCE_A_STS_SP6L_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP6L', 3)
    datum=get_datum(data, 'HCE_A_STS_SP8U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP8U', 4)
    datum=get_datum(data, 'HCE_A_STS_SP8L_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP8L', 5)
    datum=get_datum(data, 'HCE_A_STS_SP1U_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SP1U', 6)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_RAD'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_B_STS_SXS_RAD6_1_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD6_1_Pr', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_RAD6_1_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD6_1_Bu', 1)
    datum=get_datum(data, 'HCE_A_STS_SXS_RAD6_2_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD6_2_Pr', 2)
    datum=get_datum(data, 'HCE_B_STS_SXS_RAD6_2_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD6_2_Bu', 3)
    #
    datum=get_datum(data, 'HCE_B_STS_SXS_RAD8_1_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD8_1_Pr', 4)
    datum=get_datum(data, 'HCE_A_STS_SXS_RAD8_1_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD8_1_Bu', 5)
    datum=get_datum(data, 'HCE_A_STS_SXS_RAD8_2_PR_TEMP_CALL')
    plot_data(fig, i_panel, datum, 'RAD8_2_Pr', 6)
    datum=get_datum(data, 'HCE_B_STS_SXS_RAD8_2_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'RAD8_2_Bu', 7)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_BRK/CP'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'HCE_A_STS_SXS_BRK6_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'BRK6_Pr', 0)
    datum=get_datum(data, 'HCE_B_STS_SXS_BRK6_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'BRK6_Bu', 1)
    datum=get_datum(data, 'HCE_B_STS_SXS_BRK8_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'BRK8_Pr', 2)
    datum=get_datum(data, 'HCE_A_STS_SXS_BRK8_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'BRK8_Bu', 3)
    #
    datum=get_datum(data, 'HCE_A_STS_SXS_CP6_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'CP6_Pr', 4)
    datum=get_datum(data, 'HCE_B_STS_SXS_CP6_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'CP6_Bu', 5)
    datum=get_datum(data, 'HCE_B_STS_SXS_CP8_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'CP8_Pr', 6)
    datum=get_datum(data, 'HCE_A_STS_SXS_CP8_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'CP8_Bu', 7)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SXT-S'.format(i_panel), showgrid=True, row=i_panel, col=1)
    #
    datum=get_datum(data, 'HCE_B_STS_SXT_S_PR1_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_1_Pr', 0)
    datum=get_datum(data, 'HCE_A_STS_SXT_S_BU1_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_1_Bu', 3)
    datum=get_datum(data, 'HCE_B_STS_SXT_S_PR2_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_2_Pr', 1)
    datum=get_datum(data, 'HCE_A_STS_SXT_S_BU2_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_2_Bu', 4)
    datum=get_datum(data, 'HCE_B_STS_SXT_S_PR3_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_3_Pr', 8)
    datum=get_datum(data, 'HCE_A_STS_SXT_S_BU3_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_3_Bu', 5)
    datum=get_datum(data, 'HCE_A_STS_SXT_S_PR4_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_4_Pr', 2)
    datum=get_datum(data, 'HCE_B_STS_SXT_S_BU4_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'SXT_S_4_Bu', 0)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_EBOX'.format(i_panel), showgrid=True, row=i_panel, col=1)
    #
    datum=get_datum(data, 'HCE_B_STS_SXS_CDE_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'CDE_Pr', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_CDE_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'CDE_Bu', 1)
    datum=get_datum(data, 'HCE_A_STS_SXS_ADRC_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'ADRC', 8)
    datum=get_datum(data, 'HCE_B_STS_SXS_PSU_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'PSU_Pr', 2)
    datum=get_datum(data, 'HCE_A_STS_SXS_PSU_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'PSU_Bu', 3)
    datum=get_datum(data, 'HCE_A_STS_XBOX_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'XBOX_Pr', 4)
    datum=get_datum(data, 'HCE_B_STS_XBOX_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'XBOX_Bu', 5)
    datum=get_datum(data, 'HCE_A_STS_SXS_PSP_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'PSP_Pr', 0)
    datum=get_datum(data, 'HCE_B_STS_SXS_PSP_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'PSP_Bu', 1)

    return fig

#-------------------------------------------------------
def update_fig_vis(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.VIS_ST'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    for i, c in enumerate(['SC_A', 'SC_B', 'PC_A', 'PC_B']):
        datum=get_datum(data, 'MSE_SXS_VIS{}_{}_ST'.format(i+1,c))
        plot_data(fig, i_panel, datum, 'Status_{}'.format(c), i, enum=['unlock', 'lock'])

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_SCA'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_SCA_PR_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_SCA_Pr', 0, enum=['off', 'on'])
    datum=get_datum(data, 'HCE_A_STS_SXS_VIS_SCA_BU_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_SCA_Bu', 4, enum=['off', 'on'])

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SCA'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_SCA_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_SCA_Pr', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_VIS_SCA_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_SCA_Bu', 4)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_SCB'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_SCB_PR_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_SCB_Pr', 0, enum=['off', 'on'])
    datum=get_datum(data, 'HCE_B_STS_SXS_VIS_SCB_BU_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_SCB_Bu', 4, enum=['off', 'on'])

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_SCB'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_SCB_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_SCB_Pr', 0)
    datum=get_datum(data, 'HCE_B_STS_SXS_VIS_SCB_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_SCB_Bu', 4)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_PCA'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_PCA_PR_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_PCA_Pr', 0, enum=['off', 'on'])
    datum=get_datum(data, 'HCE_B_STS_SXS_VIS_PCA_BU_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_PCA_Bu', 4, enum=['off', 'on'])

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_PCA'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_PCA_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_PCA_Pr', 0)
    datum=get_datum(data, 'HCE_B_STS_SXS_VIS_PCA_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_PCA_Bu', 4)

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.HTR_PCB'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_PCB_PR_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_PCB_Pr', 0, enum=['off', 'on'])
    datum=get_datum(data, 'HCE_A_STS_SXS_VIS_PCB_BU_ON_OFF')
    plot_data(fig, i_panel, datum, 'HTR_PCB_Bu', 4, enum=['off', 'on'])

    # Panel 9
    i_panel += 1
    fig.update_yaxes(title_text='{}.T_PCB'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'M_HCE_STS_SXS_VIS_PCB_PR_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_PCB_Pr', 0)
    datum=get_datum(data, 'HCE_A_STS_SXS_VIS_PCB_BU_TEMP_CAL')
    plot_data(fig, i_panel, datum, 'T_PCB_Bu', 4)  

    return fig

#-------------------------------------------------------
def update_fig_dr(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0
    def dr_used(pe):
        if (pe > 0 ) and (pe < 128 ):
            return 1
        else:
            return 0

    """
    data_all_new=data_all.applymap(dr_used)
    def get_lv_pe_used(lv):
        npe=data_all_new[eval('\'DR_A_LV_%02X_CURR_PE\'' %lv)]
        for i in range(1,32):
            npe += data_all_new[eval('\'DR_A_LV_%02X_USED_PE%d\'') % (lv,i)]
        return npe
"""

    def get_lv_pe_used(data, lvs):
        data_all = None
        for lv in lvs:
            datum=data["DR_A_LV_{:02X}_CURR_PE".format(lv)].apply(lambda x: dr_used(x))
            for j in range(1,32):
                datum+=data["DR_A_LV_{:02X}_USED_PE{:d}".format(lv,j)].apply(lambda x: dr_used(x))

            if (data_all is None):
                data_all = datum
            else:
                data_all = pd.concat([data_all, datum], axis=1)
        data_all=pd.DataFrame(data_all)
        data_all.interpolate('ffill', inplace=True)
        data_all['NPE'] = data_all.sum(axis=1)

        return data_all['NPE']
    
    # Panel 1
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE rep/No use'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'DR_A_UNUSED_PE_NUM')
    plot_data(fig, i_panel, datum, 'PE no use', 0)
    datum=get_datum(data, 'DR_A_REP_QUE_CNT')
    plot_data(fig, i_panel, datum, 'PE rep queue', 2)

    # Panel 2
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE high'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [82,90])
    plot_data(fig, i_panel, datum, 'SXS/52,5Ah', 3)
    datum=get_lv_pe_used(data, [83,91])
    plot_data(fig, i_panel, datum, 'SXI/53,5Bh', 5)

    # Panel 3
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE med'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [98,106])
    plot_data(fig, i_panel, datum, 'SXS/62,6Ah', 3)
    datum=get_lv_pe_used(data, [99,107])
    plot_data(fig, i_panel, datum, 'SXI/63,6Bh', 5)

    # Panel 4
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE low'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [114,122])
    plot_data(fig, i_panel, datum, 'SXS/72,7Ah', 3)
    datum=get_lv_pe_used(data, [115,123])
    plot_data(fig, i_panel, datum, 'SXI/73,7Bh', 5)

    # Panel 5
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE HK'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [2])
    plot_data(fig, i_panel, datum, 'Ess/02h', 2)
    datum=get_lv_pe_used(data, [81])
    plot_data(fig, i_panel, datum, 'Mis/51h', 6)

    # Panel 6
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE Bus: AOCP+'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [32,33,34,35])
    plot_data(fig, i_panel, datum, 'AOCP-A/20-23h', 1)
    datum=get_lv_pe_used(data, [64,65])
    plot_data(fig, i_panel, datum, 'DR/40-41h', 3)
    datum=get_lv_pe_used(data, [5])
    plot_data(fig, i_panel, datum, 'HCE/05h', 5)
    datum=get_lv_pe_used(data, [6])
    plot_data(fig, i_panel, datum, 'GPS/06h', 7)
    datum=get_lv_pe_used(data, [7])
    plot_data(fig, i_panel, datum, 'BCCU/07h', 3)

    # Panel 7
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE Bus: SMU'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [3])
    plot_data(fig, i_panel, datum, 'SMU/03h', 1)
    datum=get_lv_pe_used(data, [4])
    plot_data(fig, i_panel, datum, 'NW/04h', 3)
    datum=get_lv_pe_used(data, [69])
    plot_data(fig, i_panel, datum, 'CmdLog/45h', 5)
    datum=get_lv_pe_used(data, [70])
    plot_data(fig, i_panel, datum, 'Time/46h', 7)

    # Panel 8
    i_panel += 1
    fig.update_yaxes(title_text='{}.PE Notif'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_lv_pe_used(data, [0,1])
    plot_data(fig, i_panel, datum, 'SC/00-01h', 1)
    datum=get_lv_pe_used(data, [80])
    plot_data(fig, i_panel, datum, 'Mis/50h', 3)

    return fig

#-------------------------------------------------------
def update_fig_rf(fig, data, rates, quads, flg_adrc, flg_cde, flg_dist, flg_bus):
    
    i_panel = 0

    # Panel 1 (PCU)
    i_panel += 1
    fig.update_yaxes(title_text='{}.PCU_I'.format(i_panel), showgrid=True, row=i_panel, col=1)
    datum=get_datum(data, 'PCU_TCIM_STRP_I_CAL')
    plot_data(fig, i_panel, datum, 'I_TCIM_STRP', 0)
    datum=get_datum(data, 'PCU_TCIM_XMOD_XPA_I_CAL')
    plot_data(fig, i_panel, datum, 'I_XMOD_XPA', 3)  
    datum=get_datum(data, 'PCU_GPSP_I_CAL')
    plot_data(fig, i_panel, datum, 'I_GPSR', 6)
    
    # Panel 2-4 (TCIM)
    for c in ['S_A', 'S_B', 'X']:
        i_panel += 1
        fig.update_yaxes(title_text='{}.TCIM_{}'.format(i_panel,c), showgrid=True, row=i_panel, col=1, type='category')
        datum=get_datum(data, 'TCIM_{}_TCIM_ID_1'.format(c))
        plot_data(fig, i_panel, datum, '{}: ID 1'.format(c), 0, enum=['B','A'])
        datum=get_datum(data, 'TCIM_{}_TCIM_ID_2'.format(c))
        plot_data(fig, i_panel, datum, '{}: ID 2'.format(c), 2, enum=['B','A']) 
        if (c == 'X'): 
            datum=get_datum(data, 'TCIM_{}_TRP_SEL_ST'.format(c))
            plot_data(fig, i_panel, datum, '{}: side'.format(c), 4, enum=['A','B'])
        else:
            datum=get_datum(data, 'TCIM_{}_STRP_SEL_ST'.format(c))
            plot_data(fig, i_panel, datum, '{}: side'.format(c), 4, enum=['A','B'])
        datum=get_datum(data, 'TCIM_{}_TCIM_MODE'.format(c))
        plot_data(fig, i_panel, datum, '{}: mode'.format(c), 6, enum=['stdby','ope'])
        #datum=get_datum(data, 'TCIM_{}_TLM_RATE'.format(c))
        #plot_data(fig, i_panel, datum, '{}: rate'.format(c), 8)

    # Panel 5 (S, RX)
    i_panel += 1
    fig.update_yaxes(title_text='{}.S_Rx'.format(i_panel,c), showgrid=True, row=i_panel, col=1, type='category')
    for AB1 in ABs : 
        for AB2 in ABs :
            i_color=0
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_USB_RNG_ENA_DIS'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{} : Rx RNG'.format(AB1, AB2), i_color, enum=['ena', 'dis'])
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_USB_MODE_ENA_DIS'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{} : Rx USB'.format(AB1, AB2), i_color+3, enum=['ena', 'dis'])
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_QPSK_MODE_ENA_DIS'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{} : Rx QPSK'.format(AB1, AB2), i_color+6, enum=['ena', 'dis'])

    # Panel 6 (S, Tx)
    i_panel += 1
    fig.update_yaxes(title_text='{}.S_Tx'.format(i_panel,c), showgrid=True, row=i_panel, col=1, type='category')
    for AB1 in ABs : 
        for AB2 in ABs :
            i_color=0
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_TX_ON_OFF'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{} : Tx RF'.format(AB1, AB2), i_color, enum=['on', 'off'])
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_USB_TX_ENA_DIS'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{} : Tx USB'.format(AB1, AB2), i_color+3, enum=['ena', 'dis'])
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_USB_TX_PWR_H_L'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{} : Tx PWR'.format(AB1, AB2), i_color+6, enum=['hi', 'lo'])

    # Panel 7 (X, Tx)
    i_panel += 1
    fig.update_yaxes(title_text='{}.X_Tx'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    #datum=get_datum(data, 'MSE_XPA_XPA_ONOFF', enum=['on', 'off'])
    #plot_data(fig, i_panel, datum, 'X : Tx RF', 0)
    datum=get_datum(data, 'MSE_XMOD_PWR_ST')
    plot_data(fig, i_panel, datum, 'X : Tx RF', 0, enum=['on', 'off'])
    datum=get_datum(data, 'MSE_XMOD_MODULATION_ST')
    plot_data(fig, i_panel, datum, 'X : Tx MOD', 0, enum=['on', 'off'])

    # Panel 8 (power)
    i_panel += 1
    fig.update_yaxes(title_text='{}.Power (dBm)'.format(i_panel,c), showgrid=True, row=i_panel, col=1)
    i_color=0
    for AB1 in ABs : 
        for AB2 in ABs :
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_INP_LVL_CAL'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{}: P'.format(AB1, AB2), i_color)
            datum=get_datum(data, 'TCIM_S_{}_STRP_{}_AGC_VOLT_CAL'.format(AB1, AB2))
            plot_data(fig, i_panel, datum, 'S_{}_{}: P'.format(AB1, AB2), i_color+1)
            i_color+=2
    datum=get_datum(data, 'MSE_XPA_PWR_MONI')
    plot_data(fig, i_panel, 10*np.log10(datum), 'X: P', 0)

    # Panel 9 (GPS)
    i_panel += 1
    fig.update_yaxes(title_text='{}.GPSR sts'.format(i_panel), showgrid=True, row=i_panel, col=1, type='category')
    datum=get_datum(data, 'MSE_GPSR_ORBIT_MODE')
    plot_data(fig, i_panel, datum, 'GPSR: orbit mode', 0, enum=['orbit','earth'])
    datum=get_datum(data, 'MSE_GPSR_ACQ_MODE')
    plot_data(fig, i_panel, datum, 'GPSR: acq mode', 2, enum=['cold','warm','navi'])
    datum=get_datum(data, 'MSE_GPSR_OPR_MODE')
    plot_data(fig, i_panel, datum, 'GPSR: op mode', 4, enum=['c/o','n/a','nom','sram'])

    return fig

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir name.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-R', '--resample',
        help='Resample seconds.',
        dest='resample',
        type=int,
        default=0,
        nargs='?'
        )
    parser.add_argument(
        '-E', '--evtlists',
        help='Event lists.',
        dest='evtlists',
        type=str,
        nargs='*'
        )
    parser.add_argument(
        '-p', '--plot',
        help='Plot.',
        dest='plot',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '--with-adrg',
        help='ADRG is used in place of ADRC.',
        dest='flg_adrc',
        action='store_false',
        default=True
        )
    parser.add_argument(
        '--with-shigse',
        help='SHI GSE is used in place of CDE.',
        dest='flg_cde',
        action='store_false',
        default=True
        )
    parser.add_argument(
        '--with-distgse',
        help='Power GSE is used in place of SXS-DIST.',
        dest='flg_dist',
        action='store_false',
        default=True
        )
    parser.add_argument(
        '--with-busgse',
        help='Power GSE is used in place of PCU.',
        dest='flg_bus',
        action='store_false',
        default=True
        )

    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    events = get_events(args.evtlists, datetime_start, datetime_end)
    
    # Plot    
    plot = args.plot[0]

    # Outstem
    str_start = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    outdir = args.outdir[0]
    if not exists(outdir):
        os.makedirs(outdir)
    outstem = '%s/%s_%s-%s' % (outdir, plot, str_start, str_end)

    # Tweak for rates and lcs
    rates=None
    quads=None
    if ('rates_' in plot):
        rates=[plot.split("_")[1]]
        plot_new='rates'
    elif ('lc_' in plot):
        quads=[plot.split("_")[1]]
        plot_new='lc'
    else:
        plot_new=plot

    # Tweak for dr
    if (plot == 'dr'):
        resample = (args.resample // 4 + 1)*4
    else:
        resample = args.resample

    if plot_new in n_panels.keys():
        dbhks = eval('dbhks_'+plot_new)
        update_fig = eval('update_fig_'+plot_new)
        plot_title = plot.upper()
        n_panel = n_panels[plot_new]
        plotly_hk(args.indir[0], dbhks, n_panel, plot_title, update_fig, outstem, datetime_start, datetime_end, resample, events, rates=rates, quads=quads, flg_adrc=args.flg_adrc, flg_cde=args.flg_cde, flg_dist=args.flg_dist, flg_bus=args.flg_bus)        
    else:
        print("%s is not supported." % plot)
        exit(1)