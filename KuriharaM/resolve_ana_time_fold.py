#!/usr/bin/env python3

############################################################
# Description
############################################################
# Fold time domain data with a given period.

########################################################
# Imports
########################################################
import argparse
import datetime
import numpy as np
import pandas as pd
import os
from scipy.interpolate import interp1d
from resolve_plot_sd_wfrb import make_plot_td
from resolve_utils import save_files
from resolve_makedb_psd import parse_accrawdata

########################################################
# User-defined parameters
########################################################
resolution = 100
cycles_show = 2
kinds=['sdac', 'sdpx', 'gse']

########################################################
# Functions
########################################################
#-------------------------------------------------------
def main(infile, period, kind, flg_norm):
    
    # Modify period for cycles to show.
    period *= cycles_show

    # Load data
    if (kind == 'sdpx'):
        chnum = 36
        data_ch = pd.read_pickle(infile)
        sampling_rate = 12.5e3
        len_ns = 50*1024
    elif (kind == 'sdac'):
        chnum = 4
        data_ch = pd.read_pickle(infile)
        sampling_rate = 12.5e3
        len_ns = 50*1024
    elif (kind == 'gse'):
        chnum = 8
        format = 'npz'
        voltages, dt, shotlength, stime = parse_accrawdata(infile, format, chnum)
        sampling_rate = 1./dt
        len_ns = int(shotlength*sampling_rate)

    # Set time bins
    Xs = np.linspace(0,len_ns-1,len_ns)/sampling_rate
    n_bin = int(np.floor(len_ns/sampling_rate/period) * resolution)
    Xs_int = np.linspace(0,n_bin-1,n_bin) * period/resolution
    Xs_int_fold = np.linspace(0,resolution-1,resolution) * period / resolution
    Ys = None

    for ch in range(chnum):
        if (kind == 'sdpx' ):
            if (ch ==12):
                datum_ch = np.zeros(len_ns)
            else:
                datum_ch = data_ch[(data_ch.PIXEL==ch) & (data_ch.NOISEREC_MODE==2)].iloc[:,3:].values.flatten() 
        elif (kind == 'sdac' ):
            datum_ch = data_ch[(data_ch.PSP_ID==ch) & (data_ch.NOISEREC_MODE==2)].iloc[:,3:].values.flatten() 
        elif (kind == 'gse'):
            datum_ch = voltages[ch]

        # Truncate
        datum_ch = datum_ch[:len_ns]
        if (len(datum_ch) < len_ns):
            datum_ch = np.zeros(len_ns)

        # Detrend
        datum_ch = datum_ch - datum_ch.mean()

        # Interpolate
        f = interp1d(Xs, datum_ch, bounds_error=False)
        datum_ch_int = f(Xs_int)

        # Fold
        datum_ch_int_2D = datum_ch_int.reshape((int(len(datum_ch_int)/resolution),resolution))
        datum_ch_int_fold = np.average(datum_ch_int_2D[:-1], axis = 0) # Remove the last fold for possible truncation.
        if (flg_norm is True):
            datum_ch_int_fold -= np.mean(datum_ch_int_fold)
            datum_ch_int_fold /= np.std(datum_ch_int_fold)
        # Stack
        if (Ys is None):
            Ys = datum_ch_int_fold
        else:
            Ys = np.vstack([Ys,datum_ch_int_fold])
    return [Xs_int_fold, Ys]


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infile',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output dir.',
        dest='outdir',
        default='./',
        type=str,
        nargs='+'
        )
    parser.add_argument(
        '-p', '--period',
        help='Period to fold (s)',
        dest='period',
        type=float,
        nargs=1
        )
    parser.add_argument(
        '-k', '--kind',
        help='kind of data: {}'.format(kinds),
        dest='kind',
        choices=kinds,
        type=str,
        nargs=1
        )
    parser.add_argument(
        '--norm',
        help='Normalize the output.',
        dest='flg_norm',
        action='store_true',
        default=False
        )

    args = parser.parse_args()

    infile_name = os.path.split(args.infile[0])[1] 

    # time
    datetime_start = infile_name.replace('raw','_').replace('.','_').split('_')[1]
    datetime_start = datetime.datetime.strptime(datetime_start, '%Y%m%d-%H%M%S')
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')    
    
    Xs_int_fold, Ys = main(args.infile[0], args.period[0], args.kind[0], args.flg_norm)
    title='TD data folded by {:.2e}s for {} cycles in {} ({})'.format(args.period[0], cycles_show, args.kind[0], str_start2)
    outstem = args.outdir[0] + '/{}_fold_{}_p{:.4f}'.format(args.kind[0], str_start1, args.period[0])
    data_td = make_plot_td(Xs_int_fold, Ys.T, title, outstem, 'Signal (Raw)')
    save_files(data_td, None, "%s.npz" % outstem)
