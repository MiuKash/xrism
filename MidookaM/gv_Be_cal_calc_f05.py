#!/opt/CCDC/Python_API_2019/miniconda/bin/python

########################################################
# Imports
########################################################
from ccdc import io
from ccdc.descriptors import CrystalDescriptors
import numpy as np
import csv
import os

########################################################
# User Setting
########################################################
# Datadir
datadir='/home/astro/Work/XARM/SXS2/20190613/dat/XPD/'
outdir=datadir + 'Mercury/'
CIF=datadir+'AMS_DATA.cif'

# Energy range.
e_min = 02.0 # keV
e_max = 12.0 # keV
De = 0.01 # keV


########################################################
# Functions
########################################################
#-------------------------------------------------------
def calc_XPD(crystal, e_min, e_max, De, outdir):

    energies = np.linspace(e_min, e_max, np.int((e_max-e_min)/De)+1)
    total_intensities=[]
    
    for energy in energies:
        
        # Settoing for simulation
        wl = CrystalDescriptors.PowderPattern.Wavelength()
        wl.wavelength= 12.399786667738 / energy

        settings = CrystalDescriptors.PowderPattern.Settings()
        settings.two_theta_maximum = 179.0
        settings.two_theta_minimum = 1.0
        settings.two_theta_step = 0.02
        settings.full_width_at_half_maximum = 1e-4
        settings.wavelength = wl

        # Simulate and dump XPD pattern.
        powder_sim = CrystalDescriptors.PowderPattern.from_crystal(crystal, settings=settings)
        outfile=outdir + "XPD_sim_%.3f" % energy
        powder_sim.write_xye_file(outfile)

        # Dump peak info.
        data_peaks=[]
        for i_tick_mark in range(len(powder_sim.tick_marks)):
            tick_mark = powder_sim.tick_marks[i_tick_mark]
            SA=tick_mark.is_systematically_absent
            MI=tick_mark.miller_indices
            peak=tick_mark.two_theta
            data_peaks.append([energy, MI, SA, peak])
            print("%.3f, %s, %s, %f" % (energy, MI, SA, peak))

        outfile=outdir + "XPD_sim_%.3f_peaks.csv" % energy
        with open(outfile, 'w') as f:
            writer = csv.writer(f, delimiter='\n')
            writer.writerow(data_peaks)
            print("XPD peaks written in %s" % outfile)
        
        # Total intensity of XPD.
        total_intensity = powder_sim.integral()
        total_intensities.append(total_intensity)

    # Dump energy vs intensity.
    outfile=outdir + "XPD_sim.csv"
    with open(outfile, 'w') as f:
        writer = csv.writer(f, delimiter='\n')
        writer.writerow(zip(energies, total_intensities))
        print("XPD intensity written in %s" % outfile)
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    crystal = io.CrystalReader(CIF)[0]
    calc_XPD(crystal, e_min, e_max, De, outdir)