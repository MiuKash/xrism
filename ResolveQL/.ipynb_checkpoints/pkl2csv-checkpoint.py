#!/usr/bin/env python3

########################################################
# Imports
########################################################
import argparse
import pandas as pd


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--infiles',
        help='Input files.',
        dest='infiles',
        type=str,
        nargs='*'
        )
    args = parser.parse_args()

    for infile in args.infiles:
        data = pd.read_pickle(infile)
        outfile=infile.replace('pkl','csv')
        data.to_csv(outfile)
        print("{} converted to {}".format(infile, outfile))