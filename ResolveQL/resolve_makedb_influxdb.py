#!/usr/bin/env python3

############################################################################
# [Function]
# Register dbs into the InfluxDB.
############################################################################

########################################################
# Imports
########################################################
import numpy as np
import pandas as pd
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
import argparse


########################################################
# User-defined parameters
########################################################
url = "http://localhost:8086"
influxdb_user=u"xrism"
influxdb_pass=u"resolve123"
org = "ResolveQL"
bucket = "temp"
token = "9OsRFamusi7e-eaRKmHHbe6zCZRU-PDnIweRF67pNzhHSqfpxn1uNb71waAkhHTZCE96z00FVOA0PZKOsWtBQA=="


########################################################
# Functions
########################################################
def main(file, db):

    # Instantiation.
    client = InfluxDBClient(url=url, token=token, org=org)
    write_api = client.write_api(write_options=SYNCHRONOUS)
    query_api = client.query_api()

    p = Point("my_measurement").tag("location", "Prague").field("temperature", 25.3)
    write_api.write(bucket=bucket, record=p)


    return 0

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-d', '--db', 
        help='InfluxDB database to register.',
        dest='db',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    main(args.file, args.db)

