#!/usr/local/anaconda3/bin/python3

########################################################
# Imports
########################################################
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import pickle

import seaborn as sns
sns.set_style("white", {
    'axes.facecolor': "1.0",
    'xtick.major.size': 10.0,
    'ytick.major.size': 10.0,
    'xtick.minor.size': 6.0,
    'ytick.minor.size': 6.0,
    'xtick.direction': u'in',
    'ytick.direction': u'in',
    })
sns.set_context("talk", 1.0)
palette = sns.color_palette(n_colors=24)


########################################################
# User Setting
########################################################
# Datadir
datadir='/home/astro/Work/XARM/SXS2/20190613/dat/'

# Data files, Time shift from the controller (.log) to the logger (*.txt)
dataset_3000=[
    ['2232_close_lower_3000eV', -11.0],
    ['2145_close_upper_3000eV', -11.0],
    ['1351_open_lower_3000eV', -13.5],
    ['1905_open_upper_3000eV', -11.0],
    ]
dataset_4000=[
    ['2232_close_lower_3000eV', -11.0],
    ['2145_close_upper_3000eV', -11.0],
    ['0936_open_lower_4000eV', -13.5],
    ['0846_open_upper_4000eV', -13.5],
    ]
dataset_3456=[
    ['2232_close_lower_3000eV', -11.0],
    ['2145_close_upper_3000eV', -11.0],
    ['1113_open_lower_3456eV', -13.5],
    ['1026_open_upper_3456eV', -13.5],
    ]
dataset_2700=[
    ['2232_close_lower_3000eV', -11.0],
    ['2145_close_upper_3000eV', -11.0],
    ['1540_open_lower_2700eV', -13.5],
    ['1454_open_upper_2700eV', -13.5],
    ]

# Thresholds
#thres_ref = [0.80, 0.85] # for B/F to extract home position
time_max = 2750.0
#time_max = 100.0

# Beam size
beam_dx = 0.8 # mm
beam_dy = 1.2 # mm


# Scan parameters
scan_Dx = 35.490
scan_vx  =  0.5 # mm/s
scan_time = scan_Dx / scan_vx
scan_dy = 1.000 # mm
scan_ny = 15
scan_Dy = scan_dy * scan_ny

# Observed grids
grid_nx = int(scan_Dx/beam_dx*2)+1
grid_ny = scan_ny*2+1
grid_x = np.linspace(0, scan_Dx, grid_nx)
grid_y = np.linspace(-scan_ny, scan_ny, grid_ny)*scan_dy
grid_dx = np.diff(grid_x)[0]/2.0    
grid_dy = np.diff(grid_y)[0]/2.0

# Remapped grids = Grids
size_Grid = 0.1 # mm
Grid_nx = int((scan_Dx) / size_Grid)
Grid_ny = int((scan_Dy*2+1)/ size_Grid)

# DUT
radius = 29.0/2

# Origin of time of the data logger.
d0 = datetime.datetime(1899,12,30,0,0,0)


########################################################
# Functions
########################################################
#-------------------------------------------------------
def read_data(dataset, datadir):
    
    # Read file and correct time.    
    def read_datum(dataset, columns):
        
        datum = pd.read_csv(datadir + dataset[0] + '.txt', header = 0, names=columns, comment='#')
        events = pd.read_csv(datadir + dataset[0] + '_time.txt', names=['Time'])
        events['Time'] = events['Time'].apply(lambda x : datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f'))
        
        # Subtract time offset.
        d_start =  events['Time'][0]
        d_stop =  events['Time'][91]
        events['Time'] = events['Time'].apply(lambda x : (x- d_start).total_seconds())
        
        dt =  dataset[1]
        d_start_day = ((d_start-d0).total_seconds()-dt)/3600.0/24.0
        d_stop_day = ((d_stop-d0).total_seconds()-dt)/3600.0/24.0
        datum = datum[(datum['Time(day)']>=d_start_day) & (datum['Time(day)']<=d_stop_day)].copy()
        datum.reset_index(inplace=True)
        datum['Time(sec)']=(datum['Time(day)']-datum['Time(day)'][0])*86400
        datum.dropna(axis=0, inplace=True)
        print("%d lines found between %s and %s\n" % (len(datum), d_start, d_stop))
        
        return datum, events
        
    # Read data.
    columns_bkg=['Time(day)', 'Ip(bkg)', 'P_vac', 'Io(bkg)']
    columns_src=['Time(day)', 'Ip(src)', 'P_vac', 'Io(src)']
    
    data_bkg_low, events_bkg_low = read_datum(dataset[0], columns_bkg)
    data_bkg_upp, events_bkg_upp = read_datum(dataset[1], columns_bkg)
    data_src_low, events_src_low = read_datum(dataset[2], columns_src)
    data_src_upp, events_src_upp = read_datum(dataset[3], columns_src)

    # Merge src and bkg
    scn_low = sub_bkg(data_src_low, data_bkg_low)
    scn_upp = sub_bkg(data_src_upp, data_bkg_upp)
    
    return scn_low, scn_upp, events_src_low, events_src_upp

#-------------------------------------------------------
def sub_bkg(src_data, bkg_data):
    
    src_row = len(src_data)
    bkg_row = len(bkg_data)
    
    if src_row < bkg_row:
        overow = int(bkg_row-src_row)
        bkg_data.drop(bkg_data.head(overow).index, axis=0, inplace=True)
        bkg_data.reset_index(inplace=True)
        scn_data = pd.concat( [ src_data[['Time(sec)', 'Io(src)', 'Ip(src)']] , bkg_data[['Io(bkg)', 'Ip(bkg)']] ], axis=1)
        scn_data.dropna(axis=0, inplace=True)
        
    elif src_row > bkg_row:
        #halfoverow = int((src_row-bkg_row)*0.5)
        overow = int(src_row-bkg_row)
        src_data.drop(src_data.head(overow).index, axis=0, inplace=True)
        src_data.reset_index(inplace=True)
        scn_data = pd.concat( [ src_data[['Time(sec)', 'Io(src)', 'Ip(src)']] , bkg_data[['Io(bkg)', 'Ip(bkg)']] ], axis=1)
        scn_data.dropna(axis=0, inplace=True)

    else:
        scn_data = pd.concat( [ src_data[['Time(sec)', 'Io(src)', 'Ip(src)']] , bkg_data[['Io(bkg)', 'Ip(bkg)']] ], axis=1)
        scn_data.dropna(axis=0, inplace=True)

    scn_data['Io(net)'] = (scn_data['Io(src)']-scn_data['Io(bkg)'])*(-1)
    scn_data['Ip(net)'] = (scn_data['Ip(src)']-scn_data['Ip(bkg)'])*(-1)
    scn_data['Ip/Io(net)'] = scn_data['Ip(net)']/scn_data['Io(net)']
    
    return scn_data
    
#------------------------------------------------------- 
def make_plot(dataset, datadir, outfile_stem):

    ## Plot setting (1)
    fig = plt.figure(figsize=(8.27,11.69))
    plt.clf()
    plt.subplots_adjust(hspace=0.05)
    _s=0.5
    
    # Raw
    ax1 = plt.subplot(8,1,8)
    ax1.set_xlabel('Time (s)')
    ax1.set_ylabel('Raw $I$\n(low)')
    ax1.set_yscale('linear')
    #ax1.set_ylim(-0.3, -0.2)
    
    ax2 = plt.subplot(8,1,7, sharex=ax1)
    plt.setp(ax2.get_xticklabels(), visible=False)
    ax2.set_xlabel('')
    ax2.set_ylabel('Raw $I$\n(up)')    
    ax2.set_yscale('linear')
    #ax2.set_ylim(-0.3, -0.2)
    
    
    # Bkg-subtracted I    
    ax3 = plt.subplot(8,1,6, sharex=ax1)
    plt.setp(ax3.get_xticklabels(), visible=False)
    ax3.set_xlabel('')
    ax3.set_ylabel('Bkg-subt $I$\n(low)')
    ax3.set_yscale('linear')    
    #ax3.set_ylim(0.20, 0.30)
    
    ax4 = plt.subplot(8,1,5, sharex=ax1)
    plt.setp(ax4.get_xticklabels(), visible=False)
    ax4.set_xlabel('')
    ax4.set_ylabel('Bkg-subt $I$\n(up)')
    ax4.set_yscale('linear')
    #ax4.set_ylim(0.20, 0.30)
    
    # Back/Front
    ax5 = plt.subplot(8,1,4, sharex=ax1)
    plt.setp(ax5.get_xticklabels(), visible=False)
    ax5.set_xlabel('')
    ax5.set_ylabel('B/F\n(low)')
    ax5.set_yscale('linear')
    #ax5.set_ylim(0.10, 0.15)
    
    ax6 = plt.subplot(8,1,3, sharex=ax1)
    plt.setp(ax6.get_xticklabels(), visible=False)
    ax6.set_xlabel('')
    ax6.set_ylabel('B/F\n(up)')
    ax6.set_yscale('linear')
    #ax6.set_ylim(0.10, 0.15)
    
    # Back/Front / home
    ax7 = plt.subplot(8,1,2, sharex=ax1)
    plt.setp(ax7.get_xticklabels(), visible=False)
    ax7.set_xlabel('')
    ax7.set_ylabel('B/F/Home\n(low)')
    ax7.set_yscale('linear')
    
    ax8 = plt.subplot(8,1,1, sharex=ax1)
    plt.setp(ax8.get_xticklabels(), visible=False)
    ax8.set_xlabel('')
    ax8.set_ylabel('B/F/Home\n(up)')
    ax8.set_yscale('linear')

    # Common for all axes.
    for ax in [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8]:
        ax.set_xlim(0.0, time_max)
        ax.grid(which='both')
        ax.set_xscale('linear')


    ## Plot data
    scn_low, scn_upp, events_low, events_upp = read_data(dataset, datadir)

    # Raw    
    ax1.scatter(scn_low['Time(sec)'], scn_low['Io(src)'], s=_s, label='Src, Front', color=palette[2])
    ax1.scatter(scn_low['Time(sec)'], scn_low['Ip(src)'], s=_s, label='Src, Back', color=palette[3])
    ax1.scatter(scn_low['Time(sec)'], scn_low['Io(bkg)'], s=_s, label='Bkg, Front', color=palette[4])
    ax1.scatter(scn_low['Time(sec)'], scn_low['Ip(bkg)'], s=_s, label='Bkg, Back', color=palette[5])

    ax2.scatter(scn_upp['Time(sec)'], scn_upp['Io(src)'], s=_s, label='Src, Front', color=palette[2])
    ax2.scatter(scn_upp['Time(sec)'], scn_upp['Ip(src)'], s=_s, label='Src, Back', color=palette[3])
    ax2.scatter(scn_upp['Time(sec)'], scn_upp['Io(bkg)'], s=_s, label='Bkg, Front', color=palette[4])
    ax2.scatter(scn_upp['Time(sec)'], scn_upp['Ip(bkg)'], s=_s, label='Bkg, Back', color=palette[5])
    
    # Bkg-subtracted I
    ax3.scatter(scn_low['Time(sec)'], scn_low['Io(net)'], s=_s, label='Front', color=palette[2])
    ax3.scatter(scn_low['Time(sec)'], scn_low['Ip(net)'], s=_s, label='Back', color=palette[3])

    ax4.scatter(scn_upp['Time(sec)'], scn_upp['Io(net)'], s=_s, label='Front', color=palette[2])
    ax4.scatter(scn_upp['Time(sec)'], scn_upp['Ip(net)'], s=_s, label='Back', color=palette[3])
    
    # Get inervals.
    # base = 1 for home, 3 for forwward-moving, 5 for backward-moving.
    def get_interval(events, base):
        for i in range(scan_ny):
            i_start = base + 6*i
            i_stop = base + 6*i + 1
            time_start = events['Time'][i_start]
            time_stop = events['Time'][i_stop]
            yield time_start, time_stop

    # Select and plot ref position.
    def calc_plot_ref(scan_data, events, axes, ax_ref, color):
        # Stack data during ref position to scan_ref.
        scan_ref = None
        
        for interval in get_interval(events, 1):
            time_start, time_stop = interval
#            print(time_start, time_stop)
            scan_ref_add = scan_data[(scan_data['Time(sec)']>time_start) & (scan_data['Time(sec)']<=time_stop)].copy()
            if (scan_ref is None):
                scan_ref = scan_ref_add
            else:
                scan_ref = pd.concat([scan_ref, scan_ref_add])

            for ax in axes:
                ax.axvspan(time_start, time_stop, alpha=0.1, color=color)

        median = scan_ref['Ip/Io(net)'].median()
        ax_ref.axhline(median, color=color, linestyle=':')                
        return scan_ref, median
        
    ref_low, ref_low_median = calc_plot_ref(scn_low, events_low, [ax1, ax3, ax5, ax7], ax5, palette[0])
    ref_upp, ref_upp_median = calc_plot_ref(scn_upp, events_upp, [ax2, ax4, ax6, ax8], ax6, palette[1])
    print("Ref low mean = %.4f" % (ref_low_median))
    print("Ref upp mean = %.4f" % (ref_upp_median))
    scn_low['Trans'] = scn_low['Ip/Io(net)']/ref_low_median
    scn_upp['Trans'] = scn_upp['Ip/Io(net)']/ref_upp_median
    
    
    # Select and plot trans position.
    def calc_plot_trans(scan_data, events, dy, axes, ax_ref, color):

        # Stack data during trans position to scan_trans.
        scan_trans = None
        
        # Foward
        y=0.0
        for interval in get_interval(events, 3):
            time_start, time_stop = interval
#            print(time_start, time_stop, time_stop-time_start)
            # Filter scan data
            scan_trans_add = scan_data[(scan_data['Time(sec)']>time_start) & (scan_data['Time(sec)']<=time_stop)].copy()
            # Assign position
            scan_trans_add['x'] = (scan_trans_add['Time(sec)']-time_start)*scan_vx            
            scan_trans_add['y'] = y
            if (scan_trans is None):
                scan_trans = scan_trans_add
            else:
                scan_trans = pd.concat([scan_trans, scan_trans_add])            

            for ax in axes:
                ax.axvspan(time_start, time_stop, alpha=0.3, color=color)
            y += dy  
        
        # Backward
        y=0.0
        for interval in get_interval(events, 5):
            time_start, time_stop = interval
#            print(time_start, time_stop, time_stop-time_start)
            # Filter scan data
            scan_trans_add = scan_data[(scan_data['Time(sec)']>time_start) & (scan_data['Time(sec)']<=time_stop)].copy()
            # Assign position
            scan_trans_add['x'] = scan_Dx - (scan_trans_add['Time(sec)']-time_start)*scan_vx
            scan_trans_add['y'] = y

            if (scan_trans is None):
                scan_trans = scan_trans_add
            else:
                scan_trans = pd.concat([scan_trans, scan_trans_add])            

            for ax in axes:
                ax.axvspan(time_start, time_stop, alpha=0.3, color=color)
            y += dy
            
        median = scan_trans['Trans'].median()
        ax_ref.axhline(median, color=color, linestyle=':')                
        return scan_trans, median

    trans_low, trans_low_median = calc_plot_trans(scn_low, events_low, -scan_dy, [ax7], ax5, palette[0])
    trans_upp, trans_upp_median = calc_plot_trans(scn_upp, events_upp, +scan_dy, [ax8], ax6, palette[1])
    print("Trans low mean = %.4f" % (trans_low_median))
    print("Trans upp mean = %.4f" % (trans_upp_median))

    ax5.scatter(scn_low['Time(sec)'], scn_low['Ip/Io(net)'], s=_s, label='Data', color=palette[0])
    ax5.scatter(trans_low['Time(sec)'], trans_low['x']/scan_Dx, s=_s/5.0, label='x', color=palette[4])
    ax5.scatter(trans_low['Time(sec)'], -trans_low['y']/scan_Dy, s=_s/5.0, label='y', color=palette[5])
    ax6.scatter(scn_upp['Time(sec)'], scn_upp['Ip/Io(net)'], s=_s, label='Data', color=palette[1])
    ax6.scatter(trans_upp['Time(sec)'], trans_upp['x']/scan_Dx, s=_s/5.0, label='x', color=palette[4])
    ax6.scatter(trans_upp['Time(sec)'], trans_upp['y']/scan_Dy, s=_s/5.0, label='y', color=palette[5])

    #ax7.set_ylim(thres_trans[0], thres_trans[1])    
    ax7.set_ylim(trans_low_median-0.1, trans_low_median+0.05)
    ax7.scatter(trans_low['Time(sec)'], trans_low['Trans'], s=_s, label='trans', color=palette[0])
    ax7.axhline(trans_low_median, ls='-', lw=0.5, color=palette[0])
    
    ax8.set_ylim(trans_upp_median-0.1, trans_upp_median+0.05)
    ax8.scatter(trans_upp['Time(sec)'], trans_upp['Trans'], s=_s, label='trans', color=palette[1])
    ax8.axhline(trans_upp_median, ls='-', lw=0.5, color=palette[1])
    
    # Plot setting (2)
    for ax in [ax1, ax2, ax3, ax4, ax5, ax6]:
        ax.legend(fontsize=12, ncol=2)        
    plt.tight_layout(pad=0.4, w_pad=0.0, h_pad=0.0)
    
    outfile="%s_LC.pdf" % (outfile_stem)
    plt.savefig(outfile, papertyle='A4', orientation='portrait',format='pdf')
    print('The plot is generated in %s.' % outfile)    
    plt.close('all')
    
    trans_data =pd.concat([trans_low, trans_upp])[['x', 'y', 'Trans']]
    with open('%s_trans.pkl' % outfile_stem, mode='wb') as _f:
        pickle.dump(trans_data, _f)
    print("Data saved to %s_trans.pkl." % outfile_stem)

    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':

    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-o', '--output',
        help='Output file name origin.',
        dest='outfile',
        type=str,
        default='Scan',
        nargs='?'
        )
    args = parser.parse_args()

    # Check arguments.
    outfile = args.outfile
    
    # Plot
    # dataset, datadir, output file name
    #trans_data = make_plot(dataset_3000, datadir, "Scan_3000")    
    trans_data = make_plot(dataset_4000, datadir, "Scan_4000")
    #trans_data = make_plot(dataset_3456, datadir, "Scan_3456")
    #trans_data = make_plot(dataset_2700, datadir, "Scan_2700")