#!/usr/bin/env python3

############################################################
# [Function]
# Plot energy vs time
############################################################

########################################################
# Imports
########################################################
import argparse, os
import numpy as np
import pandas as pd
import argparse, datetime, os, re, glob
from os.path import exists
from sys import exit
#
from resolve_utils import get_datetime_from_args, lines_neutral
from resolve_plot_hk import get_events, str_now
from resolve_plot_nr_ns_2D import plotly_2D
from resolve_plot_spec import collate_events, energy_ac, energy_px, ac_min, ac_max, px_min, ac_bin, px_max, px_bin, cal_min, cal_max, cal_bin

########################################################
# User-defined parameters
########################################################


########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_spec_2D(events, datetime_start, datetime_end, resample, min, max, bin):
    
    starttime_all=[]
    spec2d_all = None
    n_bin = int((max-min)/bin)

    # Iterate over time.
    starttime = datetime_start
    while (starttime < datetime_end):
        endtime = starttime + np.timedelta64(resample, 's')
        energies = events[starttime:endtime]['Energy'].values
        spec = np.histogram(energies, bins=n_bin, range=[min, max])
        spec2d = spec[0]
        
        # Stack data        
        if spec2d_all is None:
            spec2d_all = spec2d         
        else:
            spec2d_all = np.vstack([spec2d_all,spec2d])
        starttime_all.append(starttime)
        starttime = endtime

    energy_all = spec[1][:-1] + np.diff(spec[1])[0]/2.0
    
    return starttime_all, energy_all, spec2d_all.T

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-R', '--resample',
        help='Resample seconds.',
        dest='resample',
        type=int,
        default=[600],
        nargs=1
        )
    parser.add_argument(
        '-E', '--evtlists',
        help='Event lists.',
        dest='evtlists',
        type=str,
        nargs='*'
        )
    parser.add_argument(
        '-p', '--plot',
        help='Type of plot (ac, px, or cal).',
        dest='plot',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-z', '--zrange',
        help='Range for zscale.',
        default=[None, None],
        dest='zrange',
        type=float,
        nargs=2
        )
    args = parser.parse_args()
    
    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    # outstem
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end1 = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    # title
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end2 = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')

    # events
    events=get_events(args.evtlists, datetime_start, datetime_end)

    # Collate events
    plot = args.plot[0]
    if (plot == 'ac'):
        dbdir='ac'
        base=0
    elif (plot == 'px'):
        dbdir='px_cl'
        base=1
    elif (plot == 'cal'):
        dbdir='px_cal'
        base=1
    events = collate_events(args.indir[0], dbdir, datetime_start, datetime_end, base)
    if (events is None) or len(events) == 0:
        print("No events found.")
        exit(1)
    
    # Setting
    def gen_plot(events_RoI, datetime_start, datetime_end, min, max, bin, lines=None):
        events_RoI.columns=['Energy']
        starttime_all, freq_all, psd2d_all = collate_spec_2D(events_RoI, datetime_start, datetime_end, args.resample[0], min, max, bin)
        if (psd2d_all is not None):
            plotly_2D(starttime_all, freq_all, np.log10(psd2d_all).astype(np.float16), outstem, title, datetime_start, datetime_end, events=events, lines=lines, xtype='linear', ytype='linear', ytitle=ytitle, zrange=args.zrange)

    monitor='evt'
    if (plot == 'ac'):
        min, max, bin = ac_min, ac_max, ac_bin
        energy = energy_ac
        outdir_new = args.outdir[0].replace('{}_2D'.format(monitor),'{}_2D/ac'.format(monitor))
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem=outdir_new  + '/evt_ac_%s-%s' % (str_start1, str_end1)
        title='%s (%s-%s) updated at %s' % (args.plot[0], str_start2, str_end2, str_now)
        ytitle='Energy (keV)'
        events_RoI = events[(events.PSP_ID==1) & (events.AC_ITYPE==0) & (events[energy_ac]>= min) & (events[energy_ac]<= max)][[energy_ac]].copy()
        gen_plot(events_RoI, datetime_start, datetime_end, min, max, bin)
    elif (plot == 'px'):
        min, max, bin = px_min, px_max, px_bin
        energy = energy_px
        for _px in range(0,36):
            if (_px == 12):
                continue
            outdir_new = args.outdir[0].replace('{}_2D'.format(monitor),'{}_2D/p{:02d}'.format(monitor, _px))
            #outdir_new = args.outdir[0] + '/p%02d/' % _px
            if not exists(outdir_new):
                os.makedirs(outdir_new)
            outstem=outdir_new + '/evt_p%02d_%s-%s' % (_px, str_start1, str_end1)
            title='%s p%02d (%s-%s) updated at %s' % (args.plot[0], _px, str_start2, str_end2, str_now)
            ytitle='Energy (eV)'
            events_RoI = events[(events.PIXEL==_px) & (events.ITYPE<1) & (events[energy]>= min) & (events[energy]<= max)][[energy]].copy()
            gen_plot(events_RoI, datetime_start, datetime_end, min, max, bin, lines=lines_neutral)
    elif (plot == 'cal'):
        min, max, bin = cal_min, cal_max, cal_bin
        energy = energy_px
        _px=12
        outdir_new = args.outdir[0].replace('{}_2D'.format(monitor),'{}_2D/p{:02d}'.format(monitor, _px))
        if not exists(outdir_new):
            os.makedirs(outdir_new)
        outstem=outdir_new + '/evt_p%02d_%s-%s' % (_px, str_start1, str_end1)
        title='%s p%02d (%s-%s) updated at %s' % (args.plot[0], _px, str_start2, str_end2, str_now)
        ytitle='Energy (eV)'
        events_RoI = events[(events.PIXEL==12) & (events.ITYPE<1) & (events[energy]>= min) & (events[energy]<= max)][[energy]].copy()
        gen_plot(events_RoI, datetime_start, datetime_end, min, max, bin, lines=lines_neutral)

