#!/usr/bin/env python

############################################################################
# [Function]
# Dump and plot noise spectra from FFF files.
############################################################################


########################################################
# Imports
########################################################
import argparse, os
import numpy as np
#
from resolve_plot_hk import str_now
from resolve_utils import scale_x, scale_y_base, get_freqs, A_nslim, B_nslim
from resolve_utils import save_files
from myutils import add_line, add_range

# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import seaborn as sns
#
import threading

########################################################
# User-defined parameters
########################################################
color_palette = sns.color_palette('hls', 9)
palette = ['rgb({},{},{})'.format(*[x*256 for x in rgb]) for rgb in color_palette]
dashes=['solid', 'dash', 'dot', 'dashdot', 'solid']


########################################################
# Functions
########################################################
#------------------------------------------------------------
def make_plot_ns(Xs, data_ns, title, outstem, ns_len, monitor="calorimeter"):
    "Plot nspec."

    if (monitor=="calorimeter"):
        ytitle='Power (nV/rtHz)'                 
        data_ns = data_ns * scale_y_base / np.sqrt(ns_len/1024) * 1e9  
        _range = [0, 3]
    elif (monitor=="sd"):
        ytitle='Power (nV/rtHz)'                 
        data_ns = data_ns * scale_y_base / np.sqrt(ns_len/1024) * 1e9  
        _range = [0, 4]        
    elif (monitor=="wfrb"):
        ytitle='Power (nV/rtHz)'                 
        data_ns = data_ns * scale_y_base / np.sqrt(ns_len/1024) * 1e9  
        _range = [0, 5]        
    elif (monitor=="accel"):
        ytitle='Power (mG/rtHz)'         
        data_ns = data_ns * 1e3
        _range=[-3, 2]      
    elif (monitor=="busV"):
        ytitle='Power (V/rtHz)'         
        data_ns = data_ns
        _range=[-4, 0]
    elif (monitor=="mag"):
        ytitle='Voltage (mV/rtHz)'         
        data_ns = data_ns * 1e3
        _range=[-2, 2]
    elif (monitor=="accel.LMS"):
        ytitle='Power (mG/rtHz)'         
        data_ns = data_ns * 1e3
        _range=[-3, 2]      
    # Define layout
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title ='Freq (Hz)',
            type = 'log',
            autorange = False,
            range = [np.log10(Xs[0]), np.log10(Xs[-1])],
            #tickmode = 'auto'
            ),
        yaxis=dict(
            title = ytitle,
            type = 'log',
            autorange = False,
            range = _range
            #tickmode = 'auto'
            ),
        showlegend=True,
        legend = dict(
            x = 1.02, xanchor = 'left',
            y = 1.00, yanchor = 'auto',
            bordercolor = '#444', 
            borderwidth = 0,
            ),
        )

    fig = make_subplots(
        rows=1, cols=1,
    )
    fig.update_layout(Layout)
    fig.update_layout(legend= {'itemsizing': 'constant'})

    # Plot data
    for px in range(len(data_ns[0])):
        print("Processing ch%02d" % px)
        fig.add_trace(
            go.Scattergl(
                x=Xs, y=data_ns[:,px], 
                name='%02d' % px, 
                #legendgroup=int(px/9),
                mode='lines+markers',
                marker=dict(color=palette[px%9], opacity=0.8, size=3),
                line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
            )
        )

    # Plot limits
    if (monitor=="calorimeter"):
        Xs=np.logspace(np.log10(Xs[0]), np.log10(Xs[-1]), 1024)
        Ys=A_nslim*Xs/30.0/np.sqrt((Xs/30)**2+1)+B_nslim    
        fig.add_trace(
            go.Scattergl(
                x=Xs, y=Ys, name="Cont limit", legendgroup=4,
                mode='lines',            
                line=dict(color='gray', width=1, dash='dot')
            )
        )
    
    # Add known lines.
    fig = add_range(fig, 13.93, 16.26, "STC", vert=True, log=True, color='red') #, row=2, col=1)
    fig = add_range(fig, 50.43, 53.83, "JTC", vert=True, log=True, color='blue') #, row=2, col=1)
    fig = add_range(fig, 50.43*3, 53.83*3, "JTCx3", vert=True, log=True, color='blue') #, row=2, col=1)
    fig = add_range(fig, 50.43*4, 53.83*4, "JTCx4", vert=True, log=True, color='blue') #, row=2, col=1)
    fig = add_range(fig, 50.43*7, 53.83*7, "JTCx7", vert=True, log=True, color='blue') #, row=2, col=1)
    fig = add_range(fig, 50.43*9, 53.83*9, "JTCx9", vert=True, log=True, color='blue') #, row=2, col=1)
    fig = add_line(fig, 127.0, 'MTQ', vert=True, log=True, color='green') #, row=2, col=1)
    fig = add_line(fig, 254.0, 'MTQx2', vert=True, log=True, color='green') #, row=2, col=1)
    fig = add_line(fig, 155.0, 'IRU', vert=True, log=True, color='yellow') #, row=2, col=1)
    fig = add_range(fig, 1500/60.0, 4500/60.0, 'RW', vert=True, log=True, color='orange') #, row=2, col=1)
    fig = add_range(fig, 4500, 6250, "Bkg", vert=True, log=True, color='grey') #, row=2, col=1)
    
    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False, engine='kaleido')
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    
    return data_ns    
    

########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output dir stem.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-l', '--length', 
        help='nspec length (1 or 8)',
        dest='ns_len',
        type=int,
        nargs=1
        )
    args = parser.parse_args()

    ns_len = int(args.ns_len[0]*1024)
    samplingrate=12500
    Xs=get_freqs(samplingrate, ns_len / samplingrate)
    
    infile = args.infile[0]
    outstem = args.outdir[0] + '/' + os.path.split(infile)[1].replace('.npz','')
    data_ns = np.load(infile)['arr_0']    
    title ='%1dk noise spec (%s) updated at %s' % (ns_len/1024, os.path.split(infile)[1], str_now)
    
    data_ns = make_plot_ns(Xs, data_ns, title, outstem, ns_len)
    save_files(data_ns, args.outdir[0], "%s.npz" % os.path.basename(outstem))
    