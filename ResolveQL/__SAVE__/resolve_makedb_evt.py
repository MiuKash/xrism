#!/usr/bin/env python3

############################################################################
# [Function]
# Make anti-co data db.
#
# [History]
# 2016/02/03    M. Tsujimoto    Written from scratch.
# 2020/06/10    M. Tsujimoto    Revised for Resolve.
#
############################################################################


########################################################
# Imports
########################################################
import argparse
import pandas as pd
from astropy.io import fits
from resolve_utils import index_stime, save_files


########################################################
# User-defined parameters
########################################################
## anti-co
hdu_ac='EVENTS'
hks_ac=['S_TIME','PSP_ID', 'PHA', 'DURATION']
hdu_px='EVENTS'
hks_px=['S_TIME','ITYPE', 'PIXEL', 'PHA', 'PI', 'LO_RES_PH', 'DERIV_MAX', 'RISE_TIME', 'FLAGS']
hdu_el='EVENTS'
hks_el=['S_TIME','ITYPE', 'PIXEL', 'EL_LOST_CNT','EL_REASON','EL_STOP_LP']


########################################################
# Functions
########################################################
#------------------------------------------------------------
def collate_data_evt(infile, hdu, hks):
    'Get event data from an FFF file.'

    print("Reading %s/%s in %s" % (hdu, hks, infile))

    # Open infile.
    try:
        hdulist = fits.open(infile,memmap=True)
    except:
        print('%s : File open error.' % infile)
        return None
    
    try:
        hdulist[hdu]
    except:
        print("%s : No HDU with %s found." % (infile, hdu))
        return None

    if (len(hdulist[hdu].data)==0):
        print('%s : No data found.' % infile)
        return None

    # Convert FITS.rec_array to DataFrame column by column.
    data = None
    for hk in hks:
        datum = pd.DataFrame(hdulist[hdu].data.field(hk).byteswap().newbyteorder(),columns=[hk])
        if (data is None):
            data = datum
        else:
            data = pd.concat([data,datum],axis=1) 
    
    # Clear the memory
    hdulist.close()

    # Index stime
    data = index_stime(data)    
    
    return data


#------------------------------------------------------------
def makedb(infile, outdir, table):
    
    hdu=eval('hdu_' + table)
    hks=eval('hks_' + table) 
    
    data = collate_data_evt(infile, hdu, hks)    
    if (data is None) or (len(data)==0):
        return 1
        
    if (table == 'px'):
        data = data[data.ITYPE<6]
    elif (table == 'el'):
        data = data[data.ITYPE==6]
        
    if (data is not None) and (len(data)>0):
        stime_start= data.index[0].strftime("%Y%m%d-%H%M%S")    
        stime_end = data.index[-1].strftime("%Y%m%d-%H%M%S")    
        outdir_new = outdir + '/%s/' % table
        outfile = '%s_%s_%s.pkl' % (table, stime_start, stime_end) 
        save_files(data, outdir_new, outfile, kind='pkl')
    else:
        print("No data found in %s." % infile)
   
    return 0


########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input',
        help='Input file.',
        dest='infile',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir', 
        help='Output directory.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-t', '--table', 
        help='Table (ac, px, bl).',
        dest='table',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    makedb(args.infile[0], args.outdir[0], args.table[0])
