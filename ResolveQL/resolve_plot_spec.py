#!/usr/bin/env python3

############################################################
# [Function]
# Plot spectra of px and ac during a given time.
############################################################

########################################################
# Imports
########################################################
from sys import exit
import argparse, glob, os
import pandas as pd
import numpy as np
import threading
from os.path import exists
#
from resolve_plot_hk import str_now
from resolve_utils import get_datetime_from_args, filter_by_datetime, save_files
from resolve_plot_ns import palette, dashes
# Plotly offline
import plotly.offline as offline
import plotly.graph_objects as go


########################################################
# User-defined parameters
########################################################
energy_ac = 'PI'
energy_px = 'EPI'
ac_min=20
ac_max=1000
ac_bin=10
px_min=120
px_max=12000
px_bin=1
cal_min=5500
cal_max=7000
cal_bin=1

########################################################
# Functions
########################################################
#-------------------------------------------------------
def collate_events(indir, plot, datetime_start, datetime_end, base=0):

    # All infiles.
    infiles = glob.glob('%s/%s/*.pkl' % (indir, plot))

    # Filter infiles by time        
    infiles = filter_by_datetime(infiles, 'range', datetime_start, datetime_end, base)

    # Stack events    
    events_all = None
    for infile in infiles:
        events = pd.read_pickle(infile)

        # Stack data
        if events_all is None:
            events_all = events        
        else:    
            events_all = pd.concat([events_all,events])            
    
    # Skip if none
    if (events_all is None):
        return None
        
    events_all = events_all[(events_all.index>=datetime_start) & (events_all.index<datetime_end)]
    events_all['_INDEX'] = events_all.index
    #events_all = events_all[~events_all.duplicated]
    return events_all

#-------------------------------------------------------
def collate_spec(indir, dbdir, plot, datetime_start, datetime_end, base=0):
    
    events_all = collate_events(indir, dbdir, datetime_start, datetime_end, base)    
    if (events_all is None) or (len(events_all) == 0):
        exit(1)
    
    # Read each file and stack
    if (plot == "ac"):
        phas = events_all[(events_all.PSP_ID==1) & (events_all.AC_ITYPE==0)][energy_ac].values        
        hist_all, bins_all = np.histogram(phas, bins=range(ac_min, ac_max, ac_bin))
        hist_all = hist_all / ac_bin
    elif (plot == "px"):
        hist_all = None
        for px in range(0,36):
            phas = events_all[(events_all.PIXEL==px) & (events_all.ITYPE<1)][energy_px].values
            hist, bins_all = np.histogram(phas, bins=range(px_min, px_max, px_bin)) 
            hist = hist / px_bin
            if (hist_all is None):
                hist_all = hist
            else:
                hist_all = np.vstack([hist_all, hist])
        # Sum of all pixels
        hist_all_sum=np.sum(hist_all, axis=0)
        hist_all = np.vstack([hist_all, hist_all_sum])
    elif (plot == "cal"):
        phas = events_all[(events_all.PIXEL==12) & (events_all.ITYPE<1)][energy_px].values
        hist_all, bins_all = np.histogram(phas, bins=range(cal_min, cal_max, cal_bin)) 
        hist_all = hist_all / cal_bin

    bins_all_mid = bins_all[:-1] + np.diff(bins_all)[0]/2
    
    return np.vstack([bins_all_mid, hist_all])            

#------------------------------------------------------------
def make_plot_spec(spec, plot, title, outstem):
    "Plot spectra."
    
    # Energy unit
    if (plot == 'ac'):
        unit = 'keV'
    else:
        unit = 'eV'
    
    # Define layout
    Layout = go.Layout(
        title=title,
        xaxis=dict(
            title ='Energy ({})'.format(unit),
            #type = 'linear',
            #range = [spec[0].min(), spec[0].max()],
            type = 'log',
            range = [np.log10(spec[0].min()), np.log10(spec[0].max())],
            autorange = False,
            #tickmode = 'auto'
            ),
        yaxis=dict(
            title = 'Counts/{}'.format(unit),
            type = 'log',
            autorange = True,
            #range = _range
            #tickmode = 'auto'
            ),
        showlegend=True,
        legend = dict(
            x = 1.02, xanchor = 'left',
            y = 1.00, yanchor = 'auto',
            bordercolor = '#444', 
            borderwidth = 0,
            ),
        )
    fig = go.Figure(layout=Layout)    

    # Plot data    
    if (plot == 'ac'):
        fig.add_trace(
            go.Scattergl(
                x=spec[0], y=spec[1],
                name='ac',
                mode='lines+markers',                
            )
        )
    elif (plot == 'px'):
        fig.add_trace(
            go.Scattergl(
                x=spec[0], y=spec[37],
                name='All', 
                #legendgroup=0,
                mode='lines+markers',
                marker=dict(color='black', opacity=0.8, size=3),
                line=dict(color='black', width=2, dash='solid')
            )
        )
        for px in range(0,36):
            fig.add_trace(
                go.Scattergl(
                    x=spec[0], y=spec[1+px],
                    name='%02d' % px, 
                    #legendgroup=int(px/9),
                    mode='lines+markers',
                    marker=dict(color=palette[px%9], opacity=0.8, size=3),
                    line=dict(color=palette[px%9], width=2, dash=dashes[int(px/9)])
                )
            )
    elif (plot == 'cal'):
        fig.add_trace(
            go.Scattergl(
                x=spec[0], y=spec[1],
                name='cal',
                mode='lines+markers',                
            )
        )

    # Output
    def write_static():
        fig.write_image("%s.png" % outstem, validate=False)
        print("File saved in %s.png" % (outstem))
    def write_html():
        offline.plot(fig, filename='%s.html' % outstem, auto_open=False, include_mathjax='cdn', validate=False)
        print("File saved in %s.html" % (outstem))

    t1 = threading.Thread(target=write_static)
    t2 = threading.Thread(target=write_html)
    #
    t1.start()
    t2.start()
    #
    t1.join()
    t2.join()
    
    return 0

    
########################################################
# Main routine
########################################################
if __name__ == '__main__':
    
    # Command-line parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--indir',
        help='Input dir.',
        dest='indir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-o', '--outdir',
        help='Output dir name.',
        dest='outdir',
        type=str,
        nargs=1
        )
    parser.add_argument(
        '-s', '--start',
        help='Start datetime (YYYY/MM/DD hh:mm:ss).',
        dest='start',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-e', '--end',
        help='End datetime (YYYY/MM/DD hh:mm:ss).',
        dest='end',
        type=str,
        nargs=2
        )
    parser.add_argument(
        '-p', '--plot',
        help='Type of plot (ac, px, or cal).',
        dest='plot',
        type=str,
        nargs=1
        )
    args = parser.parse_args()

    datetime_start = get_datetime_from_args(args.start)
    datetime_end = get_datetime_from_args(args.end)
    # outstem
    str_start1 = pd.to_datetime(datetime_start).strftime('%Y%m%d_%H%M%S')
    str_end1 = pd.to_datetime(datetime_end).strftime('%Y%m%d_%H%M%S')
    # title
    str_start2 = pd.to_datetime(datetime_start).strftime('%Y/%m/%d %H:%M:%S')
    str_end2 = pd.to_datetime(datetime_end).strftime('%Y/%m/%d %H:%M:%S')
    
    # get data
    if (args.plot[0] == 'ac'):
        dbdir='ac'
        base=0
    elif (args.plot[0] == 'px'):
        dbdir='px_cl'
        base=1
    elif (args.plot[0] == 'cal'):
        dbdir='px_cal'
        base=1
        
    outdir_new = args.outdir[0]
    #outdir_new = args.outdir[0].replace('spec','spec/{}'.format(args.plot[0]))
    if not exists(outdir_new):
        os.makedirs(outdir_new)
    outstem=outdir_new  + '/%s_%s-%s' % (args.plot[0], str_start1, str_end1)
    title='%s spec (%s-%s) updated at %s' % (args.plot[0], str_start2, str_end2, str_now)
    spec = collate_spec(args.indir[0], dbdir, args.plot[0], datetime_start, datetime_end, base)

    if (spec is not None) and len(spec) > 0:
        make_plot_spec(spec, args.plot[0], title, outstem)
        save_files(spec, outdir_new, "%s.npz" % os.path.basename(outstem))