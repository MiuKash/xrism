#!/bin/bash

############################################################
# Description
############################################################
# Make db and plots for Resolve QL.
# See https://xrsrv1.isas.jaxa.jp/confluence/pages/viewpage.action?pageId=73695251 for description of the products.


############################################################
# Command-line options
############################################################
# The input sources are one of the following, which are distinguished by -C option.
# (1) Resolve-PPL/PL for all ground test campaigns.
# (2) QL-FFF for in-orbit operations.
# (3) Trend archive for in-orbit operation.
#
# dbs are generated for individual ${TC}. Plots are generated for ALL.
#
# -L ${LOOP} : reprocess given number of days before today.
# -C ${TCs}: reprocess for TCs.
#
# -o : reprocess all files.
# -q : quick mode (ignore files older than ${ctime}).
# -d : debug mode (selected processes for debugging only).
#
# -t : enable data transfer
# -c : enable processing of commands and ODB
# -h : enable processing of HK data
# -n : enable processing of noise data
# -e : enable processing of evt data
# -m : enable processing of monitor GSE data
#
# -b : enable processing makedb
# -p : enable processing plots
# -i : index HTML generation only (disable processing plots). 

# Default setting
TC_default=TC7
LOOP=0
#
flg_overwrite=0 # False (0) or True (1)
flg_quick=0 # False (0) or True (1)
flg_debug=0 # False (0) or True (1)
#
flg_trans=0 # False (0) or True (1)
flg_com=0 # False (0) or True (1)
flg_hk=0 # False (0) or True (1)
flg_noise=0 # False (0) or True (1)
flg_evt=0 # False (0) or True (1)
flg_mon=0 # False (0) or True (1)
#
flg_db=0 # False (0) or True (1)
flg_plot=0 # False (0) or True (1)
flg_index_only=0 # False (0) or True (1)


############################################################
# Setting
############################################################
# prefix
prefix="xa[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
prefix_gen="${prefix}gen"
prefix_rsl="${prefix}rsl"
prefix_xtd="xtd_a0.hk.gz"

# Scripts from raw data to db.
makedb_shi_gse="${HOME}/bin/resolve_makedb_shi_gse.py"
makedb_lhp_gse=${makedb_shi_gse}
makedb_adrg="${HOME}/bin/resolve_makedb_adrg.py"
makedb_power_gse1="${HOME}/bin/resolve_makedb_power_gse1.py"
makedb_power_gse2="${HOME}/bin/resolve_makedb_power_gse2.py"
makedb_dmsfbv_gse="${HOME}/bin/resolve_makedb_dmsfbv_gse.py"
makedb_pc="${HOME}/bin/resolve_makedb_env.py"
makedb_env="${HOME}/bin/resolve_makedb_env.py"
makedb_tvac="${HOME}/bin/resolve_makedb_env.py"
makedb_mon_gse="${HOME}/bin/resolve_makedb_psd.py"
makedb_fff="${HOME}/bin/resolve_makedb_fff.py"
makedb_lc="${HOME}/bin/resolve_makedb_lc.py"
makedb_nm="${HOME}/bin/resolve_makedb_nm.py -t nm"
makedb_bl="${HOME}/bin/resolve_makedb_lc.py"
makedb_nr_ns="${HOME}/bin/resolve_makedb_nr_ns.py"	
makedb_cmd="${HOME}/bin/resolve_makedb_cmd.py" 
makedb_odb="${HOME}/bin/resolve_makedb_odb.py" 

# Scripts from db to plots.
plot_hk="${HOME}/bin/resolve_plot_hk.py"
plot_ns="${HOME}/bin/resolve_plot_ns.py"
plot_nr_ns="${HOME}/bin/resolve_plot_nr_ns.py"
plot_nr_ns_2D="${HOME}/bin/resolve_plot_nr_ns_2D.py"
plot_psd_each="${HOME}/bin/resolve_plot_psd_each.py"
plot_psd="${HOME}/bin/resolve_plot_psd.py"
plot_psd_2D="${HOME}/bin/resolve_plot_psd_2D.py"
plot_sd_wfrb="${HOME}/bin/resolve_plot_sd_wfrb.py"
plot_map="${HOME}/bin/resolve_plot_map.py"
plot_spec="${HOME}/bin/resolve_plot_spec.py"
plot_evt="${HOME}/bin/resolve_plot_evt.py"
plot_evt_2D="${HOME}/bin/resolve_plot_evt_2D.py"
plot_avgptmpl="${HOME}/bin/resolve_plot_avgptmpl.py"

# Scripts from db to HTML.
make_index="${HOME}/bin/resolve_html_index.py"
make_html_cmd_odb="${HOME}/bin/resolve_html_cmd_odb.py"

# Scripts to yield range and evtlist.
yield_range="${HOME}/bin/resolve_yield_range.py"
yield_evtlist="${HOME}/bin/resolve_yield_evtlist.py"

# Scripts for conversion
conv_shi_gse="python ${HOME}/bin/conv_shigse.py"

# Dirs common to all TCs
gitdir="/data7/git/SIB2_DATA/"
power_gse1_files_src=${gitdir}/forGPIB_inWindows7_atSHI/log/
power_gse2_files_src=${gitdir}/nec_gse/Resolve_Power_Container/CSV/
cmdlogdir=${gitdir}/cmdlog

############################################################
# Functions (Common)
############################################################
# TC setting
function setting(){
	
	TC=$1

	shi_gse_files_src=""
	adrg_files_pattern=""
	shi_gse_files_pattern=""
	lhp_gse_files_pattern=""
	dmsfbv_gse_files_pattern=""
	power_gse1_files_pattern=""
	power_gse2_files_pattern=""
	env_gse_files_pattern=""
	pc_gse1_files_pattern=""
	pc_gse2_files_pattern=""
	pc_gse3_files_pattern=""
	#
	tables_rsl=""
	tables_xtd=""
	tables_gen1=""
	tables_gen2=""
	plots=""

	# Ground tests
	if [ "${TC}x" = "PostVibx" ] || [ "${TC}x" = "CSI_TESTx" ] || [ "${TC}x" = "1909_CSI_TESTx" ]; then
		TC="CSI_TEST"
		topdir="/data7/1909_CSI_TEST/"
		#
		TC_start="2019/08/31 00:00:00"
		TC_end="2019/10/18 00:00:00"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		#plots="${plots} plumbing cde_pow lhp_gse adrg env" # GSE 
		plots="${plots} xbox" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		# SHI GSE
		#shi_gse_files_pattern="Dewar_log_*_TC1_*.xlsx"
		shi_gse_files_ver=0
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
	elif [ "${TC}x" = "TC1x" ] || [ "${TC}x" = "1911_ResolveIT_TC1x" ]; then
		TC="TC1" 
		topdir="/data7/1911_ResolveIT_TC1"
		#
		TC_start="2019/11/23 00:00:00"
		TC_end="2019/12/23 00:00:00"		
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"	
		plots="${plots} plumbing cde_pow adrg" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		adrg_files_pattern="TC1*.tsv"
		adrg_time0="2019-12-07T00:00:00"
		# SHI GSE
		shi_gse_files_pattern="Dewar_log_*_TC1_*.xlsx"
		shi_gse_files_ver=1
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
		power_gse1_files_pattern="all_2019-10*.txt all_2019-11*.txt"	
	elif [ "${TC}x" = "TC2x" ] || [ "${TC}x" = "2003_ResolveIT_TC2x" ] ; then
		TC="TC2"
		topdir="/data7/2003_ResolveIT_TC2/"
		#	
		TC_start="2020/03/12 00:00:00"
		TC_end="2020/04/29 00:00:00"		
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="${plots} plumbing cde_pow adrg" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		adrg_files_pattern="TC2*.tsv"
		adrg_time0="2020-03-22T15:00:00"
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2003_SHI_TC2/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=1
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20200218/
		dmsfbv_gse_files_pattern="202003* 202004* 202005*"
		# Power GSE
		power_gse1_files_pattern="all_2020-02-*.txt all_2020-03-*.txt"		
	elif [ "${TC}x" = "TC2.1x" ] || [ "${TC}x" = "2006_ResolveIT_TC2.1x" ] ; then
		TC="TC2.1"
		topdir="/data7/2006_ResolveIT_TC2.1/"
		#
		TC_start="2020/06/01 00:00:00"
		TC_end="2020/06/22 00:00:00"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="${plots} plumbing cde_pow lhp_gse" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		adrg_files_pattern="TC2.1*.tsv"
		adrg_time0="2020-06-08T15:00:00"
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2006_SHI_TC2.1/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=2
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20200218/
		dmsfbv_gse_files_pattern="202006*"
		# LHP GSE
		lhp_gse_files_pattern="XRISM-202006*xlsx"
		lhp_gse_files_ver=5
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE
		power_gse1_files_pattern="all_2020-06-*.txt"	
	elif [ "${TC}x" = "2007_DWR_repairx" ] ; then 
		TC="DWR_repair"
		topdir="/data7/2007_DWR_repair/"
		#
		TC_start="2020/07/01 00:00:00"
		TC_end="2020/09/02 00:00:00"
		#
		tables_rsl=""
		plots="plumbing env"
		# ADRG
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2007_ISAS/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=3
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20200218/
		dmsfbv_gse_files_pattern="202007*"
		# LHP GSE
		# Power GSE
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2008_SHI_Leak_Search/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=3
		# Particle counter GSE
		pc_files_src=${gitdir}/HHPC_Particle_Counter/2007_DWR_deInteg/
		pc_gse1_files_pattern="CR100-HPC-DATA-20????_????_UT.TSV"
		pc_gse2_files_pattern="Small-CR-HPC-DATA-20????_????_UT.TSV"	
	elif [ "${TC}x" = "TC1Ax" ] || [ "${TC}x" = "2105_ResolveIT_TC1Ax" ] ; then
		TC="TC1A"
		topdir="/data7/2105_ResolveIT_TC1A/"
		#
		TC_start="2021/05/04 00:00:00"
		TC_end="2021/05/22 00:00:00"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="${plots} plumbing cde_pow" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2105_SHI_TC1A/
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=3
		# DMSfbv GSE
		# LHP GSE
		# Power GSE
		power_gse1_files_pattern="all_2021-05-*"	
	elif [ "${TC}x" = "TC2Ax" ] || [ "${TC}x" = "2107_ResolveIT_TC2Ax" ] ; then
		TC="TC2A"
		topdir="/data7/2107_ResolveIT_TC2A/"
		#
		TC_start="2021/07/19 00:00:00"
		TC_end="2021/08/27 00:00:00"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="${plots} plumbing cde_pow env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		adrg_files_pattern="TC-2A*.tsv"
		adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2107_TKSC_TC2A
		shi_gse_files_pattern="XRISM-*.xlsx"
		shi_gse_files_ver=4
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="202107* 202108*"
		# LHP GSE
		# Power GSE
		power_gse1_files_pattern="all_2021-07-* all_2021-08-*"
	elif [ "${TC}x" = "TC3x" ] || [ "${TC}x" = "2108_ResolveIT_TC3x" ] ; then
		TC="TC3"
		topdir="/data7/2108_ResolveIT_TC3/"
		#
		TC_start="2021/08/27 00:00:00"
		TC_end="2021/09/30 00:00:00"
		#
		plot_hk="${plot_hk} --with-distgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3"
		plots="${plots} plumbing cde_pow lhp_gse env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.				
		# ADRG
		#adrg_files_pattern="TC-3*.tsv"
		#adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2107_TKSC_TC2A
		shi_gse_files_pattern="XRISM-20210827*.xlsx XRISM-20210828*.xlsx XRISM-20210829*.xlsx XRISM-2021083*.xlsx XRISM-202109*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="2021082* 202109*"
		# LHP GSE
		lhp_gse_files_pattern="XRISM-20210*xlsx"
		lhp_gse_files_ver=8 # 7
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE
		power_gse1_files_pattern="all_2021-08-* all_2021-09-*"
		# Env monitor
		env_files_pattern="*.xlsx"
	elif [ "${TC}x" = "TC4x" ] || [ "${TC}x" = "2110_ResolveIT_TC4x" ] ; then
		TC="TC4"
		topdir="/data7/2110_ResolveIT_TC4/"
		#
		TC_start="2021/10/01 00:00:00"
		TC_end="2021/12/28 00:00:00"
		#
		plot_hk="${plot_hk} --with-busgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 fwe dist_pcu"
		plots="${plots} plumbing lhp_gse env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu fwe cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		#adrg_files_pattern="TC-3*.tsv"
		#adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2110_TKSC_TC4
		shi_gse_files_pattern="XRISM-2021*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="202110* 202111* 202112* 202201*"
		# LHP GSE
		lhp_gse_files_pattern="Dewar_log_2111*"
		lhp_gse_files_ver=8 # 7
		lhp_gse_time0="2020-06-05T16:11:12"
		# Power GSE (CDE)
		#power_gse1_files_pattern=""
		# Power GSE 2 (SXS-FAN, FWE)
		power_gse2_files_pattern="fanfwe_2021*.csv"
		# Env monitor
		env_files_pattern="*.xlsx"
	elif [ "${TC}x" = "TC5x" ] || [ "${TC}x" = "2201_ResolveIT_TC5x" ] ; then
		TC="TC5"
		topdir="/data7/2201_ResolveIT_TC5/"
		#
		TC_start="2022/01/24 00:00:00"
		TC_end="2022/03/31 00:00:00"
		#
		plot_hk="${plot_hk} --with-busgse"
		tables_rsl="coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 dist_pcu"
		plots="${plots} plumbing env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# ADRG
		adrg_files_pattern="TC-5*.txt"
		adrg_time0="2019-12-07T00:00:00"		
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2110_TKSC_TC4
		shi_gse_files_pattern="XRISM-2022*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		dmsfbv_gse_files_pattern="202201* 202202* 202203*"
		# LHP GSE
		# Power GSE (CDE)
		#power_gse1_files_pattern=""
		# Power GSE 2 (SXS-FAN, FWE)
		power_gse2_files_pattern="fanfwe_2022*.csv"
		# Env monitor
		env_files_pattern="*.xlsx"
		# Particle counter GSE
		pc_gse3_files_pattern="組2*.xls"
	elif [ "${TC}x" = "TC6x" ] || [ "${TC}x" = "2203_XRISM_PFT_TC6x" ] ; then
		TC="TC6"
		topdir="/data7/2203_XRISM_PFT_TC6/"
		#
		TC_start="2022/05/12 00:00:00"
		TC_end="2022/06/30 00:00:00"
		#TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plot_hk="${plot_hk}"
		tables_rsl="${tables_rsl} coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 fwe dist_pcu"
		tables_xtd="${tables_xtd} sxi_cd"
		tables_gen1="${tables_gen1} pcu bccu1 bccu2"
		tables_gen1="${tables_gen1} smu_a_tcim_s_a smu_a_tcim_s_b smu_a_tcim_x smu_b_tcim_s_a smu_b_tcim_s_b smu_b_tcim_x"
		tables_gen1="${tables_gen1} smu_a_hce_a smu_a_hce_b smu_a_mhce smu_b_hce_a smu_b_hce_b smu_b_mhce"
		tables_gen1="${tables_gen1} smu_a_mse smu_b_mse"
		tables_gen1="${tables_gen1} dr_a dr_a_lv1 dr_a_lv2 dr_a_lv3 dr_a_lv4 dr_a_lv5 dr_a_lv6 dr_a_lv7 dr_a_lv8"
		tables_gen1="${tables_gen1} dr_b dr_b_lv1 dr_b_lv2 dr_b_lv3 dr_b_lv4 dr_b_lv5 dr_b_lv6 dr_b_lv7 dr_b_lv8"
		tables_gen2="${tables_gen2} acpa_nom acpb_nom"
		tables_gen2="${tables_gen2} acpa_sg acpb_sg"
		tables_gen2="${tables_gen2} acpa_mtq_a acpa_mtq_b acpb_mtq_a acpb_mtq_b"
		#tables_gen2="${tables_gen2} acpa_nm_1 acpa_nm_2 acpb_nm_1 acpb_nm_2"
		#tables_gen2="${tables_gen2} acpa_sg_a acpa_sg_b acpb_sg_a acpb_sg_b"
		#tables_gen2="${tables_gen2} acpa_pd_1 acpa_pd_2 acpb_pd_1 acpb_pd_2"
		plots="${plots} plumbing" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu fwe cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		plots="${plots} aocs eps pcu_pow rf hce dr lhp vis"
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# SHI GSE
		#shi_gse_files_src=${gitdir}/shi_gse/2110_TKSC_TC4
		#shi_gse_files_pattern="XRISM-2022*.xlsx"
		#shi_gse_files_ver=6
		# DMSfbv GSE
		#dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		#dmsfbv_gse_files_pattern="202201* 202202* 202203*"
		# Env monitor
		env_files_pattern="*.xlsx"
		# Particle counter GSE
		pc_gse3_files_pattern="組2*.xls"
	elif [ "${TC}x" = "TC7x" ] || [ "${TC}x" = "2207_XRISM_PFT_TC7x" ] ; then
		TC="TC7"
		topdir="/data7/2207_XRISM_PFT_TC7/"
		#
		TC_start="2022/07/14 00:00:00"
		TC_end="2022/09/08 00:00:00"
		TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plot_hk="${plot_hk}"
		tables_rsl="${tables_rsl} coolers xboxa xboxb psp_stat1 psp_stat2 psp_stat3 fwe dist_pcu"
		tables_xtd="${tables_xtd} sxi_cd"
		tables_gen1="${tables_gen1} pcu bccu1 bccu2"
		tables_gen1="${tables_gen1} smu_a_tcim_s_a smu_a_tcim_s_b smu_a_tcim_x smu_b_tcim_s_a smu_b_tcim_s_b smu_b_tcim_x"
		tables_gen1="${tables_gen1} smu_a_hce_a smu_a_hce_b smu_a_mhce smu_b_hce_a smu_b_hce_b smu_b_mhce"
		tables_gen1="${tables_gen1} smu_a_mse smu_b_mse"
		tables_gen1="${tables_gen1} dr_a dr_a_lv1 dr_a_lv2 dr_a_lv3 dr_a_lv4 dr_a_lv5 dr_a_lv6 dr_a_lv7 dr_a_lv8"
		tables_gen1="${tables_gen1} dr_b dr_b_lv1 dr_b_lv2 dr_b_lv3 dr_b_lv4 dr_b_lv5 dr_b_lv6 dr_b_lv7 dr_b_lv8"
		tables_gen2="${tables_gen2} acpa_nom acpb_nom"
		tables_gen2="${tables_gen2} acpa_sg acpb_sg"
		tables_gen2="${tables_gen2} acpa_mtq_a acpa_mtq_b acpb_mtq_a acpb_mtq_b"
		#tables_gen2="${tables_gen2} acpa_nm_1 acpa_nm_2 acpb_nm_1 acpb_nm_2"
		#tables_gen2="${tables_gen2} acpa_sg_a acpa_sg_b acpb_sg_a acpb_sg_b"
		#tables_gen2="${tables_gen2} acpa_pd_1 acpa_pd_2 acpb_pd_1 acpb_pd_2"
		plots="${plots} plumbing env" # GSE 
		plots="${plots} dist_pcu adrc xbox temp_cc temp_csi temp_dwr  fwe cc ccfreq jtc sc_eff pc_eff" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls" # rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		plots="${plots} aocs eps pcu_pow rf hce dr lhp vis xtend"
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		# SHI GSE
		shi_gse_files_src=${gitdir}/shi_gse/2110_TKSC_TC4
		shi_gse_files_pattern="XRISM-202207*.xlsx XRISM-202208*.xlsx XRISM-202209*.xlsx"
		shi_gse_files_ver=6
		# DMSfbv GSE
		#dmsfbv_gse_files_src=${gitdir}/DAQFactoryExpress/Data/20210717/
		#dmsfbv_gse_files_pattern="202201* 202202* 202203*"
		# Env monitor
		env_files_pattern="*.xlsx"
		# Particle counter GSE
		pc_gse3_files_pattern="組2*.xls"
		# Tvac monitor
		tvac1_files_pattern="XRISMシステム熱真空試験*-11.csv"
		tvac2_files_pattern="No1_2022-*.CSV"
	elif [ "${TC}x" = "TC8x" ] || [ "${TC}x" = "2210_XRISM_PFT_TC8x" ] ; then
		TC="TC8"
		topdir="/data7/2210_XRISM_PFT_TC8/"
	elif [ "${TC}x" = "TC9x" ] || [ "${TC}x" = "2212_XRISM_PFT_TC9x" ] ; then
		TC="TC9"
		topdir="/data7/2212_XRISM_PFT_TC9/"
	elif [ "${TC}x" = "TC10x" ] || [ "${TC}x" = "2303_XRISM_PFT_TC10x" ] ; then
		TC="TC10"
		topdir="/data7/2303_XRISM_PFT_TC10/"
	elif [ "${TC}x" = "TC11x" ] || [ "${TC}x" = "2305_XRISM_PFT_TC11x" ] ; then
		TC="TC11"
		topdir="/data7/2305_XRISM_PFT_TC11/"
	elif [ "${TC}x" = "VL4x" ] ; then
		TC="ALL"
		topdir="/data7/ALL/"
		#
		TC_start="2019/08/31 00:00:00"
		TC_end="2019/10/15 00:00:00"
		#TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plots="${plots}" # GSE 
		plots="${plots} xbox" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		#
	elif [ "${TC}x" = "VL3x" ] ; then
		TC="ALL"
		topdir="/data7/ALL/"
		#
		TC_start="2019/12/01 00:00:00"
		TC_end="2022/03/31 00:00:00"
		#TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plots="${plots} plumbing cde_pow lhp adrg env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu fwe cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		#
	elif [ "${TC}x" = "VL2x" ] ; then
		TC="ALL"
		topdir="/data7/ALL/"
		#
		TC_start="2022/05/12 00:00:00"
		#TC_end="2023/03/31 00:00:00"
		TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plots="${plots} plumbing cde_pow lhp adrg env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu fwe cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		plots="${plots} aocs eps pcu_pow rf hce dr lhp vis"
		#
	elif [ "${TC}x" = "VL1x" ] ; then
		TC="ALL"
		topdir="/data7/ALL/"
		#
		#TC_start="2019/08/31 00:00:00"
		#TC_end="2022/03/31 00:00:00"
		#TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plots="${plots} plumbing cde_pow lhp adrg env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu fwe cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		plots="${plots} aocs eps pcu_pow rf hce dr lhp vis"
		#
	elif [ "${TC}x" = "ALLx" ] ; then
		TC="ALL"
		topdir="/data7/ALL/"
		#
		TC_start="2019/08/31 00:00:00"
		#TC_end="2022/03/31 00:00:00"
		TC_end=`date --date='00:00 next day' "+%Y/%m/%d %H:%M:%S"`
		#
		plots="${plots} plumbing cde_pow lhp adrg env" # GSE 
		plots="${plots} temp_cc temp_csi temp_dwr dist_pcu fwe cc ccfreq jtc sc_eff pc_eff xbox adrc" # FM
		plots="${plots} rates_hp rates_mp rates_ms rates_lp rates_ls rates_qd rates_sd rates_tlm rates_lost rates_rej rates_pedb" # Rates
		#plots="${plots} nm bl gain lc_a0 lc_a1 lc_b0 lc_b1" # These plots are made for -e or -n option.
		plots="${plots} aocs eps pcu_pow rf hce dr lhp vis"
		#
	else
		cat <<EOF
echo "Test campaign ${TC} is not supported."
exit -1
EOF
	fi
	
	# Input path setting
	fffdir="${topdir}/fff/"
	qlffdir="${topdir}/qlff/"
	sxsfffdir="${topdir}/sxsfff/"
	pldir="${topdir}/PL/"
	shigsedir="${topdir}/shi_gse/"	
	lhpgsedir="${topdir}/lhp_gse/"
	adrgdir="${topdir}/adrg/"
	powergsedir="${topdir}/power_gse1/"
	powergse2dir="${topdir}/power_gse2/"
	dmsfbvgsedir="${topdir}/dmsfbv/"
	pcgsedir="${topdir}/pc/"
	envdir="${topdir}/env/"
	tvacdir="${topdir}/tvac/"
	acceldir="${topdir}/accel/"
	busVdir="${topdir}/busV/"
	rfdir="${topdir}/rf/"

	# Output path setting
	dbdir=`realpath "${topdir}/db/"`
	plotdir=`realpath "${topdir}/plots/"`
	[ -d ${dbdir} ] || mkdir -p ${dbdir}
	[ -d ${plotdir} ] || mkdir -p ${plotdir}
	tempdir="${plotdir}/.template/"

	# event and ODB list file
	TC_start_str=`echo ${TC_start} | sed 's+/++g;s+:++g;s/ /-/'`
	TC_end_str=`echo ${TC_end} | sed 's+/++g;s+:++g;s/ /-/'`
	evtlist="${plotdir}/evtlist.csv"
	odbfile="${dbdir}/odb/odb_${TC_start_str}_${TC_end_str}.pkl"
}

# Update proclist
function update_proclist(){

	local proclist
	proclist=$1

	# mkdir & touch if not exist.
	[ -d ${proclist%/*} ] || mkdir -p ${proclist%/*}
	touch ${proclist}

	# uniq
	cat <<EOF
cat ${proclist} | sort -k 2 | uniq > ${proclist}_tmp
/bin/mv ${proclist}_tmp ${proclist}
EOF
}

# Checksum
function checksum(){
	
	file="$1"
	proclist="$2"

	md5sum_new=`md5sum "${file}" | awk '{print $1}'`
	md5sum_old=`egrep "${file}$" "${proclist}" | awk '{print $1}' | head -1` 	
	
	if [ "${md5sum_new}x" = "${md5sum_old}x" ] ; then
		echo "1"
	else
		cat ${proclist} | egrep -v "${file}$" | sort -k +2 | uniq > ${proclist}_tmp
		/bin/mv ${proclist}_tmp ${proclist}
		echo "0"
	fi
}

# Update event list
function update_evtlist(){

	touch ${evtlist}
	tmp_evtlist1=`mktemp /tmp/${USER}/evtlst_XXXXXXX`
	tmp_evtlist2=`mktemp /tmp/${USER}/evtlst_XXXXXXX`
	${yield_evtlist} -d ${dbdir} -p ${pldir} > ${tmp_evtlist1}
	cat ${evtlist} ${tmp_evtlist1} | sort | uniq > ${tmp_evtlist2}
	/bin/cp ${tmp_evtlist2} ${evtlist}
	/bin/rm ${tmp_evtlist1} ${tmp_evtlist2}
}

# get_last
function update_latest(){
	latest=${dbdir}/latest.txt
	/bin/cp /dev/null ${latest}
	echo "[QL] Plots updated at https://xrsrv1.isas.jaxa.jp/~tsujimot/ResolveQL/ using" >> ${latest}
	echo "last proc (UT)    db file" >> ${latest}
	echo "-----------------------------" >> ${latest}

	dirs='cmd odb temp coolers tvac2 shi_gse dmsfbv_gse ns/1k nr_ns/p00 sd/px ac nr accel/psd_32.0s_10.0kHz busV/psd_32.0s_25.0kHz'
	for dir in ${dirs} ; do
		last=`/bin/ls ${dbdir}/${dir}/ | egrep -v '(proclist|.txt)' | sort -r | head -1 | sed 's/\..*$//' | awk -F"_" '{print $NF}'`
		echo "${last##*/}, ${dir}" >> ${latest}
	done
	echo "-----------------------------" >> ${latest}
}

# Get data
# The following data are copied manually: ADRG, LHP GSE, Paricle counter 
function get_data(){

	# PPL FFF
	if [ -f ${fffdir}/.fetch.sh ] ; then
		cat <<EOF
echo "==> Getting new data for PPL FFF from rfxarm..."
sh ${fffdir}/.fetch.sh get
EOF
	fi
	
	# QLFF
	if [ -f ${qlffdir}/.fetch.sh ] ; then
		cat <<EOF
echo "==> Getting new data for QLFF from rfxarm..."
sh ${qlffdir}/.fetch.sh get
EOF
	fi

	# SXS FFF
#	cat <<EOF
#echo "==> Getting new data (SXS FFF) from rfxarm..."
#sh ${fffdir}/.fetch.sh get
#EOF

	# Accel
	if [ -f ${topdir}/accel/.fetch.sh ] ; then
		cat <<EOF
echo "==> Getting new data for accel from rfxarm..."
sh ${topdir}/accel/.fetch.sh get
EOF
	fi

	# Bus V ripple
#	cat <<EOF
#echo "==> Getting new data for busV from rfxarm..."
#sh ${topdir}/busV/.fetch.sh get
#EOF

	# Tvac data
	cat <<EOF
echo "==> Getting new data for tvac from rfxarm..."
sh ${topdir}/tvac/.fetch.sh get
EOF

	# RF data
	cat <<EOF
echo "==> Getting new data for RF from rfxarm..."
sh ${topdir}/rf/.fetch.sh get
EOF

	# git
	cat <<EOF	
echo "==> Getting new data (SHI, DMSfbv, Power GSE) from git..."
git --git-dir=${gitdir}/.git --work-tree=${gitdir} pull
EOF
	cat <<EOF	
echo "==> Getting new data (eclispe) from git..."
git --git-dir=${gitdir}/../eclipse/.git --work-tree=${gitdir}/../eclipse/ pull
EOF

	# SHI GSE
	if [ "${shi_gse_files_pattern}x" != "x" ] && [ "${shi_gse_files_src}x" != "x" ] ; then
		_src=${shi_gse_files_src}
		_dest=${shigsedir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${shi_gse_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" -print0 | xargs -0 realpath | sort` ; do
			cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi	    

    # DMSfbv GSE
	if [ "${dmsfbv_gse_files_pattern}x" != "x" ]  && [ "${dmsfbv_gse_files_src}x" != "x" ] ; then
		_src=${dmsfbv_gse_files_src}
		_dest=${dmsfbvgsedir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${dmsfbv_gse_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" -print0 | xargs -0 realpath | sort` ; do
				cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi	    

	# Power GSE
	if [ "${power_gse1_files_pattern}x" != "x" ]  ; then
		_src=${power_gse1_files_src}
		_dest=${powergsedir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${power_gse1_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" -print0 | xargs -0 realpath | sort` ; do
				cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi  

	# Power GSE2
	if [ "${power_gse2_files_pattern}x" != "x" ]  ; then
		_src=${power_gse2_files_src}
		_dest=${powergse2dir}
		[ -d ${_dest} ] || mkdir -p ${_dest}
		for _pattern in ${power_gse2_files_pattern} ; do
			for _f in `find ${_src}/ -type f -name "${_pattern}" -print0 | xargs -0 realpath | sort` ; do
				cat <<EOF
/bin/cp ${_f} ${_dest}/
EOF
			done
		done
	fi
}

# Put data
function put_data(){

	# rsync to xrsrv1.
	cat <<EOF
echo "==> Syncing toppy to xrsrv1...."
sh ${topdir}/plots/.fetch.sh put
EOF
}


############################################################
# Functions (makedb; seqnum, cmd)
############################################################
# odb
function makedb_odb(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}
	proclist=${dbdir}/odb/proclist.txt
	update_proclist ${proclist}

	if [ ${flg_overwrite} -eq 1 ]; then
		[ -f ${odbfile} ] && /bin/rm ${odbfile}
	fi

	[ -d ${dbdir}/odb ] || mkdir -p ${dbdir}/odb

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "?????????.dat" -print0 | xargs -0 realpath | grep "/odb/" | sort`; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_odb} -a ${_f} -o ${odbfile}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# command
function makedb_cmd(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}
	proclist=${dbdir}/cmd/proclist.txt
	update_proclist ${proclist}

	for _f in `find ${cmdlogdir}/ -ctime ${ctime} -name "*cml*" -print0 | xargs -0 realpath | grep -v "SAVE" | grep -v "ASTRO-H" | sort`; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			tmp_cmdlog=`mktemp /tmp/${USER}/XXXXXX_cmdlog`
			cat <<EOF
nkf -w ${_f} > ${tmp_cmdlog}
${makedb_cmd} -i ${tmp_cmdlog} -o ${dbdir}/cmd/
[ \$? ] && md5sum "${_f}" >> ${proclist}
/bin/rm ${tmp_cmdlog}
EOF
		fi
	done
}

############################################################
# Functions (makedb; FITS)
############################################################
# SMU
function makedb_hk_gen1(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}

	tables=$*
	for table in ${tables} ; do
		proclist=${dbdir}/fff/proclist_hk_${table}.txt
		update_proclist ${proclist}
		for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_gen}_a0.hk1.gz" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

# AOCP
function makedb_hk_gen2(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}

	tables=$*
	for table in ${tables} ; do
		proclist=${dbdir}/fff/proclist_hk_${table}.txt
		update_proclist ${proclist}
		for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_gen}_a0.hk2.gz" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

# Xtend
function makedb_hk_xtd(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}

	tables=$*
	for table in ${tables} ; do
		proclist=${dbdir}/fff/proclist_hk_${table}.txt
		update_proclist ${proclist}
		for _f in `find ${qlffdir}/ -ctime ${ctime} -name "*xtd_a0.hk.gz" -print0 | xargs -0 realpath | sort` ; do
			_f=`realpath ${_f}`
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

# Resolve using PPL
function makedb_hk_rsl(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}

	tables=$*
	for table in ${tables} ; do
		proclist=${dbdir}/fff/proclist_hk_${table}.txt
		update_proclist ${proclist}
		for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0.hk1.gz" -print0 | xargs -0 realpath | sort` ; do
			_f=`realpath ${_f}`
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

# Resolve using PL
function makedb_hk_rsl_pl(){
	
	[ -d ${fffdir} ] || /bin/mkdir -p ${fffdir}

	tables=$*
	for table in ${tables} ; do
		proclist=${dbdir}/fff/proclist_hk_${table}.txt
		update_proclist ${proclist}
		for _f in `find ${pldir}/ -ctime ${ctime} -name "${prefix_rsl}_a0.hk1.gz" -print0 |  xargs -0 realpath | egrep '/[0-9]{9}/resolve/hk/' | egrep -v '(old|obsolete)' | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${table}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	done
}

# function makedb_hk_rsl_sxsfff(){
#	
# 	proclist=${dbdir}/fff/proclist_hk_sxsfff.txt
# 	update_proclist ${proclist}
#	
# 	for _f in `find ${sxsfffdir}/ -ctime ${ctime} -name "ah[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]_sxs.hk.gz" -print0 | xargs -0 realpath | sort` ; do		
# 		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
# 			cat <<EOF
# ${makedb_fff} -i ${_f} -o ${dbdir}
# [ \$? ] && md5sum "${_f}" >> ${proclist}
# EOF
# 		fi
# 	done
# }

# ns1k, ns8k
function makedb_ns(){
	
	proclist=${dbdir}/fff/proclist_ns.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0.hk2.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ns1k
${makedb_fff} -i ${_f} -o ${dbdir} -t ns8k
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# avgp, templ
function makedb_avgptmpl(){
	
	proclist=${dbdir}/fff/proclist_avgptmpl.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0.hk2.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t avgp
${makedb_fff} -i ${_f} -o ${dbdir} -t tmpl
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# pixHK
function makedb_pixhk(){
	proclist=${dbdir}/fff/proclist_pixhk.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0.hk1.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t psp_pixel
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# wfrb (px)
function makedb_wfrbpx(){
	
	proclist=${dbdir}/fff/proclist_wfrbpx.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0pxwf_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t wfrbpx
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# wfrb (ac)
function makedb_wfrbac(){

	proclist=${dbdir}/fff/proclist_wfrbac.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0acwf_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t wfrbac
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# nr
function makedb_nr(){
	
	proclist=${dbdir}/fff/proclist_nr.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0pxnr_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t nr
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# pr
function makedb_pr(){
	
	proclist=${dbdir}/fff/proclist_pr.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0pxpr_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t pr
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# sd
function makedb_sdpx(){
	
	proclist=${dbdir}/fff/proclist_sdpx.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0pxnr_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir}/ -t sdpx
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}
function makedb_sdac(){
	
	proclist=${dbdir}/fff/proclist_sdac.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0acnr_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir}/ -t sdac
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# evt
function makedb_evt(){

	evts=$*
	for evt in ${evts} ; do
		
		proclist=${dbdir}/fff/proclist_${evt}.txt
		update_proclist ${proclist}

		if [ "${evt}x" = "px_calx" ] ; then
			name="${prefix_rsl}_a0pxcal*_uf.evt.gz"
		elif [ "${evt}x" = "px_clx" ] ; then
			name="${prefix_rsl}_p0px*_cl.evt.gz"
		elif [ "${evt}x" = "px_ufx" ] ; then
			name="${prefix_rsl}_a0px_uf.evt.gz"
		elif [ "${evt}x" = "elx" ] ; then
			name="${prefix_rsl}_el.gti.gz"
		elif [ "${evt}x" = "acx" ] ; then
			name="${prefix_rsl}_a0ac_uf.evt.gz"
		fi
		for _f in `find ${pldir}/ -ctime ${ctime} -name "${name}" -print0 | xargs -0 realpath | egrep '[0-9]{9}/resolve/event_' | egrep -v '(old|obsolete)' | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ${evt}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done	
	done
}

# lc
function makedb_lc(){

	[ -d ${dbdir}/lc/ ] || mkdir -p ${dbdir}/lc/
	
	evts=$*
	for evt in ${evts} ; do

		proclist=${dbdir}/fff/proclist_lc_${evt}.txt
		update_proclist ${proclist}

		${yield_range_TC} -r d | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label=`echo ${_l} | awk '{print $1}'`
			_start=`echo ${_l} | awk '{print $2,$3}'`
			_end=`echo ${_l} | awk '{print $4,$5}'`
			_res=`echo ${_l} | awk '{print $6}'`
			_dir=`echo ${_l} | awk '{print $7}'`

			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] ; then
				cat <<EOF
${makedb_lc} -t ${evt} -i ${dbdir} -o ${dbdir}/ -s ${_start} -e ${_end} -r 16
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
			fi
		done
	done
}


# bl
function makedb_bl(){

	[ -d ${dbdir}/bl/ ] || mkdir -p ${dbdir}/bl/
	
	evts="bl"
	for evt in ${evts} ; do

		proclist=${dbdir}/fff/proclist_${evt}.txt
		update_proclist ${proclist}

		${yield_range_TC} -r d | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label=`echo ${_l} | awk '{print $1}'`
			_start=`echo ${_l} | awk '{print $2,$3}'`
			_end=`echo ${_l} | awk '{print $4,$5}'`
			_res=`echo ${_l} | awk '{print $6}'`
			_dir=`echo ${_l} | awk '{print $7}'`

			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] ; then
				cat <<EOF
${makedb_bl} -t ${evt} -i ${dbdir} -o ${dbdir}/ -s ${_start} -e ${_end} -r 900
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
			fi
		done
	done
}

# gain histroy file
function makedb_ghf(){
	
	proclist=${dbdir}/fff/proclist_ghf.txt
	update_proclist ${proclist}
	for _f in `find ${pldir}/ -ctime ${ctime} -name "${prefix_rsl}_010_pxcal.ghf.gz" -print0 | xargs -0 realpath | egrep '/[0-9]{9}/resolve/event_uf/' | egrep -v '(old|obsolete)' | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_fff} -i ${_f} -o ${dbdir} -t ghf
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# nr_ns
function makedb_nr_ns(){

	proclist=${dbdir}/fff/proclist_nr_ns.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0pxnr_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_nr_ns} -i ${_f} -o ${dbdir} -s 100
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

# nm
function makedb_nm(){

	proclist=${dbdir}/fff/proclist_nm.txt
	update_proclist ${proclist}

	for _f in `find ${fffdir}/ -ctime ${ctime} -name "${prefix_rsl}_a0pxnr_uf.evt.gz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
			cat <<EOF
${makedb_nm} -i ${_f} -o ${dbdir} -s 600
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
}

############################################################
# Functions (makedb; GSE)
############################################################
# monitor psd
function makedb_mon_gse(){

	chnum=$1
	monitor=$2
	proclist=${dbdir}/${monitor}/proclist.txt
	update_proclist ${proclist}

	if [ "${monitor}x" == "accelx" ] ; then
		for _f in `find ${topdir}/${monitor}/ -ctime ${ctime} -name "raw[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].npz" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_mon_gse} -i ${_f} -o ${dbdir}/${monitor}/ -f 'npz' -c ${chnum} -g 1 1 1 1 1 1 1 1
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	elif [ "${monitor}x" == "busVx" ] ; then
		for _f in `find ${topdir}/${monitor}/ -ctime ${ctime} -name "raw[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].npz" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_mon_gse} -i ${_f} -o ${dbdir}/${monitor}/ -f 'npz' -c ${chnum} -g 12 6
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	elif [ "${monitor}x" == "accel.LMSx" ] ; then
		for _f in `find ${topdir}/${monitor}/ -ctime ${ctime} -name "raw[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9].npz" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_mon_gse} -i ${_f} -o ${dbdir}/${monitor}/ -f 'npz' -c ${chnum} -g 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done

	fi
}

# ADRG
function makedb_adrg(){
	
	proclist=${dbdir}/adrg/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${adrgdir} ] ; then
		for _f in `find ${adrgdir}/ -ctime ${ctime} -name "${adrg_files_pattern}" -print0 | xargs -0 realpath | sort` ; do	
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_adrg} -i "${_f}" -o ${dbdir}/ -b ${adrg_time0}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# MD3/4 GSE
function makedb_shi_gse(){

	proclist=${dbdir}/shi_gse/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${shigsedir} ] ; then
		for _f in `find ${shigsedir} -ctime ${ctime} -name "*.xlsx" -print0 | xargs -0 realpath | sort` ; do		
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				if [ "${shi_gse_time0}x" != "x" ] ; then
					opt="-b ${shi_gse_time0}"
				else
					opt=""
				fi
				cat <<EOF
${makedb_shi_gse} -i "${_f}" -o ${dbdir}/ --format ${shi_gse_files_ver} ${opt}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi		
}

# DMS filter bypass valve GSE
function makedb_dmsfbv_gse(){
	
	proclist=${dbdir}/dmsfbv_gse/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${dmsfbvgsedir} ] ; then
		for _f in `find ${dmsfbvgsedir}/ -ctime ${ctime} -name "20*" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_dmsfbv_gse} -i ${_f} -o ${dbdir}/
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# SXS-LHP GSE
function makedb_lhp_gse(){

	proclist=${dbdir}/lhp_gse/proclist.txt
	update_proclist ${proclist}
	if [ -d ${lhpgsedir} ] ; then
	
		for _f in `find ${lhpgsedir}/ -ctime ${ctime} -name "*.xlsx" -print0 | xargs -0 realpath | sort` ; do		
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_lhp_gse} -i "${_f}" -o ${dbdir}/ --format ${lhp_gse_files_ver} -b ${lhp_gse_time0}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi		
}

# CDE power GSE
function makedb_power_gse1(){	

	proclist=${dbdir}/power_gse1/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${powergsedir} ] ; then
		for _f in `find ${powergsedir}/ -ctime ${ctime} -name "*.txt" -print0 | xargs -0 realpath | sort` ; do				
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_power_gse1} -i ${_f} -o ${dbdir}/
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# SXS-DIST power GSE
function makedb_power_gse2(){	

	proclist=${dbdir}/power_gse2/proclist.txt
	update_proclist ${proclist}
	
	if [ -d ${powergse2dir} ] ; then
		for _f in `find ${powergse2dir}/ -ctime ${ctime} -name "*.csv" -print0 | xargs -0 realpath | sort` ; do				
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_power_gse2} -i ${_f} -o ${dbdir}/
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# Environment monitors
function makedb_env(){
	
	proclist=${dbdir}/env/proclist.txt
	update_proclist ${proclist}

	if [ -d ${envdir} ] ; then
		for _f in `find ${envdir}/ -ctime ${ctime} -name "${env_files_pattern}" -print0 | xargs -0 realpath | sort` ; do	
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${makedb_env} -i "${_f}" -o ${dbdir}/ --format 0
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
	fi
}

# Tvac monitor
function makedb_tvac(){
	
	if [ -d ${tvacdir} ] ; then

		# Format 4
		if [ "${tvac1_files_pattern}x" != "x" ] ; then
			proclist=${dbdir}/tvac1/proclist.txt
			update_proclist ${proclist}
			
			for _f in `find ${tvacdir}/ -ctime ${ctime} -name "${tvac1_files_pattern}" -print0 | xargs -0 realpath | sort` ; do
				if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
					cat <<EOF
${makedb_tvac} -i ${_f} -o ${dbdir}/ --format 4
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
				fi
			done
		fi

		# Format 5
		if [ "${tvac2_files_pattern}x" != "x" ] ; then
			proclist=${dbdir}/tvac2/proclist.txt
			update_proclist ${proclist}
	
			for _f in `find ${tvacdir}/ -ctime ${ctime} -name "${tvac2_files_pattern}" -print0 | xargs -0 realpath | sort` ; do
				if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
					cat <<EOF
${makedb_tvac} -i ${_f} -o ${dbdir}/ --format 5
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
				fi
			done
		fi
	fi
}

# Particle Counters
function makedb_pc(){
	
	if [ -d ${pcgsedir} ] ; then

		# Format 1
		if [ "${pc_gse1_files_pattern}x" != "x" ] ; then
			proclist=${dbdir}/pc1/proclist.txt
			update_proclist ${proclist}
			
			for _f in `find ${pcgsedir}/ -ctime ${ctime} -name "${pc_gse1_files_pattern}" -print0 | xargs -0 realpath | sort` ; do
				if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
					cat <<EOF
${makedb_pc} -i ${_f} -o ${dbdir}/ --format 1
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
				fi
			done
		fi

		# Format 2
		if [ "${pc_gse2_files_pattern}x" != "x" ] ; then
			proclist=${dbdir}/pc2/proclist.txt
			update_proclist ${proclist}
	
			for _f in `find ${pcgsedir}/ -ctime ${ctime} -name "${pc_gse2_files_pattern}" -print0 | xargs -0 realpath | sort` ; do
				if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
					cat <<EOF
${makedb_pc} -i ${_f} -o ${dbdir}/ --format 2
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
				fi
			done
		fi

		# Format 3
		if [ "${pc_gse3_files_pattern}x" != "x" ] ; then
			proclist=${dbdir}/pc3/proclist.txt
			update_proclist ${proclist}
	
			for _f in `find ${pcgsedir}/ -ctime ${ctime} -name "${pc_gse3_files_pattern}" -print0 | xargs -0 realpath | sort` ; do
				if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
					cat <<EOF
${makedb_pc} -i ${_f} -o ${dbdir}/ --format 3
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
				fi
			done
		fi
	fi
}



############################################################
# Functions (plot; one-shot)
############################################################
# Get year and month from file name
function get_ym(){
	local _f __f _y _m _ys _ye _ms _me
	_f=$1
	__f=${_f##*/}
	_ys=$2
	_ye=`echo ${_ys} | awk '{print $1+3}'`
	_ms=`echo ${_ys} | awk '{print $1+4}'`
	_me=`echo ${_ys} | awk '{print $1+5}'`
	_y=`echo ${__f} | cut -c ${_ys}-${_ye}`
	_m=`echo ${__f} | cut -c ${_ms}-${_me}`
	echo ${_y} ${_m} 
}

# ns
function plot_ns_each(){

	local _y _m plotdir_new
	
	lens="$*"
	
	for len in ${lens} ; do
		plotdir_new=${plotdir}/ns_${len}k/
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		proclist=${plotdir_new}/proclist.txt
		update_proclist ${proclist}

		if [ ${flg_index_only} -eq 0 ] ; then
		for _f in `find ${dbdir}/ns/${len}k/ -name "ns${len}k_*-*.npz" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				_y=`get_ym ${_f} 6 | awk '{print $1}'`
				_m=`get_ym ${_f} 6 | awk '{print $2}'`
				plotdir_new_day=${plotdir_new}/${_y}/${_m}
				[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
				cat <<EOF
${plot_ns} -i ${_f} -o ${plotdir_new_day}/ -l ${len}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today | egrep '^[lymwdhso]' | while read _l; do
			_label_prefix="o"
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ns${len}k
EOF
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso]' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} 
EOF
		done
	done
}

# wfrb & sd
function plot_sd_wfrb_each(){
	
	local _y _m plotdir_new

	plots="$*"
	for plot in ${plots} ; do

		plotdir_new=${plotdir}/${plot}
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		for subdir in time freq ; do
			[ -d ${plotdir_new}/${subdir} ] || mkdir -p ${plotdir_new}/${subdir}				
			[ -d ${plotdir_new}_2D/${subdir} ] || mkdir -p ${plotdir_new}_2D/${subdir}				
		done
		proclist=${plotdir_new}/proclist.txt
		update_proclist ${proclist}

		if [ ${flg_index_only} -eq 0 ] ; then
		tmp_px=`mktemp /tmp/${USER}/XXXXXX_px.lst`
		tmp_ac=`mktemp /tmp/${USER}/XXXXXX_ac.lst`
		find ${dbdir}/${plot}/px/ -name "${plot}px_*-*.pkl" -print0 | xargs -0 realpath | sort > ${tmp_px}
		# find ${dbdir}/sd/ac/ -name "${plot}ac_*-*.pkl" -print0 | xargs -0 realpath | sort > ${tmp_ac}
		sed "s+/${plot}/px/${plot}px+/${plot}/ac/${plot}ac+" ${tmp_px} > ${tmp_ac}

		paste ${tmp_px} ${tmp_ac} | while read _line ; do
			_f_px=`echo ${_line} | awk '{print $1}'`
			_f_ac=`echo ${_line} | awk '{print $2}'`

			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f_px} ${proclist}` -eq 0 ]  ; then
				cat <<EOF
${plot_sd_wfrb} -i ${_f_px} ${_f_ac} -m ${plot} -o ${plotdir_new}
[ \$? ] && md5sum "${_f_px}" >> ${proclist}
EOF
			fi
		done
		/bin/rm ${tmp_ac} ${tmp_px}
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today| egrep '^[lymwdhso]' | while read _l; do
			_label_prefix="o"
			_dir=`echo ${_l} | awk '{print $7}'`
			cat <<EOF
${make_index} -t ${plotdir_new}/time/${_dir} -r ${_label_prefix} -d ${dbdir} --src ${plot}_time
${make_index} -t ${plotdir_new}/freq/${_dir} -r ${_label_prefix} -d ${dbdir} --src ${plot}_freq
${make_index} -t ${plotdir_new}_2D/time/${_dir} -r ${_label_prefix} -d ${dbdir} --src ${plot}_time
${make_index} -t ${plotdir_new}_2D/freq/${_dir} -r ${_label_prefix} -d ${dbdir} --src ${plot}_freq
EOF
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso]' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			cat <<EOF
${make_index} -t ${plotdir_new}/time/${_dir} -r ${_label_prefix} -d ${dbdir}
${make_index} -t ${plotdir_new}/freq/${_dir} -r ${_label_prefix} -d ${dbdir}
${make_index} -t ${plotdir_new}_2D/time/${_dir} -r ${_label_prefix} -d ${dbdir}
${make_index} -t ${plotdir_new}_2D/freq/${_dir} -r ${_label_prefix} -d ${dbdir}
EOF
		done
	done
}

# average pulse & templates
function plot_avgptmpl_each(){
	
	local plotdir_new 

	plots="$*"
	for plot in ${plots} ; do

		plotdir_new=${plotdir}/${plot}/
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		proclist=${plotdir_new}/proclist.txt
		update_proclist ${proclist}

		if [ ${flg_index_only} -eq 0 ] ; then
		for _f in `find ${dbdir}/${plot}/ -name "${plot}_*-*.pkl" -print0 | xargs -0 realpath | sort` ; do
			if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ]  ; then
				_y=`get_ym ${_f} 6 | awk '{print $1}'`
				_m=`get_ym ${_f} 6 | awk '{print $2}'`
				plotdir_new_day=${plotdir_new}/${_y}/${_m}/
				[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
				cat <<EOF
${plot_avgptmpl} -i ${_f} -o ${plotdir_new_day}/ -t ${plot}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
			fi
		done
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix="o"
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ${plot}
EOF
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day}/ -r ${_label_prefix} -d ${dbdir}
EOF
		done
	done
}

# pixhk
function copy_pixhk_each(){

	local _y _m plotdir_new

	plotdir_new="${plotdir}/pixhk/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	if [ ${flg_index_only} -eq 0 ] ; then
	for _f in `find ${dbdir}/psp_pixel/ -name "psp_pixel_*-*.xls" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ] ; then			
			_y=`get_ym ${_f} 11 | awk '{print $1}'`
			_m=`get_ym ${_f} 11 | awk '{print $2}'`
			plotdir_new_day="${plotdir_new}/${_y}/${_m}"
			[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
/bin/cp ${_f} ${plotdir_new_day}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
	fi

	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix="o"
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src psp_pixel
EOF
	done
	${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
	done
}

# psd each
function plot_psd_each(){

	local _y _m plotdir_new

	shotlength=`echo $1 | awk '{printf("%.1f",$1)}'`
	samplingrate=`echo $2 | awk '{printf("%.1f",$1)}'`
	samplingrate_kHz=`echo $2 | awk '{printf("%.1f",$1/1e3)}'`
	chnum=$3
	monitor=$4
	nba="psda"
	
	#plotdir_new="${plotdir}/${monitor}/${nba}_${shotlength}s_${samplingrate_kHz}kHz_each/"
	plotdir_new="${plotdir}/${monitor}_each/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	if [ ${flg_index_only} -eq 0 ] ; then
	for _f in `find ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz/ -name "${nba}_*-*.npz" -print0 | xargs -0 realpath | sort` ; do
		if [ ${flg_overwrite} -eq 1 ] || [ `checksum ${_f} ${proclist}` -eq 0 ] ; then			
			cat <<EOF
${plot_psd_each} -l ${shotlength} -r ${samplingrate} -i ${_f} -o ${plotdir_new}/ -c ${chnum} -m ${monitor}
[ \$? ] && md5sum "${_f}" >> ${proclist}
EOF
		fi
	done
	fi
}


############################################################
# Functions (plot; range)
############################################################
# hk
function plot_hk(){

	local plotdir_new
	plots="$*"
		
	for plot in ${plots} ; do
		
		if [ "rates" = `echo ${plot} | awk -F"_" '{print $1}'` ] ; then
			rates=`echo ${plot} | awk -F"_" '{print $2}'`
			proclist=${plotdir}/rates/${rates}/proclist.txt
			plotdir_new="${plotdir}/rates/${rates}/"
			plot_new="${plot}"
		elif [ "lc" = `echo ${plot} | awk -F"_" '{print $1}'` ] ; then
			lc=`echo ${plot} | awk -F"_" '{print $2}'`
			proclist=${plotdir}/lc/${lc}/proclist.txt
			plotdir_new="${plotdir}/lc/${lc}/"
			plot_new="${plot}"
		else
			proclist=${plotdir}/${plot}/proclist.txt
			plotdir_new="${plotdir}/${plot}/"
			plot_new="${plot}"
		fi
		update_proclist ${proclist}
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}

		if [ ${flg_index_only} -eq 0 ] ; then
		
		[ ${LOOP} -ne 0 ] && plot_range="d" # produce daily plots only in LOOP.
		[ ${LOOP} -eq 0 ] && plot_range="ymwd"
		[ ${LOOP} -eq -1 ] && plot_range="ymw"
		
		${yield_range_TC} -r ${plot_range} | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label=`echo ${_l} | awk '{print $1}'`
			_start=`echo ${_l} | awk '{print $2,$3}'`
			_end=`echo ${_l} | awk '{print $4,$5}'`
			_res=`echo ${_l} | awk '{print $6}'`
			_dir=`echo ${_l} | awk '{print $7}'`

			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ] ; then
				plotdir_new_day="${plotdir_new}/${_dir}"
				[ -d "${plotdir_new_day}" ] || mkdir -p "${plotdir_new_day}"
				cat <<EOF
echo "${_label}"
${plot_hk} -p ${plot} -i ${dbdir} -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -R ${_res} -E ${evtlist}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
			fi
		done
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ${plot_new} --bottom
EOF
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ${plot_new}
EOF
		done
	done
}

# px, ac, cal (spec)
function plot_spec(){

	local plotdir_new

	plots="$*"
	for plot in ${plots} ; do

		plotdir_new="${plotdir}/spec/${plot}"
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		proclist=${plotdir_new}/proclist.txt
		update_proclist ${proclist}

		if [ ${flg_index_only} -eq 0 ] ; then
		${yield_range_TC} -r s | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label=`echo ${_l} | awk '{print $1}'`
			_start=`echo ${_l} | awk '{print $2,$3}'`
			_end=`echo ${_l} | awk '{print $4,$5}'`
			_res=`echo ${_l} | awk '{print $6}'`
			_dir=`echo ${_l} | awk '{print $7}'`
			
			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ] ; then
				plotdir_new_day="${plotdir_new}/${_dir}"
				[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
				cat <<EOF
echo "${_label}"
${plot_spec} -p ${plot} -i ${dbdir} -o ${plotdir_new_day}/ -s ${_start} -e ${_end}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
			fi
		done
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix="s"
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ${plot} --bottom
EOF
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
		done
	done
}

# px, ac, cal (1D)
function plot_evt(){

	local plotdir_new
	chnum=36
	resample=100

	plots="$*"
	for plot in ${plots} ; do

		plotdir_new="${plotdir}/evt/"
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		proclist=${plotdir_new}/proclist_${plot}.txt
		update_proclist ${proclist}

		if [ ${flg_index_only} -eq 0 ] ; then
		${yield_range_TC} -r s | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label=`echo ${_l} | awk '{print $1}'`
			_start=`echo ${_l} | awk '{print $2,$3}'`
			_end=`echo ${_l} | awk '{print $4,$5}'`
			_res=`echo ${_l} | awk '{print $6}'`
			_dir=`echo ${_l} | awk '{print $7}'`

			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
				plotdir_new_day="${plotdir_new}/${_dir}"
				#[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
				cat <<EOF
echo "${_label}"
${plot_evt} -p ${plot} -i ${dbdir} -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -r 100
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
			fi
		done
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix="s"
			_dir=`echo ${_l} | awk '{print $7}'`
			for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ac ; do
				plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
				[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src evt_${_p} --bottom
EOF
			done
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ac ; do
				plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
				[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
			done
		done
	done
}

# px, ac, cal (2D)
function plot_evt_2D(){

	local plotdir_new
	chnum=36

	plots=$*		
	for plot in ${plots} ; do

		plotdir_new="${plotdir}/evt_2D/"
		[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
		proclist=${plotdir_new}/proclist_${plot}.txt
		update_proclist ${proclist}

		if [ ${flg_index_only} -eq 0 ] ; then
		${yield_range_TC} -r s | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label=`echo ${_l} | awk '{print $1}'`
			_start=`echo ${_l} | awk '{print $2,$3}'`
			_end=`echo ${_l} | awk '{print $4,$5}'`
			_res=`echo ${_l} | awk '{print $6}'`
			_dir=`echo ${_l} | awk '{print $7}'`

			if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
				plotdir_new_day="${plotdir_new}/${_dir}"
				#[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
				cat <<EOF
echo "${_label}"
${plot_evt_2D} -p ${plot} -i ${dbdir} -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -R 600 -E ${evtlist}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
			fi
		done
		fi

		# Remove empty dirs.
		cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

		# Make index.
		${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix="s"
			_dir=`echo ${_l} | awk '{print $7}'`
			for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ac ; do
				plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
				[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src evt_${_p} --bottom
EOF
			done
		done
		${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
			_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
			_dir=`echo ${_l} | awk '{print $7}'`
			for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ac ; do
				plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
				[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
			done
		done
	done
}

# map
function plot_map(){

	local plotdir_new

	plotdir_new="${plotdir}/map/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	if [ ${flg_index_only} -eq 0 ] ; then
	${yield_range_TC} -r s | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`

		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
echo "${_label}"
${plot_map} -i ${dbdir} -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -r 600 -g Hp Mp Ms Lp Ls
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi

	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix="s"
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src map_HP+MP+MS+LP+LS --bottom
EOF
    done
	${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
    done
}		

# nr_ns_1D
function plot_nr_ns(){

	local plotdir_new

	plotdir_new="${plotdir}/nr_ns/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	if [ ${flg_index_only} -eq 0 ] ; then
	${yield_range_TC} -r h | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`

		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
echo "${_label}"
${plot_nr_ns} -i ${dbdir}/nr_ns/ -o ${plotdir_new_day}/ -s ${_start} -e ${_end}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi
	
	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r d --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src nr_ns --bottom
EOF
    done
	${yield_range_TC} -r lym --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
    done
}		

# nr_ns_2D
function plot_nr_ns_2D(){
	
	local plotdir_new
	chnum=36

	plotdir_new=${plotdir}/nr_ns_2D/
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}	
	
	if [ ${flg_index_only} -eq 0 ] ; then

	[ ${LOOP} -ne 0 ] && plot_range="d"
	[ ${LOOP} -eq 0 ] && plot_range="mwd"
	[ ${LOOP} -eq -1 ] && plot_range="w"

	${yield_range_TC} -r ${plot_range} | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`

		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			#[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
echo "${_label}"
${plot_nr_ns_2D} -i ${dbdir}/nr_ns/ -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -R ${_res} -E ${evtlist} -z 0.5 3.0
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi

	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ; do
			plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src nr_ns_${_p} --bottom
EOF
		done
    done
	${yield_range_TC} -r y --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ; do
			plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src nr_ns_${_p}
EOF
		done
    done
	${yield_range_TC} -r l --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		for _p in `seq ${chnum} | awk '{printf("p%02d\n",$1-1)}'` ; do
			plotdir_new_day="${plotdir_new}/${_p}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
		done
    done
}

# psd
function plot_psd(){

	local plotdir_new

	shotlength=`echo $1 | awk '{printf("%.1f",$1)}'`
	samplingrate=`echo $2 | awk '{printf("%.1f",$1)}'`
	samplingrate_kHz=`echo $2 | awk '{printf("%.1f",$1/1e3)}'`	
	chnum=$3
	monitor=$4
	nba="psda"

	if ! [ -d ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz ] ; then
		return 0
	fi
	
	#plotdir_new="${plotdir}/${monitor}/${nba}_${shotlength}s_${samplingrate_kHz}kHz/"
	plotdir_new="${plotdir}/${monitor}/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	if [ ${flg_index_only} -eq 0 ] ; then

	[ ${LOOP} -ne 0 ] && plot_range="h"
	[ ${LOOP} -eq 0 ] && plot_range="dh"
	[ ${LOOP} -eq -1 ] && plot_range="d"

	${yield_range_TC} -r ${plot_range} | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`

		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
echo "${_label}"
${plot_psd} -l ${shotlength} -r ${samplingrate} -i ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz/ -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -c ${chnum} -m ${monitor}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi

	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r d --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ${monitor} --bottom
EOF
    done
	${yield_range_TC} -r lym --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
    done
}

# psd_2D
function plot_psd_2D(){
	
	local plotdir_new

	shotlength=`echo $1 | awk '{printf("%.1f",$1)}'`
	samplingrate=`echo $2 | awk '{printf("%.1f",$1)}'`
	samplingrate_kHz=`echo $2 | awk '{printf("%.1f",$1/1e3)}'`	
	chnum=$3
	monitor=$4
	nba="psda"

	if ! [ -d ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz ] ; then
		return 0
	fi
		
	#plotdir_new="${plotdir}/${monitor}_2D/${nba}_${shotlength}s_${samplingrate_kHz}kHz/"
	plotdir_new="${plotdir}/${monitor}_2D/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}	
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	# Make plots
	if [ ${flg_index_only} -eq 0 ] ; then

	[ ${LOOP} -ne 0 ] && plot_range="h"
	[ ${LOOP} -eq 0 ] && plot_range="dh"
	[ ${LOOP} -eq -1 ] && plot_range="d"

	${yield_range_TC} -r ${plot_range} | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`

		# For hourly plots, make the fine most resolution.
		if [ `echo ${_label} | egrep -c "^h"` -eq 1 ] ; then
			resample=2
		elif [ `echo ${_label} | egrep -c "^d"` -eq 1 ] ; then
			resample=30
		fi

		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			#[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
echo "${_label}"
${plot_psd_2D} -l ${shotlength} -r ${samplingrate} -i ${dbdir}/${monitor}/psd_${shotlength}s_${samplingrate_kHz}kHz/ -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -R ${resample} -E ${evtlist} -c ${chnum} -m ${monitor}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi

	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r d --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		
		for _c in `seq ${chnum} | awk '{printf("c%02d\n",$1-1)}'` ; do
			plotdir_new_day="${plotdir_new}/${_c}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --src ${monitor}_${_c} --bottom
EOF
		done
	done
	${yield_range_TC} -r lym --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		for _c in `seq ${chnum} | awk '{printf("c%02d\n",$1-1)}'` ; do
			plotdir_new_day="${plotdir_new}/${_c}/${_dir}"
			[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
		done
    done

}

############################################################
# Functions (html)
############################################################
# Command table
function make_html_odb(){

	local start end plotdir_new

	plot="odb"

	plotdir_new="${plotdir}/${plot}/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	# Make html.
	if [ ${flg_index_only} -eq 0 ] ; then
	${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d "${plotdir_new_day}" ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
${make_html_cmd_odb} -i ${dbdir}/${plot}/ -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -t ${plot}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi
	
	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r m --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --ssi ${plot} --bottom
EOF
	done
	${yield_range_TC} -r ly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
	done
}

# Command table
function make_html_cmd(){

	local start end plotdir_new

	plot="cmd"
		
	plotdir_new="${plotdir}/${plot}/"
	[ -d ${plotdir_new} ] || mkdir -p ${plotdir_new}
	proclist=${plotdir_new}/proclist.txt
	update_proclist ${proclist}

	# Make html.
	if [ ${flg_index_only} -eq 0 ] ; then
	${yield_range_TC} -r d | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label=`echo ${_l} | awk '{print $1}'`
		_start=`echo ${_l} | awk '{print $2,$3}'`
		_end=`echo ${_l} | awk '{print $4,$5}'`
		_res=`echo ${_l} | awk '{print $6}'`
		_dir=`echo ${_l} | awk '{print $7}'`
		if [ ${flg_overwrite} -eq 1 ] || [ `grep -c ${_label} ${proclist}` -eq 0 ] || [ ${LOOP} -gt 0 ]; then
			plotdir_new_day="${plotdir_new}/${_dir}"
			[ -d ${plotdir_new_day} ] || mkdir -p ${plotdir_new_day}
			cat <<EOF
${make_html_cmd_odb} -i ${dbdir}/${plot}/ -o ${plotdir_new_day}/ -s ${_start} -e ${_end} -t ${plot}
[ \$? ] && echo "${_label}" >> ${proclist}
EOF
		fi
	done
	fi

	# Remove empty dirs.
	cat <<EOF
find ${plotdir_new} -type d | sort | tac | xargs rmdir --ignore-fail-on-non-empty
EOF

	# Make index.
	${yield_range_TC} -r d --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir} --ssi ${plot} --bottom
EOF
	done
	${yield_range_TC} -r mly --include-today | egrep '^[lymwdhso][0-9]{8}' | while read _l; do
		_label_prefix=`echo ${_l} | awk '{print $1}' | cut -c 1`
		_dir=`echo ${_l} | awk '{print $7}'`
		plotdir_new_day="${plotdir_new}/${_dir}"
		[ -d ${plotdir_new_day} ] && cat <<EOF
${make_index} -t ${plotdir_new_day} -r ${_label_prefix} -d ${dbdir}
EOF
	done
}


############################################################
# Main routine
############################################################
OPTIND=1
while getopts "dtchnembpoqoiL:C:" OPT ; do
    case ${OPT} in
	d) flg_debug=1 ;;
	t) flg_trans=1 ;;
	c) flg_com=1 ;;
	h) flg_hk=1 ;;
	n) flg_noise=1 ;;
	e) flg_evt=1 ;;
	m) flg_mon=1 ;;
	b) flg_db=1 ;;
	p) flg_plot=1 ;;
	o) flg_overwrite=1 ;;
	q) flg_quick=1 ;;
	i) flg_index_only=1 ;;
	L) LOOP=${OPTARG} ;;
	C) TC=${OPTARG} ;; # -C needs to be the last in the comand line.
    esac
done

# -C option (or lack thereof)
if  [ "${TC}x" = "x" ] ; then
	TCs=${TC_default}
else
	shift `expr ${OPTIND} - 2`
	TCs="$*"
fi 

# -q mode
if [ ${flg_quick} -eq 1 ] ; then
	ctime=-3
else
	ctime=-10000
fi

for TC in ${TCs} ; do

	setting ${TC}

	# yeild range
	yield_range_TC="${yield_range} -s ${TC_start} -e ${TC_end} --odb ${dbdir}/odb/ --loop ${LOOP}"
	
	# Show abstract
	date=`export LC_ALL=C ; date`
	cat <<EOF
	echo "=========================================================================="
	echo "==> Process starting at ${date} for ${TC} for the last ${LOOP} days."
	echo "==>   Mode     : [${flg_debug}] Debug. [${flg_overwrite}] Overwrite. [${flg_index_only}] Index only."
	echo "==>   Retrieval: [${flg_trans}] Transfer from rfxarm2. [${flg_quick}] Process only recent files."
	echo "==>   Data     : [${flg_com}] Cmd [${flg_hk}] HK [${flg_evt}] Evt [${flg_noise}] Noise [${flg_mon}] Monitor"
	echo "==>   Products : [${flg_db}] database [${flg_plot}] plot"
	echo "=========================================================================="
EOF

	if [ ${flg_debug} -eq 1 ]  ; then
		: # Need something. Do not delete.
		####################
		# Debug
		####################
		get_data
		#tables_gen1=""
		#tables_gen1="${tables_gen1} dr_a dr_a_lv1 dr_a_lv2 dr_a_lv3 dr_a_lv4 dr_a_lv5 dr_a_lv6 dr_a_lv7 dr_a_lv8"
		#tables_gen1="${tables_gen1} dr_b dr_b_lv1 dr_b_lv2 dr_b_lv3 dr_b_lv4 dr_b_lv5 dr_b_lv6 dr_b_lv7 dr_b_lv8"
		#makedb_hk_gen1 ${tables_gen1}
		#plot_hk dr
		makedb_pr
		#plot_hk plumbing
		#update_evtlist
		#update_latest
		put_data
	else
		####################
		# Transfer (1) get
		####################
		if [ ${flg_trans} -eq 1 ] ; then
			get_data
		fi

		####################
		# Commands & ODB
		####################
		if [ ${flg_com} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				makedb_cmd
				makedb_odb
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				make_html_odb
				make_html_cmd
			fi			
		fi

		####################
		# HK
		####################
		if [ ${flg_hk} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				# GSE
				# makedb_adrg
				# makedb_power_gse1
				# makedb_power_gse2
				#makedb_lhp_gse
				#makedb_shi_gse
				#makedb_dmsfbv_gse
				#makedb_pc
				#makedb_env
				makedb_tvac

				# HK
				makedb_hk_rsl ${tables_rsl}
				#makedb_hk_rsl temp
				makedb_hk_rsl_pl temp
				makedb_hk_xtd ${tables_xtd}
				makedb_hk_gen1 ${tables_gen1}
				makedb_hk_gen2 ${tables_gen2}
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				# Update evtlist
				update_evtlist
				# plot (hk)
				plot_hk ${plots}
				# plot (map)
				plot_map
			fi			
		fi

		####################
		# Noise
		####################
		if [ ${flg_noise} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				# Noise (PPL)
				makedb_ns
				makedb_avgptmpl
				makedb_pixhk
				makedb_sdpx
				makedb_sdac
				makedb_wfrbpx
				makedb_wfrbac	
				# Noise (db)
				makedb_nr_ns
				makedb_nm
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then	
				# Update evtlist
				update_evtlist
				# plot (noise)
				plot_ns_each 1 8
				plot_sd_wfrb_each sd wfrb
				plot_nr_ns
				plot_nr_ns_2D
				# plot (PSP par)
				plot_avgptmpl_each avgp tmpl
				copy_pixhk_each
				# plot (HK)
				plot_hk nm	
			fi			
		fi

		####################
		# Events
		####################
		if [ ${flg_evt} -eq 1 ]  ; then
			
			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				# Records (pr, nr)
				#makedb_pr
				makedb_nr
				# Gain history file (PL)
				makedb_ghf
				# Events (px, el, cal, ac from PL)
				makedb_evt px_cal px_cl px_uf el ac
				# Baseline from db
				makedb_bl
				# Light curves (px_cl, ,px_uf, ac, el from db)
				makedb_lc ac el px_cl px_uf
			fi

			####################
			# plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				# Update evtlist
				update_evtlist
				# plot (HK)
				plot_hk gain bl lc_a0 lc_a1 lc_b0 lc_b1
				# plot (evt)
				plot_evt ac cal px
				plot_evt_2D ac cal px
				plot_spec ac cal px
			fi
		fi

		####################
		# Monitor
		####################
		if [ ${flg_mon} -eq 1 ]  ; then

			####################
			# makedb
			####################
			if [ ${flg_db} -eq 1 ]  ; then
				makedb_mon_gse 8 "accel"
				makedb_mon_gse 2 "busV"
				#makedb_mon_gse 8 "mag"
			fi

			####################
			# Plots
			####################
			if [ ${flg_plot} -eq 1 ]  ; then
				# Update evtlist
				update_evtlist
				# plot (accel)
				plot_psd 32.0 10e3 8 "accel"
				plot_psd_2D 32.0 10e3 8 "accel"
				#plot_psd_each 32.0 10e3 8 "accel"
				# plot (busV)
				plot_psd 32.0 25e3 2 "busV"
				plot_psd_2D 32.0 25e3 2 "busV"
				# plot (mag)
				#plot_psd 32.0 10e3 8 "mag"
				#plot_psd_2D 32.0 10e3 8 "mag"
				#plot_psd 8.0 5e3 8 "mag"
				#plot_psd_2D 8.0 5e3 8 "mag"
			fi
		fi

		####################
		# Transfer (2) put
		####################
		if [ ${flg_trans} -eq 1 ]  ; then

			# updates latest db
			update_latest
			
			# put
			put_data
		fi
	fi
done